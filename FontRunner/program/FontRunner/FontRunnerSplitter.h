/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

class ISplitterListener;

// A custom splitter so we can recieve notifications of pane size change
class CFontRunnerSplitter : public CSplitterWnd
{
	DECLARE_DYNAMIC(CFontRunnerSplitter)

public:
	CFontRunnerSplitter();
	virtual ~CFontRunnerSplitter();

protected:
	virtual void TrackRowSize(int y, int row);
	virtual void TrackColumnSize(int x, int col);

public:
	virtual void SetListener(ISplitterListener* pListener);

protected:
	boost::shared_ptr<ISplitterListener> m_pListener;
};


// listener interface
class ISplitterListener
{
public:
	virtual void OnTrackRowSize(int y, int row) = 0;
	virtual void OnTrackColumnSize(int x, int col) = 0;
};


// a listener class so listeners don't necessarily have to derive from ISplitterListener
template <class T>
class CSplitterListener : public ISplitterListener
{
public:
	typedef boost::function<void (int, int)> function_type;
	typedef void (T::*memberfunction_type)(int, int);

public:
	CSplitterListener(memberfunction_type pTrackRowSize,
					  memberfunction_type pTrackColumnSize,
					  T* pClass)
		: trackrowsize(boost::bind(pTrackRowSize, pClass, _1, _2)),
		  trackcolumnsize(boost::bind(pTrackColumnSize, pClass, _1, _2))
	{}

private:
	void OnTrackRowSize(int y, int row)
	{
		trackrowsize(y, row);
	}

	void OnTrackColumnSize(int x, int col)
	{
		trackcolumnsize(x, col);
	}

private:
	function_type trackrowsize, trackcolumnsize;
};


// helper function for connecting a listener to a splitter
template <class T>
void BindListenerToSplitter(T* pClass,
							CFontRunnerSplitter& splitter,
							typename CSplitterListener<T>::memberfunction_type pTrackRowSize,
							typename CSplitterListener<T>::memberfunction_type pTrackColSize)
{
	splitter.SetListener(new CSplitterListener<T>(pTrackRowSize, pTrackColSize, pClass));
}
