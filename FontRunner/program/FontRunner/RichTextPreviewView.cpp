/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// RichTextPreviewView.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "RichTextPreviewView.h"
#include "ProgramOptions.h"
#include "FontRunnerSignals.h"
#include "FontRunnerDoc.h"
#include "PaletteDlg.h"
#include "OpenTypeData.h"
#include "FontItem.h"
#include "FontManager.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CRichTextPreviewView

IMPLEMENT_DYNCREATE(CRichTextPreviewView, CView)

CRichTextPreviewView::CRichTextPreviewView()
	: m_pRichEdit(NULL),
	  m_cstrRichEditDLL(_T("RICHED20.DLL")),
	  m_bInitialUpdated(false)
{
}

CRichTextPreviewView::~CRichTextPreviewView()
{
}

void CRichTextPreviewView::RefreshFontView()
{
	bool bInit = false;

	// create an edit control
	if (!m_strCurrentFaceName.IsEmpty() && !m_pRichEdit)
	{
		CRect rcClient;
		GetClientRect(&rcClient);

		// load rich edit library
		::LoadLibrary(m_cstrRichEditDLL);

		m_pRichEdit = new CRichEditCtrl();
		m_pRichEdit->Create(WS_CHILD | WS_VISIBLE | ES_MULTILINE,
							rcClient, this, ID_PREVIEW_RICHEDIT);
		m_pRichEdit->ModifyStyleEx(0, WS_EX_STATICEDGE);
		bInit = true;
	}
	else if (m_strCurrentFaceName.IsEmpty() && m_pRichEdit)
	{
		SaveOptions();

		m_pRichEdit->DestroyWindow();
		delete m_pRichEdit;
		m_pRichEdit = NULL;

		// Make certain the rich edit module is unloaded.  It can lock font
		// files even when they're not being used can cause all kinds of havoc.
		HMODULE hRichEdit = ::GetModuleHandle(m_cstrRichEditDLL);
		while (hRichEdit)
		{
			::FreeLibrary(hRichEdit);
			hRichEdit = ::GetModuleHandle(m_cstrRichEditDLL);
		}

		return;
	}
	else if (m_strCurrentFaceName.IsEmpty() && !m_pRichEdit)
		return;

	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// set new foreground color
	CHARFORMAT2	cf;
	ZeroMemory(&cf, sizeof(CHARFORMAT2));
	cf.cbSize = sizeof(CHARFORMAT2);
	cf.dwMask = CFM_FACE | CFM_SIZE | CFM_BOLD | CFM_ITALIC | CFM_UNDERLINE | CFM_COLOR | CFM_CHARSET;
	cf.dwEffects = (pOptions->IsPreviewFontBold() ? CFE_BOLD : 0) +
				   (pOptions->IsPreviewFontItalic() ? CFE_ITALIC : 0) +
				   (pOptions->IsPreviewFontUnderlined() ? CFE_UNDERLINE : 0);

	cf.yHeight = pOptions->GetPreviewFontSize() * 20;
	cf.crTextColor = pOptions->GetPreviewFontColor();
	cf.bPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;
	
	// if face name is blank, go to arial
	if (m_strCurrentFaceName.IsEmpty())
		m_strCurrentFaceName = _T("Arial");

	// font name cannot exceed LF_FACESIZE
	CString strFontName = m_strCurrentFaceName;
	if (strFontName.GetLength() >= LF_FACESIZE)
		m_strCurrentFaceName = strFontName.Left(LF_FACESIZE - 1);
	else
		m_strCurrentFaceName = strFontName;
	wcscpy_s(cf.szFaceName, LF_FACESIZE, (LPCTSTR)m_strCurrentFaceName);
	cf.yOffset = 0;
	cf.bCharSet = m_pFontItem->GetOpenTypeData()->IsSymbol() ? SYMBOL_CHARSET : DEFAULT_CHARSET;
	cf.bPitchAndFamily = FF_DONTCARE;

	// see if there is already a selection
	CHARRANGE range;
	m_pRichEdit->GetSel(range);
	
	bool bExistingSel = (range.cpMax != range.cpMin);

	if (bInit)
		m_pRichEdit->SetSel(0, -1);

	// format selected text in selection
	if (bExistingSel)
		m_pRichEdit->SetSelectionCharFormat(cf);
	else
		m_pRichEdit->SendMessage(EM_SETCHARFORMAT, SCF_ALL | SCF_DEFAULT, (LPARAM)&cf);

	// if initializing, set the preview text
	if (bInit)
	{
		m_pRichEdit->ReplaceSel(pOptions->GetPreviewText());
		m_pRichEdit->SetSel(0, 0);
	}

	// also set background color
	m_pRichEdit->SetBackgroundColor(FALSE, pOptions->GetPreviewFontBackgroundColor());
}

BEGIN_MESSAGE_MAP(CRichTextPreviewView, CView)
	ON_MESSAGE(WM_PALETTE_COLORCHANGE, OnPaletteColorChange)
	ON_COMMAND(ID_PREVIEW_BOLD, OnBoldButton)
	ON_COMMAND(ID_PREVIEW_ITALIC, OnItalicButton)
	ON_COMMAND(ID_PREVIEW_UNDERLINE, OnUnderlineButton)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_BOLD, OnUpdateBoldButton)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_ITALIC, OnUpdateItalicButton)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_UNDERLINE, OnUpdateUnderlineButton)
	ON_WM_RBUTTONUP()
	ON_WM_SIZE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CRichTextPreviewView drawing

void CRichTextPreviewView::OnDraw(CDC* pDC)
{
	if (m_strCurrentFaceName.IsEmpty())
	{
		// get size of view
		CRect rcClient;
		GetClientRect(&rcClient);

		pDC->FillSolidRect(rcClient, RGB(0xFF, 0xFF, 0xFF));

		// display a message indicating there is no font selected
		CFont fntMessage;
		fntMessage.CreateFont(25, 0, 0, 0, FW_NORMAL,
							  FALSE, FALSE, FALSE, ANSI_CHARSET,
							  OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
							  DEFAULT_QUALITY, FF_DONTCARE,
							  _T("Tahoma"));

		// select font, color and background mode
		CFont* pfntOld = pDC->SelectObject(&fntMessage);
		COLORREF clrOld = pDC->SetTextColor(RGB(0x80, 0x80, 0x80));
		int nOldMode = pDC->SetBkMode(TRANSPARENT);

		CString strNoFont;
		strNoFont.LoadString(IDS_FONTMAPVIEW_NOSELECTION);
		pDC->DrawText(strNoFont, rcClient,
					  DT_SINGLELINE | DT_NOPREFIX | DT_CENTER | DT_VCENTER);

		// restore old font, color and background mode
		pDC->SelectObject(pfntOld);
		pDC->SetTextColor(clrOld);
		pDC->SetBkMode(nOldMode);
	}
}

void CRichTextPreviewView::SaveOptions()
{
	if (!m_pRichEdit)
		return;

	// select all text
	m_pRichEdit->SetSel(0, -1);
	
	// the MFC function GetSelText uses an ANSI string, we need Unicode
	// so we'll get the selected text ourselves
	CHARRANGE cr;
	cr.cpMin = cr.cpMax = 0;
	m_pRichEdit->SendMessage(EM_EXGETSEL, 0, (LPARAM)&cr);

	// create temp buffer
	boost::scoped_array<TCHAR> pszBuffer(new TCHAR[cr.cpMax - cr.cpMin + 1]);
	m_pRichEdit->SendMessage(EM_GETSELTEXT, 0, (LPARAM)pszBuffer.get());

	// save selected text
	theApp.GetProgramOptions()->SetPreviewText(pszBuffer.get());

	// deselect all
	m_pRichEdit->SetSel(0, 0);
}

void CRichTextPreviewView::SetFontHeight(int nSize)
{
	// save new height to options
	theApp.GetProgramOptions()->SetPreviewFontSize(nSize);

	// refresh to show changes
	RefreshFontView();
}

void CRichTextPreviewView::OnSelectedFontChange(const CFontItem* pFontItem)
{
	// save new name
	if (pFontItem)
	{
		m_strCurrentFaceName =
			pFontItem->GetOpenTypeData()->GetName(CruxTechnologies::OpenTypeFontData::CNameRecord::kNameID_FullFontName)->c_str();
		m_pFontItem.reset(theApp.GetFontManager()->AddReference(pFontItem),
						  boost::bind(&CFontManager::RemoveReference, theApp.GetFontManager(), _1));
	}
	else
	{
		m_pFontItem.reset();
		m_strCurrentFaceName.Empty();
	}

	// refresh to show changes
	RefreshFontView();
}

void CRichTextPreviewView::OnDetailSizeChange(unsigned short nSize)
{
	SetFontHeight((int)nSize);
}

void CRichTextPreviewView::OnPreviewRTFCharacter(TCHAR ch)
{
	TCHAR szText[2];
	szText[0] = ch;
	szText[1] = '\0';

	long nLength = m_pRichEdit->GetTextLength();

	m_pRichEdit->SetSel(nLength, nLength);
	m_pRichEdit->ReplaceSel(szText);
}

void CRichTextPreviewView::OnFontRunnerShutdown()
{
	SaveOptions();
}

int CRichTextPreviewView::OnUnManageFontItem(const CFontItem* pItem)
{
	if (m_pFontItem.get() == pItem)
	{
		// reset causes RemoveReference
		m_pFontItem.reset();
		m_strCurrentFaceName.Empty();

		// refresh to show changes
		RefreshFontView();

		return 1;
	}

	return 0;
}

void CRichTextPreviewView::OnFontFileDeleted(const CFontItem* pItem)
{
	if (m_pFontItem.get() == pItem)
	{
		// reset causes RemoveReference
		m_pFontItem.reset();
		m_strCurrentFaceName.Empty();

		// refresh to show changes
		RefreshFontView();
	}
}

void CRichTextPreviewView::OnDetailColorChange(COLORREF color, bool bForeground)
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// only update if we're in preview mode
	if (pOptions->GetFontDetailMode() != CProgramOptions::kMode_Preview)
		return;

	if (bForeground)
		pOptions->SetPreviewFontColor(color);
	else
		pOptions->SetPreviewFontBackgroundColor(color);

	// refresh to show changes
	RefreshFontView();
}


// CRichTextPreviewView diagnostics

#ifdef _DEBUG
void CRichTextPreviewView::AssertValid() const
{
	CView::AssertValid();
}

#ifndef _WIN32_WCE
void CRichTextPreviewView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}
#endif
#endif //_DEBUG


// CRichTextPreviewView message handlers
BOOL CRichTextPreviewView::Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext)
{
	// remove border from this view
	dwStyle ^= WS_BORDER;

	return CView::Create(lpszClassName, lpszWindowName, dwStyle, rect, pParentWnd, nID, pContext);
}

void CRichTextPreviewView::OnInitialUpdate()
{
	if (m_bInitialUpdated)
		return;

	CView::OnInitialUpdate();

	// is there a selected font?
	const CFontItem* pFontItem = NULL;

	if (CFontRunnerDoc::GetDoc())
		pFontItem = CFontRunnerDoc::GetDoc()->GetCurrentFontItem();

	if (pFontItem)
		OnSelectedFontChange(pFontItem);

	// listen to selected font change signals
	CFontRunnerSignals* pSignals = theApp.GetSignals();
	pSignals->ConnectTo_SelectedFontChange(boost::bind(&CRichTextPreviewView::OnSelectedFontChange, this, _1));
	pSignals->ConnectTo_DetailSizeChange(boost::bind(&CRichTextPreviewView::OnDetailSizeChange, this, _1));
	pSignals->ConnectTo_Shutdown(boost::bind(&CRichTextPreviewView::OnFontRunnerShutdown, this));
	pSignals->ConnectTo_PreviewRTFCharacter(boost::bind(&CRichTextPreviewView::OnPreviewRTFCharacter, this, _1));
	pSignals->ConnectTo_UnManageFontItem(boost::bind(&CRichTextPreviewView::OnUnManageFontItem, this, _1));
	pSignals->ConnectTo_FontFileDeleted(boost::bind(&CRichTextPreviewView::OnFontFileDeleted, this, _1));
	pSignals->ConnectTo_DetailColorChange(boost::bind(&CRichTextPreviewView::OnDetailColorChange, this, _1, _2));

	m_bInitialUpdated = true;
}

void CRichTextPreviewView::OnBoldButton()
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// negate bold flag
	pOptions->SetPreviewFontBold(!pOptions->IsPreviewFontBold());

	// refresh to show changes
	RefreshFontView();
}

void CRichTextPreviewView::OnItalicButton()
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// negate italic flag
	pOptions->SetPreviewFontItalic(!pOptions->IsPreviewFontItalic());

	// refresh to show changes
	RefreshFontView();
}

void CRichTextPreviewView::OnUnderlineButton()
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// negate underline flag
	pOptions->SetPreviewFontUnderlined(!pOptions->IsPreviewFontUnderlined());

	// refresh to show changes
	RefreshFontView();
}

void CRichTextPreviewView::OnUpdateBoldButton(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontBold());
}

void CRichTextPreviewView::OnUpdateItalicButton(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontItalic());
}

void CRichTextPreviewView::OnUpdateUnderlineButton(CCmdUI *pCmdUI)
{
	pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontUnderlined());
}

LRESULT CRichTextPreviewView::OnPaletteColorChange(WPARAM wParam, LPARAM lParam)
{
	COLORREF clrNew = (COLORREF)wParam;

	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// 0 means foreground color
	if (lParam == 0)
		pOptions->SetPreviewFontColor(clrNew);
	else
		pOptions->SetPreviewFontBackgroundColor(clrNew);

	// refresh to show changes
	RefreshFontView();

	return 0L;
}

void CRichTextPreviewView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// get main menu
	CMenu* pMainMenu = AfxGetApp()->GetMainWnd()->GetMenu();

	// sub menu we're interested in is the 3rd one (index 2)
	CMenu* pPreviewMenu = pMainMenu->GetSubMenu(2);

	ClientToScreen(&point);

	// Show menu
	pPreviewMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y,  AfxGetApp()->GetMainWnd());

	CView::OnRButtonUp(nFlags, point);
}

void CRichTextPreviewView::OnSize(UINT nType, int cx, int cy)
{
	if (m_pRichEdit)
		m_pRichEdit->MoveWindow(0, 0, cx, cy);

	CView::OnSize(nType, cx, cy);
}

void CRichTextPreviewView::OnDestroy()
{
	if (m_pRichEdit)
	{
		m_pRichEdit->DestroyWindow();
		delete m_pRichEdit;
		m_pRichEdit = NULL;
	}

	CView::OnDestroy();
}