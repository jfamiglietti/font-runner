/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxcmn.h"
#include <vector>
#include "afxwin.h"

// CSelectFavoriteFoldersDialog dialog

class CSelectFavoriteFoldersDialog : public CDialog
{
	DECLARE_DYNAMIC(CSelectFavoriteFoldersDialog)

public:
	CSelectFavoriteFoldersDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSelectFavoriteFoldersDialog();

// Dialog Data
	enum { IDD = IDD_SELECTFAVORITES_DIALOG };

// overrides
protected:
	virtual BOOL OnInitDialog();

public:
	const CString& GetSelectedPath() const { return m_strSelectedPath; }

private:
	void AddFolderToListCtrl(const std::wstring& str);
	void SelectAll();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

private:
	CString m_strSelectedPath;
	CListCtrl m_ctrlFavoritesSelectionList;
	CImageList m_ilFavorites;
	CButton m_btnOk;

	DECLARE_MESSAGE_MAP()
	afx_msg void OnLvnItemchangedFavoritesSelectionList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkFavoritesSelectionList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedOk();
};
