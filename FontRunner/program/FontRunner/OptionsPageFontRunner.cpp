// OptionsPageGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "ProgramOptions.h"
#include "FontRunnerSignals.h"
#include "OptionsPageFontRunner.h"

#include "FontProjects.h"
#include "WinToolbox.h"
#include "help/Help.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COptionsPageFontRunner dialog

IMPLEMENT_DYNAMIC(COptionsPageFontRunner, CPropertyPage)
COptionsPageFontRunner::COptionsPageFontRunner()
	: CPropertyPage(COptionsPageFontRunner::IDD), m_nLocationsLimitEdit(20)
{
	m_psp.dwFlags |= PSP_HASHELP;
}

COptionsPageFontRunner::~COptionsPageFontRunner()
{
}

BOOL COptionsPageFontRunner::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// initialize combos
	InitStartupCombo();

	// startup options
	m_strThisFolderEdit = pOptions->GetDefaultFolder();
	m_ctrlStartupCombo.SetCurSel(pOptions->GetStartupOption() == CProgramOptions::kStartupOption_DefaultFolder ? 0 : 1);

	m_btnShowFullPathInTitleBarCheck.SetCheck(pOptions->ShowFullPathInTitleBar() ? BST_CHECKED : BST_UNCHECKED);

	m_btnClearLocationsOnExitCheck.SetCheck(pOptions->GetClearLocationsOnExit() ? BST_CHECKED : BST_UNCHECKED);
	m_btnLimitLocationsCheck.SetCheck(pOptions->GetLimitLocations() ? BST_CHECKED : BST_UNCHECKED);
	m_nLocationsLimitEdit = pOptions->GetLocationsLimit();
	m_ctrlLocationsLimitEdit.EnableWindow(pOptions->GetLimitLocations());

	UpdateData(false);

	return true;
}

BOOL COptionsPageFontRunner::OnApply()
{
	// read data from controls
	UpdateData(true);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// startup options
	CString strText;
	m_ctrlThisFolderEdit.GetWindowText(strText);
	pOptions->SetDefaultFolder(strText);

	if (m_ctrlStartupCombo.GetCurSel() == 0)
		pOptions->SetStartupOption(CProgramOptions::kStartupOption_DefaultFolder);
	else
		pOptions->SetStartupOption(CProgramOptions::kStartupOption_LastItem);

	pOptions->SetClearLocationsOnExit(m_btnClearLocationsOnExitCheck.GetCheck() == BST_CHECKED);
	pOptions->SetLimitLocations(m_btnLimitLocationsCheck.GetCheck() == BST_CHECKED);

	if (m_nLocationsLimitEdit == 0)
	{
		m_nLocationsLimitEdit = 1;
		UpdateData(false);
	}

	pOptions->SetLocationsLimit(m_nLocationsLimitEdit);

	pOptions->ShowFullPathInTitleBar(m_btnShowFullPathInTitleBarCheck.GetCheck() == BST_CHECKED);

	// fire options change
	theApp.GetSignals()->Fire_OptionsChange();

	return CPropertyPage::OnApply();
}

void COptionsPageFontRunner::InitStartupCombo()
{
	CString strText;
	strText.LoadString(IDS_OPTIONSDIALOG_OPENDEFAULTFOLDER);
	m_ctrlStartupCombo.AddString(strText);

	strText.LoadString(IDS_OPTIONSDIALOG_OPENLAST);
	m_ctrlStartupCombo.AddString(strText);
}

void COptionsPageFontRunner::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_THISFOLDER_EDIT, m_strThisFolderEdit);
	DDX_Control(pDX, IDC_THISFOLDER_EDIT, m_ctrlThisFolderEdit);
	DDX_Control(pDX, IDC_BROWSEFOLDER_BUTTON, m_btnBrowseThisFolder);
	DDX_Control(pDX, IDC_THISFOLDER_EDIT, m_ctrlThisFolderEdit);
	DDX_Control(pDX, IDC_FULLPATH_CHECK, m_btnShowFullPathInTitleBarCheck);
	DDX_Control(pDX, IDC_STARTUPOPEN_COMBO, m_ctrlStartupCombo);
	DDX_Control(pDX, IDC_CLEARLOCATIONS_CHECK, m_btnClearLocationsOnExitCheck);
	DDX_Control(pDX, IDC_LOCATIONLIMIT_CHECK, m_btnLimitLocationsCheck);
	DDX_Control(pDX, IDC_LOCATIONS_EDIT, m_ctrlLocationsLimitEdit);
	DDX_Text(pDX, IDC_LOCATIONS_EDIT, m_nLocationsLimitEdit);
}

BEGIN_MESSAGE_MAP(COptionsPageFontRunner, CPropertyPage)
	ON_EN_CHANGE(IDC_THISFOLDER_EDIT, OnEnChangeThisFolderEdit)
	ON_BN_CLICKED(IDC_BROWSEFOLDER_BUTTON, OnBnClickedBrowseFolder)
	ON_CBN_SELCHANGE(IDC_STARTUPOPEN_COMBO, &COptionsPageFontRunner::OnCbnSelchangeStartupCombo)
	ON_NOTIFY_RANGE(PSN_HELP, 0x0000, 0xFFFF, OnHelpButton)
	ON_BN_CLICKED(IDC_USECURRENT_BUTTON, &COptionsPageFontRunner::OnBnClickedUsecurrentButton)
	ON_BN_CLICKED(IDC_FULLPATH_CHECK, &COptionsPageFontRunner::OnBnClickedFullpathCheck)
	ON_BN_CLICKED(IDC_CLEARLOCATIONS_CHECK, &COptionsPageFontRunner::OnBnClickedClearLocationsCheck)
	ON_BN_CLICKED(IDC_LOCATIONLIMIT_CHECK, &COptionsPageFontRunner::OnBnClickedLimitLocationsCheck)
	ON_BN_CLICKED(IDC_CLEARLOCATIONS_BUTTON, &COptionsPageFontRunner::OnBnClickedClearLocationsNow)
	ON_EN_CHANGE(IDC_LOCATIONS_EDIT, &COptionsPageFontRunner::OnEnChangeLocationsEdit)
END_MESSAGE_MAP()


// COptionsPageFontRunner message handlers

void COptionsPageFontRunner::OnEnChangeThisFolderEdit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontRunner::OnBnClickedBrowseFolder()
{
	CString strFolder = CWinToolbox::BrowseForFolder(m_hWnd, IDS_GENERAL_STARTUPFOLDER);

	if (!strFolder.IsEmpty())
	{
		m_ctrlThisFolderEdit.SetWindowText(strFolder);
		SetModified(true);
	}
}

void COptionsPageFontRunner::OnHelpButton(UINT /*id*/, NMHDR* /*pNotifyStruct*/, LRESULT* /*result*/)
{
	HtmlHelp(HIDP_OPTIONS_GENERAL, HH_HELP_CONTEXT);
}

void COptionsPageFontRunner::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	AfxGetApp()->HtmlHelp(dwData, nCmd);
}

void COptionsPageFontRunner::OnCbnSelchangeStartupCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontRunner::OnBnClickedUsecurrentButton()
{
	m_strThisFolderEdit = CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ();
	UpdateData(false);
	SetModified(true);
}

void COptionsPageFontRunner::OnBnClickedFullpathCheck()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontRunner::OnBnClickedClearLocationsCheck()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontRunner::OnBnClickedLimitLocationsCheck()
{
	// notify property sheet of change
	SetModified(true);

	m_ctrlLocationsLimitEdit.EnableWindow(m_btnLimitLocationsCheck.GetCheck() == BST_CHECKED);
}

void COptionsPageFontRunner::OnBnClickedClearLocationsNow()
{
	theApp.GetSignals()->Fire_ClearLocationBar();
}

void COptionsPageFontRunner::OnEnChangeLocationsEdit()
{
	// notify property sheet of change
	SetModified(true);

	UpdateData(true);
	if (m_nLocationsLimitEdit == 0)
	{
		CString strTitle, strMessage;
		strTitle.LoadString(IDS_OPTIONSDIALOG_LOCATIONLIMITZERO_TITLE);
		strMessage.LoadString(IDS_OPTIONSDIALOG_LOCATIONLIMITZERO);
		m_ctrlLocationsLimitEdit.ShowBalloonTip(strTitle, strMessage, TTI_WARNING);
	}
	else
		m_ctrlLocationsLimitEdit.HideBalloonTip();
}