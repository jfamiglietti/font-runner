/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// SelectFavoriteFoldersDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FavoriteFolders.h"
#include "SelectFavoriteFoldersDialog.h"

#include <shlwapi.h>
#include <boost/bind.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CSelectFavoriteFoldersDialog dialog

IMPLEMENT_DYNAMIC(CSelectFavoriteFoldersDialog, CDialog)

CSelectFavoriteFoldersDialog::CSelectFavoriteFoldersDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CSelectFavoriteFoldersDialog::IDD, pParent)
{

}

CSelectFavoriteFoldersDialog::~CSelectFavoriteFoldersDialog()
{
}

void CSelectFavoriteFoldersDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_FAVORITES_SELECTION_LIST, m_ctrlFavoritesSelectionList);
	DDX_Control(pDX, IDOK, m_btnOk);
}

BOOL CSelectFavoriteFoldersDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	// add one column to list control
	m_ctrlFavoritesSelectionList.InsertColumn(0, _T("Folder"));

	// setup imagelist
	m_ilFavorites.Create(16, 16, ILC_COLOR32, 0, 100);
	
	// attach it to the list control
	m_ctrlFavoritesSelectionList.SetImageList(&m_ilFavorites, LVSIL_SMALL);

	// add favorites to list
	CFavoriteFolders* pFavorites = theApp.GetFavoriteFolders();
	pFavorites->ForEach(boost::bind(&CSelectFavoriteFoldersDialog::AddFolderToListCtrl, this, _1));

	return TRUE;
}

void CSelectFavoriteFoldersDialog::AddFolderToListCtrl(const std::wstring& str)
{
	if (!::PathIsDirectory(str.c_str()))
		return;

	// get icon from shell
	SHFILEINFO sfi;
	if (::SHGetFileInfo(str.c_str(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_SMALLICON))
	{
		int nImage = m_ilFavorites.Add(sfi.hIcon);
		::DestroyIcon(sfi.hIcon);

		m_ctrlFavoritesSelectionList.InsertItem(0, str.c_str(), nImage);
		m_ctrlFavoritesSelectionList.SetColumnWidth(0, LVSCW_AUTOSIZE);
	}
}


BEGIN_MESSAGE_MAP(CSelectFavoriteFoldersDialog, CDialog)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_FAVORITES_SELECTION_LIST, &CSelectFavoriteFoldersDialog::OnLvnItemchangedFavoritesSelectionList)
	ON_NOTIFY(NM_DBLCLK, IDC_FAVORITES_SELECTION_LIST, &CSelectFavoriteFoldersDialog::OnNMDblclkFavoritesSelectionList)
	ON_BN_CLICKED(IDOK, &CSelectFavoriteFoldersDialog::OnBnClickedOk)
END_MESSAGE_MAP()


// CSelectFavoriteFoldersDialog message handlers

void CSelectFavoriteFoldersDialog::OnLvnItemchangedFavoritesSelectionList(NMHDR* /*pNMHDR*/, LRESULT *pResult)
{
	//LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	m_btnOk.EnableWindow(m_ctrlFavoritesSelectionList.GetSelectedCount());

	*pResult = 0;
}

void CSelectFavoriteFoldersDialog::OnNMDblclkFavoritesSelectionList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	if (pNMItemActivate->iItem >= 0)
		OnBnClickedOk();

	*pResult = 0;
}

void CSelectFavoriteFoldersDialog::OnBnClickedOk()
{
	int n = m_ctrlFavoritesSelectionList.GetNextItem(-1, LVNI_SELECTED);
	if (n != -1)
		m_strSelectedPath = m_ctrlFavoritesSelectionList.GetItemText(n, 0);
	
	OnOK();
}
