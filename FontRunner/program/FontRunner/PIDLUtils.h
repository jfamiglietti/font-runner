/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>

namespace pidlutils
{
	UINT getsize(LPCITEMIDLIST pidl);
	LPITEMIDLIST createpidl(UINT cbSize);
	LPITEMIDLIST concatpidls(LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2);
	LPITEMIDLIST copyitemid(LPITEMIDLIST lpi);
	bool pidls_are_equal(IShellFolder* pShellFolder, LPCITEMIDLIST pidl1, LPCITEMIDLIST pidl2);
	LPITEMIDLIST copy1st(LPITEMIDLIST lpi);
	LPITEMIDLIST to_normal(IShellFolder* pShellFolder, LPCITEMIDLIST pidl);
};

// a basic smart pointer for shell-allocated PIDLs
struct pidl_ptr
{
	pidl_ptr() : pidl(NULL), bILFree(false)
	{}

	pidl_ptr(LPITEMIDLIST p)
		: pidl(p), bILFree(false)
	{}

	~pidl_ptr()
	{
		reset();
	}

	// returns address of PIDL
	LPITEMIDLIST* operator & ()
	{
		return &pidl;
	}

	// returns the pidl itself
	LPITEMIDLIST get() const
	{
		return pidl;
	}

	void reset(LPITEMIDLIST p = NULL)
	{
		bILFree ? ::ILFree(pidl) : ::CoTaskMemFree(pidl);

		pidl = p;
	}

	void useILFree(bool bUse = true)
	{
		bILFree = bUse;
	}

	// assignment
	const LPITEMIDLIST operator = (const LPITEMIDLIST p)
	{
		reset(p);
		return p;
	}

private:
	LPITEMIDLIST pidl;
	bool bILFree;
};

// A self-maintaining object for shell-allocated pidls
// combined with a boost::shared_ptr, this makes pidls pretty easy to contain and clean up
struct pidl_object
{
	typedef boost::function<void (LPITEMIDLIST)> deleter_type;

	pidl_object(LPITEMIDLIST pidl)
		: m_pidl(pidl)
	{}

	pidl_object(LPITEMIDLIST pidl, deleter_type deleter)
		: m_pidl(pidl), m_deleter(deleter)
	{}

	~pidl_object()
	{
		if (m_pidl && m_deleter)
			m_deleter(m_pidl);
	}

	size_t size() const
	{
		return ::ILGetSize(m_pidl);
	}

	LPITEMIDLIST clone() const
	{
		return ::ILClone(m_pidl);
	}

	LPITEMIDLIST get() const
	{
		return m_pidl;
	}

// no public assignment operator
private:
	const pidl_object& operator = (const pidl_object& src)
	{
		m_pidl = src.m_pidl;
		return *this;
	}

private:
	LPITEMIDLIST m_pidl;
	deleter_type m_deleter;
};

// pidl object pointer
typedef boost::shared_ptr<pidl_object> pidlobject_ptr;