/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FindFrameBase.h"
#include "TransparentStatic.h"

// CFindCharacterFrameWnd frame

class CFindCharacterFrameWnd : public CFindFrameBase, public boost::signals2::trackable
{
	DECLARE_DYNCREATE(CFindCharacterFrameWnd)

public:
	CFindCharacterFrameWnd(const wchar_t* pszCharDesc = NULL,
						   wchar_t character = 0,
						   CFontItem* pFontItem = NULL,
						   LPCTSTR lpszSearchRoot = NULL,
						   bool bProject = false);
	virtual ~CFindCharacterFrameWnd();

public:
	bool Create(CWnd* pParentWnd);

private:
	void SetLookForPreviewFont(CStatic* pwndFontNameStatic);
	void OnOptionsChange();
	int UnManageFontItem(const CFontItem* pItem);
	void OnFontFileDeleted(const CFontItem* pItem);

// functions for the search thread
private:
	bool Searching() const;
	void EnteringFolder(LPCTSTR szPath);
	void Found(CFontItem* pItem);

// overrides
protected:
	CFontListView::eMode GetFontListViewMode() const;

private:
	const wchar_t*		m_pszCharDesc;
	CTransparentStatic	m_wndLookingForStatic;
	boost::shared_ptr<CFontItem>	m_pFontItem;
	wchar_t				m_character;
	CFont				m_fntLookFor;
	bool m_bSearchSubFolders;
	bool m_bSearchRemovableDisks;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLookInSelChange();
	afx_msg void OnLookInDeleteItem(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnUpdateOptions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateSearchButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCancelButton(CCmdUI* pCmdUI);
	afx_msg void OnOptionsSearchSubFolders();
	afx_msg void OnOptionsSearchRemovableDisks();
	afx_msg void OnSearchButton();
	afx_msg void OnCancelButton();
	afx_msg LRESULT OnFontFound(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEnteringFolder(WPARAM wParam, LPARAM lParam);
};


