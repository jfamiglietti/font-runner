/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/shared_ptr.hpp>

// property sheet pages prototypes
class COptionsPageGeneral;
class COptionsPageFontRunner;
class COptionsPagePrinting;
class COptionsPageFontViewing;

// COptionsSheet
class COptionsSheet : public CPropertySheet
{
	DECLARE_DYNAMIC(COptionsSheet)

public:
	COptionsSheet(UINT nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	COptionsSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
	virtual ~COptionsSheet();

// Attributes
public:
	boost::shared_ptr<COptionsPageGeneral>		m_pGeneralPage;
	boost::shared_ptr<COptionsPagePrinting>		m_pPrintingPage;
	boost::shared_ptr<COptionsPageFontViewing>	m_pFontViewingPage;

// overrides
public:
	INT_PTR DoModal();

protected:
	DECLARE_MESSAGE_MAP()
};


