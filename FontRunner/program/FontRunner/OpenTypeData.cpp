/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "OpenTypeData.h"

#include <boost/scoped_array.hpp>
#include <fstream>
#include <memory>
#include <algorithm>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace CruxTechnologies;

//////////////////////////////////////////////////////////
// internal exception class...
// used for reporting errors during initialization
class COpenTypeException : public std::exception
{
public:
	COpenTypeException(COpenTypeData::eStatus nStatus)
		: m_nStatus(nStatus)
	{
	}

	COpenTypeData::eStatus GetStatus() const
	{
		return m_nStatus;
	}

private:
	COpenTypeData::eStatus m_nStatus;
};
//////////////////////////////////////////////////////////


COpenTypeData::COpenTypeData()
{
}

COpenTypeData::~COpenTypeData()
{
}

// all this does is make sure the fstream is closed when it goes out of scope
struct smart_fstream
{
	~smart_fstream()
	{
		if (fs.is_open())
			fs.close();
	}

	std::fstream fs;
};

COpenTypeData::eStatus COpenTypeData::Initialize(const wchar_t* pszFileName, size_t nFileSize)
{
	// file stream and buffer
	smart_fstream smartfs;

	try
	{
		// How big is this file?  Conservatively, let's cap it at 50MB.
		// We really want to weed out the absurdly large
		if (nFileSize >= 52428800)
			throw COpenTypeException(kStatus_FileTooBig);

		smartfs.fs.open(pszFileName, std::ios_base::in | std::ios_base::binary);

		if (smartfs.fs.fail())
			throw COpenTypeException(kStatus_OpenFileError);

		// create a buffer for the whole file
		boost::scoped_array<char> pBuffer(new char[nFileSize]);

		// read the whole file
		smartfs.fs.read(pBuffer.get(), (std::streamsize)nFileSize);
		if (smartfs.fs.fail())
			throw COpenTypeException(kStatus_ReadFileError);

		// done with file
		smartfs.fs.close();

		// record current position
		char* pCurrent = pBuffer.get();

		// get header
		size_t nSizeRemaining = nFileSize;
		size_t size = m_Header.Initialize(pBuffer.get(), nSizeRemaining);
		pCurrent += size;

		if (size == 0)
			throw COpenTypeException(kStatus_CorruptFileError);

		nSizeRemaining -= size;

		// get table directories
		for (int nTableDir = 0; nTableDir < m_Header.NumTables(); ++nTableDir)
		{
			std::auto_ptr<OpenTypeFontData::CTableDirectory>
				pTableDirectory(new OpenTypeFontData::CTableDirectory());
			size = pTableDirectory->Initialize(pCurrent, nSizeRemaining);
			pCurrent += size;
			nSizeRemaining -= size;

			if (size == 0 || pTableDirectory->Offset() >= nFileSize)
				throw COpenTypeException(kStatus_CorruptFileError);

			if (pTableDirectory->Tag() == 'cmap')
			{
				// save position
				char* pPrev = pCurrent;

				// move to table offset
				pCurrent = pBuffer.get() + pTableDirectory->Offset();

				size = m_cmapTable.Initialize(pCurrent, nSizeRemaining);
				nSizeRemaining -= size;

				if (size == 0)
					throw COpenTypeException(kStatus_CorruptFileError);

				// return to previous position
				pCurrent = pPrev;
			}
			if (pTableDirectory->Tag() == 'name')
			{
				// save position
				char* pPrev = pCurrent;

				// move to table offset
				pCurrent = pBuffer.get() + pTableDirectory->Offset();

				size = m_NamingTable.Initialize(pCurrent, nSizeRemaining);
				nSizeRemaining -= size;

				if (size == 0)
					throw COpenTypeException(kStatus_CorruptFileError);

				// return to previous position
				pCurrent = pPrev;
			}
			if (pTableDirectory->Tag() == 'OS/2')
			{
				// save position
				char* pPrev = pCurrent;

				// move to table offset
				pCurrent = pBuffer.get() + pTableDirectory->Offset();

				size = m_OS2Table.Initialize(pCurrent, nSizeRemaining);
				nSizeRemaining -= size;

				if (size == 0)
					throw COpenTypeException(kStatus_CorruptFileError);

				// return to previous position
				pCurrent = pPrev;
			}
		}
	}
	catch (COpenTypeException& ex)
	{
		return ex.GetStatus();
	}

	// initialization was successful
	return kStatus_OK;
}

// function object for matching a name record.
class CNameRecordMatch
{
public:
	CNameRecordMatch(OpenTypeFontData::CNameRecord::eNameID nID,
					 uint16_t nLanguageID,
					 OpenTypeFontData::ePlatformID nPlatformID)
		: m_nID(nID),
		  m_nLanguageID(nLanguageID),
		  m_nPlatformID(nPlatformID)
	{
	}

	bool operator()(const OpenTypeFontData::CNameRecord& record) const
	{
		return ((record.NameID() == m_nID) &&
				(record.LanguageID() == m_nLanguageID) &&
				(record.PlatformID() == m_nPlatformID));
	}

private:
	OpenTypeFontData::CNameRecord::eNameID m_nID;
	uint16_t m_nLanguageID;
	OpenTypeFontData::ePlatformID m_nPlatformID;
};

// Gets a name string.
// Default language ID is American English, default platform is Microsoft.
// For other Microsoft LCID's see http://www.microsoft.com/OpenType/OTSpec/name.htm.
// Returns a borrowed pointer to the name string object.  Caller does not own
// pointer, so caller should not delete it.
const std::wstring* COpenTypeData::GetName(
							OpenTypeFontData::CNameRecord::eNameID nID,
							uint16_t nLanguageID,
							OpenTypeFontData::ePlatformID nPlatformID) const
{
	// do nothing if no naming table
	if (!m_NamingTable.Initialized())
		return NULL;

	// bring naming table class into this namespace
	using OpenTypeFontData::CNamingTable;

	// get reference to name table records
	const CNamingTable::NameRecordContainer_Type& records = m_NamingTable.GetNameRecords();

	CNamingTable::NameRecordContainer_Type::const_iterator it =
		std::find_if(records.begin(),
					 records.end(),
					 CNameRecordMatch(nID, nLanguageID, nPlatformID));

	if (it == records.end())
		return NULL;
	else
		return it->Name();
}

// Returns a reference to the container of all name records
const OpenTypeFontData::CNamingTable::NameRecordContainer_Type& COpenTypeData::GetNameRecords() const
{
	return m_NamingTable.GetNameRecords();
}

const OpenTypeFontData::CcmapTable::EncodingRecordList_Type* 
	COpenTypeData::GetCharMaps() const
{
	return m_cmapTable.GetMaps();
}

const OpenTypeFontData::COS2Table& COpenTypeData::GetOS2Table() const
{
	return m_OS2Table;
}

bool COpenTypeData::IsSymbol() const
{
	return m_cmapTable.IsSymbol();
}
