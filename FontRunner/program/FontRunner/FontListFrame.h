/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "TransparentStatic.h"


// CFontListFrame frame

class CFontListFrame : public CFrameWnd
{
	DECLARE_DYNCREATE(CFontListFrame)
protected:
	CFontListFrame();           // protected constructor used by dynamic creation
	virtual ~CFontListFrame();

	// overrides
protected:
	BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	BOOL IsFrameWnd() const;

	// signal slots
private:
	void OnViewingSystemFonts(bool bSystemFonts);
	void OnSelectedFolderChange(const CString& strFolder, const CString*);

private:
	CFont				m_fntCaption;
	CReBar				m_wndRebar;
	CTransparentStatic	m_wndStaticText;
	CImageList			m_wndRebarImageList;
	CButton				m_btnExplore;
	CString				m_strCurrentFolder;
	bool				m_bSystemFonts;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnBnClickedExplore();
};


