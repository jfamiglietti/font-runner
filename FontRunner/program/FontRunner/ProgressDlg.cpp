/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// ProgressDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "ProgressDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CProgressDlg dialog

IMPLEMENT_DYNAMIC(CProgressDlg, CDialog)
CProgressDlg::CProgressDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDlg::IDD, pParent),
	  m_strProgressStatic(_T("")),
	  m_bCanceled(false),
	  m_bSetTotal(false),
	  m_bUpdateProgressText(true),
	  m_nCurrent(0),
	  m_nTotal(-1),
	  m_nThreadsFinished(0),
	  m_nThreadsTotal(0),
	  m_nTimerID(0)
{
}

CProgressDlg::~CProgressDlg()
{
}

BOOL CProgressDlg::Create(CWnd* pParentWnd)
{
	return CDialog::Create(CProgressDlg::IDD, pParentWnd);
}

void CProgressDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_WORK_PROGRESS, m_ctrlProgress);
	DDX_Control(pDX, IDC_PROGRESS_STATIC, m_ctrlProgressStatic);
	DDX_Text(pDX, IDC_PROGRESS_STATIC, m_strProgressStatic);

	// make sure progress control has correct range
	if (!m_bSetTotal)
	{
		m_ctrlProgress.SetRange(0, (short)m_nTotal);
		m_bSetTotal = true;
	}
}

void CProgressDlg::Increment()
{
	::InterlockedIncrement(&m_nCurrent);
}

void CProgressDlg::Start(int nTotal, int nThreads)
{
	m_nTotal = nTotal;
	m_nThreadsTotal = nThreads;

	CComPtr<ITaskbarList> pTaskbarList = NULL;
	if (SUCCEEDED(::CoCreateInstance(CLSID_TaskbarList, NULL, CLSCTX_INPROC_SERVER, IID_ITaskbarList, (void**)&pTaskbarList)))
		m_pTaskbarList = pTaskbarList;

	if (m_pTaskbarList)
	{
		HWND hwnd = AfxGetMainWnd()->GetSafeHwnd();
		m_pTaskbarList->SetProgressState(hwnd, TBPF_NORMAL);
		m_pTaskbarList->SetProgressValue(hwnd, 0, nTotal);
	}
}

void CProgressDlg::Finish()
{
	::InterlockedIncrement(&m_nThreadsFinished);
}

void CProgressDlg::SetProgress(int nPos)
{
	m_ctrlProgress.SetPos(nPos);

	if (m_pTaskbarList)
		m_pTaskbarList->SetProgressValue(AfxGetMainWnd()->GetSafeHwnd(), nPos, m_nTotal);

	// set progress text
	if (m_bUpdateProgressText)
	{
		CString strFormat;
		strFormat.LoadString(IDS_GENERAL_READINGFONTS);
		m_strProgressStatic.Format(strFormat, nPos, m_nTotal);
		m_ctrlProgressStatic.SetWindowText(m_strProgressStatic);
	}
}

void CProgressDlg::SetProgressText(UINT nID)
{
	// load string
	m_strProgressStatic.LoadString(nID);
	UpdateData(FALSE);
}

void CProgressDlg::SetProgressText(const CString& strText)
{
	// use passed-in string
	m_strProgressStatic = strText;
	UpdateData(FALSE);
}

void CProgressDlg::UpdateProgressText(bool bUpdate)
{
	m_bUpdateProgressText = bUpdate;
}

void CProgressDlg::ShowCancelButton(bool bShow)
{
	if (::IsWindow(m_hWnd))
		GetDlgItem(IDC_CANCEL_BUTTON)->ShowWindow(bShow ? SW_SHOW : SW_HIDE);
}


BEGIN_MESSAGE_MAP(CProgressDlg, CDialog)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CANCEL_BUTTON, OnBnClickedCancelButton)
END_MESSAGE_MAP()


// CProgressDlg message handlers

int CProgressDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	m_nTimerID = SetTimer(1, 50, NULL);

	return 0;
}

void CProgressDlg::OnDestroy()
{
	if (m_nTimerID)
		KillTimer(m_nTimerID);
}

void CProgressDlg::OnBnClickedCancelButton()
{
	m_bCanceled = true;

	if (m_pTaskbarList)
		m_pTaskbarList->SetProgressState(AfxGetMainWnd()->GetSafeHwnd(), TBPF_NOPROGRESS);

	CDialog::OnCancel();
}

void CProgressDlg::OnTimer(UINT_PTR nIDEvent)
{
	ASSERT(m_nTimerID == nIDEvent);
	if (m_nTimerID == nIDEvent)
	{
		if (m_nThreadsFinished >= m_nThreadsTotal)
		{
			if (m_pTaskbarList)
				m_pTaskbarList->SetProgressState(AfxGetMainWnd()->GetSafeHwnd(), TBPF_NOPROGRESS);

			OnOK();
		}
		else
			SetProgress(m_nCurrent);
	}
}
