/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FontRunnerSignals.h"

// CMenuBar

class CMenuBar : public CToolBar
{
	DECLARE_DYNAMIC(CMenuBar)

public:
	CMenuBar();
	virtual ~CMenuBar();

public:
	bool Create(CWnd* pParent);
	void SetPrefix(bool bPrefix);
	void Activate(bool bActive);
	bool ShowDropDownMenu(int nID);
	bool ShowDropDownMenuByChar(UINT nChar);
	bool OnKeyMenu(UINT nKey);
	void Highlight(int nItem);
	boost::signals2::connection connect_to_rclick(const rclick_signal_type::slot_type& slot);

public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	bool OnMenuInput(MSG* pMSG);
	bool InSubMenu() const;
	void SetFromSystemParams();

	static LRESULT CALLBACK OnMessageHook(int code, WPARAM wParam, LPARAM lParam);

private:
	CMenu m_menu;
	int m_nCurrentActiveMenu, m_nSubMenuCount;
	HMENU m_hSelectedMenu;
	bool m_bSelectedSubMenu, m_bCues, m_bWndActive, m_bPrefix;
	unsigned short m_nCurrentCommandID;
	CWnd* m_pPrevFocus;

	static HHOOK s_hMenuHook;
	static CMenuBar* s_pMenuBar;
	static UINT s_uwmPopupMenu;

	rclick_signal_type m_rclick_signal;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg void OnTBNDropDown(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnCustomDraw(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnPopupMenu(WPARAM wParam, LPARAM lParam);
};


