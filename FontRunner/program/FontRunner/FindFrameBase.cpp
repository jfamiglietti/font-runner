/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FindFrameBase.cpp : implementation file
//

#include "stdafx.h"
#include "FindFrameBase.h"

#include "pidlutils.h"
#include "ShellUtils.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "ProgramOptions.h"
#include "FontListView.h"
#include "FontManager.h"
#include "HRESULTException.h"
#include "SelectFavoriteFoldersDialog.h"

#include "SelectProjectsDialog.h"
#include "FontProjects.h"

#include <shlwapi.h>
#include <strsafe.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

UINT CFindFrameBase::s_rwmFontFound = ::RegisterWindowMessage(_T("BB523987-3FD0-4167-9734-2806EAC3C0A9"));
UINT CFindFrameBase::s_rwmFinished = ::RegisterWindowMessage(_T("9D677BB0-5A0F-4fc8-A3F9-1A7FC930456C"));
UINT CFindFrameBase::s_rwmEnteringFolder = ::RegisterWindowMessage(_T("B251D034-17B9-4d1c-9C26-51AA30B0C208"));

void SearchFolder::operator()(LPCTSTR szSearchFolder) const
{
	// just return if we're done searching
	if (!m_pController->Searching())
		return;

	try
	{
		CheckResultFunctor CheckResult;

		m_pController->EnteringFolder(szSearchFolder);

		CComPtr<IShellFolder> pFolder, pDesktopFolder;
		CheckResult(::SHGetDesktopFolder(&pDesktopFolder));

		pidl_ptr pidlFolder;
		TCHAR szFolder[MAX_PATH];
		CheckResult(::StringCchCopy(szFolder, MAX_PATH, szSearchFolder));

		CheckResult(pDesktopFolder->ParseDisplayName(NULL, NULL, szFolder, NULL, &pidlFolder, NULL));
		CheckResult(pDesktopFolder->BindToObject(pidlFolder.get(), NULL, IID_IShellFolder, (void**)&pFolder));

		// enumerate sub-folders
		CComPtr<IEnumIDList> pEnumFolders;
		CheckResult(pFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumFolders));
		LPITEMIDLIST pidlTemp = NULL;

		// make a list of sub-folders
		directorylist_type folders;
		while (pEnumFolders->Next(1, &pidlTemp, NULL) == S_OK)
		{
			// make sure we're still searching
			if (!m_pController->Searching())
				return;

			pidl_ptr pidl(pidlTemp);
			CComPtr<IShellFolder> pSubFolder;
			
			TCHAR szFolder[MAX_PATH];
			ShellUtils::GetName(pFolder, pidl.get(), SHGDN_FORPARSING, szFolder, MAX_PATH);

			// skip folders that aren't really folders
			if (!::PathIsDirectory(szFolder))
				continue;

			folders.push_back(szFolder);

#ifdef _DEBUG
			CString strDebug;
			strDebug.Format(_T("Searching %s...\n"), szFolder);
			OutputDebugString(strDebug);
#endif
		}

		// search sub-folders
		std::for_each(folders.begin(), folders.end(), SearchFolder(m_pController));

		// one more time, make sure we're still going
		if (!m_pController->Searching())
			return;

		// search this folder
		TCHAR szSearchString[MAX_PATH];
		CheckResult(::StringCchCopy(szSearchString, MAX_PATH, szSearchFolder));
		::PathAppend(szSearchString, _T("*.ttf"));

		WIN32_FIND_DATA FindData;
		HANDLE hFindHandle = ::FindFirstFile(szSearchString, &FindData);

		if (hFindHandle == INVALID_HANDLE_VALUE)
		{
			// found nothing, just return
			::FindClose(hFindHandle);
			return;
		}

		CFontManager* pFontManager = theApp.GetFontManager();
		do
		{
			TCHAR szFileName[MAX_PATH];
			CheckResult(::StringCchCopy(szFileName, MAX_PATH, szSearchFolder));
			::PathAppend(szFileName, FindData.cFileName);

			CFontItem* pFontItem = pFontManager->AddReference(szFileName);
			m_pController->Found(pFontItem);
			pFontManager->RemoveReference(pFontItem);

		// keep going as long as there are files and the user hasn't canceled
		} while (::FindNextFile(hFindHandle, &FindData) && m_pController->Searching());

	}
	catch (CHRESULTException&)
	{
		return;
	}
}

void SearchProject::operator()(const CString& strProjectName) const
{
	CFontProjects* pProjects = theApp.GetFontProjects();

	projectlist_type::const_iterator itProject = pProjects->Find(strProjectName);

	if (itProject != pProjects->GetListEnd())
	{
		CFontManager* pFontManager = theApp.GetFontManager();

		for (filelist_type::const_iterator it = itProject->second->begin();
			 it != itProject->second->end(); ++it)
		{
			if (!m_pController->Searching())
				break;

			CFontItem* pFontItem = pFontManager->AddReference(*it);
			m_pController->Found(pFontItem);
			pFontManager->RemoveReference(pFontItem);
		}
	}
}

// CFindFrameBase
CFindFrameBase::CFindFrameBase(LPCTSTR lpszSearchRoot, bool bProject)
  : m_pFontListView(NULL),
    m_strCurrentFolder(lpszSearchRoot),
	m_bSearching(false),
	m_bCanceled(false),
    m_bLookInValid(true),
	m_bLookInMyComputer(false),
	m_nLookInSelection(-1),
	m_bCurrentFolderIsProject(bProject)
{
}

CFindFrameBase::~CFindFrameBase()
{
}

bool CFindFrameBase::Create(CWnd* pParentWnd, UINT nTitle)
{
	// need a parent for this window
	ASSERT(pParentWnd != NULL);

	// load title from resources
	CString strTitle;
	strTitle.LoadString(nTitle);

	return Create(pParentWnd, strTitle);
}

bool CFindFrameBase::Create(CWnd *pParentWnd, LPCTSTR lpszTitle)
{
	// sign up for shutdown signal
	theApp.GetSignals()->ConnectTo_Shutdown(boost::bind(&CFindFrameBase::OnFontRunnerShutdown, this));

	// need a parent for this window
	ASSERT(pParentWnd != NULL);

	// use regular arrow cursor for this window
	HCURSOR hArrowCursor = (HCURSOR)::LoadImage(NULL, IDC_ARROW, IMAGE_CURSOR, 0, 0, LR_DEFAULTSIZE | LR_SHARED);

	DWORD dwStyle = WS_POPUP | WS_THICKFRAME | WS_CAPTION | WS_SYSMENU | MFS_SYNCACTIVE | WS_VISIBLE;

	POINT pt;
	SIZE size;
	theApp.GetProgramOptions()->GetFindWindowPos(pt);
	theApp.GetProgramOptions()->GetFindWindowSize(size);

	if (size.cx == -1 && size.cy == -1)
	{
		// opening for the first time.  Which monitor is the main window on?
		HMONITOR hMonitor = ::MonitorFromWindow(theApp.GetMainWnd()->GetSafeHwnd(), MONITOR_DEFAULTTOPRIMARY);

		MONITORINFO mi;
		memset(&mi, 0, sizeof(MONITORINFO));
		mi.cbSize = sizeof(MONITORINFO);
		::GetMonitorInfo(hMonitor, &mi);

		// put it in the middle of the monitor
		LONG nRawX = (LONG)((mi.rcWork.right - mi.rcWork.left) * 0.25);
		LONG nRawY = (LONG)((mi.rcWork.bottom - mi.rcWork.top) * 0.25);

		pt.x = (LONG)(mi.rcWork.left + nRawX);
		pt.y = (LONG)(mi.rcWork.top + nRawY);
		size.cx = (mi.rcWork.right - mi.rcWork.left) - (nRawX * 2);
		size.cy = (mi.rcWork.bottom - mi.rcWork.top) - (nRawY * 2);
	}

	RECT rect = { pt.x, pt.y, pt.x + size.cx, pt.y + size.cy };
	return (CMiniFrameWnd::CreateEx(WS_EX_NOPARENTNOTIFY,
				AfxRegisterWndClass(0, hArrowCursor),
				lpszTitle, dwStyle, rect, pParentWnd) != 0);
}

BOOL CFindFrameBase::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* /*pContext*/)
{
	boost::scoped_ptr<CCreateContext> pCC(new CCreateContext);
	
	pCC->m_pCurrentDoc = CFontRunnerDoc::GetDoc();
	pCC->m_pCurrentFrame = this;
	pCC->m_pNewViewClass = RUNTIME_CLASS(CFontListView);
	m_pFontListView = dynamic_cast<CFontListView*>(CreateView(pCC.get()));

	// put font list view in search mode before inital updating it
	if (m_pFontListView)
		m_pFontListView->SetMode(GetFontListViewMode());

	InitialUpdateFrame(NULL, TRUE);

	return TRUE;
}

void CFindFrameBase::PostNcDestroy()
{
	// clean up after ourselves
	delete this;
}

int CFindFrameBase::ShowCurrent(IShellFolder* pFolder, LPITEMIDLIST pidlParseCurrent, LPITEMIDLIST pidlFQItem, COMBOBOXEXITEM& cbi, CComboBoxEx* pCombo, CImageList* pImageList)
{
	// init last inserted to -1 (nothing)
	int nDefaultSelection = -1;
	
	// some reusables
	SHFILEINFO sfi;
	TCHAR szName[MAX_PATH];

	while (pFolder && pidlParseCurrent && pidlParseCurrent->mkid.cb)
	{
		// next item
		++cbi.iItem;
		++cbi.iIndent;

		// get the 1st pidl from the list.  We need to isolate it for
		// alot of these shell functions to work properly.
		LPITEMIDLIST pidl1st = pidlutils::copy1st(pidlParseCurrent);

		// we also need the current FQ pidl
		pidlFQItem = pidlutils::concatpidls(pidlFQItem, pidl1st);

		// get shell icon
		if (::SHGetFileInfo((LPCTSTR)pidlFQItem, 0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_ICON | SHGFI_SMALLICON))
		{
			cbi.iImage = cbi.iSelectedImage = pImageList->Add(sfi.hIcon);
			::DestroyIcon(sfi.hIcon);
		}

		// get name
		ShellUtils::GetName(pFolder, pidl1st, SHGDN_NORMAL, szName, MAX_PATH);
		cbi.pszText = szName;

		// move to next folder
		IShellFolder* pNext = NULL;
		HRESULT hResult = pFolder->BindToObject(pidl1st, NULL, IID_IShellFolder, (void**)&pNext);
		pFolder->Release();

		// done with 1st pidl
		::CoTaskMemFree(pidl1st);

		// save FQ pidl
		cbi.lParam = reinterpret_cast<LPARAM>(new CFindFrameBase::FindComboData(pidlFQItem));
		
		// insert it
		nDefaultSelection = pCombo->InsertItem(&cbi);
		
		pFolder = SUCCEEDED(hResult) ? pNext : NULL;

		// move to next pidl
		pidlParseCurrent = ::ILGetNext(pidlParseCurrent);
	}

	return nDefaultSelection;
}

int CFindFrameBase::InitFolderCombo(CComboBoxEx* pCombo,
							   CImageList* pImageList,
							   const CString& strCurrentFolder,
							   bool bAddFonts,
							   bool bAddBrowseItem,
							   bool bAddFavorites,
							   bool bAddProjects,
							   bool bCurrentIsProject)
{
	int nDefaultSelection = -1;

	// create image list
	pImageList->Create(16, 16, ILC_COLOR32, 32, 32);
	pCombo->SetImageList(pImageList);

	// init a reusable combo item
	COMBOBOXEXITEM cbi;
	ZeroMemory(&cbi, sizeof(COMBOBOXEXITEM));	// zero this structure out
	cbi.mask = CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_INDENT | CBEIF_LPARAM | CBEIF_TEXT;

	try
	{
		CheckResultFunctor CheckResult;

		// get desktop folder
		CComPtr<IShellFolder> pDesktopFolder = NULL;
		CheckResult(::SHGetDesktopFolder(&pDesktopFolder));

		pidl_ptr pidlCurrentFolderFQ;
		if (!strCurrentFolder.IsEmpty() && !bCurrentIsProject)
			CheckResult(pDesktopFolder->ParseDisplayName(theApp.GetMainWnd()->GetSafeHwnd(),
														 NULL,
														 (LPOLESTR)(LPCTSTR)strCurrentFolder,
														 NULL,
														 &pidlCurrentFolderFQ,
														 NULL));

		// insert desktop folder
		SHFILEINFO sfi;
		pidl_ptr pidlDesktop;
		CheckResult(::SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlDesktop));
		if (::SHGetFileInfo((LPCTSTR)pidlDesktop.get(), 0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_ICON | SHGFI_SMALLICON))
		{
			cbi.iImage = cbi.iSelectedImage = pImageList->Add(sfi.hIcon);
			::DestroyIcon(sfi.hIcon);
		}

		// get the desktop's display name
		TCHAR szName[MAX_PATH];
		ShellUtils::GetName(pDesktopFolder, pidlDesktop.get(), SHGDN_NORMAL, szName, MAX_PATH);
		cbi.pszText = szName;

		// Make a copy.  We'll deallocate it later
		cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(pidlutils::copyitemid(pidlDesktop.get())));

		// insert desktop
		pCombo->InsertItem(&cbi);

		// pidlFQ is the current fully-qualified PIDL of the item we're working with
		pidl_ptr pidlFQ(pidlutils::copyitemid(pidlDesktop.get()));

		// enumerate (and insert) everything under the desktop
		CComPtr<IEnumIDList> pEnumDesktop;
		if (SUCCEEDED(pDesktopFolder->EnumObjects(NULL, SHCONTF_FOLDERS, &pEnumDesktop)))
		{
			// indent combo item
			++cbi.iIndent;

			// need "My Computer" item because we're going to show everything under that too
			pidl_ptr pidlMyComputer;
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_DRIVES, NULL, 0, &pidlMyComputer));

			// need Recycle Bin, Control Panel and Network because we're going to skip them
			pidl_ptr pidlRecycleBin, pidlControlPanel, pidlNetwork;
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_BITBUCKET, NULL, 0, &pidlRecycleBin));
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_CONTROLS, NULL, 0, &pidlControlPanel));
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_NETWORK, NULL, 0, &pidlNetwork));

			// points at item in the current folder item list
			// advance it past the desktop level
			LPITEMIDLIST pidlParse = pidlCurrentFolderFQ.get();

			// desktop enum loop
			LPITEMIDLIST pidl = NULL; // reusable pidl
			pidl_ptr pidlptr;
			while (pEnumDesktop->Next(1, &pidl, NULL) == S_OK)
			{
				// wrap in smart pointer to ensure it gets cleaned up
				pidlptr.reset(pidl);

				// skip recycle bin
				HRESULT hTest = pDesktopFolder->CompareIDs(0, pidl, pidlRecycleBin.get());
				if (HRESULT_CODE(hTest) == 0)
					continue;

				// skip control panel
				hTest = pDesktopFolder->CompareIDs(0, pidl, pidlControlPanel.get());
				if (HRESULT_CODE(hTest) == 0)
					continue;

				// skip network
				hTest = pDesktopFolder->CompareIDs(0, pidl, pidlNetwork.get());
				if (HRESULT_CODE(hTest) == 0)
					continue;

				// next item
				++cbi.iItem;

				// get shell icon
				if (::SHGetFileInfo((LPCTSTR)pidl, 0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_ICON | SHGFI_SMALLICON))
				{
					cbi.iImage = cbi.iSelectedImage = pImageList->Add(sfi.hIcon);
					::DestroyIcon(sfi.hIcon);
				}

				// get name
				ShellUtils::GetName(pDesktopFolder, pidl, SHGDN_NORMAL, szName, MAX_PATH);
				cbi.pszText = szName;

				// save FQ pidl in item data parameter
				cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(pidlutils::concatpidls(pidlFQ.get(), pidl)));

				// insert it
				int nLastInsert = pCombo->InsertItem(&cbi);

				// if this is the "(My) Computer" item, we want to enum everything under that too
				if (pidlutils::pidls_are_equal(pDesktopFolder, pidl, pidlMyComputer.get()))
				{
					nDefaultSelection = nLastInsert;

					// make fully-qualified pidl for My Computer
					pidl_ptr pidlFQMyComputer(pidlutils::concatpidls(pidlFQ.get(), pidl));

					// advance parsing pidl beyond My Computer if that's what we're looking for
					LPITEMIDLIST pidlParseMyComputer = NULL;
					if (pidlutils::pidls_are_equal(pDesktopFolder, pidl, pidlParse))
						pidlParseMyComputer = ::ILGetNext(pidlParse);

					++cbi.iIndent;
					int nLastIndent = cbi.iIndent;

					// we need shell folder for My Computer
					CComPtr<IShellFolder> pMyComputerFolder;
					CheckResult(pDesktopFolder->BindToObject(pidl, NULL, IID_IShellFolder, (void**)&pMyComputerFolder));

					// we need enumerator for My Computer
					CComPtr<IEnumIDList> pMyComputerEnum;
					if (SUCCEEDED(pMyComputerFolder->EnumObjects(theApp.GetMainWnd()->GetSafeHwnd(), SHCONTF_STORAGE, &pMyComputerEnum)))
					{
						LPITEMIDLIST pidlMyComputerEnum = NULL;
						while (pMyComputerEnum->Next(1, &pidlMyComputerEnum, NULL) == S_OK)
						{
							// next item
							++cbi.iItem;

							// get shell icon
							LPITEMIDLIST pidlFQItem = pidlutils::concatpidls(pidlFQMyComputer.get(), pidlMyComputerEnum);
							if (::SHGetFileInfo((LPCTSTR)pidlFQItem, 0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_ICON | SHGFI_SMALLICON))
							{
								cbi.iImage = cbi.iSelectedImage = pImageList->Add(sfi.hIcon);
								::DestroyIcon(sfi.hIcon);
							}

							// get name
							ShellUtils::GetName(pMyComputerFolder, pidlMyComputerEnum, SHGDN_NORMAL, szName, MAX_PATH);
							cbi.pszText = szName;

							// save FQ pidl
							cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(pidlFQItem));

							// insert it
							nLastInsert = pCombo->InsertItem(&cbi);

							// if current item is under "My Computer", add it
							if (pidlParseMyComputer && pidlutils::pidls_are_equal(pMyComputerFolder, pidlParseMyComputer, pidlMyComputerEnum))
							{
								nDefaultSelection = nLastInsert;

								IShellFolder* pFolder = NULL;
								CheckResult(pMyComputerFolder->BindToObject(pidlMyComputerEnum, NULL, IID_IShellFolder, (void**)&pFolder));
								LPITEMIDLIST pidlParseCurrent = ::ILGetNext(pidlParseMyComputer);

								nLastInsert =
									ShowCurrent(pFolder, pidlParseCurrent, pidlFQItem, cbi, pCombo, pImageList);

								if (nLastInsert != -1)
									nDefaultSelection = nLastInsert;

								// restore old indent value
								cbi.iIndent = nLastIndent;

								// NULL parse pidl because we're done with it
								pidlParse = NULL;
							}

							// free temp enum pidl
							::CoTaskMemFree(pidlMyComputerEnum);
						}
					}
					// back off "My Computer" indent
					--cbi.iIndent;
				}

				// if current item is elsewhere, add it
				if (pidlParse && pidlutils::pidls_are_equal(pDesktopFolder, pidlParse, pidl))
				{
					IShellFolder* pFolder = NULL;
					CheckResult(pDesktopFolder->BindToObject(pidl, NULL, IID_IShellFolder, (void**)&pFolder));
					LPITEMIDLIST pidlParseCurrent = ::ILGetNext(pidlParse);

					LPITEMIDLIST pidlFQItem = pidlutils::concatpidls(pidlDesktop.get(), pidl);

					int nLastIndent = cbi.iIndent;
					nDefaultSelection =
						ShowCurrent(pFolder, pidlParseCurrent, pidlFQItem, cbi, pCombo, pImageList);

					// restore old indent value
					cbi.iIndent = nLastIndent;
				}
			}
		}

		if (bAddFonts)
		{
			++cbi.iItem;
			cbi.iIndent = 0;

			CString strWindowsFonts;
			strWindowsFonts.LoadString(IDS_FIND_WINDOWSFONTS);
			cbi.pszText = strWindowsFonts.GetBuffer();
			strWindowsFonts.ReleaseBuffer();

			// get windows fonts location
			LPITEMIDLIST pidlFonts = NULL;
			CheckResult(::SHGetFolderLocation(NULL, CSIDL_FONTS, NULL, 0, &pidlFonts));

			HICON hIcon = theApp.LoadIcon(theApp.VistaOrLater() ? IDI_FONTFOLDER_VISTA_ICON : IDI_FONTFOLDER_ICON);
			cbi.iImage = cbi.iSelectedImage = pImageList->Add(hIcon);
			::DestroyIcon(hIcon);

			// Fonts pidl is item data
			cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(pidlFonts));

			// insert it
			pCombo->InsertItem(&cbi);
		}

		if (bAddBrowseItem)
		{
			++cbi.iItem;
			cbi.iIndent = 0;

			CString strBrowse;
			strBrowse.LoadString(IDS_FIND_BROWSE);
			cbi.pszText = strBrowse.GetBuffer();
			strBrowse.ReleaseBuffer();

			// add browse icon to image list
			HICON hIcon = theApp.LoadIcon(theApp.VistaOrLater() ? IDI_BROWSEFOLDERS_VISTA_ICON : IDI_BROWSEFOLDERS_ICON);
			cbi.iImage = cbi.iSelectedImage = pImageList->Add(hIcon);
			::DestroyIcon(hIcon);

			// add item data
			cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(NULL, FindComboData::browse_type));

			// insert it
			pCombo->InsertItem(&cbi);
		}

		if (bAddFavorites)
		{
			++cbi.iItem;
			cbi.iIndent = 0;

			CString strFavorites;
			strFavorites.LoadString(IDS_FIND_FAVORITES);
			cbi.pszText = strFavorites.GetBuffer();
			strFavorites.ReleaseBuffer();

			// add favorites icon to image list
			HICON hIcon = theApp.LoadIcon(theApp.VistaOrLater() ? IDI_FAVORITES_VISTA_ICON : IDI_FAVORITES_ICON);
			cbi.iImage = cbi.iSelectedImage = pImageList->Add(hIcon);
			::DestroyIcon(hIcon);

			// add item data
			cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(NULL, FindComboData::favorites_type));

			// insert it
			pCombo->InsertItem(&cbi);
		}

		if (bAddProjects)
		{
			++cbi.iItem;
			cbi.iIndent = 0;

			CString strProjects;
			strProjects.LoadString(IDS_FIND_PROJECTS);
			cbi.pszText = strProjects.GetBuffer();
			strProjects.ReleaseBuffer();

			// add Projects icon to image list
			HICON hIcon = theApp.LoadIcon(IDI_FONTPROJECT_ICON);
			cbi.iImage = pImageList->Add(hIcon);
			::DestroyIcon(hIcon);

			hIcon = theApp.LoadIcon(IDI_FONTPROJECT_OPEN_ICON);
			cbi.iSelectedImage = pImageList->Add(hIcon);
			::DestroyIcon(hIcon);

			// add item data
			cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(NULL, FindComboData::projects_type));

			// insert it
			pCombo->InsertItem(&cbi);

			if (bAddProjects && bCurrentIsProject)
			{
				++cbi.iItem;
				++cbi.iIndent;
				TCHAR szName[MAX_PATH];
				::StringCchCopy(szName, MAX_PATH, strCurrentFolder);
				cbi.pszText = szName;

				// add item data
				cbi.lParam = reinterpret_cast<LPARAM>(new FindComboData(szName));

				nDefaultSelection = pCombo->InsertItem(&cbi);
			}
		}

		// select current
		if (nDefaultSelection >= 0)
			pCombo->SetCurSel(nDefaultSelection);
	}
	catch (CHRESULTException&)
	{
		return -1;
	}

	// return index of selected item
	return nDefaultSelection;
}

bool CFindFrameBase::GetSearchPath(LPCITEMIDLIST pidl, LPTSTR pszPath, bool* pbMyComputer)
{
	// init to false
	if (pbMyComputer)
		*pbMyComputer = false;
	
	pidl_ptr pidlComputer;
	::SHGetFolderLocation(NULL, CSIDL_DRIVES, NULL, 0, &pidlComputer);

	// need desktop folder
	CComPtr<IShellFolder> pDesktopFolder;
	::SHGetDesktopFolder(&pDesktopFolder);

	if (pDesktopFolder->CompareIDs(0, pidl, pidlComputer.get()) == 0)
	{
		if (pszPath)
			ShellUtils::GetName(pDesktopFolder, pidl, SHGDN_FORPARSING, pszPath, MAX_PATH);

		if (pbMyComputer)
			*pbMyComputer = true;
		
		return true;
	}

	TCHAR szPath[MAX_PATH];

	if (pszPath == NULL)
		pszPath = szPath;

	BOOL bResult = ::SHGetPathFromIDList(pidl, pszPath);

	return (bResult != 0);
}

int CALLBACK CFindFrameBase::FolderBrowserCallback( HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData )
{
	switch (uMsg)
	{
		case BFFM_INITIALIZED:
			// Set the starting folder in the browser
			if (lpData)
				::SendMessage(hwnd, BFFM_SETSELECTION, (WPARAM)TRUE, lpData);
			break;

		case BFFM_SELCHANGED:
			::SendMessage(hwnd, BFFM_ENABLEOK, 0,
						  (LPARAM)GetSearchPath(reinterpret_cast<LPITEMIDLIST>(lParam)));
			break;
	}

	return 0;
}

void CFindFrameBase::LookInSelChange()
{
	int nSelection = m_wndLookInCombo.GetCurSel();

	CFindFrameBase::FindComboData* pData =
		reinterpret_cast<CFindFrameBase::FindComboData*>(m_wndLookInCombo.GetItemData(nSelection));
	
	if (pData->nType == FindComboData::browse_type)
	{
		CString strPrompt, strReturn;
		strPrompt.LoadString(IDS_FIND_LOOK_IN);

		// BROWSEINFO structure
		BROWSEINFO bi;

		// allocate memory for BROWSEINFO structure
		memset(&bi, 0, sizeof(BROWSEINFO));

		TCHAR szDisplayName[MAX_PATH];
		TCHAR szPath[MAX_PATH];

		bi.hwndOwner = m_hWnd;
		bi.pidlRoot = NULL;
		bi.pszDisplayName = szDisplayName;
		bi.lpszTitle = (LPCTSTR)strPrompt;
		bi.ulFlags = BIF_RETURNONLYFSDIRS | BIF_NEWDIALOGSTYLE | BIF_NONEWFOLDERBUTTON;
		bi.lpfn = CFindFrameBase::FolderBrowserCallback;
		bi.lParam = (LPARAM)(LPCTSTR)m_strCurrentFolder;

		LPITEMIDLIST piid = NULL;
		
		// call the shell function
		piid = ::SHBrowseForFolder(&bi);

		// re-initialize the look-in combo using the result
		if (piid && CFindFrameBase::GetSearchPath(piid, szPath))
		{
			m_strCurrentFolder = szPath;

			// delete everything
			for (int i = m_wndLookInCombo.GetCount() - 1; i >= 0; --i)
				m_wndLookInCombo.DeleteString(i);

			// image list too
			m_ilLookIn.DeleteImageList();

			// re-init the combo
			m_nLookInSelection = CFindFrameBase::InitFolderCombo(&m_wndLookInCombo, &m_ilLookIn, m_strCurrentFolder);
		}
		
		// this will select the browsed item or restore the old selection
		m_wndLookInCombo.SetCurSel(m_nLookInSelection);

		// Release the ITEMIDLIST if we got one
		if (piid)
		{
			LPMALLOC lpMalloc;
			VERIFY(::SHGetMalloc(&lpMalloc) == NOERROR);
			lpMalloc->Free(piid);
			lpMalloc->Release();
		}
	}
	else if (pData->nType == FindComboData::favorites_type)
	{
		CSelectFavoriteFoldersDialog dlg(this);
		if (dlg.DoModal() == IDOK)
		{
			m_strCurrentFolder = dlg.GetSelectedPath();

			// delete everything
			for (int i = m_wndLookInCombo.GetCount() - 1; i >= 0; --i)
				m_wndLookInCombo.DeleteString(i);

			// image list too
			m_ilLookIn.DeleteImageList();

			// re-init the combo
			m_nLookInSelection = CFindFrameBase::InitFolderCombo(&m_wndLookInCombo, &m_ilLookIn, m_strCurrentFolder);
		}

		// this will select the browsed item or restore the old selection
		m_wndLookInCombo.SetCurSel(m_nLookInSelection);
	}
	else if (pData->nType == FindComboData::projects_type)
	{
		CSelectProjectsDialog dlg(this);
		if (dlg.DoModal() == IDOK)
		{
			m_strCurrentFolder.Empty();

			// delete everything
			for (int i = m_wndLookInCombo.GetCount() - 1; i >= 0; --i)
				m_wndLookInCombo.DeleteString(i);

			// image list too
			m_ilLookIn.DeleteImageList();

			// re-init combo
			m_nLookInSelection =
				CFindFrameBase::InitFolderCombo(&m_wndLookInCombo,
												&m_ilLookIn,
												dlg.GetSelectedProject(),
												true, true, true, true, true);
		}

		// this will select the browsed item or restore the old selection
		m_wndLookInCombo.SetCurSel(m_nLookInSelection);
	}
	else
	{
		// enable search button only if a real filesystem item OR my computer was selected
		CFindFrameBase::FindComboData* pData =
			reinterpret_cast<CFindFrameBase::FindComboData*>(m_wndLookInCombo.GetItemData(nSelection));

		// enable/disable
		m_bLookInValid = CFindFrameBase::GetSearchPath(pData->pidl, NULL, &m_bLookInMyComputer);

		m_nLookInSelection = nSelection;
	}
}

unsigned int __cdecl CFindFrameBase::SearchThreadFunction(void* pParam)
{
	unsigned int nResult = 0;

	try
	{
		CheckResultFunctor CheckResult;

		// we own the search data, so wrap it in a smart pointer
		boost::scoped_ptr<SearchThreadData> pData(reinterpret_cast<SearchThreadData*>(pParam));

		// we own the controller too
		boost::scoped_ptr<CSearchControllerBase> pController(pData->pController);

		// what did we get a pointer to?

		// the desktop?
		CComPtr<IShellFolder> pDesktopFolder;
		::SHGetDesktopFolder(&pDesktopFolder);
		pidl_ptr pidlDesktop;
		CheckResult(::SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlDesktop));

		// Computer?
		pidl_ptr pidlComputer;
		CheckResult(::SHGetFolderLocation(NULL, CSIDL_DRIVES, NULL, 0, &pidlComputer));

		if (pDesktopFolder->CompareIDs(0, pData->pidlRoot, pidlDesktop.get()) == 0)
		{
			SearchFolder sf(pController.get());

			TCHAR szFolder[MAX_PATH];
			CheckResult(::SHGetFolderPath(NULL, CSIDL_DESKTOP, NULL, SHGFP_TYPE_CURRENT, szFolder));
			sf(szFolder);
		}
		else if (pDesktopFolder->CompareIDs(0, pData->pidlRoot, pidlComputer.get()) == 0)
		{
			// search all drives
			
			// get length of drive strings
			DWORD dwBufferSize = ::GetLogicalDriveStrings(0, NULL);

			// fill array with drive strings
			boost::scoped_array<TCHAR> pStrings(new TCHAR[dwBufferSize]);
			::GetLogicalDriveStrings(dwBufferSize, pStrings.get());

			// search fixed hard drives
			TCHAR* pDrive = pStrings.get();
			int nDrives = (dwBufferSize - 1) / 4; // 4 TCHARs: (drive letter, colon, backslash, NULL)
			ASSERT(((dwBufferSize - 1) % 4) == 0); // no remainder
			for (int nDrive = 0; nDrive < nDrives; ++nDrive, pDrive += 4)
			{
				UINT nDriveType = ::GetDriveType(pDrive);

				if ((nDriveType == DRIVE_FIXED) ||
					(nDriveType == DRIVE_REMOVABLE && pData->bSearchRemovableDisks))
				{
					SearchFolder sf(pController.get());
					sf(pDrive);
				}
			}
		}
		else
		{
			// this is just a regular folder, search it
			CComPtr<IShellFolder> pParent;
			LPCITEMIDLIST pidlFolder;
			CheckResult(::SHBindToParent(pData->pidlRoot, IID_IShellFolder, (void**)&pParent, &pidlFolder));

			TCHAR szFolder[MAX_PATH];
			ShellUtils::GetName(pParent, pidlFolder, SHGDN_FORPARSING, szFolder, MAX_PATH);

			SearchFolder sf(pController.get());
			sf(szFolder);
		}

		// notify window that we're done
		pData->pWnd->PostMessage(s_rwmFinished);
	}
	catch (CHRESULTException&)
	{
		nResult = 1;
	}

	return nResult;
}

unsigned int __cdecl CFindFrameBase::SearchProjectFunction(void* pParam)
{
	unsigned int nResult = 0;

	// take ownership of search data
	boost::scoped_ptr<SearchProjectData> pData(reinterpret_cast<SearchProjectData*>(pParam));

	// take ownership of the controller
	boost::scoped_ptr<CSearchControllerBase> pController(pData->pController);

	SearchProject sp(pController.get());
	sp(pData->strProjectName);

	// notify window that we're done
	pData->pWnd->PostMessage(s_rwmFinished);

	return nResult;
}

void CFindFrameBase::OnFontRunnerShutdown()
{
	m_bSearching = false;
	m_bCanceled  = true;
}