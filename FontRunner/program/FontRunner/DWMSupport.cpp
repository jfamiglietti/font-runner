/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "StdAfx.h"
#include "DWMSupport.h"

int CDWMSupport::s_nRefCount = 0;
HMODULE CDWMSupport::s_hModule = NULL;
bool CDWMSupport::s_bLibLoadAttempted = false;

void* CDWMSupport::s_pDwmExtendFrameIntoClientArea = NULL;
void* CDWMSupport::s_pDwmEnableBlurBehindWindow = NULL;
void* CDWMSupport::s_pDwmIsCompositionEnabled = NULL;

typedef HRESULT (__stdcall *DWMEXTENDFRAMEINTOCLIENTAREA_PTR)(HWND hWnd, const MARGINS* pMarInset);
typedef HRESULT (__stdcall *DWMENABLEBLURBEHINDWINDOW_PTR)(HWND hWnd, const DWM_BLURBEHIND *pBlurBehind);
typedef HRESULT (__stdcall *DWMISCOMPOSITIONENABLED_PTR)(BOOL *pfEnabled);

CDWMSupport::CDWMSupport()
{
	++s_nRefCount;

	if (s_bLibLoadAttempted)
		return;

	if (!s_hModule)
	{
		s_hModule = ::LoadLibrary(_T("dwmapi.dll"));

		if (s_hModule)
		{
			s_pDwmExtendFrameIntoClientArea = ::GetProcAddress(s_hModule, "DwmExtendFrameIntoClientArea");
			s_pDwmEnableBlurBehindWindow = ::GetProcAddress(s_hModule, "DwmEnableBlurBehindWindow");
			s_pDwmIsCompositionEnabled = ::GetProcAddress(s_hModule, "DwmIsCompositionEnabled");
		}

		s_bLibLoadAttempted = true;
	}
}

CDWMSupport::~CDWMSupport()
{
	--s_nRefCount;

	if (s_hModule && s_nRefCount == 0)
	{
		s_pDwmExtendFrameIntoClientArea = NULL;
		s_pDwmEnableBlurBehindWindow = NULL;
		s_pDwmIsCompositionEnabled = NULL;

		::FreeLibrary(s_hModule);
		s_hModule = NULL;

		s_bLibLoadAttempted = false;
	}
}

bool CDWMSupport::CheckAvailability() const
{
	return (s_hModule != NULL);
}

HRESULT CDWMSupport::ExtendFrameIntoClientArea(HWND hWnd, const MARGINS *pMarInset)
{
	if (!s_pDwmExtendFrameIntoClientArea)
		return E_FAIL;

	return ((DWMEXTENDFRAMEINTOCLIENTAREA_PTR)s_pDwmExtendFrameIntoClientArea)(hWnd, pMarInset);
}

HRESULT CDWMSupport::EnableBlurBehindWindow(HWND hWnd, const DWM_BLURBEHIND *pBlurBehind)
{
	if (!s_pDwmEnableBlurBehindWindow)
		return E_FAIL;

	return ((DWMENABLEBLURBEHINDWINDOW_PTR)s_pDwmEnableBlurBehindWindow)(hWnd, pBlurBehind);
}

bool CDWMSupport::IsCompositionEnabled() const
{
	if (!s_pDwmEnableBlurBehindWindow)
		return false;

	BOOL bComposition = FALSE;
	HRESULT hResult = ((DWMISCOMPOSITIONENABLED_PTR)s_pDwmIsCompositionEnabled)(&bComposition);

	return (SUCCEEDED(hResult) && bComposition);
}
