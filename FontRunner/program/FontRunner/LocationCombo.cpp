/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// LocationCombo.cpp : implementation file
//

#include "stdafx.h"
#include "LocationCombo.h"
#include "FontRunner.h"
#include "FontRunnerSignals.h"
#include "ShellUtils.h"
#include "pidlutils.h"
#include "HRESULTException.h"
#include "Filelist.h"
#include "FontProjects.h"
#include "ProgramOptions.h"

#include <shlwapi.h>
#include <strsafe.h>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

struct combo_itemdata
{
	combo_itemdata() : m_bProject(false)
	{}

	combo_itemdata(bool bProject, const wchar_t* pszName)
		: m_bProject(bProject), m_strName(pszName)
	{}

	bool m_bProject;
	std::wstring m_strName;
};

typedef boost::shared_ptr<combo_itemdata> itemdataptr_type;
typedef std::vector<itemdataptr_type> itemdatalist_type;

struct CLocationCombo::_impl
{
	itemdatalist_type itemdatalist;
};

// CLocationCombo

IMPLEMENT_DYNAMIC(CLocationCombo, CComboBoxEx)

CLocationCombo::CLocationCombo()
  : m_nProjectIconIndex(-1),
	m_nFolderIconIndex(-1),
	m_pImpl(new _impl)
{

}

CLocationCombo::~CLocationCombo()
{
}

int CLocationCombo::AddPath(LPCTSTR szPath, bool bAddToTop)
{
	// combo item
	COMBOBOXEXITEM cbi;
	ZeroMemory(&cbi, sizeof(COMBOBOXEXITEM));	// zero this structure out
	cbi.mask = CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_LPARAM | CBEIF_TEXT;

	cbi.iItem = bAddToTop ? 0 : GetCount();

	// get icon from shell
	SHFILEINFO sfi;
	int nImage = m_nFolderIconIndex;
	if (::SHGetFileInfo(szPath, 0, &sfi, sizeof(SHFILEINFO), SHGFI_ICON | SHGFI_SMALLICON))
	{
		nImage = m_iml.Add(sfi.hIcon);
		::DestroyIcon(sfi.hIcon);
	}

	cbi.iImage = cbi.iSelectedImage = nImage;

	// copy path into name buffer
	TCHAR szName[MAX_PATH];
	StringCchCopy(szName, MAX_PATH, szPath);

	// make itemdata
	itemdataptr_type ptr(new combo_itemdata(false, szName));
	cbi.lParam = (LPARAM)ptr.get();

	// get a better display name if this isn't a real path
	if (!::PathIsDirectory(szPath))
	{
		try
		{
			// function to check HRESULTs
			CheckResultFunctor CheckResult;

			CComPtr<IShellFolder> pDesktop;
			CheckResult(::SHGetDesktopFolder(&pDesktop));

			// need a pidl
			pidl_ptr pidl;
			CheckResult(pDesktop->ParseDisplayName(NULL, NULL, szName, NULL, &pidl, NULL), false);

			// get display name
			ShellUtils::GetName(pDesktop, pidl.get(), SHGDN_INFOLDER, szName, MAX_PATH);
		}
		catch (CHRESULTException&)
		{
			return -1;
		}
	}

	// set text
	cbi.pszText = szName;

	// add to combo
	int nNewItem = InsertItem(&cbi);
	if (nNewItem != -1)
	{
		// save item data
		m_pImpl->itemdatalist.push_back(ptr);
	}

	return nNewItem;
}

int CLocationCombo::FindPath(LPCTSTR szPath)
{
	// nothing to do if no items in combo
	int nCount = GetCount();
	if (nCount == 0)
		return -1;

	try
	{
		CheckResultFunctor CheckResult;

		// get fully-qualified PIDL of incoming path
		CComPtr<IShellFolder> pDesktop;
		CheckResult(::SHGetDesktopFolder(&pDesktop));

		pidl_ptr pidlIncoming;
		TCHAR szCompare[MAX_PATH];
		CheckResult(::StringCchCopy(szCompare, MAX_PATH, szPath));
		CheckResult(pDesktop->ParseDisplayName(NULL, NULL, szCompare, NULL, &pidlIncoming, NULL));

		for (int n = 0; n < nCount; ++n)
		{
			combo_itemdata* pItemData = reinterpret_cast<combo_itemdata*>(GetItemData(n));
			if (!pItemData->m_bProject && ::PathFileExists(pItemData->m_strName.c_str()))
			{
				pidl_ptr pidlCompare;
				CheckResult(::StringCchCopy(szCompare, MAX_PATH, pItemData->m_strName.c_str()));
				CheckResult(pDesktop->ParseDisplayName(NULL, NULL, szCompare, NULL, &pidlCompare, NULL));

				if (::ILIsEqual(pidlIncoming.get(), pidlCompare.get()))
					return n;
			}
		}
	}
	catch (CHRESULTException&)
	{
	}

	return -1;
}

void CLocationCombo::ClearLocationHistory()
{
	// delete all combo items
	ResetContent();

	// dump item data
	m_pImpl->itemdatalist.clear();

	// delete history file
	CString strHistoryFile = theApp.GetAppDataFilename(IDS_OPTIONSDIALOG_LOCATIONFILE, false);
	if (::PathFileExists(strHistoryFile))
		::DeleteFile(strHistoryFile);
}

struct find_itemdata
{
	find_itemdata(combo_itemdata* ptr) : m_ptr(ptr)
	{}

	bool operator()(const itemdatalist_type::value_type& item) const
	{
		return (item.get() == m_ptr);
	}

private:
	combo_itemdata* m_ptr;
};

void CLocationCombo::ApplyLimit()
{
	if (!theApp.GetProgramOptions()->GetLimitLocations())
		return;

	int nLimit = theApp.GetProgramOptions()->GetLocationsLimit();
	while (GetCount() > nLimit)
	{
		// grab item data before deleting string
		combo_itemdata* pItemData = (combo_itemdata*)GetItemData(GetCount() - 1);
		DeleteString(GetCount() - 1);

		// find item data on list
		itemdatalist_type& items = m_pImpl->itemdatalist;
		itemdatalist_type::iterator it =
			std::find_if(items.begin(), items.end(), find_itemdata(pItemData));

		if (it != items.end())
			items.erase(it);
	}
}

int CLocationCombo::AddProject(const std::wstring& strName, bool bAddToTop)
{
	// combo item
	COMBOBOXEXITEM cbi;
	ZeroMemory(&cbi, sizeof(COMBOBOXEXITEM));	// zero this structure out
	cbi.mask = CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_LPARAM | CBEIF_TEXT;
	cbi.iItem = bAddToTop ? 0 : GetCount();
	cbi.iImage = cbi.iSelectedImage = m_nProjectIconIndex;

	// set text
	TCHAR szName[MAX_PATH];
	StringCchCopy(szName, MAX_PATH, strName.c_str());
	cbi.pszText = szName;

	// make itemdata
	combo_itemdata* pItemData = new combo_itemdata(true, szName);
	cbi.lParam = (LPARAM)pItemData;

	// add to combo
	int nNewItem = InsertItem(&cbi);
	if (nNewItem != -1)
	{
		// save item data
		itemdataptr_type ptr(pItemData);
		m_pImpl->itemdatalist.push_back(ptr);
	}

	return nNewItem;
}

int CLocationCombo::FindProject(const std::wstring& strName)
{
	// nothing to do if no items in combo
	int nCount = GetCount();
	if (nCount == 0)
		return -1;

	for (int n = 0; n < nCount; ++n)
	{
		combo_itemdata* pItemData = reinterpret_cast<combo_itemdata*>(GetItemData(n));
		
		if (pItemData->m_bProject && strName.compare(pItemData->m_strName) == 0)
			return n;
	}

	return -1;
}

void CLocationCombo::Cleanup()
{
	// nothing to do if no items in combo
	int nCount = GetCount();
	if (nCount == 0)
		return;

	CFontProjects* pProjects = theApp.GetFontProjects();

	int n = 0;
	while (n < nCount)
	{
		combo_itemdata* pItemData = reinterpret_cast<combo_itemdata*>(GetItemData(n));
		
		bool bDeleted = false;
		if ((pItemData->m_bProject && pProjects->Find(pItemData->m_strName.c_str()) == pProjects->GetListEnd()) ||
			(!pItemData->m_bProject && !::PathFileExists(pItemData->m_strName.c_str())))
		{
			DeleteItem(n);
			bDeleted = true;
			--nCount;
		}
		else
			++n;
	}
}

void CLocationCombo::MoveToTop(int nIndex)
{
	// found, so move it to the top
	TCHAR szText[MAX_PATH];
	COMBOBOXEXITEM cbi;
	ZeroMemory(&cbi, sizeof(COMBOBOXEXITEM));	// zero this structure out
	cbi.mask = CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_LPARAM | CBEIF_TEXT;
	cbi.iItem = nIndex;
	cbi.pszText = szText;
	cbi.cchTextMax = MAX_PATH;
	GetItem(&cbi);

	// delete it
	DeleteItem(nIndex);

	// re-insert it at the top
	cbi.iItem = 0;
	InsertItem(&cbi);
}

CComPtr<IXMLDOMNode> FindLocationsNode(CComPtr<IXMLDOMDocument>& pXMLDoc)
{
	// get the document element (this is the shoot data)
	CComPtr<IXMLDOMNodeList> pDocNodeList = NULL;
	pXMLDoc->get_childNodes(&pDocNodeList);

	// look for the locations node
	long nLength = 0;
	pDocNodeList->get_length(&nLength);
	for (long nDocNodeIndex = 0; nDocNodeIndex < nLength; nDocNodeIndex++)
	{
		CComPtr<IXMLDOMNode> pTest;
		pDocNodeList->nextNode(&pTest);

		if (pTest)
		{
			CComBSTR bstrName;
			pTest->get_nodeName(&bstrName);

			if (bstrName == "locations")
				return pTest;
		}
	}

	return NULL;
}

void CLocationCombo::LoadLocationHistory()
{
	// if file exists, load it
	CString strHistoryFile = theApp.GetAppDataFilename(IDS_OPTIONSDIALOG_LOCATIONFILE, false);
	if (!::PathFileExists(strHistoryFile))
		return;
	
	// function to check HRESULTs
	CheckResultFunctor CheckResult;

	try
	{
		itemdatalist_type& items = m_pImpl->itemdatalist;
		items.clear();

		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		VARIANT_BOOL bSuccess = FALSE;
		CComVariant varFileName(strHistoryFile);

		pXMLDoc->load(varFileName, &bSuccess);

		if (bSuccess)
		{
			CComPtr<IXMLDOMNode> pLocationsNode = FindLocationsNode(pXMLDoc);

			// not valid file if node isn't found
			if (!pLocationsNode)
				return;

			// iterate through child nodes
			CComPtr<IXMLDOMNodeList> pLocations = NULL;
			pLocationsNode->get_childNodes(&pLocations);

			long nLength = 0;
			CheckResult(pLocations->get_length(&nLength));
			for (int nLocationIndex = 0; nLocationIndex < nLength; ++nLocationIndex)
			{
				CComPtr<IXMLDOMNode> pLocation;
				pLocations->nextNode(&pLocation);

				DOMNodeType nType;
				pLocation->get_nodeType(&nType);

				if (nType == NODE_ELEMENT)
				{
					// make sure this is a project element
					CComBSTR bstrName;
					pLocation->get_nodeName(&bstrName);

					if (bstrName != "location")
						continue;

					CComPtr<IXMLDOMNamedNodeMap> pNodeMap = NULL;
					pLocation->get_attributes(&pNodeMap);

					bool bProject = false;

					// get project attribute
					CComPtr<IXMLDOMNode> pProjectNode = NULL;
					pNodeMap->getNamedItem(CComBSTR("project"), &pProjectNode);

					// if no project, assume directory
					if (pProjectNode)
					{
						pProjectNode->get_text(&bstrName);
						bProject = (bstrName == "true");
					}

					// get name attribute
					CComPtr<IXMLDOMNode> pNameNode = NULL;
					pNodeMap->getNamedItem(CComBSTR("name"), &pNameNode);

					// if no name, we cannot continue
					if (pNameNode)
					{
						pNameNode->get_text(&bstrName);
						CString strName(bstrName);
						
						if (bProject)
						{
							std::wstring str(strName);
							AddProject(str, false);
						}
						else
							AddPath(strName, false);
					}
				}
			}

			// apply any history limits
			ApplyLimit();
		}
	}
	catch (CHRESULTException&)
	{
		return;
	}
}

struct AddLocation
{
	AddLocation(IXMLDOMDocument* pXMLDoc, IXMLDOMNode* pParent)
		: m_pXMLDoc(pXMLDoc), m_pParent(pParent)
	{}

	void operator()(const combo_itemdata* value) const
	{
		// create element
		CComPtr<IXMLDOMElement> pElement = NULL;
		CheckResult(m_pXMLDoc->createElement(CComBSTR("location"), &pElement));

		// create attribute
		CComPtr<IXMLDOMAttribute> pProjectAttribute = NULL;
		CheckResult(m_pXMLDoc->createAttribute(CComBSTR("project"), &pProjectAttribute));

		CheckResult(pProjectAttribute->put_nodeTypedValue(CComVariant(value->m_bProject ? _T("true") : _T("false"))));

		// add to attributes
		CComPtr<IXMLDOMNamedNodeMap> pNodeMap = NULL;
		CheckResult(pElement->get_attributes(&pNodeMap));

		IXMLDOMNode* pNewChild = NULL; // ATL bug prevents this from being a smart pointer
		CheckResult(pNodeMap->setNamedItem(pProjectAttribute, &pNewChild));
		CheckResult(m_pParent->appendChild(pElement, &pNewChild));
		
		pNewChild->Release();
		pNewChild = NULL;

		// create attribute
		CComPtr<IXMLDOMAttribute> pNameAttribute = NULL;
		CheckResult(m_pXMLDoc->createAttribute(CComBSTR("name"), &pNameAttribute));

		CheckResult(pNameAttribute->put_nodeTypedValue(CComVariant(value->m_strName.c_str())));

		CheckResult(pNodeMap->setNamedItem(pNameAttribute, &pNewChild));
		CheckResult(m_pParent->appendChild(pElement, &pNewChild));
		
		pNewChild->Release();
	}

private:
	IXMLDOMDocument* m_pXMLDoc;
	IXMLDOMNode* m_pParent;
	CheckResultFunctor CheckResult;
};

void CLocationCombo::SaveLocationHistory() const
{
	// get name of XML file
	CString strHistoryFile = theApp.GetAppDataFilename(IDS_OPTIONSDIALOG_LOCATIONFILE);

	CheckResultFunctor CheckResult;

	try
	{
		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		CComPtr<IXMLDOMProcessingInstruction> pPI = NULL;
		CheckResult(pXMLDoc->createProcessingInstruction(CComBSTR("xml"), CComBSTR("version=\"1.0\""), &pPI));

		// make a null variant
		CComVariant varNull;
		varNull.vt = VT_NULL;

		// make a temp node
		CComPtr<IXMLDOMNode> pTemp = NULL;

		// insert it
		CheckResult(pXMLDoc->insertBefore(pPI, varNull, &pTemp));

		// create a root element for locations
		CComVariant varElementNodeType(NODE_ELEMENT);
		CComPtr<IXMLDOMNode> pLocationsNode = NULL, pTempRoot = NULL;
		CheckResult(pXMLDoc->createNode(varElementNodeType, CComBSTR("locations"), CComBSTR(""), &pTempRoot));

		// stick it in the document
		CheckResult(pXMLDoc->appendChild(pTempRoot, &pLocationsNode));

		// add locations to locations element
		AddLocation addtoxml(pXMLDoc, pLocationsNode);
		for (int n = 0; n < GetCount(); ++n)
		{
			combo_itemdata* pItemData = reinterpret_cast<combo_itemdata*>(GetItemData(n));
			addtoxml(pItemData);
		}

		// finally, save document to file
		CheckResult(pXMLDoc->save(CComVariant(strHistoryFile)));
	}
	catch (CHRESULTException&)
	{
		return;
	}
}


BEGIN_MESSAGE_MAP(CLocationCombo, CComboBoxEx)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_WM_DROPFILES()
	ON_CONTROL_REFLECT(CBN_SELCHANGE, OnSelChange)
	ON_CONTROL_REFLECT(CBN_DROPDOWN, OnDropDown)
END_MESSAGE_MAP()



// CLocationCombo message handlers
int CLocationCombo::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CComboBoxEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// make imagelist
	m_iml.Create(16, 16, ILC_COLOR32, 2, 256);

	// add project icon
	HICON hIcon = theApp.LoadIcon(IDI_FONTPROJECT_ICON);
	m_nProjectIconIndex = m_iml.Add(hIcon);
	::DestroyIcon(hIcon);

	// add folder icon
	hIcon = theApp.LoadIcon(IDI_FOLDER_ICON);
	m_nFolderIconIndex = m_iml.Add(hIcon);
	::DestroyIcon(hIcon);

	// set imagelist
	SetImageList(&m_iml);

	// load history
	LoadLocationHistory();

	DragAcceptFiles(true);

	CFontRunnerSignals* pSignals = theApp.GetSignals();

	pSignals->ConnectTo_OptionsChange(boost::bind(&CLocationCombo::OnOptionsChange, this));
	pSignals->ConnectTo_ClearLocationBar(boost::bind(&CLocationCombo::OnClearLocationBar, this));
	pSignals->ConnectTo_SelectedFolderChange(boost::bind(&CLocationCombo::OnSelectedFolderChange, this, _1, _2));
	pSignals->ConnectTo_SelectedProjectChange(boost::bind(&CLocationCombo::OnSelectedProjectChange, this, _1));

	return 0;
}

void CLocationCombo::OnDestroy()
{
	if (theApp.GetProgramOptions()->GetClearLocationsOnExit())
		ClearLocationHistory();
	else
		SaveLocationHistory();
}

void CLocationCombo::OnDropFiles(HDROP hDrop)
{
	theApp.SwitchToDroppedItem(hDrop);
}

BOOL CLocationCombo::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		// get text from combo
		CString strText;
		GetWindowText(strText);

		if (::PathIsDirectory(strText))
			theApp.GetSignals()->Fire_SelectedFolderChange(strText);
		else
		{
			projectlist_type::const_iterator itFound = theApp.GetFontProjects()->Find(strText);
			if (itFound != theApp.GetFontProjects()->GetListEnd())
			{
				std::wstring wstrText(strText);
				theApp.GetSignals()->Fire_SelectedProjectChange(wstrText);
			}
		}

		return TRUE;
	}

	return CComboBoxEx::PreTranslateMessage(pMsg);
}

void CLocationCombo::OnOptionsChange()
{
	ApplyLimit();
}

void CLocationCombo::OnClearLocationBar()
{
	// hang onto current item
	combo_itemdata* pItemData = (combo_itemdata*)GetItemData(0);

	bool bProject = pItemData->m_bProject;

	TCHAR szPath[MAX_PATH];
	::StringCchCopy(szPath, MAX_PATH, pItemData->m_strName.c_str());

	// clear it
	ClearLocationHistory();

	// add current item back
	if (bProject)
	{
		std::wstring wPath(szPath);
		AddProject(wPath);
	}
	else
		AddPath(szPath);

	SetCurSel(0);
}

void CLocationCombo::OnSelectedFolderChange(const CString& strFolder, const CString* /*pstrFile*/)
{
	// do we already have this item on our list?
	int nItem = FindPath(strFolder);

	if (nItem != -1)
	{
		if (nItem != 0)
		{
			MoveToTop(nItem);
			nItem = 0;
		}
	}
	else
	{
		// not found, so add it
		nItem = AddPath(strFolder);

		// apply any history limits
		ApplyLimit();
	}

	SetCurSel(nItem);
}

void CLocationCombo::OnSelectedProjectChange(const std::wstring& strProject)
{
	// do we already have this item on our list?
	int nItem = FindProject(strProject);

	if (nItem != -1)
	{
		if (nItem != 0)
		{
			MoveToTop(nItem);
			nItem = 0;
		}
	}
	else
	{
		// not found, so add it
		nItem = AddProject(strProject);

		// apply any history limits
		ApplyLimit();
	}

	SetCurSel(nItem);
}

void CLocationCombo::OnSelChange()
{
	int nItem = GetCurSel();
	if (nItem > 0)
	{
		combo_itemdata* pItemData = reinterpret_cast<combo_itemdata*>(GetItemData(nItem));

		if (pItemData->m_bProject)
			theApp.GetSignals()->Fire_SelectedProjectChange(pItemData->m_strName);
		else
			theApp.GetSignals()->Fire_SelectedFolderChange(pItemData->m_strName.c_str());
	}
}

void CLocationCombo::OnDropDown()
{
	Cleanup();
}
