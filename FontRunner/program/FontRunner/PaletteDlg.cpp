/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// PaletteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerSignals.h"
#include "PaletteDlg.h"
#include "MainFrm.h"
#include "WinToolbox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPaletteDlg dialog

IMPLEMENT_DYNAMIC(CPaletteDlg, CDialog)
CPaletteDlg::CPaletteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPaletteDlg::IDD, pParent), m_strRedStatic(_T(""))
	, m_strColorStatic(_T(""))
	, m_strGreenStatic(_T(""))
	, m_strBlueStatic(_T(""))
	, m_nRedEdit(0)
	, m_nGreenEdit(0)
	, m_nBlueEdit(0)
	, m_strHTMLEdit(_T(""))
	, m_bForeground(true)
{
}

CPaletteDlg::~CPaletteDlg()
{
}

void CPaletteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RED_EDIT, m_ctrlRedEdit);
	DDX_Control(pDX, IDC_GREEN_EDIT, m_ctrlGreenEdit);
	DDX_Control(pDX, IDC_BLUE_EDIT, m_ctrlBlueEdit);
	DDX_Text(pDX, IDC_RED_STATIC, m_strRedStatic);
	DDX_Text(pDX, IDC_GREEN_STATIC, m_strGreenStatic);
	DDX_Text(pDX, IDC_BLUE_STATIC, m_strBlueStatic);
	DDX_Text(pDX, IDC_COLOR_STATIC, m_strColorStatic);
	DDX_Text(pDX, IDC_RED_EDIT, m_nRedEdit);
	DDX_Text(pDX, IDC_GREEN_EDIT, m_nGreenEdit);
	DDX_Text(pDX, IDC_BLUE_EDIT, m_nBlueEdit);
	DDX_Text(pDX, IDC_HTML_EDIT, m_strHTMLEdit);
	DDX_Control(pDX, IDC_PALETTECTRL, m_ctrlPalette);
}

void CPaletteDlg::PostNcDestroy()
{
	delete this;
}

void CPaletteDlg::InitPaletteDlg(CString strTitleText, COLORREF clrCurrent)
{
	m_ctrlPalette.put_SelectedColor(clrCurrent);

	// update controls
	m_strColorStatic = strTitleText;
	m_nRedEdit = GetRValue(clrCurrent);
	m_nGreenEdit = GetGValue(clrCurrent);
	m_nBlueEdit = GetBValue(clrCurrent);
	m_strHTMLEdit.Format(_T("#%02X%02X%02X"), m_nRedEdit, m_nGreenEdit, m_nBlueEdit);

	UpdateData(FALSE);
}

void CPaletteDlg::FixColorEdit(CEdit* pEdit)
{
	// get new text
	CString strNew;
	pEdit->GetWindowText(strNew);

	// make sure it is not blank
	if (strNew.IsEmpty())
	{
		pEdit->SetWindowText(_T("0"));
		pEdit->SetSel(0, -1);
		return;
	}

	// make sure it is not out of range
	if (_tstoi(strNew) > 255)
	{
		pEdit->SetWindowText(_T("255"));
		return;
	}
}

BEGIN_MESSAGE_MAP(CPaletteDlg, CDialog)
	ON_EN_CHANGE(IDC_RED_EDIT, OnEnChangeColorEdit)
	ON_EN_CHANGE(IDC_GREEN_EDIT, OnEnChangeColorEdit)
	ON_EN_CHANGE(IDC_BLUE_EDIT, OnEnChangeColorEdit)
	ON_EN_UPDATE(IDC_RED_EDIT, OnEnUpdateRedEdit)
	ON_EN_UPDATE(IDC_GREEN_EDIT, OnEnUpdateGreenEdit)
	ON_EN_UPDATE(IDC_BLUE_EDIT, OnEnUpdateBlueEdit)
END_MESSAGE_MAP()

BEGIN_EVENTSINK_MAP(CPaletteDlg, CDialog)
	ON_EVENT(CPaletteDlg, IDC_PALETTECTRL, 1, ColorChangePalettectrl, VTS_UI4)
	ON_EVENT(CPaletteDlg, IDC_PALETTECTRL, 2, ColorOverPalettectrl, VTS_UI4)
END_EVENTSINK_MAP()


// CPaletteDlg message handlers
BOOL CPaletteDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// show dialog
	ShowWindow(SW_SHOW);

	// change groupbox font
	CWnd* pGroupBox = GetDlgItem(IDC_COLOR_STATIC);

	// get old font and make it bold
	LOGFONT lf;
	CFont* pFont = pGroupBox->GetFont();
	pFont->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	m_fontBold.CreateFontIndirect(&lf);

	// reset font
	pGroupBox->SetFont(&m_fontBold);

	// limit edit boxes to 3 chars
	m_ctrlRedEdit.LimitText(3);
	m_ctrlGreenEdit.LimitText(3);
	m_ctrlBlueEdit.LimitText(3);

	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CPaletteDlg::OnOK()
{
	// don't close this dialog!
	// just cause focus to shift to preview window
	CMainFrame* pFrame = (CMainFrame*)AfxGetApp()->GetMainWnd();
	pFrame->GetFontViewSplit()->GetPane(1,0)->SetFocus();
}

void CPaletteDlg::OnCancel()
{
	// don't close this dialog!
	// just cause focus to shift to preview window
	CMainFrame* pFrame = (CMainFrame*)AfxGetApp()->GetMainWnd();
	pFrame->GetFontViewSplit()->GetPane(1,0)->SetFocus();
}

void CPaletteDlg::OnEnChangeColorEdit()
{
	UpdateData(TRUE);

	// make color
	COLORREF clrNew = RGB(m_nRedEdit, m_nGreenEdit, m_nBlueEdit);
	m_strHTMLEdit.Format(_T("#%02X%02X%02X"), m_nRedEdit, m_nGreenEdit, m_nBlueEdit);

	// update palette static
	m_ctrlPalette.put_SelectedColor((ULONG)clrNew);

	// forward this message to creator
	theApp.GetSignals()->Fire_DetailColorChange(clrNew, m_bForeground);
}

void CPaletteDlg::OnEnUpdateRedEdit()
{
	FixColorEdit(&m_ctrlRedEdit);
}

void CPaletteDlg::OnEnUpdateGreenEdit()
{
	FixColorEdit(&m_ctrlGreenEdit);
}

void CPaletteDlg::OnEnUpdateBlueEdit()
{
	FixColorEdit(&m_ctrlBlueEdit);
}

void CPaletteDlg::ColorChangePalettectrl(unsigned long NewColor)
{
	// get new color
	m_nRedEdit = GetRValue(NewColor);
	m_nGreenEdit = GetGValue(NewColor);
	m_nBlueEdit = GetBValue(NewColor);
	m_strHTMLEdit.Format(_T("#%02X%02X%02X"), m_nRedEdit, m_nGreenEdit, m_nBlueEdit);

	UpdateData(FALSE);

	// forward this message to creator
	theApp.GetSignals()->Fire_DetailColorChange(NewColor, m_bForeground);
}

void CPaletteDlg::ColorOverPalettectrl(unsigned long OverColor)
{
	m_strRedStatic.Format(_T("%d"), GetRValue(OverColor));
	m_strGreenStatic.Format(_T("%d"), GetGValue(OverColor));
	m_strBlueStatic.Format(_T("%d"), GetBValue(OverColor));

	// update controls
	UpdateData(FALSE);
}

void CPaletteDlg::SetForeground(bool bForeground)
{
	m_bForeground = bForeground;
}

bool CPaletteDlg::IsForeground() const
{
	return m_bForeground;
}
