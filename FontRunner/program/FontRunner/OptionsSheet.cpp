/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// OptionsSheet.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "OptionsSheet.h"

#include "OptionsPageGeneral.h"
#include "OptionsPageFontRunner.h"
#include "OptionsPagePrinting.h"
#include "OptionsPageFontViewing.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COptionsSheet

IMPLEMENT_DYNAMIC(COptionsSheet, CPropertySheet)
COptionsSheet::COptionsSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
}

COptionsSheet::COptionsSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
}

COptionsSheet::~COptionsSheet()
{
}

INT_PTR COptionsSheet::DoModal()
{
	// create property sheets
	m_pGeneralPage.reset(new COptionsPageGeneral());

	m_pPrintingPage.reset(new COptionsPagePrinting());
	m_pFontViewingPage.reset(new COptionsPageFontViewing());

	AddPage(m_pGeneralPage.get());
	AddPage(m_pPrintingPage.get());
	AddPage(m_pFontViewingPage.get());

	// add help button to sheet
	m_psh.dwFlags |= PSH_HASHELP;

	return CPropertySheet::DoModal();
}


BEGIN_MESSAGE_MAP(COptionsSheet, CPropertySheet)
END_MESSAGE_MAP()


// COptionsSheet message handlers
