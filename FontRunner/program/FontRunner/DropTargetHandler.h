/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/signals2.hpp>
#include <boost/bind.hpp>

// define signals for the IDropTarget interface functions
typedef boost::signals2::signal<void (IDataObject*, DWORD, POINTL, DWORD*)> DragEnterSignalType;
typedef boost::signals2::signal<void (DWORD, POINTL, DWORD*)> DragOverSignalType;
typedef boost::signals2::signal<void ()> DragLeaveSignalType;
typedef boost::signals2::signal<void (IDataObject*, DWORD, POINTL, DWORD*)> DropSignalType;

class CDropTargetHandler : public IDropTarget
{
public:
	CDropTargetHandler();
	virtual ~CDropTargetHandler();

public:
	//
    // IUnknown members
	//
    HRESULT __stdcall QueryInterface(REFIID iid, void ** ppvObject);
    ULONG __stdcall AddRef();
    ULONG __stdcall Release();

	//
	// IDropTarget members
	//
	HRESULT __stdcall DragEnter(IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect);
    HRESULT __stdcall DragOver(DWORD grfKeyState, POINTL pt, DWORD *pdwEffect);
    HRESULT __stdcall DragLeave();
    HRESULT __stdcall Drop(IDataObject *pDataObj, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect);

	// for connecting to this class
public:
	void Connect(const DragEnterSignalType::slot_type& dragenter,
				 const DragOverSignalType::slot_type& dragover,
				 const DragLeaveSignalType::slot_type& dragleave,
				 const DropSignalType::slot_type& drop);

private:
    LONG m_nRefCount;
	DragEnterSignalType		m_sigDragEnter;
	DragOverSignalType		m_sigDragOver;
	DragLeaveSignalType		m_sigDragLeave;
	DropSignalType			m_sigDrop;
};
