/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FindNameFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "FindNameFrameWnd.h"

#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "ProgramOptions.h"
#include "PrettyToolbar.h"
#include "HRESULTException.h"
#include "pidlutils.h"
#include "ShellUtils.h"
#include "FontManager.h"
#include "FontItem.h"
#include "WinToolbox.h"

#include <strsafe.h>
#include <boost/regex.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFindNameFrameWnd

IMPLEMENT_DYNCREATE(CFindNameFrameWnd, CFindFrameBase)

CFindNameFrameWnd::CFindNameFrameWnd(LPCTSTR lpszSearchRoot, bool bProject)
  : CFindFrameBase(lpszSearchRoot, bProject),
	m_bSearchSubFolders(theApp.GetProgramOptions()->SearchSubFolders()),
	m_bSearchRemovableDisks(theApp.GetProgramOptions()->SearchRemovableDisks()),
	m_bMatchCase(theApp.GetProgramOptions()->SearchMatchCase()),
	m_bMatchWord(theApp.GetProgramOptions()->SearchMatchWord()),
	m_bUseRegex(theApp.GetProgramOptions()->SearchUsingRegex())
{
}

CFindNameFrameWnd::~CFindNameFrameWnd()
{
}

bool CFindNameFrameWnd::Create(CWnd* pParentWnd)
{
	return CFindFrameBase::Create(pParentWnd, IDS_FINDNAME_TITLE);
}

CFontListView::eMode CFindNameFrameWnd::GetFontListViewMode() const
{
	return CFontListView::kMode_FindName;
}

bool CFindNameFrameWnd::Searching() const
{
	return m_bSearching;
}

void CFindNameFrameWnd::EnteringFolder(LPCTSTR szPath)
{
	TCHAR* pPath = NULL;
	size_t nSize = 0;

	// reallocate string on heap for trip across thread boundary
	if (SUCCEEDED(::StringCchLength(szPath, MAX_PATH, &nSize)))
	{
		++nSize;
		pPath = new TCHAR[nSize];
		::StringCchCopy(pPath, nSize, szPath);
	}

	PostMessage(s_rwmEnteringFolder, reinterpret_cast<WPARAM>(pPath));
}

void CFindNameFrameWnd::Found(CFontItem *pItem)
{
	if (pItem && pItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
	{
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pString =
			pItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

		// a CString makes lowercase conversion easy
		CString strTargetTemp(pString->c_str());

		// convert to lowercase
		if (!m_bMatchCase)
			strTargetTemp.MakeLower();

		// convert target to ANSI
		int nSize = ::WideCharToMultiByte(CP_ACP, 0, strTargetTemp, -1, NULL, 0, NULL, NULL);
		boost::scoped_array<char> pTarget(new char[nSize]);
		::WideCharToMultiByte(CP_ACP, 0, strTargetTemp, -1, pTarget.get(), nSize, NULL, NULL);
		std::string strTarget(pTarget.get());

		// do regex search
		boost::smatch match;
		boost::regex expression(m_strExpression);
		
		if (boost::regex_search(strTarget, match, expression))
		{
			// add temporary reference to ensure item still exists when this
			// message is eventually handled
			theApp.GetFontManager()->AddReference(pItem);
			PostMessage(s_rwmFontFound, reinterpret_cast<WPARAM>(pItem));
		}
	}
}

BOOL CFindNameFrameWnd::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		OnSearchButton();
		return true;
	}

	return false;
}


BEGIN_MESSAGE_MAP(CFindNameFrameWnd, CFindFrameBase)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_FIND_LOOKIN_COMBO, OnLookInSelChange)
	ON_NOTIFY(CBEN_DELETEITEM, IDC_FIND_LOOKIN_COMBO, &CFindNameFrameWnd::OnLookInDeleteItem)
	ON_NOTIFY(TBN_DROPDOWN, AFX_IDW_TOOLBAR, OnToolbarDropDown)
	ON_COMMAND(ID_FINDOPTIONS_SUBFOLDERS, OnOptionsSearchSubFolders)
	ON_COMMAND(ID_FINDOPTIONS_MATCHCASE, OnOptionsSearchMatchCase)
	ON_COMMAND(ID_FINDOPTIONS_MATCHWORD, OnOptionsSearchMatchWord)
	ON_COMMAND(ID_FINDOPTIONS_USEREGEX, OnOptionsUseRegex)
	ON_COMMAND(ID_FINDOPTIONS_REMOVABLE, OnOptionsSearchRemovableDisks)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_BUTTON, OnUpdateOptions)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_SUBFOLDERS, OnUpdateOptionsSearchSubFolders)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_MATCHCASE, OnUpdateOptionsSearchMatchCase)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_MATCHWORD, OnUpdateOptionsSearchMatchWord)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_USEREGEX, OnUpdateOptionsUseRegex)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_REMOVABLE, OnUpdateOptionsSearchRemovableDisks)
	ON_COMMAND(ID_FINDSEARCH_BUTTON, OnSearchButton)
	ON_COMMAND(ID_FINDCANCEL_BUTTON, OnCancelButton)
	ON_UPDATE_COMMAND_UI(ID_FINDSEARCH_BUTTON, OnUpdateSearchButton)
	ON_UPDATE_COMMAND_UI(ID_FINDCANCEL_BUTTON, OnUpdateCancelButton)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmFontFound, OnFontFound)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmFinished, OnSearchFinished)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmEnteringFolder, OnEnteringFolder)
END_MESSAGE_MAP()


// CFindNameFrameWnd message handlers

int CFindNameFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFindFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	// create rebar
	if (!m_wndRebar.Create(this, 0))
	{
		TRACE0("Failed to create rebar\n");
		return -1;
	}

	// create name edit
	// size doesn't matter here -- the rebar will resize it
	UINT nEditHeight = 0;
	if (m_wndNameEdit.Create(WS_VISIBLE | WS_CHILD, CRect(0, 0, 20, 20), this, IDC_FINDBYNAME_LOOKIN_COMBO))
	{
		// measure font
		CDC* pDC = GetDC();
		CFont* pOldFont = pDC->SelectObject(theApp.GetDialogFont());

		nEditHeight = CWinToolbox::GetEditHeight(pDC);

		pDC->SelectObject(pOldFont);
		ReleaseDC(pDC);

		// set font
		m_wndNameEdit.SetFont(theApp.GetDialogFont());

		::SetWindowLongPtr(m_wndNameEdit.GetSafeHwnd(), GWL_EXSTYLE, WS_EX_CLIENTEDGE);
	}
	else
	{
		TRACE0("Failed to create edit control\n");
		return -1;
	}

	// create look in combo
	if (!m_wndLookInCombo.Create(CBS_DROPDOWNLIST | CBS_SORT | WS_VSCROLL | WS_VISIBLE | WS_CHILD,
								 CRect(0, 0, 200, 400), this, IDC_FIND_LOOKIN_COMBO))
	{
		TRACE0("Failed to create \"Look In\" combo control\n");
		return -1;
	}

	m_nLookInSelection =
		CFindFrameBase::InitFolderCombo(&m_wndLookInCombo, &m_ilLookIn, m_strCurrentFolder, true, true, true, true, m_bCurrentFolderIsProject);

	// create toolbar
	if (!m_pwndSearchToolbar.get())
		m_pwndSearchToolbar.reset(new CPrettyToolbar());
	m_pwndSearchToolbar->SetBorders(1, 1, 1, 1);
	if (!m_pwndSearchToolbar->CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_pwndSearchToolbar->LoadToolBar(IDR_FIND_TOOLBAR))
	{
		TRACE0("Failed to create toolbar\n");
        return -1; // fail to create
	}

	m_pwndSearchToolbar->LoadToolBar(kToolbarButtonSize,
									 IDB_FINDTOOLBAR_NORMAL,
									 IDB_FINDTOOLBAR_HOT,
									 IDB_FINDTOOLBAR_DISABLED,
									 IDB_FINDTOOLBAR_NORMAL256,
									 IDB_FINDTOOLBAR_HOT256);

	// options button is a dropdown
	TBBUTTONINFO tbbi;
	std::memset(&tbbi, 0, sizeof(TBBUTTONINFO));
	tbbi.cbSize = sizeof(TBBUTTONINFO);
	tbbi.dwMask = TBIF_STYLE;
	tbbi.fsStyle = BTNS_WHOLEDROPDOWN;
	m_pwndSearchToolbar->GetToolBarCtrl().SetButtonInfo(ID_FINDOPTIONS_BUTTON, &tbbi);

	CRect rectToolbar;
	int nLastButton = m_pwndSearchToolbar->GetToolBarCtrl().GetButtonCount() - 1;
	m_pwndSearchToolbar->GetItemRect(nLastButton, &rectToolbar);
	rectToolbar.left = 0;

	/////////////////////////////////////////////////////////////////////////////////////////
	// now, we set up the rebar

	// add name edit to rebar
	CString strText;
	strText.LoadString(IDS_FINDNAME_LOOKFOR);
	m_wndRebar.AddBar(&m_wndNameEdit, strText, NULL, RBBS_NOGRIPPER);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
	rbbi.cxMinChild = 0;
	rbbi.cyMinChild = nEditHeight;
	rbbi.cx = rbbi.cxIdeal = rbbi.cyMinChild * 9;
	m_wndRebar.GetReBarCtrl().SetBandInfo(0, &rbbi);

	// add combo to rebar
	strText.LoadString(IDS_FIND_LOOK_IN);
	m_wndRebar.AddBar(&m_wndLookInCombo, strText, NULL, RBBS_NOGRIPPER);

	// add toolbar
	m_wndRebar.AddBar(m_pwndSearchToolbar.get(), NULL, NULL, RBBS_NOGRIPPER);
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
	rbbi.cxMinChild = rectToolbar.Width();
	rbbi.cyMinChild = rectToolbar.Height();
	rbbi.cyMaxChild = rbbi.cyMinChild;
	rbbi.cx = rbbi.cxIdeal = rectToolbar.Width();
	m_wndRebar.GetReBarCtrl().SetBandInfo(2, &rbbi);

	m_wndStatusBar.Create(this);

	// focus search edit
	m_wndNameEdit.SetFocus();

	return 0;
}

void CFindNameFrameWnd::OnDestroy()
{
	// save window pos
	CRect rectWindow;
	GetWindowRect(&rectWindow);

	CProgramOptions* pOptions = theApp.GetProgramOptions();

	pOptions->SetFindWindowPos(CPoint(rectWindow.left, rectWindow.top));
	pOptions->SetFindWindowSize(CSize(rectWindow.Width(), rectWindow.Height()));
	pOptions->SetSearchSubFolders(m_bSearchSubFolders);
	pOptions->SetSearchRemovableDisks(m_bSearchRemovableDisks);
	pOptions->SetSearchMatchCase(m_bMatchCase);
	pOptions->SetSearchMatchWord(m_bMatchWord);
	pOptions->SetSearchUsingRegex(m_bUseRegex);
}

void CFindNameFrameWnd::OnLookInSelChange()
{
	CFindFrameBase::LookInSelChange();
}

void CFindNameFrameWnd::OnLookInDeleteItem(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMCOMBOBOXEX* pItem = reinterpret_cast<NMCOMBOBOXEX*>(pNMHDR);

	// just delete the item data
	delete reinterpret_cast<CFindFrameBase::FindComboData*>(pItem->ceItem.lParam);

	*pResult = 0;
}

void CFindNameFrameWnd::OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult)
{
	NMTOOLBAR* pNMToolbar = (NMTOOLBAR*)pNMHDR;

	// this is the only dropdown notification we're handling
	ASSERT(pNMToolbar->iItem == ID_FINDOPTIONS_BUTTON);

	if (pNMToolbar->iItem == ID_FINDOPTIONS_BUTTON)
	{
		// create a popup menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		// load first option
		CString strMenu;
		strMenu.LoadString(IDS_FIND_SUBFOLDERS);

		MENUITEMINFO mii;
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
		mii.fType = MFT_STRING;
		mii.wID = ID_FINDOPTIONS_SUBFOLDERS;
		mii.cch = strMenu.GetLength();
		mii.dwTypeData = strMenu.GetBuffer();
		mii.fState = m_bSearchSubFolders ? MFS_CHECKED : MFS_UNCHECKED;

		menu.InsertMenuItem(ID_FINDOPTIONS_SUBFOLDERS, &mii);
		strMenu.ReleaseBuffer();

		// load "match case"
		strMenu.LoadString(IDS_FIND_MATCHCASE);
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
		mii.fType = MFT_STRING;
		mii.wID = ID_FINDOPTIONS_MATCHCASE;
		mii.cch = strMenu.GetLength();
		mii.dwTypeData = strMenu.GetBuffer();
		mii.fState = m_bSearchSubFolders ? MFS_CHECKED : MFS_UNCHECKED;

		menu.InsertMenuItem(ID_FINDOPTIONS_MATCHCASE, &mii);
		strMenu.ReleaseBuffer();

		// load "match whole word"
		strMenu.LoadString(IDS_FIND_MATCHWORD);
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
		mii.fType = MFT_STRING;
		mii.wID = ID_FINDOPTIONS_MATCHWORD;
		mii.cch = strMenu.GetLength();
		mii.dwTypeData = strMenu.GetBuffer();
		mii.fState = m_bSearchSubFolders ? MFS_CHECKED : MFS_UNCHECKED;

		menu.InsertMenuItem(ID_FINDOPTIONS_MATCHWORD, &mii);
		strMenu.ReleaseBuffer();

		// load "use regex"
		strMenu.LoadString(IDS_FIND_USEREGEX);
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
		mii.fType = MFT_STRING;
		mii.wID = ID_FINDOPTIONS_USEREGEX;
		mii.cch = strMenu.GetLength();
		mii.dwTypeData = strMenu.GetBuffer();
		mii.fState = m_bUseRegex ? MFS_CHECKED : MFS_UNCHECKED;

		menu.InsertMenuItem(ID_FINDOPTIONS_USEREGEX, &mii);
		strMenu.ReleaseBuffer();

		if (m_bLookInMyComputer)
		{
			strMenu.LoadString(IDS_FIND_REMOVABLEDISKS);

			::memset(&mii, 0, sizeof(MENUITEMINFO));
			mii.cbSize = sizeof(MENUITEMINFO);
			mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
			mii.fType = MFT_STRING;
			mii.wID = ID_FINDOPTIONS_REMOVABLE;
			mii.cch = strMenu.GetLength();
			mii.dwTypeData = strMenu.GetBuffer();
			mii.fState = m_bSearchRemovableDisks ? MFS_CHECKED : MFS_UNCHECKED;

			menu.InsertMenuItem(ID_FINDOPTIONS_REMOVABLE, &mii);
			strMenu.ReleaseBuffer();
		}

		// get screen coordinates of toolbar button
		CRect rectScreen, rectButton;
		m_pwndSearchToolbar->GetToolBarCtrl().GetWindowRect(&rectScreen);
		UINT nIndex = m_pwndSearchToolbar->GetToolBarCtrl().CommandToIndex(ID_FINDOPTIONS_BUTTON);
		m_pwndSearchToolbar->GetToolBarCtrl().GetItemRect(nIndex, &rectButton);
		CPoint pt(rectScreen.left + rectButton.left,
				  rectScreen.top + rectButton.top + rectButton.bottom);

		// show menu
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);
	}

	*pResult = 0;
}

void CFindNameFrameWnd::OnOptionsSearchSubFolders()
{
	m_bSearchSubFolders = !m_bSearchSubFolders;
}

void CFindNameFrameWnd::OnOptionsSearchMatchCase()
{
	m_bMatchCase = !m_bMatchCase;
}

void CFindNameFrameWnd::OnOptionsSearchMatchWord()
{
	m_bMatchWord = !m_bMatchWord;
}

void CFindNameFrameWnd::OnOptionsUseRegex()
{
	m_bUseRegex = !m_bUseRegex;
}

void CFindNameFrameWnd::OnOptionsSearchRemovableDisks()
{
	m_bSearchRemovableDisks = m_bSearchRemovableDisks;
}

void CFindNameFrameWnd::OnUpdateOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bSearching);
}

void CFindNameFrameWnd::OnUpdateOptionsSearchSubFolders(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bSearchSubFolders);
}

void CFindNameFrameWnd::OnUpdateOptionsSearchMatchCase(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bMatchCase);
}

void CFindNameFrameWnd::OnUpdateOptionsSearchMatchWord(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bMatchWord);
}

void CFindNameFrameWnd::OnUpdateOptionsUseRegex(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bUseRegex);
}

void CFindNameFrameWnd::OnUpdateOptionsSearchRemovableDisks(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bSearchRemovableDisks);
}

void CFindNameFrameWnd::OnSearchButton()
{
	// get search string
	CString strSearchString;
	m_wndNameEdit.GetWindowText(strSearchString);

	// can't be empty
	if (strSearchString.IsEmpty())
		return;

	// if there are already search results, make sure the user wants to clear
	// it before continuing
	if (GetFontListView()->GetListCtrl().GetItemCount())
	{
		int nResult = AfxMessageBox(IDS_FIND_NEWSEARCHCONFIRM, MB_YESNO + MB_ICONQUESTION);

		// if user says no, just forget it and return
		if (nResult == IDNO)
			return;

		// clear the list
		GetFontListView()->GetListCtrl().DeleteAllItems();
	}

	// toggle flag
	m_bSearching = true;

	FindComboData* pComboData =
		reinterpret_cast<FindComboData*>(m_wndLookInCombo.GetItemData(m_wndLookInCombo.GetCurSel()));

	if (!m_bMatchCase)
		strSearchString.MakeLower();

	if (m_bMatchWord)
	{
		strSearchString.Insert(0, _T("\\<"));
		strSearchString.Append(_T("\\>"));
	}
	
	// convert it to ANSI
#ifdef UNICODE
	int nSize = ::WideCharToMultiByte(CP_ACP, 0, strSearchString, -1, NULL, 0, NULL, NULL);

	boost::scoped_ptr<char> pTarget(new char[nSize]);
	::WideCharToMultiByte(CP_ACP, 0, strSearchString, -1, pTarget.get(), nSize, NULL, NULL);

	m_strExpression = pTarget.get();
#else
	m_strExpression = strSearchString;
#endif

	if (!m_bUseRegex)
	{
		// not using regex, so escape special regex chars .[]{}()\*+?|^$
		// we'll actually use a regular expression for this
		boost::smatch match;
		boost::regex expression("[\\.\\[\\]\\{\\}\\(\\)\\\\\\*\\+\\?\\|\\^\\$+]");
		std::string strReplace("\\\\$&");

		try
		{
			m_strExpression = boost::regex_replace(m_strExpression, expression, strReplace);
		}
		catch (std::runtime_error&)
		{
			AfxMessageBox(IDS_GENERAL_BADREGEX, MB_OK + MB_ICONERROR);
			return;
		}
	}

	if (pComboData->nType == FindComboData::PIDL_type)
	{
		// init SearchThreadData structure
		SearchThreadData* pData = new SearchThreadData;
		pData->pWnd = this;
		pData->pidlRoot = pComboData->pidl;
		pData->bSearchRemovableDisks = m_bSearchRemovableDisks;
		pData->pController = new CSearchController<CFindNameFrameWnd>(
									&CFindNameFrameWnd::Searching,
									&CFindNameFrameWnd::EnteringFolder,
									&CFindNameFrameWnd::Found,
									this);

		// start thread
		AfxBeginThread(SearchThreadFunction, pData);
	}
	else if (pComboData->projects_type)
	{
		SearchProjectData* pData = new SearchProjectData;
		pData->pWnd = this;
		pData->strProjectName = pComboData->strProjectName;
		pData->pController = new CSearchController<CFindNameFrameWnd>(
									&CFindNameFrameWnd::Searching,
									&CFindNameFrameWnd::EnteringFolder,
									&CFindNameFrameWnd::Found,
									this);

		// start thread
		AfxBeginThread(SearchProjectFunction, pData);
	}
}

void CFindNameFrameWnd::OnCancelButton()
{
	m_bSearching = false;
	m_bCanceled  = true;
}

void CFindNameFrameWnd::OnUpdateSearchButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bSearching);
}

void CFindNameFrameWnd::OnUpdateCancelButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bSearching && !m_bCanceled);
}

LRESULT CFindNameFrameWnd::OnEnteringFolder(WPARAM wParam, LPARAM /*lParam*/)
{
	LPCTSTR pPath = reinterpret_cast<LPCTSTR>(wParam);
	m_wndStatusBar.Searching(pPath);

	if (pPath)
		delete [] pPath;

	return 0;
}

LRESULT CFindNameFrameWnd::OnFontFound(WPARAM wParam, LPARAM /*lParam*/)
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(wParam);
	GetFontListView()->AddFoundItem(pItem);

	// remove temporary reference
	theApp.GetFontManager()->RemoveReference(pItem);

	return 0;
}

LRESULT CFindNameFrameWnd::OnSearchFinished(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	m_bSearching = false;

	int nCount = GetFontListView()->GetListCtrl().GetItemCount();

	if (m_bCanceled)
	{
		m_wndStatusBar.Canceled(nCount);
		m_bCanceled = false;
	}
	else
		m_wndStatusBar.Finished(nCount);

	return 0;
}