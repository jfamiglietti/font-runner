/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxwin.h"


// CRenameDlg dialog

class CRenameDlg : public CDialog
{
	DECLARE_DYNAMIC(CRenameDlg)

public:
	CRenameDlg(LPCTSTR lpszOldName, CWnd* pParent = NULL);   // standard constructor
	virtual ~CRenameDlg();

public:
	CString GetNewName() const;

// Dialog Data
private:
	enum { IDD = IDD_RENAME_DIALOG };
	CEdit m_ctrlNewNameEdit;
	LPCTSTR m_lpszOldName;
	CString m_strNewName;
	CStatic m_ctrlIconStatic;
	HICON	m_hIcon;

// overrides
protected:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// message map
	DECLARE_MESSAGE_MAP()
};
