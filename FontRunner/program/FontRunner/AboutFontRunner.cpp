/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "FontRunner.h"
#include "ProgramOptions.h"
#include "AboutFontRunner.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_strVersionStatic(_T(""))
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_VERSION_STATIC, m_strVersionStatic);
	DDX_Control(pDX, IDC_ICON_STATIC, m_ctrlIconStatic);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// App command to run the dialog
void CFontRunnerApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

BOOL CAboutDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString strURL;

	// create version string
	CFontRunnerApp* pApp = (CFontRunnerApp*)AfxGetApp();
	AfxFormatString1(m_strVersionStatic, IDS_ABOUT_VERSION, pApp->GetVersionString());

	// don't need to clean up icon because static control takes ownership of it
	HICON hIcon = (HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME), IMAGE_ICON, 48, 48, LR_DEFAULTCOLOR);
	m_ctrlIconStatic.SetIcon(hIcon);
	m_ctrlIconStatic.SetWindowPos(NULL, 0, 0, 48, 48, SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOZORDER);

	// update controls
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
