/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "Filelist.h"
#include <boost/weak_ptr.hpp>

//========================================================================================
class CFontProjects
{
public:
	CFontProjects();
	~CFontProjects();

public:
	void Load(LPCTSTR szFilename) throw(...);
	void Save(LPCTSTR szFilename) throw(...);

	// project management
	void Add(LPCTSTR szProjectName);
	void Remove(LPCTSTR szProjectName);
	bool Rename(LPCTSTR szOldName, LPCTSTR szNewName);
	bool Exists(LPCTSTR szProjectName) const;
	projectlist_type::const_iterator Find(LPCTSTR szProjectName) const;
	
	void AddFile(LPCTSTR szProjectName, LPCTSTR szFileName);
	void RemoveFile(LPCTSTR szProjectName, LPCTSTR szFontName);
	bool FileExists(LPCTSTR szProjectName, LPCTSTR szFontName) const;

	std::wstring GetNewProjectName(LPCTSTR szAttemptedName) const;

	// for iterating projects
	size_t GetProjectCount() const;
	projectlist_type::const_iterator GetListBeginning() const;
	projectlist_type::const_iterator GetListEnd() const;

	boost::weak_ptr<filelist_type> GetFileList(LPCTSTR szProjectname) const;

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;
};

//========================================================================================
class CFontProjectException
{
public:
	enum eError
	{
		kError_AlreadyExists,
		kError_ProjectNotFound,
		kError_FileNotFound,
		kError_NamesExhausted // when we try and make a unique new name and cannot
	};

	CFontProjectException(eError nError) : m_nError(nError)
	{}

	eError GetError() const
	{
		return m_nError;
	}

private:
	eError m_nError;
};