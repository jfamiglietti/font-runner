/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/shared_ptr.hpp>

class CProgramOptions
{
public:
	CProgramOptions();
	virtual ~CProgramOptions();

public:
	void Load();
	void Save();

public:
	static CString GetRegistryLocation();

	// frame/pane size and position
	bool GetMainWindowMaximized() const;
	void SetMainWindowMaximized(bool bMaximized);

	void GetMainWindowPosition(POINT& pt) const;
	void SetMainWindowPosition(const POINT& pt);

	void GetMainWindowSize(SIZE& size) const;
	void SetMainWindowSize(const SIZE& size);

	DWORD GetTreeViewWidth() const;
	void SetTreeViewWidth(DWORD dwWidth);

	DWORD GetTreeViewHeight() const;
	void SetTreeViewHeight(DWORD dwWidth);

	DWORD GetFontListHeight() const;
	void SetFontListHeight(DWORD dwHeight);

	void GetRestoredWindowPosition(POINT& pt) const;
	void GetRestoredWindowSize(SIZE& size) const;
	DWORD GetRestoredTreeViewWidth() const;
	DWORD GetRestoredTreeViewHeight() const;
	DWORD GetRestoredFontListHeight() const;

	void SaveRestoredMetrics();

	// font list view options
	DWORD GetFontNameColWidth() const;
	void SetFontNameColWidth(DWORD dwWidth);

	DWORD GetFileNameColWidth() const;
	void SetFileNameColWidth(DWORD dwWidth);

	DWORD GetSizeColWidth() const;
	void SetSizeColWidth(DWORD dwWidth);

	DWORD GetFontListCharSize() const;
	void SetFontListCharSize(DWORD dwWidth);

	DWORD GetFontListSortColumn() const;
	void SetFontListSortColumn(DWORD dwColumn);

	bool IsFontListSortDescending() const;
	void SetFontListSortDescending(bool bDescending);
	bool ToggleFontListSortDescending();

	CString GetFontListText() const;
	void SetFontListText(const CString& strText);

	bool AutoInstall() const;
	void SetAutoInstall(bool bSet);

	bool ShowFullPathInTitleBar() const;
	void ShowFullPathInTitleBar(bool bShow);

	// font preview options
	enum eFontDetailMode
	{
		kMode_Preview = 0,
		kMode_FontMap,

		// Must be last item in enumerator
		kMode_Count
	};
	eFontDetailMode GetFontDetailMode() const;
	void SetFontDetailMode(eFontDetailMode dwMode);

	DWORD GetPreviewFontSize() const;
	void SetPreviewFontSize(DWORD dwWidth);

	DWORD GetPreviewFontColor() const;
	void SetPreviewFontColor(DWORD dwColor);

	DWORD GetPreviewFontBackgroundColor() const;
	void SetPreviewFontBackgroundColor(DWORD dwColor);

	bool IsPreviewFontItalic() const;
	void SetPreviewFontItalic(bool bItalic);

	bool IsPreviewFontBold() const;
	void SetPreviewFontBold(bool bBold);

	bool IsPreviewFontUnderlined() const;
	void SetPreviewFontUnderlined(bool bUnderlined);

	CString GetPreviewText() const;
	void SetPreviewText(LPCTSTR lpszText);

	// font map
	DWORD GetFontMapSize() const;
	void SetFontMapSize(DWORD dwSize);

	DWORD GetFontMapFontColor() const;
	void SetFontMapFontColor(DWORD dwColor);

	DWORD GetFontMapFontBackgroundColor() const;
	void SetFontMapFontBackgroundColor(DWORD dwColor);

	// printing
	DWORD GetPrintCharSize() const;
	void SetPrintCharSize(DWORD dwSize);

	CString GetPrintSampleTextLine1() const;
	void SetPrintSampleTextLine1(const CString& strText);

	CString GetPrintSampleTextLine2() const;
	void SetPrintSampleTextLine2(const CString& strText);

	// character detail window
	void GetCharDetailWindowPos(POINT& pt) const;
	void SetCharDetailWindowPos(const POINT& pt);

	void GetCharDetailWindowSize(SIZE& size) const;
	void SetCharDetailWindowSize(const SIZE& size);

	// search options
	bool SearchRemovableDisks() const;
	void SetSearchRemovableDisks(bool bSearch);

	bool SearchSubFolders() const;
	void SetSearchSubFolders(bool bSearch);

	bool SearchMatchCase() const;
	void SetSearchMatchCase(bool bSearch);

	bool SearchMatchWord() const;
	void SetSearchMatchWord(bool bSearch);

	bool SearchUsingRegex() const;
	void SetSearchUsingRegex(bool bSearch);

	// startup options
	enum eStartupOption
	{
		kStartupOption_DefaultFolder,
		kStartupOption_DefaultProject,
		kStartupOption_LastItem
	};
	eStartupOption GetStartupOption() const;
	void SetStartupOption(eStartupOption nStartupOption);

	CString GetDefaultFolder() const;
	void SetDefaultFolder(LPCTSTR lpszFolder);

	CString GetDefaultProject() const;
	void SetDefaultProject(LPCTSTR lpszProject);

	CString GetLastFolder() const;
	void SetLastFolder(LPCTSTR lpszFolder);

	CString GetLastProject() const;
	void SetLastProject(LPCTSTR lpszProject);

	bool UsingFontProject() const;
	void UseFontProject(bool bUse);

	// rendering options
	enum eRenderingOption
	{
		kRenderingOption_UseDefault,
		kRenderingOption_Antialiasing,
		kRenderingOptions_ClearType
	};

	eRenderingOption GetFontListRenderingOption() const;
	void SetFontListRenderingOption(eRenderingOption nOption);

	eRenderingOption GetFontMapRenderingOption() const;
	void SetFontMapRenderingOption(eRenderingOption nOption);

	eRenderingOption GetCharDetailRenderingOption() const;
	void SetCharDetailRenderingOption(eRenderingOption nOption);

	// find window
	void GetFindWindowPos(POINT& pt) const;
	void SetFindWindowPos(const POINT& pt);

	void GetFindWindowSize(SIZE& size) const;
	void SetFindWindowSize(const SIZE& size);

	size_t GetMainRebarLayoutSize() const;
	void GetMainRebarLayout(unsigned char* pLayoutData, size_t nSize) const;
	void SetMainRebarLayout(const unsigned char* pLayoutData, size_t nSize);

	// Location of updater.  Set when Font Runner is updated so it can
	// delete the file when it is restarted
	CString GetUpdaterLocation() const;
	void SetUpdaterLocation(LPCTSTR szLocation);
	void ClearUpdaterLocation();

	bool GetClearLocationsOnExit() const;
	void SetClearLocationsOnExit(bool bClear);

	bool GetLimitLocations() const;
	void SetLimitLocations(bool bLimit);

	int GetLocationsLimit() const;
	void SetLocationsLimit(int nLimit);

	DWORD GetFontListDropOption() const;
	void SetFontListDropOption(DWORD dwOption);

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;
};
