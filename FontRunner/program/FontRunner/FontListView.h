/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "FontProjects.h"
#include "FontRunnerSignals.h"

#include <boost/scoped_array.hpp>

// FontListView.h : header file
//

// some default values
#define LEFT_MARGIN			720		// 1/2 inch (in TWIPS)
#define RIGHT_MARGIN		720		// 1/2 inch (in TWIPS)
#define TOP_MARGIN			1440	// 1 inch (in TWIPS)

class CFontItem;
class CFontFileDataSource;

/////////////////////////////////////////////////////////////////////////////
// CFontListView view

class CFontListView : public CListView, public boost::signals2::trackable
{
protected:
	CFontListView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CFontListView)

public:
	enum eMode
	{
		kMode_View,
		kMode_FindSimilar,
		kMode_FindName,
		kMode_FindCharacter
	};

// Operations
public:
	void CleanUpFontList();
	void ShowFontsInDirectory(const CString& pstrDirectory = _T(""), const CString* pstrSelect = NULL);
	void ShowFontsInList(const filelist_type& filelist, const CString* pstrSelect = NULL);
	CString GetSelFontName();
	void SaveOptions();
	bool SetHeaderArrow(const int* pnLastItem = NULL);
	void SetFontSelection(const CString& strFilename);
	void SelectAll();

	// printing
	void SetupForPrinting(CDC* pDC, CPrintInfo* pInfo);
	void PrintFont(CDC* pDC, int nPageItem, CFontItem* pFontListItemData);

	// functions and variables for watching the current directory
	static HANDLE			s_hStopWatching;
	static CFontListView*	s_pwndFontListView;
	static UINT AFX_CDECL WatchFileSystemThread(LPVOID pParam);

	CWinThread* m_pWatchThread;
	static bool s_bThreadRunning;

	void WatchCurrentFolder();
	void StopWatchingCurrentFolder();

	void SetMode(eMode nMode);
	eMode GetMode() const;

	void SetLookForCharacter(wchar_t ch);
	wchar_t GetLookForCharacter() const;

	void SetSimilarTo(LPCTSTR pszSimilarTo);
	LPCTSTR GetSimilarTo() const;
	void AddSimilarItem(const CFontItem* pItem);
	void AddFoundItem(const CFontItem* pItem);
	void AddNewFile(const CString& strFileName);
	void AddNewFile(const std::wstring& strFileName);

	int GetSortedColumn() const;
	bool IsSortDescending() const;

	void MarkSelectedPrivate();
	void RestoreSelectedCharacteristics();

private:
	void DeleteSelectedFiles();
	void RemoveSelectedFilesFromProject();
	static UINT __stdcall ThreadedCreateList(LPVOID pParam);

// Attributes
protected:
	CString		m_strCurrentDirectory;
	bool		m_bViewingFontProject;
	bool		m_bViewingSystemFonts;

	// general purpose printing font (for font info)
	CFont*		m_pGeneralFont;

	// number of pixels per inch
	int			m_nPPIX, m_nPPIY;

	// physical size of page in logical pixels
	int			m_nPageWidth, m_nPageHeight;

	// area of page we will use
	CRect		m_rectUsableArea;

	// height of each font item
	int			m_nFontItemHeight;

	// how high is font, really
	int			m_nRealFontHeight;

	// number of fonts we can fit on a page
	int			m_nFontsPerPage;

	// item right-clicked on
	int			m_nRClickItem;

	// version of common controls library
	DWORD		m_dwCOMCTL32Version;

	// cached pointer to status bar
	CStatusBar*	m_pwndStatusBar;

	// current mouse position
	CPoint		m_ptMouse;

	// size of icon
	CSize		m_sizIcon;

	// lets us know we're already adjusting the width of header items
	bool		m_bAdjustingHeader;

	// current tooltip text
	boost::scoped_array<TCHAR> m_pszCurrentTipText;

	// to prevent folder multiple folder change message processing
	CCriticalSection m_csFolderChange;

	// constants
	enum
	{
		kIconPadding = 2,
		kToolTipWidth = 255
	};

	enum eFontListColumns
	{
		// Column 0
		kColumn_FontName,

		// Column 1
		kColumn_FileName,
		kColumn_Similarity = kColumn_FileName,
		
		// Column 2
		kColumn_Size,

		// make sure this is ALWAYS LAST in this enumeration
		kColumn_NumColumns
	};

	int m_nOptimalColumnSize[kColumn_NumColumns];
	int m_nMinHeaderSize[kColumn_NumColumns];

	// drag n drop
	IDropTarget*	m_pDropTarget;
	CComPtr<IDropTargetHelper> m_pDropTargetHelper;

	// have to keep track of imagelist ourselves because setting it
	// directly in the control messes up the computed MeasureItem height
	HIMAGELIST m_hImageList;

	// In search mode, the font list view can contain fonts from different folders.
	// Dragging files into a font list view in this mode is not allowed.
	eMode m_nMode;

	// the full pathname of the font being compared to
	LPCTSTR m_pszSimilarTo;

	// character that we're looking for
	wchar_t m_chLookFor;

	// tells us whether we're currently signaling a font change
	bool m_bSignalingFontChange;

	// tells us whether we should signal a font change
	bool m_bSignalFontChange;

	static UINT s_uwmFolderChange;

// drag and drop ops
protected:
	void BeginDrag();
	void DragEnter(IDataObject *pDataObject, DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
	void DragOver(DWORD grfKeyState, POINTL pt, DWORD* pdwEffect);
	void DragLeave();
	void Drop(IDataObject *pDataObject, DWORD grfKeyState, POINTL pt, DWORD *pdwEffect);
	void CreateDragBitmap(const POINT& pt, SHDRAGIMAGE* pshdi);

	// fills a vector of character pointers with selected file names
	// returns the total number of bytes (not characters) required by these strings
	size_t GetSelectedFileNames(std::vector<LPCTSTR>& files) const;

	void FireSelectedFontChange(const CFontItem* pItem);

	// creates a data object from selected fonts
	CFontFileDataSource* CreateDataSourceForDropFiles() const;
	CFontFileDataSource* CreateDataSourceForProjectData() const;

// signal handlers
protected:
	void OnSelectedProjectChange(const std::wstring& strProject);
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnSelectedFontChange(const CFontItem* pFontItem);
	void OnPreviewSizeChange(unsigned short nSize);
	void OnAddToMainFontListView(const std::vector<std::wstring>& list);
	void OnTempAutoInstallChange(bool bAutoInstall);
	void OnFontRunnerShutdown();
	void OnRefresh();
	void OnOptionsChange();
	int OnUnManageFontItem(const CFontItem* pFontItem);
	void OnFontFileDeleted(const CFontItem* pFontItem);

// list sorting
protected:
	static int CALLBACK FontListSortProc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort);
	int m_nSortColumn;
	bool m_bSortDescending;

// owner drawing
private:
	struct ITEMMETRICS
	{
		int nIconPos, nTextPos;
		int nMargin;
		int nPadding;
		int nNormalFontHeight;
		int nInfoLineHeight;
		int nPreviewLineHeight;
	};
	ITEMMETRICS m_im;
	void ComputeItemMetrics();


// Overrides
public:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DrawItem(LPDRAWITEMSTRUCT pDrawItemStruct);
	virtual void MeasureItem(LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual INT_PTR OnToolHitTest(CPoint point, TOOLINFO* pTI) const;
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrintPreview(CDC* pDC, CPrintInfo* pInfo, POINT point, CPreviewView* pView);
	virtual void PreSubclassWindow();

// Implementation
protected:
	virtual ~CFontListView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDestroy();
	afx_msg void OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteAllItems(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeleteItem(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFilePrintPreview();
	afx_msg void OnFontlistInstall();
	afx_msg void OnFontlistOpen();
	afx_msg void OnFontlistExplore();
	afx_msg void OnFontlistFindSimilar();
	afx_msg void OnFolderMenu_Delete();
	afx_msg void OnUpdateFolderMenu_Delete(CCmdUI* pCmdUI);
	afx_msg void OnBadFontMenu_WhatsWrong();
	afx_msg void OnBadFontMenu_Delete();
	afx_msg void OnUpdateBadFontMenu_Delete(CCmdUI* pCmdUI);
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFontlistFontInfo();
	afx_msg void OnFontlistProperties();
	afx_msg void OnFontListMenu_ShowContaining();
	afx_msg void OnHdnItemclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg LRESULT OnFolderChange(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnTTNNeedText(UINT nID, NMHDR* pTTTStruct, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnHdnDividerdblclick(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnHdnItemChanged(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBeginDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBeginRDrag(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg LRESULT OnGetIconIndex(WPARAM wParam, LPARAM lParam);
};
