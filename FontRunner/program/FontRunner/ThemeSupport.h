/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/


#pragma once

#include <uxtheme.h>
#include <tmschema.h>

class CThemeSupport
{
public:
	CThemeSupport(bool bLoadThemeLib = true);
	virtual ~CThemeSupport();

	// Windows XP theme support
	static int		s_nRefCount;
	static HMODULE	s_hThemeModule;
	static bool		s_bLibLoadAttempted;

	// Theme function pointers
	static void* s_pfnIsAppThemed;
	static void* s_pfnIsThemeActive;
	static void* s_pfnEnableTheming;
	static void* s_pfnOpenThemeData;
	static void* s_pfnCloseThemeData;
	static void* s_pfnDrawThemeBackground;
	static void* s_pfnDrawThemeParentBackground;
	static void* s_pfnDrawThemeEdge;
	static void* s_pfnDrawThemeText;
	static void* s_pfnEnableThemeDialogTexture;
	static void* s_pfnGetThemeBackgroundContentRect;
	static void* s_pfnGetThemeColor;
	static void* s_pfnDrawThemeTextEx;

	// theme functions
	static void LoadThemeLibrary();
	BOOL IsAppThemed();
	BOOL IsThemeActive();
	HRESULT EnableTheming(BOOL fEnable);
	HTHEME OpenThemeData(HWND hwnd, LPCWSTR pszClassList);
	HRESULT CloseThemeData(HTHEME hThemeData);
	HRESULT DrawThemeBackground(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, const RECT* pRect, const RECT* pClipRect);
	HRESULT DrawThemeParentBackground(HWND hWnd, HDC hdc, RECT* prc);
	HRESULT DrawThemeEdge(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, const RECT *pDestRect, UINT uEdge, UINT uFlags, RECT *pContentRect);
	HRESULT DrawThemeText(HTHEME hThemeData, HDC hdc, int iPartId, int iStateId, LPCWSTR pszText, int iCharCount, DWORD dwTextFlags, DWORD dwTextFlags2, const RECT *pRect);
	HRESULT EnableThemeDialogTexture(HWND hWnd, DWORD dwFlags);
	HRESULT GetThemeBackgroundContentRect(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, const RECT *pBoundingRect, RECT *pContentRect);
	HRESULT GetThemeColor(HTHEME hTheme, int nPartID, int nStateID, int nPropID, COLORREF* pclr);
	HRESULT DrawThemeTextEx(HTHEME hTheme, HDC hdc, int iPartId, int iStateId, LPCWSTR pszText, int iCharCount, DWORD dwFlags, LPRECT pRect, const DTTOPTS *pOptions);
};
