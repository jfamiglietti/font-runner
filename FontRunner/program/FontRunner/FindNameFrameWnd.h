/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FindFrameBase.h"
#include "FontRunnerSignals.h"
#include "FindStatusBar.h"

class CPrettyToolbar;

// CFindNameFrameWnd frame

class CFindNameFrameWnd : public CFindFrameBase
{
	DECLARE_DYNCREATE(CFindNameFrameWnd)
public:
	CFindNameFrameWnd(LPCTSTR lpszSearchRoot = NULL, bool bProject = false);
	virtual ~CFindNameFrameWnd();

public:
	bool Create(CWnd* pParentWnd);

// overrides
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

private:
	void InitLookInCombo(CComboBoxEx* pCombo, LPCTSTR szInitialFolder = NULL) const;
	void ToggleUI();

// functions for the search thread
private:
	bool Searching() const;
	void EnteringFolder(LPCTSTR szPath);
	void Found(CFontItem* pItem);

// overrides
protected:
	CFontListView::eMode GetFontListViewMode() const;

private:
	std::string m_strExpression;
	CEdit m_wndNameEdit;
	bool m_bSearchSubFolders;
	bool m_bSearchRemovableDisks;
	bool m_bMatchCase;
	bool m_bMatchWord;
	bool m_bUseRegex;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnLookInSelChange();
	afx_msg void OnLookInDeleteItem(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnOptionsSearchSubFolders();
	afx_msg void OnOptionsSearchMatchCase();
	afx_msg void OnOptionsSearchMatchWord();
	afx_msg void OnOptionsUseRegex();
	afx_msg void OnOptionsSearchRemovableDisks();
	afx_msg void OnUpdateOptions(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionsSearchSubFolders(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionsSearchMatchCase(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionsSearchMatchWord(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionsSearchRemovableDisks(CCmdUI* pCmdUI);
	afx_msg void OnUpdateOptionsUseRegex(CCmdUI* pCmdUI);
	afx_msg void OnSearchButton();
	afx_msg void OnCancelButton();
	afx_msg void OnUpdateSearchButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateCancelButton(CCmdUI* pCmdUI);
	afx_msg LRESULT OnFontFound(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnSearchFinished(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnEnteringFolder(WPARAM wParam, LPARAM lParam);
};


