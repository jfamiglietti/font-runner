/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <boost/shared_ptr.hpp>
#include <boost/signals2.hpp>

// prototype COpenTypeData class
namespace CruxTechnologies
{
	class COpenTypeData;
}
class CFontItem;

// CRichTextPreviewView view

class CRichTextPreviewView : public CView, public boost::signals2::trackable
{
	DECLARE_DYNCREATE(CRichTextPreviewView)

protected:
	CRichTextPreviewView();           // protected constructor used by dynamic creation
	virtual ~CRichTextPreviewView();

public:
	void SetFontHeight(int nSize);
	void SetFontFace(CString strFontName);
	void RefreshFontView();

// Attributes
protected:
	CString				m_strCurrentFaceName;
	boost::shared_ptr<CFontItem> m_pFontItem;
	
// overrides
protected:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	virtual void OnInitialUpdate();

private:
	void SaveOptions();
	void OnSelectedFontChange(const CFontItem* pData);
	void OnDetailSizeChange(unsigned short nSize);
	void OnPreviewRTFCharacter(TCHAR ch);
	void OnFontRunnerShutdown();
	int OnUnManageFontItem(const CFontItem* pItem);
	void OnFontFileDeleted(const CFontItem* pItem);
	void OnDetailColorChange(COLORREF color, bool bForeground);

private:
	// rich edit control covers entire view
	CRichEditCtrl*	m_pRichEdit;
	const CString	m_cstrRichEditDLL;
	bool			m_bInitialUpdated;

public:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnPaletteColorChange(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg void OnBoldButton();
	afx_msg void OnItalicButton();
	afx_msg void OnUnderlineButton();
	afx_msg void OnUpdateBoldButton(CCmdUI *pCmdUI);
	afx_msg void OnUpdateItalicButton(CCmdUI *pCmdUI);
	afx_msg void OnUpdateUnderlineButton(CCmdUI *pCmdUI);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
};


