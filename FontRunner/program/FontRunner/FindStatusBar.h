/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once


// CFindStatusBar

class CFindStatusBar : public CStatusBar
{
	DECLARE_DYNAMIC(CFindStatusBar)

public:
	CFindStatusBar();
	virtual ~CFindStatusBar();

// call these to notify the bar
public:
	void Searching(LPCTSTR szFolder);
	void Canceled(int nFound);
	void Finished(int nFound);

protected:
	void UpdateStatusIcon(UINT nBitmapID);

protected:
	bool m_bSearching;
	CProgressCtrl	m_wndProgressCtrl;
	CStatic			m_wndStatusIcon;
	CBitmap			m_bmpStatus;
	const int		m_cnIconWidth;
	const int		m_cnProgressWidth;
	bool			m_bProgressMarqueeOn;

	enum ePane
	{
		kPane_StatusIcon,
		kPane_StatusText,
		kPane_Progress
	};

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpcs);
	afx_msg void OnSize(UINT nType, int cx, int cy);
};


