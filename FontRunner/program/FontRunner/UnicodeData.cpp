/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdafx.h"
#include "WinToolbox.h"
#include "UnicodeData.h"
#include "FontRunnerException.h"

#include <map>
#include <algorithm>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace CruxTechnologies;

typedef std::map<wchar_t, wchar_t*> codemap_type;
typedef std::pair<wchar_t, wchar_t*> codemappair_type;

struct CUnicodeCharDescriptions::_impl
{
	_impl() : pBuffer(NULL)
	{
		CString strAppPath = CWinToolbox::GetAppPath();
		strAppPath += _T("UnicodeData.bin");

		HANDLE hFile = ::CreateFile(strAppPath,
									GENERIC_READ, 0, NULL,
									OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwRead = 0;
			DWORD dwFileSize = ::GetFileSize(hFile, NULL);

			// read the whole file, it's fastest this way
			pBuffer.reset(new BYTE[dwFileSize]);
			::ReadFile(hFile, pBuffer.get(), dwFileSize, &dwRead, NULL);
			::CloseHandle(hFile);

			// get count
			uint16_t nCount = 0;
			BYTE* pCurrent = pBuffer.get();
			::memcpy(&nCount, pCurrent, sizeof(nCount));
			pCurrent += sizeof(nCount);

			uint16_t* pValues = NULL;
			for (uint16_t i = 0; i < nCount; ++i)
			{
				pValues = (uint16_t*)pCurrent;
				pCurrent += 4;	// 2 2-byte values

				// insert into map
				codemap.insert(codemap.end(),
							   codemappair_type(*pValues, (wchar_t*)pCurrent));
				pCurrent += (pValues[1] * 2);
			}
		}
		else
			throw (CFileNotFoundException(strAppPath));
	}

	codemap_type codemap;
	boost::scoped_array<BYTE> pBuffer;
};

CUnicodeCharDescriptions::CUnicodeCharDescriptions()
 : m_pImpl(new _impl)
{	
}

const wchar_t* CUnicodeCharDescriptions::Get(wchar_t charCode) const
{
	codemap_type::const_iterator it = m_pImpl->codemap.find(charCode);

	if (it != m_pImpl->codemap.end())
		return it->second;
	else
		return NULL;
}