/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////////////////////////////////////////////////////////////////
// Key.h - This is the common Crux Windows registry class

#pragma once

class CKey
{
protected:
	HKEY	m_hOpenKey;

public:
	// Construction / Desctruction
	CKey();
	virtual ~CKey();

	// call one of these initialization methods first
	bool OpenKey(HKEY hRootKey, LPCTSTR lpszRegSection, REGSAM samDesired = KEY_READ | KEY_WRITE);
	bool CreateKey(HKEY hRootKey, LPCTSTR lpszRegSection);

	bool IsOpen() const;

	inline HKEY GetHandle() const { return m_hOpenKey; }

	// call this function last
	bool CloseKey();

	// read functions
	DWORD GetString(LPCTSTR lpszName, CString& strReturn) const;
	DWORD GetString(UINT nIDName, CString& strReturn) const;

	bool GetDWORD(LPCTSTR lpszName, DWORD* lpdwValue) const;
	bool GetDWORD(UINT nIDName, DWORD* lpdwValue) const;

	bool GetBoolean(LPCTSTR lpszName, bool& bReturn) const;
	bool GetBoolean(UINT nIDName, bool& bReturn) const;

	DWORD GetBinary(LPCTSTR lpszName, LPBYTE lpReturnData, DWORD dwReturnSize) const;
	DWORD GetBinary(UINT nIDName, LPBYTE lpReturnData, DWORD dwReturnSize) const;

	// write functions
	bool SetString(LPCTSTR lpszRegSection, LPCTSTR lpszValue) const;
	bool SetString(UINT nRegSection, LPCTSTR lpszValue) const;

	bool SetDWORD(LPCTSTR lpszRegSection, DWORD dwValue) const;
	bool SetDWORD(UINT nRegSection, DWORD dwValue) const;

	bool SetBinary(LPCTSTR lpszRegSection, void* pData, DWORD dwSize) const;
	bool SetBinary(UINT nRegSection, void* pData, DWORD dwSize) const;

	bool DeleteValue(LPCTSTR lpszName) const;

	// returns the name of a value
	bool FindValue(LPCTSTR lpszValue, CString& strReturn) const;
};