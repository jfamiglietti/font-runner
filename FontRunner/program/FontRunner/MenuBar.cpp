/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// MenuBar.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "MenuBar.h"

#include <boost/scoped_array.hpp>

HHOOK CMenuBar::s_hMenuHook = NULL;
CMenuBar* CMenuBar::s_pMenuBar = NULL;
UINT CMenuBar::s_uwmPopupMenu = ::RegisterWindowMessage(_T("31A97445-ADD1-45fc-A084-EBD1AADBF625"));

// CMenuBar

IMPLEMENT_DYNAMIC(CMenuBar, CToolBar)

CMenuBar::CMenuBar()
	: m_nCurrentActiveMenu(-1), m_nSubMenuCount(0),
	  m_hSelectedMenu(NULL),
	  m_bSelectedSubMenu(false),
	  m_bCues(false), m_bWndActive(true), m_bPrefix(false),
	  m_nCurrentCommandID(0xFFFF), m_pPrevFocus(NULL)
{
	SetFromSystemParams();
}

CMenuBar::~CMenuBar()
{
}

bool CMenuBar::Create(CWnd* pParent)
{
	if (!CToolBar::CreateEx(pParent, TBSTYLE_FLAT | TBSTYLE_LIST | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC))
		return false;

	s_pMenuBar = this;

	// create a button for each top-level menu
	m_menu.LoadMenu(IDR_MAINFRAME);
	UINT nMenuItemCount = m_menu.GetMenuItemCount();

	// allocate an array of buttons and zero it out
	boost::scoped_array<TBBUTTON> buttons(new TBBUTTON[nMenuItemCount]);
	memset(buttons.get(), 0, sizeof(TBBUTTON) * nMenuItemCount);
	
	CString strText;

	// populate button list from menus
	for (UINT index = 0; index < nMenuItemCount; ++index)
	{
		m_menu.GetMenuString(index, strText, MF_BYPOSITION);
		strText.Append(_T("\0"));
		int nStringIndex = GetToolBarCtrl().AddStrings(strText);

		buttons[index].iBitmap = I_IMAGENONE;
		buttons[index].idCommand = ID_MENU_COMMANDS + index;
		buttons[index].fsStyle = BTNS_AUTOSIZE | BTNS_DROPDOWN | BTNS_SHOWTEXT;
		buttons[index].iString = nStringIndex;
	}

	// add btutons
	GetToolBarCtrl().AddButtons(nMenuItemCount, buttons.get());

	return true;
}

void CMenuBar::SetPrefix(bool bPrefix)
{
	if (m_bPrefix == bPrefix)
		return;

	// ignore if cues are always displayed
	if (!bPrefix && m_bCues)
		return;

	m_bPrefix = bPrefix;

	RedrawWindow();
}

void CMenuBar::Activate(bool bActive)
{
	if (m_bWndActive == bActive)
		return;

	SetPrefix(false);

	// change the color of menu text
	m_bWndActive = bActive;
}

bool CMenuBar::ShowDropDownMenu(int nID)
{
	int nItem = nID - ID_MENU_COMMANDS;

	//m_nCurrentActiveMenu = nItem;
	CMenu* pSubMenu = m_menu.GetSubMenu(nItem);
	if (!pSubMenu)
		return false;

	// get screen coordinates of menu button so we can figure
	// out where to put the menu
	CRect rectScreen, rectItem;
	GetWindowRect(&rectScreen);
	GetItemRect(nItem, &rectItem);
	CPoint pt(rectScreen.left + rectItem.left,
			  rectScreen.top + rectItem.top + rectItem.bottom);

	// set keyboard and mouse hooks
	s_hMenuHook = ::SetWindowsHookEx(WH_MSGFILTER, OnMessageHook, NULL, ::GetCurrentThreadId());

	m_bSelectedSubMenu = false;
	m_nCurrentActiveMenu = nItem;
	m_nSubMenuCount = 0;

	int nResult = pSubMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON | TPM_RETURNCMD, pt.x, pt.y, this);

#ifdef _DEBUG
	if (nResult == 0)
	{
		DWORD dwError = ::GetLastError();
		if (dwError != ERROR_SUCCESS)
		{
			LPTSTR lpMsgBuf;
			::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
							FORMAT_MESSAGE_FROM_SYSTEM | 
							FORMAT_MESSAGE_IGNORE_INSERTS,
							NULL,
							dwError,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
							(LPTSTR)&lpMsgBuf,
							0,
							NULL);
			OutputDebugString(lpMsgBuf);
			::LocalFree(lpMsgBuf);
		}
	}
#endif

	m_bSelectedSubMenu = false;
	m_nCurrentActiveMenu = -1;
	m_nSubMenuCount = 0;

	// unhook hooks
	::UnhookWindowsHookEx(s_hMenuHook);
	s_hMenuHook = NULL;

	if (nResult)
	{
		GetParentFrame()->PostMessage(WM_COMMAND, MAKEWPARAM(nResult, 0));
		Highlight(-1);
	}

	return (nResult != 0);
}

bool CMenuBar::ShowDropDownMenuByChar(UINT nChar)
{
	CString strText;
	for (int index = 0; index < m_menu.GetMenuItemCount(); ++index)
	{
		m_menu.GetMenuString(index, strText, MF_BYPOSITION);

		int nAmp = strText.Find(_T('&'));
		CString strChar(strText[nAmp + 1]);
		CString strTest((TCHAR)nChar);
		if (nAmp != -1 && strChar.CompareNoCase(strTest) == 0)
		{
			Highlight(index);
			PostMessage(s_uwmPopupMenu, index + ID_MENU_COMMANDS);
		}
	}

	return true;
}

bool CMenuBar::OnKeyMenu(UINT nKey)
{
	if (nKey == VK_SPACE)
	{
		// window menu will be opened
		if (GetFocus()->m_hWnd == m_hWnd)
			Highlight(-1);
		else
			SetPrefix(false);
	}
	else if (nKey == 0)
	{
		// activate or deactivate menu bar
		if (GetFocus()->m_hWnd == m_hWnd)
		{
			SetPrefix(false);
			Highlight(-1);
		}
		else
		{
			SetPrefix(true);
			Highlight(0);
		}

		return true;
	}

	return false;
}

bool CMenuBar::OnMenuInput(MSG* pMsg)
{
	POINT pt;
	int nID = -1;

	if (pMsg->message == WM_MOUSEMOVE || pMsg->message == WM_LBUTTONDOWN)
	{
		pt.x = LOWORD(pMsg->lParam);
		pt.y = HIWORD(pMsg->lParam);
		ScreenToClient(&pt);
		nID = GetToolBarCtrl().HitTest(&pt);
	}

	if (pMsg->message == WM_MOUSEMOVE && nID >= 0 && nID != m_nCurrentActiveMenu)
	{
		SendMessage(WM_CANCELMODE);
		Highlight(nID);
		PostMessage(s_uwmPopupMenu, nID + ID_MENU_COMMANDS);
		return true;
	}
	else if (pMsg->message == WM_LBUTTONDOWN)
	{
		if (nID == m_nCurrentActiveMenu)
		{
			SendMessage(WM_CANCELMODE);
			return true;
		}
	}
	else if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
			case VK_LEFT:
				if (InSubMenu())
					return false;
				SendMessage(WM_CANCELMODE);
				nID = m_nCurrentActiveMenu - 1;
				if (nID == -1)
					nID = m_menu.GetMenuItemCount() - 1;
				Highlight(nID);
				PostMessage(s_uwmPopupMenu, nID + ID_MENU_COMMANDS);
				return true;

			case VK_RIGHT:
				if (m_bSelectedSubMenu)
					return false;
				SendMessage(WM_CANCELMODE);
				nID = m_nCurrentActiveMenu + 1;
				if (nID == (int)m_menu.GetMenuItemCount())
					nID = 0;
				Highlight(nID);
				PostMessage(s_uwmPopupMenu, nID + ID_MENU_COMMANDS);
				return true;

			case VK_ESCAPE:
				if (InSubMenu())
					return false;
				SendMessage(WM_CANCELMODE);
				return true;
		}
	}
	else if (pMsg->message == WM_MENUSELECT)
	{
		// keep track of which menu is shown and which item is selected
		m_hSelectedMenu = (HMENU)pMsg->lParam;
		m_bSelectedSubMenu = (HIWORD(pMsg->wParam) & MF_POPUP) != 0;
		if (!m_bSelectedSubMenu)
			m_nCurrentCommandID = LOWORD(pMsg->wParam);
		else
			return true;

		return false;
	}

	return false;
}

void CMenuBar::Highlight(int nItem)
{
	SendMessage(TB_SETHOTITEM, nItem);

	if (nItem < 0)
	{
		// restore focus
		if (m_pPrevFocus && IsWindow(m_pPrevFocus->m_hWnd))
			m_pPrevFocus->SetFocus();
		m_pPrevFocus = NULL;
	}
	else if (GetFocus() != this) // grab focus if we don't already have it
		m_pPrevFocus = SetFocus();
}

boost::signals2::connection CMenuBar::connect_to_rclick(const rclick_signal_type::slot_type& slot)
{
	return m_rclick_signal.connect(slot);
}

bool CMenuBar::InSubMenu() const
{
	return (m_nSubMenuCount > 1);
}

void CMenuBar::SetFromSystemParams()
{
	::SystemParametersInfo(SPI_GETKEYBOARDCUES, 0, &m_bCues, 0);
}

LRESULT CMenuBar::OnMessageHook(int code, WPARAM wParam, LPARAM lParam)
{
	MSG* pMsg = reinterpret_cast<MSG*>(lParam);

	if (s_pMenuBar && code == MSGF_MENU)
	{
		if (s_pMenuBar->OnMenuInput(pMsg))
			return TRUE;
	}

	return ::CallNextHookEx(s_hMenuHook, code, wParam, lParam);
}

BOOL CMenuBar::PreTranslateMessage(MSG *pMsg)
{
	// handle key messages when the menu bar has focus
	if (pMsg->message == WM_KEYDOWN || pMsg->message == WM_SYSKEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			PostMessage(s_uwmPopupMenu, GetToolBarCtrl().GetHotItem() + ID_MENU_COMMANDS);
			return TRUE;
		}
	}

	return CToolBar::PreTranslateMessage(pMsg);
}


BEGIN_MESSAGE_MAP(CMenuBar, CToolBar)
	ON_WM_KILLFOCUS()
	ON_WM_INITMENUPOPUP()
	ON_WM_SETTINGCHANGE()
	ON_NOTIFY_REFLECT(TBN_DROPDOWN, OnTBNDropDown)
	ON_NOTIFY_REFLECT(NM_CUSTOMDRAW, OnCustomDraw)
	ON_NOTIFY_REFLECT(NM_RCLICK, &CMenuBar::OnNMRClick)
	ON_REGISTERED_MESSAGE(CMenuBar::s_uwmPopupMenu, OnPopupMenu)
END_MESSAGE_MAP()



// CMenuBar message handlers

void CMenuBar::OnKillFocus(CWnd* pNewWnd) 
{
	Highlight(-1);
	SetPrefix(false);

	CToolBar::OnKillFocus(pNewWnd);
}

void CMenuBar::OnInitMenuPopup(CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu)
{
	CToolBar::OnInitMenuPopup(pPopupMenu, nIndex, bSysMenu);

	// pass message to frame window to let the update handlers do their job
	GetParentFrame()->SendMessage(WM_INITMENUPOPUP, (WPARAM)pPopupMenu->GetSafeHmenu(), MAKELONG(nIndex, bSysMenu));

	if (m_nCurrentActiveMenu != -1)
		++m_nSubMenuCount;
}

void CMenuBar::OnSettingChange(UINT /*uFlags*/, LPCTSTR /*lpszSection*/)
{
	SetFromSystemParams();
}

void CMenuBar::OnTBNDropDown(NMHDR* pNMHDR, LRESULT *pResult)
{
	NMTOOLBAR* pNMToolbar = (NMTOOLBAR*)pNMHDR;

	// these are the only toolbar dropdown notifications we're handling
	ASSERT(pNMToolbar->iItem >= ID_MENU_COMMANDS && pNMToolbar->iItem <= ID_MENU_COMMANDS_END);

	if (pNMToolbar->iItem >= ID_MENU_COMMANDS && pNMToolbar->iItem <= ID_MENU_COMMANDS_END)
		ShowDropDownMenu(pNMToolbar->iItem);

	*pResult = 0;
}

void CMenuBar::OnCustomDraw(NMHDR* pNMHDR, LRESULT *pResult)
{
	LPNMTBCUSTOMDRAW lpNMCustomDraw = (LPNMTBCUSTOMDRAW)pNMHDR;

	switch (lpNMCustomDraw->nmcd.dwDrawStage)
	{
	case CDDS_PREPAINT:
		*pResult = CDRF_NOTIFYITEMDRAW;
		break;

	case CDDS_ITEMPREPAINT:
		{
			CDC dc;
			dc.Attach(lpNMCustomDraw->nmcd.hdc);
			CRect rcRect = lpNMCustomDraw->nmcd.rc;

			int nState = lpNMCustomDraw->nmcd.uItemState;

			BOOL bHilight = nState & (CDIS_HOT | CDIS_SELECTED);

			if (bHilight)	// draw highlight rectangle
			{
				CBrush brHighlight;
				brHighlight.CreateSysColorBrush(COLOR_HIGHLIGHT);
				CBrush brMenuHilight;
				brMenuHilight.CreateSysColorBrush(COLOR_MENUHILIGHT);
				dc.FillRect(&rcRect, &brMenuHilight);
				dc.FrameRect(&rcRect, &brHighlight);
			}

			TCHAR buf[80];
			SendMessage(TB_GETBUTTONTEXT, lpNMCustomDraw->nmcd.dwItemSpec, (LPARAM)buf);

			CString strText = buf;

			// draw text
			dc.SelectObject(GetFont());
			dc.SetTextColor(GetSysColor(bHilight ? COLOR_HIGHLIGHTTEXT : COLOR_MENUTEXT));
			dc.SetBkMode(TRANSPARENT);
			dc.DrawText(strText, &rcRect, DT_VCENTER | DT_CENTER | DT_SINGLELINE
				| (m_bPrefix ? 0 : DT_HIDEPREFIX));

			dc.Detach();
			*pResult = CDRF_SKIPDEFAULT;
		}
		break;
	}
}

void CMenuBar::OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCLICK pNMClick = reinterpret_cast<LPNMCLICK>(pNMHDR);
	
	// convert to screen coordinates
	POINT pt = { pNMClick->pt.x, pNMClick->pt.y };
	ClientToScreen(&pt);

	// fire signal
	m_rclick_signal(pt.x, pt.y);

	*pResult = 0;
}


LRESULT CMenuBar::OnPopupMenu(WPARAM wParam, LPARAM /*lParam*/)
{
	ShowDropDownMenu((int)wParam);

	return 0;
}
