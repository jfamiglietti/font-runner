/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// UpdateAvailableDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "UpdateAvailableDialog.h"


// CUpdateAvailableDialog dialog

IMPLEMENT_DYNAMIC(CUpdateAvailableDialog, CDialog)

CUpdateAvailableDialog::CUpdateAvailableDialog(LPCTSTR szURL, CWnd* pParent /*=NULL*/)
	: CDialog(CUpdateAvailableDialog::IDD, pParent), m_strURL(szURL)
{
}

CUpdateAvailableDialog::~CUpdateAvailableDialog()
{
}

void CUpdateAvailableDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_UPDATEDETAILS_WEBBROWSER, m_browser);
	DDX_Control(pDX, IDOK, m_ctrlContinueButton);
}

BOOL CUpdateAvailableDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_browser.Navigate(m_strURL, NULL, NULL, NULL, NULL);

	return TRUE;
}


BEGIN_MESSAGE_MAP(CUpdateAvailableDialog, CDialog)
	ON_WM_PAINT()
END_MESSAGE_MAP()


// CUpdateAvailableDialog message handlers
BEGIN_EVENTSINK_MAP(CUpdateAvailableDialog, CDialog)
	ON_EVENT(CUpdateAvailableDialog, IDC_UPDATEDETAILS_WEBBROWSER, 104, CUpdateAvailableDialog::DownloadCompleteUpdatedetailsWebbrowser, VTS_NONE)
END_EVENTSINK_MAP()

void CUpdateAvailableDialog::DownloadCompleteUpdatedetailsWebbrowser()
{
	// show browser window and enable continue button
	m_browser.ShowWindow(SW_SHOW);
	m_ctrlContinueButton.EnableWindow(TRUE);
}

void CUpdateAvailableDialog::OnPaint()
{
	PAINTSTRUCT ps;
	CDC* pDC = BeginPaint(&ps);

	// create a big tahoma
	CFont fntDisplay;
	fntDisplay.CreateFont(38, 0, 0, 0, FW_NORMAL,
						  FALSE, FALSE, FALSE, ANSI_CHARSET,
						  OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						  FF_DONTCARE, _T("Tahoma"));

	// save the previous state of the DC and set it up
	int nSavedDC = pDC->SaveDC();
	pDC->SelectObject(&fntDisplay);
	pDC->SetTextColor(RGB(0,0,0));
	pDC->SetBkMode(TRANSPARENT);

	// figure out how big the window is
	CRect rcClient;
	GetClientRect(&rcClient);

	// get the display string
	CString strDisplay;
	strDisplay.LoadString(IDS_UPDATEAVAILABLEDIALOG_LOADING);

	CSize size = pDC->GetTextExtent(strDisplay);
	pDC->TextOut(rcClient.Width() / 2 - size.cx / 2, rcClient.Height() / 2 - size.cy / 2, strDisplay);

	// restore DC state and end painting
	pDC->RestoreDC(nSavedDC);
	EndPaint(&ps);
}