/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FileList.h"

class CFontItem;

// CFontProjectView view

class CFontProjectView : public CListView
{
	DECLARE_DYNCREATE(CFontProjectView)

protected:
	CFontProjectView();           // protected constructor used by dynamic creation
	virtual ~CFontProjectView();

// drop target ops
private:
	void DragEnter(IDataObject *pDataObject, DWORD grfKeyState, POINTL ptl, DWORD* pdwEffect);
	void DragOver(DWORD grfKeyState, POINTL ptl, DWORD* pdwEffect);
	void DragLeave();
	void Drop(IDataObject *pDataObject, DWORD grfKeyState, POINTL ptl, DWORD *pdwEffect);

// Font Runner events
private:
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnSelectedProjectChange(const std::wstring& strProjectName);
	void OnAddToProject(const std::vector<std::wstring>& list);
	void OnRemovedFromProject(const CFontItem* pFontItem);

// misc
private:
	void InsertFontProjects();
	void DragCleanup();
	void Rename(int nItem);
	void Delete(int nItem); 
	void AddFilesToProject(int nIndex, const std::vector<std::wstring>& list);

// overrides
protected:
	virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG* pMSG);

// data
private:
	int m_nDropTarget;
	bool m_bOverValidItem;
	IDropTargetHelper* m_pDropTargetHelper;
	IDataObject* m_pDraggedObject;
	CString m_strOldLabelName;
	bool m_bDisableSignals;
	bool m_bInitialUpdateDone;

	// tree view imagelist
	CImageList m_ilProjects;
	int m_nNormalIconIndex;

	// right-click item
	int	m_nRClickItem;

public:
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnPaint();
	afx_msg void OnDestroy();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnClick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnProjectAddButton();
	afx_msg void OnProjectDeleteButton();
	afx_msg void OnProjectRenameButton();
	afx_msg void OnUpdateProjectAddButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProjectDeleteButton(CCmdUI* pCmdUI);
	afx_msg void OnUpdateProjectRenameButton(CCmdUI* pCmdUI);
	afx_msg void OnLvnItemChanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnFontProjectMenuRename();
	afx_msg void OnFontProjectMenuDelete();
};

struct AddProjectsToList
{
	AddProjectsToList(CListCtrl& list, int nIcon)
		: m_list(list),
		  m_nIcon(nIcon)
	{
	}

	void operator()(const projectlist_type::value_type& value) const
	{
		CString strFormatString, strDisplayText;
		strFormatString.LoadString(IDS_FONTPROJECTVIEW_DISPLAYSTRING);
		strDisplayText.Format(strFormatString, value.first, value.second->size());
		int nItem = m_list.InsertItem(0, strDisplayText, m_nIcon);
		m_list.SetItemData(nItem, (DWORD_PTR)new CString(value.first));
		m_list.SetColumnWidth(0, LVSCW_AUTOSIZE);
	}

	CListCtrl& m_list;
	int m_nIcon;
};
