/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/shared_ptr.hpp>

// a single history item
struct HistoryItem
{
	enum eType
	{
		kType_Folder,
		kType_Project
	};
	eType nType;
	CString strName;
};

// The history list class
class CHistoryList
{
public:
	CHistoryList();
	virtual ~CHistoryList();

public:
	// Appends an item to the end of the history list.
	// Any history items after the current one are erased.
	void Add(HistoryItem::eType nType, const LPCTSTR szName);

	// Clears the entire history list
	void Reset();

	// Moves to the beginning of the list and returns the first item
	const HistoryItem* GoToBeginning();

	// Moves to the end of the list and returns the last item
	const HistoryItem* GoToEnd();

	// Indicates whether the list is empty
	bool IsEmpty() const;

	// Indicates whether there is an item before the current one
	bool CanGoBack() const;

	// Indicates whether there is an item after the current one
	bool CanGoForward() const;

	// Moves back one item
	const HistoryItem* GoBack();

	// Moves forward one item
	const HistoryItem* GoForward();

	// Deletes current item and everything after it and moves back one item
	const HistoryItem* RemoveCurrent();

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;
};
