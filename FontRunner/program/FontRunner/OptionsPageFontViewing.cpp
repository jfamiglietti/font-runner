/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// OptionsPageFontViewing.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "ProgramOptions.h"
#include "FontRunnerSignals.h"
#include "OptionsPageFontViewing.h"

#include "help/Help.h"
#include <algorithm>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COptionsPageFontViewing dialog

IMPLEMENT_DYNAMIC(COptionsPageFontViewing, CPropertyPage)

COptionsPageFontViewing::COptionsPageFontViewing()
	: CPropertyPage(COptionsPageFontViewing::IDD),
	  m_hInfoIcon(NULL),
	  m_nFontListDropOption(0)
{
	m_psp.dwFlags |= PSP_HASHELP;
}

COptionsPageFontViewing::~COptionsPageFontViewing()
{
	if (m_hInfoIcon)
		::DestroyIcon(m_hInfoIcon);
}

BOOL COptionsPageFontViewing::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// setup combos
	SetupRenderingCombo(&m_ctrlPreviewRenderingCombo);
	SetupRenderingCombo(&m_ctrlFontMapRenderingCombo);
	SetupRenderingCombo(&m_ctrlCharacterDetailRenderingCombo);

	// set values
	m_strFontListPreviewEdit = pOptions->GetFontListText();
	m_ctrlPreviewRenderingCombo.SetCurSel(pOptions->GetFontListRenderingOption());

	m_ctrlFontMapRenderingCombo.SetCurSel(pOptions->GetFontMapRenderingOption());
	m_ctrlCharacterDetailRenderingCombo.SetCurSel(pOptions->GetCharDetailRenderingOption());
	m_nFontListDropOption = (int)pOptions->GetFontListDropOption();

	// load info icon
	if (!m_hInfoIcon)
		m_hInfoIcon = ::LoadIcon(NULL, IDI_INFORMATION);
	m_ctrlInfoIconStatic.SetIcon(m_hInfoIcon);

	// set smoothing message
	SetupSmoothingMessage();

	UpdateData(false);
	return true;
}

int COptionsPageFontViewing::OnApply()
{
	UpdateData(true);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// get values
	pOptions->SetFontListText(m_strFontListPreviewEdit);
	pOptions->SetFontListRenderingOption((CProgramOptions::eRenderingOption)m_ctrlPreviewRenderingCombo.GetCurSel());

	pOptions->SetFontMapRenderingOption((CProgramOptions::eRenderingOption)m_ctrlFontMapRenderingCombo.GetCurSel());
	pOptions->SetCharDetailRenderingOption((CProgramOptions::eRenderingOption)m_ctrlCharacterDetailRenderingCombo.GetCurSel());
	pOptions->SetFontListDropOption((DWORD)m_nFontListDropOption);

	// fire options change
	theApp.GetSignals()->Fire_OptionsChange();

	return CPropertyPage::OnApply();
}

void COptionsPageFontViewing::SetupRenderingCombo(CComboBox* pCombo)
{
	CString strOption;

	int nIndex = 0;
	strOption.LoadString(IDS_OPTIONSDIALOG_RENDERING_DEFAULT);
	pCombo->InsertString(nIndex++, strOption);

	strOption.LoadString(IDS_OPTIONSDIALOG_RENDERING_SMOOTHING);
	pCombo->InsertString(nIndex++, strOption);

	// cleartype not available in Win2K
	OSVERSIONINFO  osvi;
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	::GetVersionEx(&osvi);

	// looking for a version higher than 5.0 (5.0 is Win2K)
	if (osvi.dwMajorVersion > 5 ||
		(osvi.dwMajorVersion == 5 && osvi.dwMinorVersion > 0))
	{
		strOption.LoadString(IDS_OPTIONSDIALOG_RENDERING_CLEARTYPE);
		pCombo->InsertString(nIndex++, strOption);
	}
}

void COptionsPageFontViewing::SetupSmoothingMessage()
{
	BOOL bSmoothing = FALSE;
	UINT nAppendID = IDS_OPTIONSDIALOG_NOSMOOTHING;
	if (::SystemParametersInfo(SPI_GETFONTSMOOTHING, 0, &bSmoothing, 0) && bSmoothing)
	{
		UINT nSmoothingType = 0;
		if (::SystemParametersInfo(SPI_GETFONTSMOOTHINGTYPE, 0, &nSmoothingType, 0))
		{
			if (nSmoothingType == FE_FONTSMOOTHINGSTANDARD)
				nAppendID = IDS_OPTIONSDIALOG_STANDARD;
			else if (nSmoothingType == FE_FONTSMOOTHINGCLEARTYPE)
				nAppendID = IDS_OPTIONSDIALOG_CLEARTYPE;
		}
	}

	// make string
	CString strMessage, strAppend;
	strMessage.LoadString(IDS_OPTIONSDIALOG_RENDERINGSETTING);
	strAppend.LoadString(nAppendID);
	strMessage.Append(strAppend);

	m_ctrlCurrentRenderingSettingStatic.SetWindowText(strMessage);
}

void COptionsPageFontViewing::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_FONTLISTPREVIEW_EDIT, m_strFontListPreviewEdit);
	DDX_Control(pDX, IDC_FONTLISTPREVIEWRENDERING_COMBO, m_ctrlPreviewRenderingCombo);
	DDX_Control(pDX, IDC_FONTMAPRENDERING_COMBO, m_ctrlFontMapRenderingCombo);
	DDX_Control(pDX, IDC_CHARDETAILRENDERING_COMBO, m_ctrlCharacterDetailRenderingCombo);
	DDX_Radio(pDX, IDC_DROPCOPY_RADIO, m_nFontListDropOption);
	DDX_Control(pDX, IDC_CURRENTRENDERINGSETTING_STATIC, m_ctrlCurrentRenderingSettingStatic);
	DDX_Control(pDX, IDC_INFOICON_STATIC, m_ctrlInfoIconStatic);
}

BEGIN_MESSAGE_MAP(COptionsPageFontViewing, CPropertyPage)
	ON_WM_SETTINGCHANGE()
	ON_EN_CHANGE(IDC_FONTLISTPREVIEW_EDIT, &COptionsPageFontViewing::OnEnChangeFontlistpreviewEdit)
	ON_CBN_SELCHANGE(IDC_FONTLISTPREVIEWRENDERING_COMBO, &COptionsPageFontViewing::OnCbnSelchangeFontlistpreviewrenderingCombo)
	ON_CBN_SELCHANGE(IDC_FONTMAPRENDERING_COMBO, &COptionsPageFontViewing::OnCbnSelchangeFontmaprenderingCombo)
	ON_CBN_SELCHANGE(IDC_CHARDETAILRENDERING_COMBO, &COptionsPageFontViewing::OnCbnSelchangeChardetailrenderingCombo)
	ON_BN_CLICKED(IDC_DROPCOPY_RADIO, &COptionsPageFontViewing::OnBnClickedDropcopyRadio)
	ON_BN_CLICKED(IDC_DROPOPEN_RADIO, &COptionsPageFontViewing::OnBnClickedDropopenRadio)
	ON_NOTIFY_RANGE(PSN_HELP, 0x0000, 0xFFFF, OnHelpButton)
END_MESSAGE_MAP()


// COptionsPageFontViewing message handlers

void COptionsPageFontViewing::OnSettingChange(UINT /*uFlags*/, LPCTSTR /*lpszSection*/)
{
	SetupSmoothingMessage();
}

void COptionsPageFontViewing::OnEnChangeFontlistpreviewEdit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontViewing::OnCbnSelchangeFontlistpreviewrenderingCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontViewing::OnCbnSelchangeFontmaprenderingCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontViewing::OnCbnSelchangeChardetailrenderingCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageFontViewing::OnBnClickedDropcopyRadio()
{
	int nOldValue = m_nFontListDropOption;
	UpdateData(true);
	if (m_nFontListDropOption != nOldValue)
		SetModified(true);
}

void COptionsPageFontViewing::OnBnClickedDropopenRadio()
{
	int nOldValue = m_nFontListDropOption;
	UpdateData(true);
	if (m_nFontListDropOption != nOldValue)
		SetModified(true);
}

void COptionsPageFontViewing::OnHelpButton(UINT /*id*/, NMHDR* /*pNotifyStruct*/, LRESULT* /*result*/)
{
	HtmlHelp(HIDP_OPTIONS_FONTVIEWING, HH_HELP_CONTEXT);
}

void COptionsPageFontViewing::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	AfxGetApp()->HtmlHelp(dwData, nCmd);
}
