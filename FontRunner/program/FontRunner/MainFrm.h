/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// MainFrm.h : interface of the CMainFrame class
//
#pragma once

#include "LocationCombo.h"
#include "FontDetailFrame.h"
#include "FontRunnerSplitter.h"
#include "FontRunnerReBar.h"
#include "ThemeSupport.h"
#include <boost/scoped_ptr.hpp>

class CMenuBar;
class CPrettyToolbar;
class CHistoryList;

class CMainFrame : public CFrameWnd, public CThemeSupport
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

public:
	bool m_bRefreshing;

// Operations
public:
	inline CSplitterWnd* GetMainSplitter() { return &m_wndSplitterMain; }
	inline CSplitterWnd* GetTreeViewSplit() { return &m_wndTreeViewSplit; }
	inline CSplitterWnd* GetFontViewSplit() { return &m_wndFontViewSplit; }

	inline CStatusBar* GetStatusBar() { return &m_wndStatusBar; }

	void RemoveCurrentHistoryItem();

// Overrides
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// constants
public:
	enum
	{
		kToolbarButtonSize = 24
	};

private:  // control bar embedded members
	CStatusBar				m_wndStatusBar;
	boost::scoped_ptr<CMenuBar>			m_pwndMenuBar;
	boost::scoped_ptr<CPrettyToolbar>	m_pwndToolbar;
	boost::scoped_ptr<CPrettyToolbar>	m_pwndFontListToolbar;
	CComboBox				m_wndFontListSizeCombo;
	CLocationCombo			m_wndLocationCombo;
	CFontRunnerReBar		m_wndRebar;
	CToolTipCtrl			m_ctrlFavoritesBalloon;
	TOOLINFO				m_tiFavoritesBalloon;
	UINT_PTR				m_nFavoritesBalloonTimer;
	const UINT_PTR			m_nFavoritesBalloonTimerID;
	const unsigned int		m_cnNumBands;

	// pointers to frames
	CFontDetailFrame* m_pPreviewFrame;

// for favorites menu population
private:
	CMenu* m_pFavoritesMenu;
	HDC m_hdcFavoritesMenu;
	UINT m_nFavoritesMenuID;
	bool m_bFoundCurrentFavorite;
	std::wstring m_strCurrentFavoriteFolder;
	boost::scoped_ptr<CHistoryList> m_pHistoryList;
	bool m_bHistoryNav;
	
	CImageList m_ilPrintMenu;

protected:
	CSize			m_sizWindow;
	bool			m_bMaximized, m_bMinimized;
	bool			m_bMaximizedOnStartup, m_bSysCommandRestore;
	int				m_nDetailPaneHeight;
	bool			m_bMainToolBarVisible;
	UINT			m_nMainToolBarIndex;
	bool			m_bLocationBarVisible;
	UINT			m_nLocationBarIndex;
	bool			m_bFontListPreviewSizeToolbarVisible;
	UINT			m_nFontListPreviewSizeToolbarIndex;
	bool			m_bFontSharingButtonVisible;
	UINT			m_nFontSharingButtonIndex;
	int				m_nCurrentActiveMenu;
	bool			m_bTrackingHotMenuItem;

public:		// splitter windows
	CFontRunnerSplitter	m_wndSplitterMain;
	CFontRunnerSplitter	m_wndTreeViewSplit;
	CFontRunnerSplitter	m_wndFontViewSplit;

protected:
	bool CreateMenuBar();
	bool CreateMainToolbar();
	bool InitFavoritesBalloon();
	bool ShowFavoritesBalloon(const CString* pstrFolder);
	void PopFavoritesBalloon();
	void GotoURL(UINT nURLID);
	void OnFontRunnerShutdown();
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnSelectedProjectChange(const std::wstring& strProjectName);
	void OnPrintDropDown();
	void OnFavoritesDropDown();
	void OnFavoritesEnum(const std::wstring& str);
	void UpdateBandColors();

	// splitter listeners
	void OnTrackRow_MainSplitter(int y, int row);
	void OnTrackCol_MainSplitter(int x, int col);
	void OnTrackRow_TreeSplitter(int y, int row);
	void OnTrackCol_TreeSplitter(int x, int col);
	void OnTrackRow_DetailSplitter(int y, int row);
	void OnTrackCol_DetailSplitter(int x, int col);

	// toolbar listeners
	void OnToolbarRClick(unsigned int x, unsigned int y);

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	
public:
	afx_msg LRESULT OnAreYouFontRunner(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu);
	afx_msg void OnMenuBarCommands(UINT nCommandID);
	afx_msg void OnViewRefresh();
	afx_msg void OnViewInstalled();
	afx_msg void OnHelpContentsandindexf1();
	afx_msg void OnUpdateStatusBarPane(CCmdUI *pCmdUI);
	afx_msg void OnPreviewFgcolor();
	afx_msg void OnPreviewBgcolor();
	afx_msg void OnPreviewBold();
	afx_msg void OnPreviewUnderline();
	afx_msg void OnPreviewItalic();
	afx_msg void OnUpdatePreviewBold(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePreviewUnderline(CCmdUI *pCmdUI);
	afx_msg void OnUpdatePreviewItalic(CCmdUI *pCmdUI);
	afx_msg void OnViewBack();
	afx_msg void OnViewForward();
	afx_msg void OnViewAutoInstall();
	afx_msg void OnUpdateViewAutoInstall(CCmdUI* pCmdUI);
	afx_msg void OnViewMainToolBar();
	afx_msg void OnUpdateViewMainToolBar(CCmdUI* pCmdUI);
	afx_msg void OnViewLocationBar();
	afx_msg void OnUpdateViewLocationBar(CCmdUI* pCmdUI);
	afx_msg void OnViewFontListPreviewSize();
	afx_msg void OnUpdateViewFontListPreviewSize(CCmdUI* pCmdUI);
	afx_msg void OnViewFontSharingButton();
	afx_msg void OnUpdateViewFontSharingButton(CCmdUI* pCmdUI);
	afx_msg void OnToolbarLockthetoolbars();
	afx_msg void OnUpdateToolbarLockthetoolbars(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewBack(CCmdUI *pCmdUI);
	afx_msg void OnUpdateViewForward(CCmdUI *pCmdUI);
	afx_msg void OnViewUpFolder();
	afx_msg void OnUpdateFontListSize(CCmdUI* pCmdUI);
	afx_msg void OnUpdateViewUpFolder(CCmdUI *pCmdUI);
	afx_msg void OnUpdateFavorites(CCmdUI *pCmdUI);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSizing(UINT fwSide, LPRECT pRect);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnWindowPosChanging(WINDOWPOS* lpwndpos);
	afx_msg void OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult);
	afx_msg void OnAddFavorite();
	afx_msg void OnUpdateAddFavorite(CCmdUI* pCmdUI);
	afx_msg void OnManageFavorites();
	afx_msg void OnFavoritesMenu(UINT nID);
	afx_msg void OnEditSelectAll();
	afx_msg void OnFontListSizeSelChange();
	afx_msg void OnFontListSizeEditChange();
	afx_msg void OnHelpContentsAndIndex();
	afx_msg void OnHelpWhatsNew();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnNotifyFavoritesBalloonPop(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnSysColorChange();
	afx_msg LRESULT OnThemeChanged();
};


