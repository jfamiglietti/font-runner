/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// OptionsPagePrinting.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "OptionsPagePrinting.h"
#include "ProgramOptions.h"
#include "FontRunnerSignals.h"
#include "help/Help.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COptionsPagePrinting dialog

IMPLEMENT_DYNAMIC(COptionsPagePrinting, CPropertyPage)

COptionsPagePrinting::COptionsPagePrinting()
	: CPropertyPage(COptionsPagePrinting::IDD)
	, m_nPrintSizeEdit(0)
{
	m_psp.dwFlags |= PSP_HASHELP;
}

COptionsPagePrinting::~COptionsPagePrinting()
{
}

BOOL COptionsPagePrinting::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// printing options
	m_nPrintSizeEdit = (BYTE)pOptions->GetPrintCharSize();
	m_strPrintSampleLine1Edit = pOptions->GetPrintSampleTextLine1();
	m_strPrintSampleLine2Edit = pOptions->GetPrintSampleTextLine2();

	UpdateData(false);

	return true;
}

BOOL COptionsPagePrinting::OnApply()
{
	// read data from controls
	UpdateData(true);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// printing options
	pOptions->SetPrintCharSize(m_nPrintSizeEdit);
	pOptions->SetPrintSampleTextLine1(m_strPrintSampleLine1Edit);
	pOptions->SetPrintSampleTextLine2(m_strPrintSampleLine2Edit);

	// fire options change
	theApp.GetSignals()->Fire_OptionsChange();

	return CPropertyPage::OnApply();
}

void COptionsPagePrinting::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_PRINTSIZE_EDIT, m_nPrintSizeEdit);
	DDX_Control(pDX, IDC_PRINTSIZE_EDIT, m_ctrlPrintSizeEdit);
	DDX_Text(pDX, IDC_PRINTSAMPLELINE1_EDIT, m_strPrintSampleLine1Edit);
	DDX_Text(pDX, IDC_PRINTSAMPLELINE2_EDIT, m_strPrintSampleLine2Edit);
}


BEGIN_MESSAGE_MAP(COptionsPagePrinting, CPropertyPage)
	ON_EN_CHANGE(IDC_PRINTSIZE_EDIT, OnEnChangePrintsizeEdit)
	ON_EN_CHANGE(IDC_PRINTSAMPLELINE1_EDIT, OnEnChangePrintsampleline1Edit)
	ON_EN_CHANGE(IDC_PRINTSAMPLELINE2_EDIT, OnEnChangePrintsampleline2Edit)
	ON_BN_CLICKED(IDC_DEFAULTTEXT_BUTTON, OnBnClickedDefaulttextButton)
	ON_NOTIFY_RANGE(PSN_HELP, 0x0000, 0xFFFF, OnHelpButton)
END_MESSAGE_MAP()


// COptionsPagePrinting message handlers

void COptionsPagePrinting::OnEnChangePrintsizeEdit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPagePrinting::OnEnChangePrintsampleline1Edit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPagePrinting::OnEnChangePrintsampleline2Edit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPagePrinting::OnBnClickedDefaulttextButton()
{
	// set default strings
	m_strPrintSampleLine1Edit.LoadString(IDS_SAMPLEPRINT_LINE1);
	m_strPrintSampleLine2Edit.LoadString(IDS_SAMPLEPRINT_LINE2);
	UpdateData(false);

	// notify property sheet of change
	SetModified(true);
}

void COptionsPagePrinting::OnHelpButton(UINT /*id*/, NMHDR* /*pNotifyStruct*/, LRESULT* /*result*/)
{
	HtmlHelp(HIDP_OPTIONS_PRINTING, HH_HELP_CONTEXT);
}

void COptionsPagePrinting::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	AfxGetApp()->HtmlHelp(dwData, nCmd);
}
