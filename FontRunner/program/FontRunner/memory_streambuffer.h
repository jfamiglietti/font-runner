/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <streambuf>

namespace crux
{
	// A streambuf specialized for a memory buffer
	template <class T> 
	class memory_streambuf : public std::basic_streambuf<T>
	{
	public:
		memory_streambuf(T* buffer, size_t size) :
			  std::basic_streambuf<T>() // init base class
		{
			// call basic_streambuf's protected initialization function
			setg(buffer, buffer, (T*)(((char*)buffer) + size));
			setp(buffer, buffer, (T*)(((char*)buffer) + size));
		}
	};
}
