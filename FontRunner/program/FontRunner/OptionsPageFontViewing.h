/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxwin.h"


// COptionsPageFontViewing dialog

class COptionsPageFontViewing : public CPropertyPage
{
	DECLARE_DYNAMIC(COptionsPageFontViewing)

public:
	COptionsPageFontViewing();
	virtual ~COptionsPageFontViewing();

private:
	void SetupRenderingCombo(CComboBox* pCombo);
	void SetupSizeCombo(CComboBox* pCombo);
	void SetupSmoothingMessage();

private:
	HICON m_hInfoIcon;

// Dialog Data
	enum { IDD = IDD_FONTVIEWING_PAGE };
	CString m_strFontListPreviewEdit;
	CComboBox m_ctrlPreviewRenderingCombo;
	CComboBox m_ctrlFontMapRenderingCombo;
	CComboBox m_ctrlCharacterDetailRenderingCombo;
	CStatic m_ctrlCurrentRenderingSettingStatic;
	CStatic m_ctrlInfoIconStatic;

// overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);

// message map
	DECLARE_MESSAGE_MAP()
	afx_msg void OnSettingChange(UINT uFlags, LPCTSTR lpszSection);
	afx_msg void OnEnChangeFontlistpreviewEdit();
	afx_msg void OnCbnSelchangeFontlistpreviewrenderingCombo();
	afx_msg void OnCbnSelchangeFontmaprenderingCombo();
	afx_msg void OnCbnSelchangeChardetailrenderingCombo();
	afx_msg void OnHelpButton(UINT id, NMHDR* pNotifyStruct, LRESULT* result);
public:
	int m_nFontListDropOption;
	afx_msg void OnBnClickedDropcopyRadio();
	afx_msg void OnBnClickedDropopenRadio();
};
