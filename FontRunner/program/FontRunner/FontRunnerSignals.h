/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <boost/signals2.hpp>
#include <boost/bind.hpp>

class CFontItem;

// the whole project doesn't need to be dependent on the OpenTypeData header
// so prototype the class
namespace CruxTechnologies
{
	class COpenTypeData;
}

// a combiner for boolean return values
struct andresults
{
	typedef bool result_type;

	template <typename InputIterator>
	bool operator()(InputIterator first, InputIterator last) const
	{
		// if no slots, return false
		if (first == last)
			return false;

		// just one false results in a false result
		while (first != last)
		{
			if (!*first++)
				return false;
		}

		return true;
	}
};

// a combiner for adding the results of handlers
template <typename T> // T = a numeric type
struct addresults
{
	typedef T result_type;

	template <typename InputIterator>
	T operator()(InputIterator first, InputIterator last) const
	{
		if (first == last)
			return 0;

		T result = 0;
		while (first != last)
			result += *first++;

		return result;
	}
};

// generic signals
typedef boost::signals2::signal<void (unsigned int, unsigned int)> rclick_signal_type;

class CFontRunnerSignals
{
public:
	CFontRunnerSignals();
	virtual ~CFontRunnerSignals();

// signal definitions
private:
	// sent when currently selected folder changes
	typedef boost::signals2::signal<void (const CString&, const CString*)> SelectedFolderChange_SignalType;

	// send when currently selected project changes
	typedef boost::signals2::signal<void (const std::wstring&)> SelectedProjectChange_SignalType;

	// sent when the font that is currently selected changes
	typedef boost::signals2::signal<void (const CFontItem*)> SelectedFontChange_SignalType;

	// sent when a character is selected in the font map
	typedef boost::signals2::signal<void (wchar_t, const wchar_t*)> SelectedCodeChange_SignalType;

	// when the preview size changes
	typedef boost::signals2::signal<void (unsigned short)> SizeChange_SignalType;

	// when the temp auto install option changes
	typedef boost::signals2::signal<void (bool)> TempAutoInstall_SignalType;

	// when the program is closing down
	typedef boost::signals2::signal<void (void)> Shutdown_SignalType;

	// when its time to refresh
	typedef boost::signals2::signal<void (void)> Refresh_SignalType;

	// folder up enable
	typedef boost::signals2::signal<bool (void), andresults> FolderUpQuery_SignalType;

	// folder up
	typedef boost::signals2::signal<void (void)> FolderUp_SignalType;

	// program options change
	typedef boost::signals2::signal<void (void)> OptionsChange_SignalType;

	// navigate to folder, but don't change selection
	typedef boost::signals2::signal<void (const CString&)> NavigateToFolder_SignalType;

	// display character
	typedef boost::signals2::signal<void (const TCHAR)> PreviewRTFCharacter_SignalType;

	// when the Font Manager wants to un-manage a font
	typedef boost::signals2::signal<int (const CFontItem*), addresults<int> > UnManageFontItem_SignalType;

	// when a font is completely dereferenced and about to be destroyed
	typedef boost::signals2::signal<void (const CFontItem*)> FontDereferenced_SignalType;

	// when we want to add fonts to a project
	typedef boost::signals2::signal<void (const std::vector<std::wstring>&)> AddToProject_SignalType;

	// sent when a font is removed from a font project
	typedef boost::signals2::signal<void (const CFontItem*)> RemovedFromProject_SignalType;

	// when a font file is deleted (or moved)
	typedef boost::signals2::signal<void (const CFontItem*)> FontFileDeleted_SignalType;

	// when the font detail colors change
	typedef boost::signals2::signal<void (COLORREF, bool)> Color_SignalType;

	// when a the color palette closes
	typedef boost::signals2::signal<void ()> ColorPaletteClosed_SignalType;

	// sent to request a color palette open
	typedef boost::signals2::signal<void (bool)> OpenColorPalette_SignalType;

	// when we want to add files to the main view
	typedef boost::signals2::signal<void (const std::vector<std::wstring>&)> AddToMainFontListView_SignalType;

	// clear locations bar history
	typedef boost::signals2::signal<void ()> ClearLocationBar_SignalType;

	// viewing system fonts
	typedef boost::signals2::signal<void (bool)> ViewingSystemFonts_SignalType;

public:
	void Fire_SelectedFolderChange(const CString& strNewFolder, const CString* pstrFontSelect = NULL) const;
	boost::signals2::connection ConnectTo_SelectedFolderChange(const SelectedFolderChange_SignalType::slot_type& slot);

	void Fire_SelectedProjectChange(const std::wstring& strProjectName) const;
	boost::signals2::connection ConnectTo_SelectedProjectChange(const SelectedProjectChange_SignalType::slot_type& slot);

	void Fire_SelectedFontChange(const CFontItem* pData) const;
	boost::signals2::connection ConnectTo_SelectedFontChange(const SelectedFontChange_SignalType::slot_type& slot);

	void Fire_SelectedCodeChange(wchar_t charCode, const wchar_t* pszDescription) const;
	boost::signals2::connection ConnectTo_SelectedCodeChange(const SelectedCodeChange_SignalType::slot_type& slot);

	void Fire_PreviewSizeChange(unsigned short nSize) const;
	boost::signals2::connection ConnectTo_PreviewSizeChange(const SizeChange_SignalType::slot_type& slot);

	void Fire_TempAutoInstallChange(bool bAutoInstall) const;
	boost::signals2::connection ConnectTo_TempAutoInstallChange(const TempAutoInstall_SignalType::slot_type& slot);

	void Fire_Shutdown() const;
	boost::signals2::connection ConnectTo_Shutdown(const Shutdown_SignalType::slot_type& slot);

	void Fire_Refresh() const;
	boost::signals2::connection ConnectTo_Refresh(const Refresh_SignalType::slot_type& slot);

	bool Query_FolderUp() const;
	boost::signals2::connection ConnectTo_FolderUpQuery(const FolderUpQuery_SignalType::slot_type& slot);

	void Fire_FolderUp() const;
	boost::signals2::connection ConnectTo_FolderUp(const FolderUp_SignalType::slot_type& slot);

	void Fire_OptionsChange() const;
	boost::signals2::connection ConnectTo_OptionsChange(const OptionsChange_SignalType::slot_type& slot);

	void Fire_NavigateToFolder(const CString& strFolder) const;
	boost::signals2::connection ConnectTo_NavigateToFolder(const NavigateToFolder_SignalType::slot_type& slot);

	void Fire_PreviewRTFCharacter(TCHAR ch) const;
	boost::signals2::connection ConnectTo_PreviewRTFCharacter(const PreviewRTFCharacter_SignalType::slot_type& slot);

	int Fire_UnManageFontItem(const CFontItem* pFontItem) const;
	boost::signals2::connection ConnectTo_UnManageFontItem(const UnManageFontItem_SignalType::slot_type& slot);

	void Fire_FontDereferenced(const CFontItem* pFontItem) const;
	boost::signals2::connection ConnectTo_FontDereferenced(const FontDereferenced_SignalType::slot_type& slot);

	void Fire_AddToProject(const std::vector<std::wstring>& list) const;
	boost::signals2::connection ConnectTo_AddToProject(const AddToProject_SignalType::slot_type& slot);

	void Fire_RemovedFromProject(const CFontItem* pFontItem) const;
	boost::signals2::connection ConnectTo_RemovedFromProject(const RemovedFromProject_SignalType::slot_type& slot);

	void Fire_FontFileDeleted(const CFontItem* pFontItem) const;
	boost::signals2::connection ConnectTo_FontFileDeleted(const FontFileDeleted_SignalType::slot_type& slot);

	void Fire_DetailSizeChange(unsigned short nSize) const;
	boost::signals2::connection ConnectTo_DetailSizeChange(const SizeChange_SignalType::slot_type& slot);

	void Fire_FontMapSizeChange(unsigned short nSize) const;
	boost::signals2::connection ConnectTo_FontMapSizeChange(const SizeChange_SignalType::slot_type& slot);

	void Fire_DetailColorChange(COLORREF color, bool bForeground) const;
	boost::signals2::connection ConnectTo_DetailColorChange(const Color_SignalType::slot_type& slot);

	void Fire_ColorPaletteClosed() const;
	boost::signals2::connection ConnectTo_ColorPaletteClosed(const ColorPaletteClosed_SignalType::slot_type& slot);

	void Fire_OpenColorPalette(bool bForeground) const;
	boost::signals2::connection ConnectTo_OpenColorPalette(const OpenColorPalette_SignalType::slot_type& slot);

	void Fire_AddToMainFontListView(const std::vector<std::wstring>& list) const;
	boost::signals2::connection ConnectTo_AddToMainFontListView(const AddToMainFontListView_SignalType::slot_type& slot);

	void Fire_ClearLocationBar() const;
	boost::signals2::connection ConnectTo_ClearLocationBar(const ClearLocationBar_SignalType::slot_type& slot);

	void Fire_ViewingSystemFonts(bool bSystemFonts) const;
	boost::signals2::connection ConnectTo_ViewingSystemFonts(const ViewingSystemFonts_SignalType::slot_type& slot);

private:
	SelectedFolderChange_SignalType	m_sigSelectedFolderChange;
	SelectedProjectChange_SignalType m_sigSelectedProjectChange;
	SelectedFontChange_SignalType	m_sigSelectedFontChange;
	SelectedCodeChange_SignalType	m_sigSelectedCodeChange;
	SizeChange_SignalType			m_sigPreviewSizeChange;
	TempAutoInstall_SignalType		m_sigTempAutoInstall;
	Shutdown_SignalType				m_sigShutdown;
	Refresh_SignalType				m_sigRefresh;
	FolderUpQuery_SignalType		m_sigQueryFolderUp;
	FolderUp_SignalType				m_sigFolderUp;
	OptionsChange_SignalType		m_sigOptionsChange;
	NavigateToFolder_SignalType		m_sigNavigateToFolder;
	PreviewRTFCharacter_SignalType	m_sigPreviewRTFCharacter;
	UnManageFontItem_SignalType		m_sigUnManageFontItem;
	FontDereferenced_SignalType 	m_sigFontDereferenced;
	AddToProject_SignalType			m_sigAddToProject;
	RemovedFromProject_SignalType	m_sigRemovedFromProject;
	FontFileDeleted_SignalType		m_sigFontFileDeleted;
	SizeChange_SignalType			m_sigDetailSizeChange;
	SizeChange_SignalType			m_sigFontMapSizeChange;
	Color_SignalType				m_sigDetailColor;
	ColorPaletteClosed_SignalType	m_sigColorPaletteClosed;
	OpenColorPalette_SignalType		m_sigOpenColorPalette;
	AddToMainFontListView_SignalType m_sigAddToMainFontListView;
	ClearLocationBar_SignalType		m_sigClearLocationBar;
	ViewingSystemFonts_SignalType	m_sigViewingSystemFonts;
};
