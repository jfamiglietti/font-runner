/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "StdAfx.h"
#include "DropSourceHandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CDropSourceHandler::CDropSourceHandler()
	: m_lRefCount(0)
{
}

CDropSourceHandler::~CDropSourceHandler()
{
}

//
// IUnknown members
//
HRESULT __stdcall CDropSourceHandler::QueryInterface(REFIID iid, void ** ppvObject)
{
	// check to see what interface has been requested
	if (iid == IID_IDropSource || iid == IID_IUnknown)
	{
		AddRef();
		*ppvObject = this;
		return S_OK;
	}
	else
	{
		*ppvObject = 0;
		return E_NOINTERFACE;
	}
}
ULONG __stdcall CDropSourceHandler::AddRef()
{
	return ::InterlockedIncrement(&m_lRefCount);
}

ULONG __stdcall CDropSourceHandler::Release()
{
	LONG nCount = ::InterlockedDecrement(&m_lRefCount);

	if (nCount == 0)
		delete this;

	return nCount;
}

//
// IDropSource members
//
HRESULT __stdcall CDropSourceHandler::QueryContinueDrag(BOOL fEscapePressed, DWORD grfKeyState)
{
	// if the <Escape> key has been pressed since the last call, cancel the drop
	if (fEscapePressed == TRUE)
		return DRAGDROP_S_CANCEL;

	// if the <LeftMouse> button has been released, then do the drop!
	if ((grfKeyState & MK_LBUTTON) == 0)
		return DRAGDROP_S_DROP;

	// continue with the drag-drop
	return S_OK;
}

HRESULT __stdcall CDropSourceHandler::GiveFeedback(DWORD /*dwEffect*/)
{
	return DRAGDROP_S_USEDEFAULTCURSORS;
}
