/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// OptionsPageGeneral.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "ProgramOptions.h"
#include "FontRunnerSignals.h"
#include "OptionsPageGeneral.h"

#include "FontProjects.h"
#include "WinToolbox.h"
#include "help/Help.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// COptionsPageGeneral dialog

IMPLEMENT_DYNAMIC(COptionsPageGeneral, CPropertyPage)
COptionsPageGeneral::COptionsPageGeneral()
	: CPropertyPage(COptionsPageGeneral::IDD), m_nLocationsLimitEdit(20)
{
	m_psp.dwFlags |= PSP_HASHELP;
}

COptionsPageGeneral::~COptionsPageGeneral()
{
}

BOOL COptionsPageGeneral::OnInitDialog()
{
	CPropertyPage::OnInitDialog();

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// initialize combos
	InitProjectCombo();
	InitStartupCombo();

	// startup options
	m_strThisFolderEdit = pOptions->GetDefaultFolder();

	m_btnUseCurrentFolder.EnableWindow(!CFontRunnerDoc::GetDoc()->UsingFontProject());
	m_btnUseCurrentProject.EnableWindow(CFontRunnerDoc::GetDoc()->UsingFontProject());

	m_ctrlStartupCombo.SetCurSel(pOptions->GetStartupOption());

	m_btnClearLocationsOnExitCheck.SetCheck(pOptions->GetClearLocationsOnExit() ? BST_CHECKED : BST_UNCHECKED);
	m_btnLimitLocationsCheck.SetCheck(pOptions->GetLimitLocations() ? BST_CHECKED : BST_UNCHECKED);
	m_nLocationsLimitEdit = pOptions->GetLocationsLimit();
	m_ctrlLocationsLimitEdit.EnableWindow(pOptions->GetLimitLocations());
	m_btnShowFullPathInTitleBarCheck.SetCheck(pOptions->ShowFullPathInTitleBar() ? BST_CHECKED : BST_UNCHECKED);

	UpdateData(false);

	return true;
}

struct AddProjectToCombo
{
	AddProjectToCombo(CComboBoxEx* pCombo)
		: m_pCombo(pCombo),
		  m_nIndex(0),
		  m_nSelection(-1)
	{
	}

	void operator()(const projectlist_type::value_type& item)
	{
		if (item.first == theApp.GetProgramOptions()->GetDefaultProject())
			m_nSelection = m_nIndex;

		boost::scoped_array<TCHAR> pTextBuffer(new TCHAR[MAX_PATH]);
		std::memset(pTextBuffer.get(), 0, sizeof(TCHAR) * MAX_PATH);
		_tcscpy_s(pTextBuffer.get(), MAX_PATH, item.first);

		COMBOBOXEXITEM cbi;
		ZeroMemory(&cbi, sizeof(COMBOBOXEXITEM));	// zero this structure out
		cbi.mask = CBEIF_IMAGE | CBEIF_SELECTEDIMAGE | CBEIF_TEXT;
		cbi.iItem = m_nIndex++;
		cbi.pszText = pTextBuffer.get();
		cbi.iImage = 0;
		cbi.iSelectedImage = 0;

		m_pCombo->InsertItem(&cbi);
	}

	int GetSelection() const
	{
		return m_nSelection;
	}

private:
	CComboBoxEx* m_pCombo;
	int m_nIndex;
	int m_nSelection;
};

void COptionsPageGeneral::InitProjectCombo()
{
	// create image list
	m_ilProjectCombo.Create(16, 16, ILC_COLOR32, 1, 1);

	HICON hProjectIcon = theApp.LoadIcon(IDI_FONTPROJECT_ICON);
	m_ilProjectCombo.Add(hProjectIcon);
	::DestroyIcon(hProjectIcon);
	m_ctrlThisProjectCombo.SetImageList(&m_ilProjectCombo);

	// get font projects
	CFontProjects* pProjects = theApp.GetFontProjects();

	AddProjectToCombo result =
		std::for_each(pProjects->GetListBeginning(),
					  pProjects->GetListEnd(),
					  AddProjectToCombo(&m_ctrlThisProjectCombo));

	m_ctrlThisProjectCombo.SetCurSel(result.GetSelection());
}

void COptionsPageGeneral::InitStartupCombo()
{
	CString strText;
	strText.LoadString(IDS_OPTIONSDIALOG_OPENDEFAULTFOLDER);
	m_ctrlStartupCombo.AddString(strText);

	strText.LoadString(IDS_OPTIONSDIALOG_OPENDEFAULTPROJECT);
	m_ctrlStartupCombo.AddString(strText);

	strText.LoadString(IDS_OPTIONSDIALOG_OPENLAST);
	m_ctrlStartupCombo.AddString(strText);
}

BOOL COptionsPageGeneral::OnApply()
{
	// read data from controls
	UpdateData(true);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	ASSERT(pOptions);

	// startup options

	CString strText;
	m_ctrlThisFolderEdit.GetWindowText(strText);
	pOptions->SetDefaultFolder(strText);

	if (m_ctrlThisProjectCombo.GetCurSel() != -1)
	{
		m_ctrlThisProjectCombo.GetLBText(m_ctrlThisProjectCombo.GetCurSel(), strText);
		pOptions->SetDefaultProject(strText);
	}

	pOptions->SetStartupOption((CProgramOptions::eStartupOption)m_ctrlStartupCombo.GetCurSel());

	pOptions->SetClearLocationsOnExit(m_btnClearLocationsOnExitCheck.GetCheck() == BST_CHECKED);
	pOptions->SetLimitLocations(m_btnLimitLocationsCheck.GetCheck() == BST_CHECKED);

	if (m_nLocationsLimitEdit == 0)
	{
		m_nLocationsLimitEdit = 1;
		UpdateData(false);
	}

	pOptions->SetLocationsLimit(m_nLocationsLimitEdit);

	pOptions->ShowFullPathInTitleBar(m_btnShowFullPathInTitleBarCheck.GetCheck() == BST_CHECKED);

	// fire options change
	theApp.GetSignals()->Fire_OptionsChange();

	return CPropertyPage::OnApply();
}

void COptionsPageGeneral::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_THISFOLDER_EDIT, m_strThisFolderEdit);
	DDX_Control(pDX, IDC_THISFOLDER_EDIT, m_ctrlThisFolderEdit);
	DDX_Control(pDX, IDC_BROWSEFOLDER_BUTTON, m_btnBrowseThisFolder);
	DDX_Control(pDX, IDC_THISPROJECT_COMBO, m_ctrlThisProjectCombo);
	DDX_Control(pDX, IDC_THISFOLDER_EDIT, m_ctrlThisFolderEdit);
	DDX_Control(pDX, IDC_USECURRENT_BUTTON, m_btnUseCurrentFolder);
	DDX_Control(pDX, IDC_USECURRENTPROJECT_BUTTON, m_btnUseCurrentProject);
	DDX_Control(pDX, IDC_FULLPATH_CHECK, m_btnShowFullPathInTitleBarCheck);
	DDX_Control(pDX, IDC_STARTUPOPEN_COMBO, m_ctrlStartupCombo);
	DDX_Control(pDX, IDC_CLEARLOCATIONS_CHECK, m_btnClearLocationsOnExitCheck);
	DDX_Control(pDX, IDC_LOCATIONLIMIT_CHECK, m_btnLimitLocationsCheck);
	DDX_Control(pDX, IDC_LOCATIONS_EDIT, m_ctrlLocationsLimitEdit);
	DDX_Text(pDX, IDC_LOCATIONS_EDIT, m_nLocationsLimitEdit);
}

BEGIN_MESSAGE_MAP(COptionsPageGeneral, CPropertyPage)
	ON_EN_CHANGE(IDC_THISFOLDER_EDIT, OnEnChangeThisFolderEdit)
	ON_BN_CLICKED(IDC_BROWSEFOLDER_BUTTON, OnBnClickedBrowseFolder)
	ON_CBN_SELCHANGE(IDC_THISPROJECT_COMBO, &COptionsPageGeneral::OnCbnSelchangeThisprojectCombo)
	ON_CBN_SELCHANGE(IDC_STARTUPOPEN_COMBO, &COptionsPageGeneral::OnCbnSelchangeStartupCombo)
	ON_NOTIFY_RANGE(PSN_HELP, 0x0000, 0xFFFF, OnHelpButton)
	ON_BN_CLICKED(IDC_USECURRENT_BUTTON, &COptionsPageGeneral::OnBnClickedUsecurrentButton)
	ON_BN_CLICKED(IDC_USECURRENTPROJECT_BUTTON, &COptionsPageGeneral::OnBnClickedUsecurrentprojectButton)
	ON_BN_CLICKED(IDC_FULLPATH_CHECK, &COptionsPageGeneral::OnBnClickedFullpathCheck)
	ON_BN_CLICKED(IDC_CLEARLOCATIONS_CHECK, &COptionsPageGeneral::OnBnClickedClearLocationsCheck)
	ON_BN_CLICKED(IDC_LOCATIONLIMIT_CHECK, &COptionsPageGeneral::OnBnClickedLimitLocationsCheck)
	ON_BN_CLICKED(IDC_CLEARLOCATIONS_BUTTON, &COptionsPageGeneral::OnBnClickedClearLocationsNow)
	ON_EN_CHANGE(IDC_LOCATIONS_EDIT, &COptionsPageGeneral::OnEnChangeLocationsEdit)
END_MESSAGE_MAP()


// COptionsPageGeneral message handlers

void COptionsPageGeneral::OnEnChangeThisFolderEdit()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageGeneral::OnBnClickedBrowseFolder()
{
	CString strFolder = CWinToolbox::BrowseForFolder(m_hWnd, IDS_GENERAL_STARTUPFOLDER);

	if (!strFolder.IsEmpty())
	{
		m_ctrlThisFolderEdit.SetWindowText(strFolder);
		SetModified(true);
	}
}

void COptionsPageGeneral::OnHelpButton(UINT /*id*/, NMHDR* /*pNotifyStruct*/, LRESULT* /*result*/)
{
	HtmlHelp(HIDP_OPTIONS_GENERAL, HH_HELP_CONTEXT);
}

void COptionsPageGeneral::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	AfxGetApp()->HtmlHelp(dwData, nCmd);
}

void COptionsPageGeneral::OnCbnSelchangeThisprojectCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageGeneral::OnCbnSelchangeStartupCombo()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageGeneral::OnBnClickedUsecurrentButton()
{
	m_strThisFolderEdit = CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ();
	UpdateData(false);
	SetModified(true);
}

void COptionsPageGeneral::OnBnClickedUsecurrentprojectButton()
{
	CString strProject = CFontRunnerDoc::GetDoc()->GetCurrentProject();
	if (!strProject.IsEmpty())
	{
		// find project on list
		for (int i = 0; i < m_ctrlThisProjectCombo.GetCount(); ++i)
		{
			CString strTest;
			m_ctrlThisProjectCombo.GetLBText(i, strTest);
			if (strTest == strProject)
			{
				m_ctrlThisProjectCombo.SetCurSel(i);
				break;
			}
		}

		SetModified(true);
	}
}

void COptionsPageGeneral::OnBnClickedFullpathCheck()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageGeneral::OnBnClickedClearLocationsCheck()
{
	// notify property sheet of change
	SetModified(true);
}

void COptionsPageGeneral::OnBnClickedLimitLocationsCheck()
{
	// notify property sheet of change
	SetModified(true);

	m_ctrlLocationsLimitEdit.EnableWindow(m_btnLimitLocationsCheck.GetCheck() == BST_CHECKED);
}

void COptionsPageGeneral::OnBnClickedClearLocationsNow()
{
	theApp.GetSignals()->Fire_ClearLocationBar();
}

void COptionsPageGeneral::OnEnChangeLocationsEdit()
{
	// notify property sheet of change
	SetModified(true);

	UpdateData(true);
	if (m_nLocationsLimitEdit == 0)
	{
		CString strTitle, strMessage;
		strTitle.LoadString(IDS_OPTIONSDIALOG_LOCATIONLIMITZERO_TITLE);
		strMessage.LoadString(IDS_OPTIONSDIALOG_LOCATIONLIMITZERO);
		m_ctrlLocationsLimitEdit.ShowBalloonTip(strTitle, strMessage, TTI_WARNING);
	}
	else
		m_ctrlLocationsLimitEdit.HideBalloonTip();
}
