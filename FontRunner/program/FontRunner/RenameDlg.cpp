/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// RenameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "RenameDlg.h"
#include "FontProjects.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CRenameDlg dialog

IMPLEMENT_DYNAMIC(CRenameDlg, CDialog)

CRenameDlg::CRenameDlg(LPCTSTR lpszOldName, CWnd* pParent /*=NULL*/)
	: CDialog(CRenameDlg::IDD, pParent),
	  m_lpszOldName(lpszOldName),
	  m_hIcon(NULL)
{}

CRenameDlg::~CRenameDlg()
{
	if (m_hIcon)
		::DestroyIcon(m_hIcon);
}

void CRenameDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NEWNAME_EDIT, m_ctrlNewNameEdit);
	DDX_Control(pDX, IDC_ICON_STATIC, m_ctrlIconStatic);
}

BOOL CRenameDlg::OnInitDialog()
{
	// call base class
	CDialog::OnInitDialog();

	if (m_lpszOldName)
		m_ctrlNewNameEdit.SetWindowText(m_lpszOldName);
	else
	{
		CString strTitle;
		strTitle.LoadString(IDS_RENAMEDLG_TITLE_ADD);
		SetWindowText(strTitle);

		// change icon
		if (!m_hIcon)
			m_hIcon = theApp.LoadIcon(IDI_NEWPROJECT_ICON);

		m_ctrlIconStatic.SetIcon(m_hIcon);
	}

	return true;
}

CString CRenameDlg::GetNewName() const
{
	return m_strNewName;
}

void CRenameDlg::OnOK()
{
	// just save new name
	m_ctrlNewNameEdit.GetWindowText(m_strNewName);

	if (theApp.GetFontProjects()->Exists(m_strNewName))
	{
		CString strTitle, strMessage;
		strTitle.LoadString(IDS_RENAMEDLG_EXISTS_TITLE);
		strMessage.LoadString(IDS_RENAMEDLG_EXISTS);
		MessageBox(strMessage, strTitle, MB_OK + MB_ICONEXCLAMATION);
		return;
	}

	CDialog::OnOK();
}


BEGIN_MESSAGE_MAP(CRenameDlg, CDialog)
END_MESSAGE_MAP()


// CRenameDlg message handlers
