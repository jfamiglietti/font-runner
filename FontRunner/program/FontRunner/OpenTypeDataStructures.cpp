/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "OpenTypeDataStructures.h"

#include <algorithm>

#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace CruxTechnologies::OpenTypeFontData;

//----------------------------------------------------------------------------------
// byte-swapping functions
template <typename T>
T swapbytes(T input)
{
	// generic case
	return input;
}

template <>
uint16_t swapbytes(uint16_t input)
{
	return (input >> 8) | ((input & 0xFF) << 8);
}

template <>
int16_t swapbytes(int16_t input)
{
	return (input >> 8) | ((input & 0xFF) << 8);
}

template<>
wchar_t swapbytes(wchar_t input)
{
	return (input >> 8) | ((input & 0xFF) << 8);
}

template <>
uint32_t swapbytes(uint32_t input)
{
	return (swapbytes<uint16_t>((uint16_t)(input >> 16))) |
		   (swapbytes<uint16_t>((uint16_t)(input & 0xFFFF)) << 16);
}

template <typename T>
void swapbytes_in_array(T* p, std::size_t count)
{
	for (std::size_t i = 0; i < count; ++i)
		p[i] = swapbytes<T>(p[i]);
}

//----------------------------------------------------------------------------------
// TTF/OTF header
struct CHeader::_data
{
	uint32_t	version;
	uint16_t	numTables;
	uint16_t	searchRange;
	uint16_t	entrySelector;
	uint16_t	rangeShift;
};

CHeader::CHeader() : m_pData(new _data)
{
}

CHeader::~CHeader()
{

}

std::size_t CHeader::Initialize(char* pBeginning, size_t size)
{
	if (size < sizeof(_data))
		return 0;

	std::size_t datasize = sizeof(_data);
	std::memcpy(m_pData.get(), pBeginning, datasize);

	// convert to little-endian
	m_pData->version = swapbytes(m_pData->version);
	m_pData->numTables = swapbytes(m_pData->numTables);
	m_pData->searchRange = swapbytes(m_pData->searchRange);
	m_pData->entrySelector = swapbytes(m_pData->entrySelector);
	m_pData->rangeShift = swapbytes(m_pData->rangeShift);

	// version must be 0x00010000 or 'OTTO' for a pure OpenType
	if (m_pData->version != 0x00010000 &&
		m_pData->version != 0x4f54544f)
		return 0;

	return datasize;
}

uint32_t CHeader::Version() const
{
	if (m_pData)
		return m_pData->version;
	else
		return 0;
}

uint16_t CHeader::NumTables() const
{
	if (m_pData)
		return m_pData->numTables;
	else
		return 0;
}

uint16_t CHeader::SearchRange() const
{
	if (m_pData)
		return m_pData->searchRange;
	else
		return 0;
}

uint16_t CHeader::EntrySelector() const
{
	if (m_pData)
		return m_pData->entrySelector;
	else
		return 0;
}

uint16_t CHeader::RangeShift() const
{
	if (m_pData)
		return m_pData->rangeShift;
	else
		return 0;
}

//----------------------------------------------------------------------------------
// table directory structure
struct CTableDirectory::_data
{
	uint32_t	tag;
	uint32_t	checkSum;
	uint32_t	offset;
	uint32_t	length;
};

CTableDirectory::CTableDirectory() : m_pData(NULL)
{
}

CTableDirectory::~CTableDirectory()
{
	if (m_pData)
		delete m_pData;
}

std::size_t CTableDirectory::Initialize(char* pBeginning, size_t size)
{
	if (m_pData || size < sizeof(_data))
		return 0;	// already initialized or too little data

	// grab data from file
	m_pData = new _data;
	std::size_t datasize = sizeof(_data);
	std::memcpy(m_pData, pBeginning, datasize);

	// convert to little-endian
	m_pData->tag = swapbytes(m_pData->tag);
	m_pData->checkSum = swapbytes(m_pData->checkSum);
	m_pData->offset = swapbytes(m_pData->offset);
	m_pData->length = swapbytes(m_pData->length);

	return datasize;
}

uint32_t CTableDirectory::Tag() const
{
	if (m_pData)
		return m_pData->tag;
	else
		return 0;
}

uint32_t CTableDirectory::CheckSum() const
{
	if (m_pData)
		return m_pData->checkSum;
	else
		return 0;
}

uint32_t CTableDirectory::Offset() const
{
	if (m_pData)
		return m_pData->offset;
	else
		return 0;
}

uint32_t CTableDirectory::Length() const
{
	if (m_pData)
		return m_pData->length;
	else
		return 0;
}

//----------------------------------------------------------------------------------
// name table record
struct StandardNameRecord
{
	uint16_t	platformID;
	uint16_t	platformEncodingID;
	uint16_t	languageID;
	uint16_t	nameID;
	uint16_t	stringLength;
	uint16_t	stringOffset;
};

struct CNameRecord::_data
{
	StandardNameRecord	nameRecord;
	std::wstring		strValue;
};

CNameRecord::CNameRecord(char* pStringStorageAddress)
 : m_pData(NULL),
   m_pStringStorageAddress(pStringStorageAddress)
{
}

CNameRecord::CNameRecord(const CNameRecord& src)
{
	m_pData = new _data;

	m_pData->nameRecord = src.m_pData->nameRecord;
	m_pData->strValue = src.m_pData->strValue;
}

CNameRecord::~CNameRecord()
{
	if (m_pData)
		delete m_pData;
}

std::size_t CNameRecord::Initialize(char* pBeginning, size_t size)
{
	if (m_pData || size < sizeof(StandardNameRecord))
		return 0;

	m_pData = new _data;
	std::size_t datasize = sizeof(StandardNameRecord);
	std::memcpy(&m_pData->nameRecord, pBeginning, datasize);

	// convert to little-endian
	m_pData->nameRecord.platformID = swapbytes(m_pData->nameRecord.platformID);
	m_pData->nameRecord.platformEncodingID = swapbytes(m_pData->nameRecord.platformEncodingID);
	m_pData->nameRecord.languageID = swapbytes(m_pData->nameRecord.languageID);
	m_pData->nameRecord.nameID = swapbytes(m_pData->nameRecord.nameID);
	m_pData->nameRecord.stringLength = swapbytes(m_pData->nameRecord.stringLength);
	m_pData->nameRecord.stringOffset = swapbytes(m_pData->nameRecord.stringOffset);

	// get all Microsoft strings
	if (m_pData->nameRecord.platformID == kPlatformID_Microsoft)
	{
		// copy the string into a scoped array buffer
		boost::scoped_array<std::wstring::value_type> ptr(new std::wstring::value_type[m_pData->nameRecord.stringLength]);
		memcpy(ptr.get(), m_pStringStorageAddress + m_pData->nameRecord.stringOffset, m_pData->nameRecord.stringLength);
		
		// byte-swap the buffer
		std::size_t nNumChars = m_pData->nameRecord.stringLength / sizeof(wchar_t);
		swapbytes_in_array<wchar_t>(ptr.get(), nNumChars);
		
		// store the string
		m_pData->strValue.assign(ptr.get(), nNumChars);
	}
	else
	{
		// not a Microsoft string, so we're not interested
		delete m_pData;
		m_pData = NULL;
	}

	return datasize;
}

uint16_t CNameRecord::PlatformID() const
{
	if (m_pData)
		return m_pData->nameRecord.platformID;
	else
		return 0;
}

uint16_t CNameRecord::PlatformEncodingID() const
{
	if (m_pData)
		return m_pData->nameRecord.platformEncodingID;
	else
		return 0;
}

uint16_t CNameRecord::LanguageID() const
{
	if (m_pData)
		return m_pData->nameRecord.languageID;
	else
		return 0;
}

uint16_t CNameRecord::NameID() const
{
	if (m_pData)
		return m_pData->nameRecord.nameID;
	else
		return 0;
}

const std::wstring* CNameRecord::Name() const
{
	return &m_pData->strValue;
}

//----------------------------------------------------------------------------------
// the naming table
struct StandardNameTable
{
	uint16_t	formatSelector;
	uint16_t	numberOfNameRecords;
	uint16_t	stringStorageOffset;
};

struct CNamingTable::_data
{
	StandardNameTable			nameTable;
	NameRecordContainer_Type	nameRecords;
};

CNamingTable::CNamingTable()
{
}

CNamingTable::~CNamingTable()
{
}

std::size_t CNamingTable::Initialize(char* pBeginning, size_t size)
{
	if (m_pData.get() || size < sizeof(StandardNameTable))
		return 0;	// already initialized or too little data

	m_pData.reset(new _data);

	std::size_t sizeremaining = size;
	std::size_t datasize = sizeof(StandardNameTable);
	std::memcpy(&m_pData->nameTable, pBeginning, datasize);

	sizeremaining -= datasize;

	// convert to little-endian
	m_pData->nameTable.formatSelector = swapbytes(m_pData->nameTable.formatSelector);
	m_pData->nameTable.numberOfNameRecords = swapbytes(m_pData->nameTable.numberOfNameRecords);
	m_pData->nameTable.stringStorageOffset = swapbytes(m_pData->nameTable.stringStorageOffset);

	// get name records
	char* pCurrent = pBeginning + datasize;
	char* pStringStorage = pBeginning + m_pData->nameTable.stringStorageOffset;
	m_pData->nameRecords.reserve(m_pData->nameTable.numberOfNameRecords);
	for (int c = 0; c < m_pData->nameTable.numberOfNameRecords; ++c)
	{
		CNameRecord namerecord(pStringStorage);
		size_t sizeread = namerecord.Initialize(pCurrent, sizeremaining);

		if (sizeread == 0)
			return 0;

		pCurrent += sizeread;
		sizeremaining -= sizeread;

		if (namerecord.IsInitialized())
			m_pData->nameRecords.push_back(namerecord);
	}

	return datasize;
}

bool CNamingTable::Initialized() const
{
	return (m_pData.get() != NULL);
}

uint16_t CNamingTable::FormatSelector() const
{
	if (m_pData.get())
		return m_pData->nameTable.formatSelector;
	else
		return 0;
}

uint16_t CNamingTable::NumberOfNameRecords() const
{
	if (m_pData.get())
		return m_pData->nameTable.numberOfNameRecords;
	else
		return 0;
}

uint16_t CNamingTable::StringStorageOffset() const
{
	if (m_pData.get())
		return m_pData->nameTable.stringStorageOffset;
	else
		return 0;
}

const CNamingTable::NameRecordContainer_Type& CNamingTable::GetNameRecords() const
{
	return m_pData->nameRecords;
}

//----------------------------------------------------------------------------------
// cmap structures
struct cmapHeader
{
	uint16_t version;
	uint16_t numTables;
};

struct cmapEncodingRecord
{
	uint16_t platformID;
	uint16_t encodingID;
	uint32_t offset;
};

struct Format4RecordPrefix
{
	uint16_t format;
	uint16_t length;
	uint16_t language;
	uint16_t segCountX2;
	uint16_t searchRange;
	uint16_t entrySelector;
	uint16_t rangeShift;
};

//----------------------------------------------------------------------------------
// the cmap table
struct CcmapTable::_data
{
	_data() : bSymbol(false)
	{}

	EncodingRecordList_Type	encodingRecords;
	bool bSymbol;
};

CcmapTable::CcmapTable()
{
}

std::size_t CcmapTable::Initialize(char* pTable, size_t size)
{
	if (m_pData.get() || size < sizeof(cmapHeader))
		return 0;	// already initialized or too little data

	// create internals
	m_pData.reset(new _data);

	// save current pointer
	char* pCurrent = pTable;

	cmapHeader header;
	std::size_t headerSize = sizeof(cmapHeader);
	std::memcpy(&header, pCurrent, headerSize);

	// convert to little-endian
	header.version = swapbytes(header.version);
	header.numTables = swapbytes(header.numTables);

	// advance current pointer
	pCurrent += headerSize;

	// look for Microsoft platform
	std::size_t encodingSize = sizeof(cmapEncodingRecord);
	cmapEncodingRecord encodingRecord;
	for (uint16_t nEncodingIndex = 0; nEncodingIndex < header.numTables; ++nEncodingIndex)
	{
		std::memcpy(&encodingRecord, pCurrent, encodingSize);

		// convert to little-endian
		encodingRecord.platformID = swapbytes(encodingRecord.platformID);
		encodingRecord.encodingID = swapbytes(encodingRecord.encodingID);
		encodingRecord.offset = swapbytes(encodingRecord.offset);

		if (encodingRecord.platformID == kPlatformID_Microsoft)
		{
			uint16_t* pFormat = (uint16_t*)(pTable + encodingRecord.offset);
			uint16_t nFormat = swapbytes(*pFormat);

			if (nFormat == 4)
			{
				// save this encoding record
				EncodingRecord er;
				er.nPlatformID = (ePlatformID)encodingRecord.platformID;
				er.nEncodingID = (eEncodingID)encodingRecord.encodingID;
				m_pData->encodingRecords.push_back(er);

				ReadFormat4Record(pTable + encodingRecord.offset);
			}

			if (encodingRecord.encodingID == kEncodingID_Symbol)
				m_pData->bSymbol = true;
		}

		// advance current pointer to next record
		pCurrent += encodingSize;
	}

	// return total size of everything read
	return headerSize + (encodingSize * header.numTables);
}

size_t CcmapTable::ReadFormat4Record(char* pRecordStart)
{
	char* pCurrent = pRecordStart;

	boost::scoped_ptr<Format4RecordPrefix> pFormat4Record(new Format4RecordPrefix);
	std::memcpy(pFormat4Record.get(), pRecordStart, sizeof(Format4RecordPrefix));
	pCurrent += sizeof(Format4RecordPrefix);

	pFormat4Record->format = swapbytes(pFormat4Record->format);
	pFormat4Record->length = swapbytes(pFormat4Record->length);
	pFormat4Record->language = swapbytes(pFormat4Record->language);
	pFormat4Record->segCountX2 = swapbytes(pFormat4Record->segCountX2);
	pFormat4Record->searchRange = swapbytes(pFormat4Record->searchRange);
	pFormat4Record->entrySelector = swapbytes(pFormat4Record->entrySelector);
	pFormat4Record->rangeShift = swapbytes(pFormat4Record->rangeShift);

	// compute number of segments and size of segment arrays
	uint16_t nSegments = pFormat4Record->segCountX2 / 2;

	// get end-count codes
	boost::scoped_array<uint16_t> pEndCount(new uint16_t[nSegments]);
	std::memcpy(pEndCount.get(), pCurrent, pFormat4Record->segCountX2);
	pCurrent += pFormat4Record->segCountX2;
	swapbytes_in_array<uint16_t>(pEndCount.get(), nSegments);

	// there's a pad between the end codes and the start codes
	// and it should be 0
	uint16_t nPad;
	std::memcpy(&nPad, pCurrent, sizeof(uint16_t));
	pCurrent += sizeof(uint16_t);
	ASSERT(nPad == 0);

	// get start-count codes
	boost::scoped_array<uint16_t> pStartCount(new uint16_t[nSegments]);
	std::memcpy(pStartCount.get(), pCurrent, pFormat4Record->segCountX2);
	pCurrent += pFormat4Record->segCountX2;
	swapbytes_in_array<uint16_t>(pStartCount.get(), nSegments);

	// get deltas -- uncomment if these are ever needed
	//boost::shared_array<int16_t> pDeltas(new int16_t[nSegments]);
	//std::memcpy(pDeltas.get(), pCurrent, pFormat4Record->segCountX2);
	//pCurrent += pFormat4Record->segCountX2;
	//swapbytes_in_array<int16_t>(pDeltas.get(), nSegments);

	EncodingRecord& er = m_pData->encodingRecords[m_pData->encodingRecords.size() - 1];

	if (er.pMap == NULL)
	{
		// count number of entries
		er.nMapLength = 0;

		uint16_t n = 0;
		for (; pEndCount[n] != 0xFFFF && pStartCount[n] != 0xFFFF && n < nSegments; ++n)
			er.nMapLength += pEndCount[n] - pStartCount[n] + 1;

		// the actual number of segments should include 0xFFFF,
		// but that's not always the case
		if (n + 1 < nSegments)
			nSegments = n + 1;

		er.pMap.reset(new uint16_t[er.nMapLength]);

		uint16_t nCharIndex = 0;
		for (uint16_t nSegIndex = 0; nSegIndex < (nSegments - 1); ++nSegIndex)
			for (uint16_t nSubIndex = pStartCount[nSegIndex]; nSubIndex <= pEndCount[nSegIndex] && (nCharIndex < er.nMapLength); ++nSubIndex, ++nCharIndex)
				er.pMap[nCharIndex] = nSubIndex;

		ASSERT(nCharIndex == er.nMapLength);
	}

	// return number of bytes read
	return pCurrent - pRecordStart;
}

const CcmapTable::EncodingRecordList_Type* CcmapTable::GetMaps() const
{
	return &m_pData->encodingRecords;
}

bool CcmapTable::IsSymbol() const
{
	return m_pData->bSymbol;
}


//----------------------------------------------------------------------------------
// the OS/2 (and Windows) table
COS2Table::COS2Table()
{
	// init to 0
	memset(&m_table, 0, sizeof(TableVersion3));
}

std::size_t COS2Table::Initialize(char* pBeginning, size_t size)
{
	if (size < sizeof(TableVersion3))
		return 0;

#ifdef _DEBUG
	// zero-out structure
	memset(&m_table, 0, sizeof(TableVersion3));
#endif

	// just read version 0 first
	unsigned char* pDest = reinterpret_cast<unsigned char*>(&m_table);
	unsigned char* pSource = reinterpret_cast<unsigned char*>(pBeginning);
	size_t nSize = sizeof(Part0);
	memcpy_s(pDest, sizeof(TableVersion3), pSource, nSize);
	size_t nTotalSize = nSize;
	
	// do byte-swapping
	m_table.part0.version = swapbytes(m_table.part0.version);
	m_table.part0.xAvgCharWidth = swapbytes(m_table.part0.xAvgCharWidth);
	m_table.part0.usWeightClass = swapbytes(m_table.part0.usWeightClass);
	m_table.part0.usWidthClass = swapbytes(m_table.part0.usWidthClass);
	m_table.part0.fsType = swapbytes(m_table.part0.fsType);
	m_table.part0.ySubscriptXSize = swapbytes(m_table.part0.ySubscriptXSize);
	m_table.part0.ySubscriptYSize = swapbytes(m_table.part0.ySubscriptYSize);
	m_table.part0.ySubscriptXOffset = swapbytes(m_table.part0.ySubscriptXOffset);
	m_table.part0.ySubscriptYOffset = swapbytes(m_table.part0.ySubscriptYOffset);
	m_table.part0.ySuperscriptXSize = swapbytes(m_table.part0.ySuperscriptXSize);
	m_table.part0.ySuperscriptYSize = swapbytes(m_table.part0.ySuperscriptYSize);
	m_table.part0.ySuperscriptXOffset = swapbytes(m_table.part0.ySuperscriptXOffset);
	m_table.part0.ySuperscriptYOffset = swapbytes(m_table.part0.ySuperscriptYOffset);
	m_table.part0.yStrikeoutSize = swapbytes(m_table.part0.yStrikeoutSize);
	m_table.part0.yStrikeoutPosition = swapbytes(m_table.part0.yStrikeoutPosition);
	m_table.part0.sFamilyClass = swapbytes(m_table.part0.sFamilyClass);
	swapbytes_in_array(m_table.part0.panose, 10);
	swapbytes_in_array(m_table.part0.ulCharRange, 4);
	swapbytes_in_array(m_table.part0.achVendID, 4);
	m_table.part0.fsSelection = swapbytes(m_table.part0.fsSelection);
	m_table.part0.usFirstCharIndex = swapbytes(m_table.part0.usFirstCharIndex);
	m_table.part0.usLastCharIndex = swapbytes(m_table.part0.usLastCharIndex);
	m_table.part0.sTypoAscender = swapbytes(m_table.part0.sTypoAscender);
	m_table.part0.sTypoDescender = swapbytes(m_table.part0.sTypoDescender);
	m_table.part0.sTypoLineGap = swapbytes(m_table.part0.sTypoLineGap);
	m_table.part0.usWinAscent = swapbytes(m_table.part0.usWinAscent);
	m_table.part0.usWinDescent = swapbytes(m_table.part0.usWinDescent);

	// read version 1 extras
	if (m_table.part0.version == 0x0001)
	{
		// read version 1 part
		pDest += nSize;
		pSource += nSize;
		nSize = sizeof(Part1);
		memcpy_s(pDest, sizeof(Part1), pBeginning, nSize);

		// swap bytes
		m_table.part1.ulCodePageRange1 = swapbytes(m_table.part1.ulCodePageRange1);
		m_table.part1.ulCodePageRange2 = swapbytes(m_table.part1.ulCodePageRange2);

		nTotalSize += nSize;
	}
	else if (m_table.part0.version == 0x0002 || m_table.part0.version == 0x0003)
	{
		// read version 2 & 3 parts
		pDest += nSize;
		pSource += nSize;
		nSize = sizeof(Part1) + sizeof(Part2);
		memcpy_s(pDest, sizeof(Part1) + sizeof(Part2), pBeginning, nSize);

		// swap bytes
		m_table.part1.ulCodePageRange1 = swapbytes(m_table.part1.ulCodePageRange1);
		m_table.part1.ulCodePageRange2 = swapbytes(m_table.part1.ulCodePageRange2);
		m_table.part2.sxHeight = swapbytes(m_table.part2.sxHeight);
		m_table.part2.sCapHeight = swapbytes(m_table.part2.sCapHeight);
		m_table.part2.usDefaultChar = swapbytes(m_table.part2.usDefaultChar);
		m_table.part2.usBreakChar = swapbytes(m_table.part2.usBreakChar);
		m_table.part2.usMaxContext = swapbytes(m_table.part2.usMaxContext);

		nTotalSize += nSize;
	}

	return nTotalSize;
}
