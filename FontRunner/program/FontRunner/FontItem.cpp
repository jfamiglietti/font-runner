/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontItem.h"
#include "OpenTypeData.h"
#include "WinToolbox.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "ProgramOptions.h"
#include "LowercaseComparison.h"

#include <shlwapi.h>
#include <map>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

struct CFontItem::LowerCaseCacheType
{
	std::wstring m_wstrLowerCaseName;
};

typedef std::map<CString, int> distances_type;

struct CFontItem::_impl
{
	_impl()
		: bInstalled(false),
		  bMarkedForDeletion(false),
		  dwCharacteristics(0),
		  dwFontFileSize(0),
		  nStatus(CruxTechnologies::COpenTypeData::kStatus_Uninitialized),
		  nRefCount(1),
		  bTempPrivate(false)
	{}

	~_impl()
	{
		fntDisplay.DeleteObject();

		if (bInstalled && bMarkedForDeletion)
			::RemoveFontResource(strFQFontFileName);

		else if (!bInstalled)
			::RemoveFontResourceEx(strFQFontFileName, dwCharacteristics, NULL);
	}

	boost::shared_ptr<CruxTechnologies::COpenTypeData> pOpenTypeData;
	bool	bInstalled;
	bool	bMarkedForDeletion;
	DWORD	dwCharacteristics;
	CString strFontFileSize;
	DWORD	dwFontFileSize;
	CString strFontFileName;
	CString strFQFontFileName;
	CFont	fntDisplay;
	CruxTechnologies::COpenTypeData::eStatus nStatus;
	volatile long nRefCount;
	distances_type distances;
	bool	bTempPrivate;
};

CFontItem::CFontItem(LPCTSTR szFileName)
	: m_pImpl(new _impl)
{
	// need file size
	m_pImpl->dwFontFileSize = (DWORD)CWinToolbox::GetFileSize(szFileName);

	// get the folder
	size_t nBufferLength = _tcslen(szFileName) + 1;
	boost::scoped_ptr<TCHAR> pszFolder(new TCHAR[nBufferLength]);
	_tcscpy_s(pszFolder.get(), nBufferLength, szFileName);
	::PathRemoveFileSpec(pszFolder.get());
	CString strFolder(pszFolder.get());

	// create item data
	using CruxTechnologies::COpenTypeData;
	m_pImpl->pOpenTypeData.reset(new COpenTypeData());
	m_pImpl->bInstalled = (strFolder == theApp.GetWindowsFontFolder());
	m_pImpl->strFQFontFileName = szFileName;
	m_pImpl->strFontFileName = ::PathFindFileName(szFileName);

	m_pImpl->nStatus =
		m_pImpl->pOpenTypeData->Initialize(m_pImpl->strFQFontFileName, m_pImpl->dwFontFileSize);

	// make a size string
	m_pImpl->strFontFileSize.Format(_T("%dk"), (m_pImpl->dwFontFileSize / 1024));

	bool bFontOk = (m_pImpl->nStatus == CruxTechnologies::COpenTypeData::kStatus_OK);

	std::wstring strName;
	if (bFontOk)
	{
		// grab pointer to font name string and make sure we got a valid name
		using CruxTechnologies::OpenTypeFontData::CNameRecord;
		const std::wstring* pstrName = m_pImpl->pOpenTypeData->GetName(CNameRecord::kNameID_FullFontName);

		// if no full name, try for family name
		if (!pstrName)
			pstrName = m_pImpl->pOpenTypeData->GetName(CNameRecord::kNameID_FamilyName);

		// can't find any names we can use, font is not OK
		if (!pstrName)
		{
			m_pImpl->nStatus = CruxTechnologies::COpenTypeData::kStatus_CorruptFileError;
			return;
		}

		// we have a valid name
		strName = *pstrName;

		// cache lowercase names for speed
		CacheLowerCaseNames();

		// grab pointer to program options
		CProgramOptions* pOptions = theApp.GetProgramOptions();

		std::wstring::size_type nNameLength = strName.length();
		if (!m_pImpl->bInstalled)
		{
			m_pImpl->dwCharacteristics =
				pOptions->AutoInstall() ? 0 : FR_PRIVATE;
			::AddFontResourceEx(
				m_pImpl->strFQFontFileName, m_pImpl->dwCharacteristics, 0);
		}

		LOGFONT lf;
		::ZeroMemory(&lf, sizeof(LOGFONT));

		// font name cannot exceed LF_FACESIZE
		if (nNameLength >= LF_FACESIZE)
			strName._Copy_s(lf.lfFaceName, LF_FACESIZE - 1, LF_FACESIZE - 1);
		else
			strName._Copy_s(lf.lfFaceName, LF_FACESIZE - 1, nNameLength);

		// set font face
		lf.lfHeight = pOptions->GetFontListCharSize();
		lf.lfCharSet = DEFAULT_CHARSET;

		// set quality
		switch (pOptions->GetFontListRenderingOption())
		{
			case CProgramOptions::kRenderingOption_UseDefault:
				lf.lfQuality = DEFAULT_QUALITY;
				break;
			case CProgramOptions::kRenderingOption_Antialiasing:
				lf.lfQuality = ANTIALIASED_QUALITY;
				break;
			case CProgramOptions::kRenderingOptions_ClearType:
				lf.lfQuality = CLEARTYPE_QUALITY;
				break;
		}

		// create the font
		m_pImpl->fntDisplay.CreateFontIndirect(&lf);
	}
}

CFontItem::~CFontItem()
{
}

bool CFontItem::operator < (const CFontItem& other) const
{
	return (m_pImpl->strFQFontFileName < other.GetFullFileName());
}

bool CFontItem::operator == (const CFontItem& other) const
{
	return (m_pImpl->strFQFontFileName == other.GetFullFileName());
}

bool CFontItem::FontNameEqual(LPCTSTR szFontName) const
{
	return (m_pImpl->strFQFontFileName == szFontName);
}

int CFontItem::CompareFontNames(const CFontItem* pOther) const
{
	using CruxTechnologies::OpenTypeFontData::CNameRecord;

	if (m_pLowerCaseCache.get() && pOther->m_pLowerCaseCache.get())
		return m_pLowerCaseCache->m_wstrLowerCaseName.compare(pOther->m_pLowerCaseCache->m_wstrLowerCaseName);
	else
	{
		const std::wstring* pLeft = m_pImpl->pOpenTypeData->GetName(CNameRecord::kNameID_FullFontName);
		const std::wstring* pRight = pOther->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);

		if (pLeft && pRight)
		{
			// convert to lowercase
			std::wstring lowerleft(*pLeft), lowerright(*pRight);
			std::transform(pLeft->begin(), pLeft->end(), lowerleft.begin(), lower_case<std::wstring>());
			std::transform(pRight->begin(), pRight->end(), lowerright.begin(), lower_case<std::wstring>());

			return lowerleft.compare(lowerright);
		}
		else
			return 0;
	}
}

int CFontItem::CompareFileNames(const CFontItem* pOther) const
{
	return m_pImpl->strFontFileName.CompareNoCase(pOther->GetFileName());
}

int CFontItem::CompareFileSizes(const CFontItem* pOther) const
{
	return (m_pImpl->dwFontFileSize <= pOther->GetFileSize()) ? -1 : 1;
}

// this is a bit of a hack, but it makes sorting about 10x faster.
void CFontItem::CacheLowerCaseNames()
{
	if (!m_pLowerCaseCache.get())
		m_pLowerCaseCache.reset(new LowerCaseCacheType());

	// cache lowercase name (speeds up comparisons)
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pName = m_pImpl->pOpenTypeData->GetName(CNameRecord::kNameID_FullFontName);
	
	if (pName)
	{
		m_pLowerCaseCache->m_wstrLowerCaseName = *pName;
		std::transform(pName->begin(),
					   pName->end(),
					   m_pLowerCaseCache->m_wstrLowerCaseName.begin(),
					   lower_case<std::wstring>());
	}
}

long CFontItem::AddReference()
{
	::InterlockedIncrement(&m_pImpl->nRefCount);
	return m_pImpl->nRefCount;
}

long CFontItem::RemoveReference()
{
	::InterlockedDecrement(&m_pImpl->nRefCount);
	return m_pImpl->nRefCount;
}

long CFontItem::GetRefCount() const
{
	return m_pImpl->nRefCount;
}

boost::shared_ptr<CruxTechnologies::COpenTypeData> CFontItem::GetOpenTypeData() const
{
	return m_pImpl->pOpenTypeData;
}

CruxTechnologies::COpenTypeData::eStatus CFontItem::GetStatus() const
{
	return m_pImpl->nStatus;
}

const CString& CFontItem::GetFullFileName() const
{
	return m_pImpl->strFQFontFileName;
}

const CString& CFontItem::GetFileName() const
{
	return m_pImpl->strFontFileName;
}

DWORD CFontItem::GetFileSize() const
{
	return m_pImpl->dwFontFileSize;
}

const CString& CFontItem::GetFileSizeString() const
{
	return m_pImpl->strFontFileSize;
}

CFont& CFontItem::GetDisplayFont()
{
	return m_pImpl->fntDisplay;
}

bool CFontItem::IsInstalled() const
{
	return m_pImpl->bInstalled;
}

void CFontItem::Installed(bool bInstalled)
{
	m_pImpl->bInstalled = bInstalled;
}

void CFontItem::SetTempAutoInstall(bool bTempAutoInstall)
{
	// do nothing if temp auto install status is not actually changing
	if ((bTempAutoInstall && (m_pImpl->dwCharacteristics == 0)) ||
		(!bTempAutoInstall && (m_pImpl->dwCharacteristics == FR_PRIVATE)))
		return;

	if (!m_pImpl->bInstalled)
	{
		::RemoveFontResourceEx(m_pImpl->strFQFontFileName,
							   m_pImpl->dwCharacteristics,
							   0);

		// re-install fonts
		m_pImpl->dwCharacteristics = bTempAutoInstall ? 0 : FR_PRIVATE;
		::AddFontResourceEx(m_pImpl->strFQFontFileName,
							m_pImpl->dwCharacteristics,
							0);
	}
}

void CFontItem::TemporarilyMarkPrivate()
{
	if (m_pImpl->dwCharacteristics == 0)
	{
		SetTempAutoInstall(false);
		m_pImpl->bTempPrivate = true;
	}
}

void CFontItem::RestorePreviousCharacteristics()
{
	if (m_pImpl->bTempPrivate)
	{
		SetTempAutoInstall(true);
		m_pImpl->bTempPrivate = false;
	}
}

void CFontItem::MarkForDeletion()
{
	m_pImpl->bMarkedForDeletion = true;
}

void CFontItem::UnmarkForDeletion()
{
	m_pImpl->bMarkedForDeletion = false;
}

bool CFontItem::IsMarkedForDeletion() const
{
	return m_pImpl->bMarkedForDeletion;
}

void CFontItem::SetDistance(LPCTSTR szDistanceFrom, int nDistance)
{
	distances_type& distances = m_pImpl->distances;

	// Only add.  Don't do anything if already exists
	distances_type::iterator itFound = distances.find(szDistanceFrom);
	if (itFound == distances.end())
		distances.insert(distances_type::value_type(szDistanceFrom, nDistance));
}

int CFontItem::GetDistance(LPCTSTR szDistanceFrom) const
{
	distances_type& distances = m_pImpl->distances;

	distances_type::iterator itFound = distances.find(szDistanceFrom);

	// if not found, no similarity
	if (itFound == distances.end())
		return 0;

	return itFound->second;
}

bool CFontItem::PathInUse(LPCTSTR szPath) const
{
	return (m_pImpl->strFQFontFileName.Find(szPath) == 0);
}
