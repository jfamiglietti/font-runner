/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "FontRunnerSignals.h"

// CPrettyToolbar

class CPrettyToolbar : public CToolBar
{
	DECLARE_DYNAMIC(CPrettyToolbar)

// construction/destruction
public:
	CPrettyToolbar();
	virtual ~CPrettyToolbar();

// attributes
protected:
	HIMAGELIST	m_himlNormal, m_himlHot, m_himlDisabled;

// Operations
public:
	bool LoadToolBar(int nButtonWidth,		// all buttons should be same width and height
					 UINT nToolBar,			// ID of 32-bit alpha-blended toolbar
					 UINT nToolBarHot,		// ID of 32-bit alpha-blended hot toolbar
					 UINT nToolBarDisabled,	// ID of 32-bit alpha-blended disabled toolbar
					 UINT nToolBar24,		// ID of 24-bit true color toolbar, pixel in top-left corner is transparency color
					 UINT nToolBarHot24,	// ID of 24-bit true color hot toolbar, pixel in top-left corner is transparency color
					 UINT nToolBarDisabled24 = 0);	// ID of 24-bit true color disabled toolbar, pixel in top-left corner is transparency color

	BOOL LoadToolBar(UINT nIDResource);
	boost::signals2::connection connect_to_rclick(const rclick_signal_type::slot_type& slot);

private:
	virtual void PostNcDestroy();
	HIMAGELIST SetAlphaBlendedToolBar(UINT nToolBarType, UINT nToolBar, int nButtonWidth);
	bool SetTrueColorToolBar(UINT nToolBarType, UINT nToolBar, int nButtonWidth);

private:
	rclick_signal_type m_rclick_signal;

protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult);
};


