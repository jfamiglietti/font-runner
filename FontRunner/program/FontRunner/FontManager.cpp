/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontManager.h"
#include "FontItem.h"
#include "FontRunner.h"
#include "ProgramOptions.h"
#include "FontRunnerException.h"
#include "FontRunnerSignals.h"

#include <boost/bind.hpp>
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// font item smart pointer
typedef boost::shared_ptr<CFontItem> fontitem_ptr;
typedef std::vector<fontitem_ptr> fontcontainer_type;

struct CFontManager::_impl
{
	_impl()
		: bTempAutoInstall(theApp.GetProgramOptions()->AutoInstall()),
		  bIterating(false)
	{}

	void UnManageInPath(const fontitem_ptr& pItem, CFontManager* pManager, LPCTSTR szPath)
	{
		if (pItem->PathInUse(szPath))
			pManager->UnManage(pItem.get());
	}

	fontcontainer_type fontcontainer;
	CCriticalSection critical_section;
	bool bTempAutoInstall;
	bool bIterating;
};

CFontManager::CFontManager()
	: m_pImpl(new _impl)
{
}

CFontManager::~CFontManager()
{
}

CFontItem* CFontManager::AddReference(LPCTSTR szFileName)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// lock whole function so nothing happens to iterator while we're using it
	CSingleLock lock(&critical_section, true);

	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 boost::bind(&CFontItem::FontNameEqual, _1, szFileName));

	if (itFound != fontcontainer.end())
	{
		(*itFound)->AddReference();
		return itFound->get();
	}
	else
	{
		// create new font item
		try
		{
			fontitem_ptr pItem(new CFontItem(szFileName));
			fontcontainer.push_back(pItem);

			return pItem.get();
		}
		catch (...)
		{
			throw;
		}
	}
}

struct FindFontItem
{
	FindFontItem(const CFontItem* pItem)
		: m_pItem(pItem)
	{}

	bool operator ()(const fontitem_ptr& ptr) const
	{
		return (ptr.get() == m_pItem);
	}

private:
	const CFontItem* m_pItem;
};

CFontItem* CFontManager::AddReference(const CFontItem* pItem)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// lock whole function so nothing happens to iterator while we're using it
	CSingleLock lock(&critical_section, true);

	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 FindFontItem(pItem));

	// return reference count if found
	if (itFound != fontcontainer.end())
	{
		(*itFound)->AddReference();
		return itFound->get();
	}

	// something is wrong
	ASSERT(FALSE);
	return NULL;
}

unsigned int CFontManager::RemoveReference(CFontItem* pItem)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// don't want another thread messing with the container while we search it
	CSingleLock lock(&critical_section, true);
	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 FindFontItem(pItem));

	if (itFound == fontcontainer.end())
		return 0;
	else
	{
		unsigned int nRefCount = (*itFound)->RemoveReference();
		if (nRefCount == 0)
		{
			theApp.GetSignals()->Fire_FontDereferenced(itFound->get());

			itFound->get()->SetTempAutoInstall(false);
			if (!m_pImpl->bIterating)
				fontcontainer.erase(itFound); // smart pointer and font item class will handle the rest
			else
				itFound->reset(); // delete font item by resetting smart pointer
		}

		return nRefCount;
	}
}

bool CFontManager::IsItemValid(const CFontItem* pItem) const
{
	// get reference for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// don't want another thread messing with the container while we search it
	CSingleLock lock(&critical_section, true);
	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 FindFontItem(pItem));

	return (itFound != fontcontainer.end());
}

bool CFontManager::UnManage(const CFontItem* pItem)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// don't want another thread to mess with the container while we search it
	CSingleLock lock(&critical_section, true);

	// remove fonts we find -- smart pointers and destructors will handle the rest of the cleanup
	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 FindFontItem(pItem));

	// all the handlers should release their references to this font
	if (itFound != fontcontainer.end())
	{
		long nRefCount = pItem->GetRefCount();
		int nDisconnects = theApp.GetSignals()->Fire_UnManageFontItem(itFound->get());

		ASSERT(nDisconnects == nRefCount);
		return (nDisconnects == nRefCount);
	}
	
	// font wasn't found, so it is unmanaged
	return true;
}

void CFontManager::FontFileDeleted(const CString& strFileName)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;
	CSingleLock lock(&critical_section, true);

	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 boost::bind(&CFontItem::FontNameEqual, _1, strFileName));

	// unlock otherwise call into FontItemDeleted will block up
	lock.Unlock();

	// call CFontItem overload if found
	if (itFound != fontcontainer.end())
		FontItemDeleted(itFound->get());
}

void CFontManager::FontItemDeleted(const CFontItem* pItem)
{
	// let all listeners know about delete
	theApp.GetSignals()->Fire_FontFileDeleted(pItem);

	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// don't want another thread to mess with the container while we search it
	CSingleLock lock(&critical_section, true);

	// remove fonts we find -- smart pointers and destructors will handle the rest of the cleanup
	fontcontainer_type::iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 FindFontItem(pItem));

	// found it, so erase it
	if (itFound != fontcontainer.end())
		fontcontainer.erase(itFound);
}

void CFontManager::SetTempAutoInstall(bool bTempAutoInstall)
{
	m_pImpl->bTempAutoInstall = bTempAutoInstall;

	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	CSingleLock lock(&critical_section, true);
	std::for_each(fontcontainer.begin(),
				  fontcontainer.end(),
				  boost::bind(&CFontItem::SetTempAutoInstall, _1, bTempAutoInstall));
}

bool CFontManager::GetTempAutoInstall() const
{
	return m_pImpl->bTempAutoInstall;
}

bool CFontManager::ToggleTempAutoInstall()
{
	m_pImpl->bTempAutoInstall = !m_pImpl->bTempAutoInstall;

	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	CSingleLock lock(&critical_section, true);
	std::for_each(fontcontainer.begin(),
				  fontcontainer.end(),
				  boost::bind(&CFontItem::SetTempAutoInstall, _1, m_pImpl->bTempAutoInstall));

	return m_pImpl->bTempAutoInstall;
}

bool CFontManager::PathInUse(LPCTSTR szPath) const
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;
	CSingleLock lock(&critical_section, true);

	fontcontainer_type::const_iterator itFound =
		std::find_if(fontcontainer.begin(),
					 fontcontainer.end(),
					 boost::bind(&CFontItem::PathInUse, _1, szPath));

	return (itFound != fontcontainer.end());
}

struct is_fontitem_null
{
	bool operator()(const fontitem_ptr& pItem)
	{
		return (pItem.get() == NULL);
	}
};

void CFontManager::UnManageFontsInPath(LPCTSTR szPath)
{
	// get references for convenience
	fontcontainer_type& fontcontainer = m_pImpl->fontcontainer;
	CCriticalSection& critical_section = m_pImpl->critical_section;

	// remove all fonts in supplied path -- smart pointers and destructors
	// will take care of the rest of the cleanup
	CSingleLock lock(&critical_section, true);

	// fire unmanage signal for all fonts we find
	m_pImpl->bIterating = true;
	std::for_each(fontcontainer.begin(), fontcontainer.end(),
		boost::bind(&CFontManager::_impl::UnManageInPath, m_pImpl, _1, this, szPath));
	m_pImpl->bIterating = false;

	// move all NULLs to the end
	fontcontainer_type::iterator itNewEnd =
		std::remove_if(fontcontainer.begin(), fontcontainer.end(),
			is_fontitem_null());

	// delete NULLs
	fontcontainer.erase(itNewEnd, fontcontainer.end());
}
