/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontRunnerException.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFontRunnerException::CFontRunnerException(eCode nCode)
 : m_nCode(nCode)
{
}

CFontRunnerException::CFontRunnerException(LPCTSTR szMessage)
 : m_nCode(kCode_SeeErrorMessage), m_strMessage(szMessage)
{
}

CFontRunnerException::CFontRunnerException(eCode nCode, LPCTSTR szMessage)
 : m_nCode(nCode), m_strMessage(szMessage)
{
}

CFontRunnerException::~CFontRunnerException()
{
}

CFontRunnerException::eCode CFontRunnerException::GetCode() const
{
	return m_nCode;
}

LPCTSTR CFontRunnerException::GetMessage() const
{
	return (LPCTSTR)m_strMessage;
}