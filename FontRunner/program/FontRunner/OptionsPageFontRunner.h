#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// COptionsPageFontRunner dialog

class COptionsPageFontRunner : public CPropertyPage
{
	DECLARE_DYNAMIC(COptionsPageFontRunner)

public:
	COptionsPageFontRunner();
	virtual ~COptionsPageFontRunner();

// internals
private:
	void InitStartupCombo();

// Attributes
private:

// Dialog Data
private:
	enum { IDD = IDD_GENERAL_PAGE };
	CString m_strThisFolderEdit;
	CEdit m_ctrlThisFolderEdit;
	CButton m_btnBrowseThisFolder;
	CButton m_btnShowFullPathInTitleBarCheck;
	CButton m_btnClearLocationsOnExitCheck;
	CButton m_btnLimitLocationsCheck;
	int m_nLocationsLimitEdit;
	CEdit m_ctrlLocationsLimitEdit;
	CComboBox m_ctrlStartupCombo;

// overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();
	virtual BOOL OnApply();
	virtual void HtmlHelp(DWORD_PTR dwData, UINT nCmd = 0x000F);

public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnEnChangeThisFolderEdit();
	afx_msg void OnBnClickedBrowseFolder();
	afx_msg void OnCbnSelchangeStartupCombo();
	afx_msg void OnHelpButton(UINT id, NMHDR* pNotifyStruct, LRESULT* result);
	afx_msg void OnBnClickedUsecurrentButton();
	afx_msg void OnBnClickedFullpathCheck();
	afx_msg void OnBnClickedClearLocationsCheck();
	afx_msg void OnBnClickedLimitLocationsCheck();
	afx_msg void OnBnClickedClearLocationsNow();
	afx_msg void OnEnChangeLocationsEdit();
};
