/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CProgressDlg dialog

class CProgressDlg : public CDialog
{
	DECLARE_DYNAMIC(CProgressDlg)

public:
	CProgressDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CProgressDlg();

	BOOL Create(CWnd* pParentWnd);

// operations
public:
	// increments current count without updating UI
	void Increment();

	// set the total
	void Start(int nTotal, int nThreads);

	// informs the dialog the thread is finished
	void Finish();

	// returns the number of items processed
	inline LONG GetCurrent() const { return m_nCurrent; }

	// UI update functions
	void SetProgress(int nPos);
	void SetProgressText(UINT nID);
	void SetProgressText(const CString& strText);
	void UpdateProgressText(bool bUpdate);
	void ShowCancelButton(bool bShow);
	inline bool IsCanceled() { return m_bCanceled; }

// Dialog Data
	enum { IDD = IDD_PROGRESS_DIALOG };

private:
	bool	m_bCanceled;
	bool	m_bSetTotal;
	volatile LONG m_nCurrent;
	int m_nTotal;
	volatile LONG m_nThreadsFinished;
	int m_nThreadsTotal;

	bool	m_bUpdateProgressText;
	CProgressCtrl m_ctrlProgress;
	CString	m_strProgressStatic;
	CStatic m_ctrlProgressStatic;

	UINT_PTR m_nTimerID;
	CComQIPtr<ITaskbarList3> m_pTaskbarList;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCancelButton();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
