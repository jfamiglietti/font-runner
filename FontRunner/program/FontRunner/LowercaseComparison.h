/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <locale>
#include <functional>

// lowercase conversion for std::basic_string types
template <class T>
class lower_case
{
public:
	lower_case() : m_loc("English_US") {}

	typename T::value_type operator() (typename T::value_type ch)
	{
		return std::tolower(ch, m_loc);
	}

private:
	std::locale m_loc;
};

// Performs a lowercase less-than comparison of std::basic_string types
template <class T>
struct lowercase_less : public std::binary_function<T, T, bool>
{
	bool operator()(const T& left, const T& right) const
	{
		return lowercase_compare(left, right) < 0;
	}
};

// Performs lowercase lexical comparison of std::basic_string types
template <class T>
int lowercase_compare(const T& left, const T& right)
{
	// convert to lowercase
	T lowerleft(left), lowerright(right);
	std::transform(left.begin(), left.end(), lowerleft.begin(), lower_case<T>());
	std::transform(right.begin(), right.end(), lowerright.begin(), lower_case<T>());

	return lowerleft.compare(lowerright);
}

// for use in std::find_if algorithm
// A little faster than calling lowercase_compare again and again because of the
// cached lowercase member.
template <class T>
struct lowercase_equals : public std::unary_function<T, bool>
{
	lowercase_equals(T item) : m_loweritem(item)
	{
		// convert member to lowercase
		std::transform(item.begin(), item.end(), m_loweritem.begin(), lower_case<T>());
	}

	bool operator()(const T& item) const
	{
		// convert item to lowercase and compare it to our member
		// item which is already lowercase
		T loweritem(item);
		std::transform(item.begin(), item.end(), loweritem.begin(), lower_case<T>());

		return (m_loweritem.compare(loweritem) == 0);
	}

private:
	T m_loweritem;
};
