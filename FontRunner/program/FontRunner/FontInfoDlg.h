/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"

class CFontItem;

// CFontInfoDlg dialog

class CFontInfoDlg : public CDialog
{
	DECLARE_DYNAMIC(CFontInfoDlg)

public:
	CFontInfoDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFontInfoDlg();

protected:
	CFontItem*	m_pFontItem;

public:
	void SetFontInfo(CFontItem* pFontItem);

// Dialog Data
	enum { IDD = IDD_FONTINFO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_ctrlFontInfoList;
};
