/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunnerReBar.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunnerReBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontRunnerReBar

IMPLEMENT_DYNAMIC(CFontRunnerReBar, CReBar)

CFontRunnerReBar::CFontRunnerReBar()
{

}

CFontRunnerReBar::~CFontRunnerReBar()
{
}

boost::signals2::connection CFontRunnerReBar::connect_to_rclick(const rclick_signal_type::slot_type& slot)
{
	return m_rclick_signal.connect(slot);
}

BEGIN_MESSAGE_MAP(CFontRunnerReBar, CReBar)
	ON_WM_RBUTTONUP()
END_MESSAGE_MAP()



// CFontRunnerReBar message handlers
void CFontRunnerReBar::OnRButtonUp(UINT nFlags, CPoint point)
{
	// convert to screen coordinates
	ClientToScreen(&point);
	
	// fire signal
	m_rclick_signal(point.x, point.y);

	// call base class
	CReBar::OnRButtonUp(nFlags, point);
}
