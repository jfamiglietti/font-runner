/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FindSimilarFrameWnd.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "FontRunnerDoc.h"
#include "PrettyToolbar.h"
#include "FindSimilarFrameWnd.h"
#include "OpenTypeData.h"
#include "OpenTypeDataStructures.h"
#include "FontManager.h"
#include "ProgramOptions.h"
#include "FontItem.h"

#include "FontMatcher.h"
#include "WinToolbox.h"
#include "HRESULTException.h"
#include "pidlutils.h"
#include "ShellUtils.h"

#include <strsafe.h>
#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFindSimilarFrameWnd

IMPLEMENT_DYNCREATE(CFindSimilarFrameWnd, CFindFrameBase)

CFindSimilarFrameWnd::CFindSimilarFrameWnd(CFontItem* pItem, LPCTSTR pszCurrentFolder, bool bProject)
  : CFindFrameBase(pszCurrentFolder, bProject),
    m_pFontItem(theApp.GetFontManager()->AddReference(pItem), boost::bind(&CFontManager::RemoveReference, theApp.GetFontManager(), _1)),
	m_bSearchSubFolders(theApp.GetProgramOptions()->SearchSubFolders()),
	m_bSearchRemovableDisks(theApp.GetProgramOptions()->SearchRemovableDisks())
{
}

CFindSimilarFrameWnd::~CFindSimilarFrameWnd()
{
}

bool CFindSimilarFrameWnd::Create(CWnd* pParentWnd)
{
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	boost::shared_ptr<CruxTechnologies::COpenTypeData> pOpenTypeData = m_pFontItem->GetOpenTypeData();
	const std::wstring* pstrName = pOpenTypeData->GetName(CNameRecord::kNameID_FullFontName);

	CString strTitle;
	AfxFormatString1(strTitle, IDS_FINDSIMILAR_TITLE, pstrName->c_str());

	return CFindFrameBase::Create(pParentWnd, strTitle);
}

void CFindSimilarFrameWnd::SetLookForPreviewFont(CStatic* pwndFontNameStatic)
{
	// create a font for the name static
	CProgramOptions* pOptions = theApp.GetProgramOptions();
	BYTE nQuality = DEFAULT_QUALITY;
	CProgramOptions::eRenderingOption nRenderingOption = pOptions->GetFontListRenderingOption();
	if (nRenderingOption == CProgramOptions::kRenderingOption_Antialiasing)
		nQuality = ANTIALIASED_QUALITY;
	else if (nRenderingOption == CProgramOptions::kRenderingOptions_ClearType)
		nQuality = CLEARTYPE_QUALITY;

	BYTE dwCharset = m_pFontItem->GetOpenTypeData()->IsSymbol() ? SYMBOL_CHARSET : DEFAULT_CHARSET;

	m_fntLookFor.CreateFont(24, 0, 0, 0, FW_NORMAL, false, false, false, dwCharset, OUT_DEFAULT_PRECIS,
							CLIP_DEFAULT_PRECIS, nQuality, FF_DONTCARE, m_strLookForFont);
	pwndFontNameStatic->SetFont(&m_fntLookFor);
}

void CFindSimilarFrameWnd::OnOptionsChange()
{
	m_fntLookFor.DeleteObject();

	SetLookForPreviewFont(&m_wndFontNameStatic);
	m_wndFontNameStatic.ShowWindow(SW_HIDE);
	m_wndFontNameStatic.ShowWindow(SW_SHOW);
}

int CFindSimilarFrameWnd::UnManageFontItem(const CFontItem* pItem)
{
	if (m_pFontItem.get() == pItem)
	{
		m_fntLookFor.DeleteObject();
		m_pFontItem.reset();

		// update static to reflect missing font
		m_wndFontNameStatic.SetFont(theApp.GetDialogFont(), false);

		CString strMessage;
		strMessage.LoadString(IDS_FINDSIMILAR_FONTMISSING);
		m_wndFontNameStatic.SetWindowText(strMessage);

		return 1;
	}

	return 0;
}

void CFindSimilarFrameWnd::OnFontFileDeleted(const CFontItem* pItem)
{
	if (m_pFontItem.get() == pItem)
	{
		m_fntLookFor.DeleteObject();
		m_pFontItem.reset();

		// update static to reflect missing font
		m_wndFontNameStatic.SetFont(theApp.GetDialogFont(), false);

		CString strMessage;
		strMessage.LoadString(IDS_FINDSIMILAR_FONTMISSING);
		m_wndFontNameStatic.SetWindowText(strMessage);
	}
}

CFontListView::eMode CFindSimilarFrameWnd::GetFontListViewMode() const
{
	return CFontListView::kMode_FindSimilar;
}

BEGIN_MESSAGE_MAP(CFindSimilarFrameWnd, CFindFrameBase)
	ON_WM_CREATE()
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_FIND_LOOKIN_COMBO, OnLookInSelChange)
	ON_NOTIFY(CBEN_DELETEITEM, IDC_FIND_LOOKIN_COMBO, &CFindSimilarFrameWnd::OnLookInDeleteItem)
	ON_NOTIFY(TBN_DROPDOWN, AFX_IDW_TOOLBAR, OnToolbarDropDown)
	ON_UPDATE_COMMAND_UI(ID_FINDOPTIONS_BUTTON, OnUpdateOptions)
	ON_UPDATE_COMMAND_UI(ID_FINDSEARCH_BUTTON, OnUpdateSearchButton)
	ON_UPDATE_COMMAND_UI(ID_FINDCANCEL_BUTTON, OnUpdateCancelButton)
	ON_COMMAND(ID_FINDOPTIONS_SUBFOLDERS, OnOptionsSearchSubFolders)
	ON_COMMAND(ID_FINDOPTIONS_REMOVABLE, OnOptionsSearchRemovableDisks)
	ON_COMMAND(ID_FINDSEARCH_BUTTON, OnSearchButton)
	ON_COMMAND(ID_FINDCANCEL_BUTTON, OnCancelButton)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmFontFound, OnFontFound)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmFinished, OnSearchFinished)
	ON_REGISTERED_MESSAGE(CFindFrameBase::s_rwmEnteringFolder, OnEnteringFolder)
END_MESSAGE_MAP()


// CFindSimilarFrameWnd message handlers
int CFindSimilarFrameWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFindFrameBase::OnCreate(lpCreateStruct) == -1)
		return -1;

	// tell font list view which font we're matching
	GetFontListView()->SetSimilarTo(m_pFontItem->GetFullFileName());

	// create rebar
	if (!m_wndRebar.Create(this, 0))
	{
		TRACE0("Failed to create rebar\n");
		return -1;
	}

	// create font name static
	using CruxTechnologies::OpenTypeFontData::CNameRecord;
	const std::wstring* pstrFontName = m_pFontItem->GetOpenTypeData()->GetName(CNameRecord::kNameID_FullFontName);
	m_strLookForFont = pstrFontName->c_str();
	CString strText = theApp.GetProgramOptions()->GetFontListText();
	if (!m_wndFontNameStatic.Create(strText,
									SS_ENDELLIPSIS | SS_LEFTNOWORDWRAP | SS_NOPREFIX | WS_VISIBLE | WS_CHILD,
									CRect(0, 0, 0, 0), this))
	{
		TRACE0("Failed to create font name static control\n");
		return -1;
	}

	::SetWindowLongPtr(m_wndFontNameStatic.GetSafeHwnd(), GWL_EXSTYLE, WS_EX_TRANSPARENT);
	
	// sets up preview static
	SetLookForPreviewFont(&m_wndFontNameStatic);

	// measure text
	CDC* pDC = GetDC();
	CFont* pOldFont = pDC->SelectObject(&m_fntLookFor);
	CSize sizPreviewText = pDC->GetTextExtent(strText);

	// done measuring -- restore old font and release DC
	pDC->SelectObject(pOldFont);
	ReleaseDC(pDC);

	// create look in combo
	if (!m_wndLookInCombo.Create(CBS_DROPDOWNLIST | CBS_SORT | WS_VSCROLL | WS_VISIBLE | WS_CHILD,
								 CRect(0, 0, 200, 400), this, IDC_FIND_LOOKIN_COMBO))
	{
		TRACE0("Failed to create \"Look In\" combo control\n");
		return -1;
	}

	m_nLookInSelection =
		CFindFrameBase::InitFolderCombo(&m_wndLookInCombo, &m_ilLookIn, m_strCurrentFolder, true, true, true, true, m_bCurrentFolderIsProject);

	// create toolbar
	if (!m_pwndSearchToolbar.get())
		m_pwndSearchToolbar.reset(new CPrettyToolbar());
	m_pwndSearchToolbar->SetBorders(1, 1, 1, 1);
	if (!m_pwndSearchToolbar->CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_pwndSearchToolbar->LoadToolBar(IDR_FIND_TOOLBAR))
	{
		TRACE0("Failed to create toolbar\n");
        return -1; // fail to create
	}

	m_pwndSearchToolbar->LoadToolBar(kToolbarButtonSize,
									 IDB_FINDTOOLBAR_NORMAL,
									 IDB_FINDTOOLBAR_HOT,
									 IDB_FINDTOOLBAR_DISABLED,
									 IDB_FINDTOOLBAR_NORMAL256,
									 IDB_FINDTOOLBAR_HOT256);

	// options button is a dropdown
	TBBUTTONINFO tbbi;
	std::memset(&tbbi, 0, sizeof(TBBUTTONINFO));
	tbbi.cbSize = sizeof(TBBUTTONINFO);
	tbbi.dwMask = TBIF_STYLE;
	tbbi.fsStyle = BTNS_WHOLEDROPDOWN;
	m_pwndSearchToolbar->GetToolBarCtrl().SetButtonInfo(ID_FINDOPTIONS_BUTTON, &tbbi);

	CRect rectToolbar;
	int nLastButton = m_pwndSearchToolbar->GetToolBarCtrl().GetButtonCount() - 1;
	m_pwndSearchToolbar->GetItemRect(nLastButton, &rectToolbar);
	rectToolbar.left = 0;

	/////////////////////////////////////////////////////////////////////////////////////////
	// now, we set up the rebar
	
	// add preview static to rebar
	m_wndRebar.AddBar(&m_wndFontNameStatic, NULL, NULL, RBBS_NOGRIPPER);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
	rbbi.cxMinChild = 0;
	rbbi.cyMinChild = sizPreviewText.cy;
	rbbi.cx = rbbi.cxIdeal = sizPreviewText.cx * 9;
	m_wndRebar.GetReBarCtrl().SetBandInfo(0, &rbbi);

	// add combo to rebar
	strText.LoadString(IDS_FIND_LOOK_IN);
	m_wndRebar.AddBar(&m_wndLookInCombo, strText, NULL, RBBS_NOGRIPPER);

	// add toolbar
	m_wndRebar.AddBar(m_pwndSearchToolbar.get(), NULL, NULL, RBBS_NOGRIPPER);
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE;
	rbbi.cxMinChild = rectToolbar.Width();
	rbbi.cyMinChild = rectToolbar.Height();
	rbbi.cyMaxChild = rbbi.cyMinChild;
	rbbi.cx = rbbi.cxIdeal = rectToolbar.Width();
	m_wndRebar.GetReBarCtrl().SetBandInfo(2, &rbbi);

	m_wndStatusBar.Create(this);

	CFontRunnerSignals* pSignals = theApp.GetSignals();

	// connect to signals
	pSignals->ConnectTo_OptionsChange(boost::bind(&CFindSimilarFrameWnd::OnOptionsChange, this));
	pSignals->ConnectTo_UnManageFontItem(boost::bind(&CFindSimilarFrameWnd::UnManageFontItem, this, _1));
	pSignals->ConnectTo_FontFileDeleted(boost::bind(&CFindSimilarFrameWnd::OnFontFileDeleted, this, _1));

	return 0;
}

void CFindSimilarFrameWnd::OnDestroy()
{
	// save window pos
	CRect rectWindow;
	GetWindowRect(&rectWindow);

	CProgramOptions* pOptions = theApp.GetProgramOptions();

	pOptions->SetFindWindowPos(CPoint(rectWindow.left, rectWindow.top));
	pOptions->SetFindWindowSize(CSize(rectWindow.Width(), rectWindow.Height()));
	pOptions->SetSearchSubFolders(m_bSearchSubFolders);
	pOptions->SetSearchRemovableDisks(m_bSearchRemovableDisks);
}

bool CFindSimilarFrameWnd::Searching() const
{
	return m_bSearching;
}

void CFindSimilarFrameWnd::EnteringFolder(LPCTSTR szPath)
{
	TCHAR* pPath = NULL;
	size_t nSize = 0;

	// reallocate string on heap for trip across thread boundary
	if (SUCCEEDED(::StringCchLength(szPath, MAX_PATH, &nSize)))
	{
		++nSize;
		pPath = new TCHAR[nSize];
		::StringCchCopy(pPath, nSize, szPath);
	}

	PostMessage(s_rwmEnteringFolder, reinterpret_cast<WPARAM>(pPath));
}

void CFindSimilarFrameWnd::Found(CFontItem* pItem)
{
	// ignore the item used in this search
	if (m_pFontItem.get() == pItem)
		return;

	using CruxTechnologies::OpenTypeFontData::COS2Table;
	const COS2Table& os2Target = m_pFontItem->GetOpenTypeData()->GetOS2Table();

	if (pItem && pItem->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
	{
		const COS2Table& os2Test = pItem->GetOpenTypeData()->GetOS2Table();

		// they need to at least be in the same family
		if (os2Test.m_table.part0.panose[0] != 0 && os2Test.m_table.part0.panose[0] != 1 &&
			os2Test.m_table.part0.panose[0] == os2Target.m_table.part0.panose[0])
		{
			int nDistance =
				CFontMatcher::ComputeDistance((const PANOSE*)os2Target.m_table.part0.panose,
											  (const PANOSE*)os2Test.m_table.part0.panose);

			if (nDistance < 50)
			{
				pItem->SetDistance(m_pFontItem->GetFullFileName(), nDistance);

				// add temporary reference to ensure item still exists when this
				// message is eventually handled
				theApp.GetFontManager()->AddReference(pItem);
				PostMessage(s_rwmFontFound, reinterpret_cast<WPARAM>(pItem));
			}
		}
	}
}

void CFindSimilarFrameWnd::OnUpdateOptions(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bSearching);
}

void CFindSimilarFrameWnd::OnUpdateSearchButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(!m_bSearching);
}

void CFindSimilarFrameWnd::OnUpdateCancelButton(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(m_bSearching && !m_bCanceled);
}

void CFindSimilarFrameWnd::OnToolbarDropDown(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMTOOLBAR* pNMToolbar = (NMTOOLBAR*)pNMHDR;

	// this is the only dropdown notification we're handling
	ASSERT(pNMToolbar->iItem == ID_FINDOPTIONS_BUTTON);

	if (pNMToolbar->iItem == ID_FINDOPTIONS_BUTTON)
	{
		// create a popup menu
		CMenu menu;
		menu.CreatePopupMenu();
		
		// load first option
		CString strMenu;
		strMenu.LoadString(IDS_FIND_SUBFOLDERS);

		MENUITEMINFO mii;
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
		mii.fType = MFT_STRING;
		mii.wID = ID_FINDOPTIONS_SUBFOLDERS;
		mii.cch = strMenu.GetLength();
		mii.dwTypeData = strMenu.GetBuffer();
		mii.fState = m_bSearchSubFolders ? MFS_CHECKED : MFS_UNCHECKED;

		menu.InsertMenuItem(ID_FINDOPTIONS_SUBFOLDERS, &mii);
		strMenu.ReleaseBuffer();

		if (m_bLookInMyComputer)
		{
			strMenu.LoadString(IDS_FIND_REMOVABLEDISKS);

			::memset(&mii, 0, sizeof(MENUITEMINFO));
			mii.cbSize = sizeof(MENUITEMINFO);
			mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING;
			mii.fType = MFT_STRING;
			mii.wID = ID_FINDOPTIONS_REMOVABLE;
			mii.cch = strMenu.GetLength();
			mii.dwTypeData = strMenu.GetBuffer();
			mii.fState = m_bSearchRemovableDisks ? MFS_CHECKED : MFS_UNCHECKED;

			menu.InsertMenuItem(ID_FINDOPTIONS_REMOVABLE, &mii);
			strMenu.ReleaseBuffer();
		}

		// get screen coordinates of toolbar button
		CRect rectScreen, rectButton;
		m_pwndSearchToolbar->GetToolBarCtrl().GetWindowRect(&rectScreen);
		UINT nIndex = m_pwndSearchToolbar->GetToolBarCtrl().CommandToIndex(ID_FINDOPTIONS_BUTTON);
		m_pwndSearchToolbar->GetToolBarCtrl().GetItemRect(nIndex, &rectButton);
		CPoint pt(rectScreen.left + rectButton.left,
				  rectScreen.top + rectButton.top + rectButton.bottom);

		// show menu
		menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);
	}

	*pResult = 0;
}

void CFindSimilarFrameWnd::OnOptionsSearchSubFolders()
{
	m_bSearchSubFolders = !m_bSearchSubFolders;
}

void CFindSimilarFrameWnd::OnOptionsSearchRemovableDisks()
{
	m_bSearchRemovableDisks = !m_bSearchRemovableDisks;
}

void CFindSimilarFrameWnd::OnSearchButton()
{
	// if there are already search results, make sure the user wants to clear
	// it before continuing
	if (GetFontListView()->GetListCtrl().GetItemCount())
	{
		int nResult = AfxMessageBox(IDS_FIND_NEWSEARCHCONFIRM, MB_YESNO + MB_ICONQUESTION);

		// if user says no, just forget it and return
		if (nResult == IDNO)
			return;

		// clear the list
		GetFontListView()->GetListCtrl().DeleteAllItems();
	}

	// toggle flag
	m_bSearching = true;

	FindComboData* pComboData =
		reinterpret_cast<FindComboData*>(m_wndLookInCombo.GetItemData(m_wndLookInCombo.GetCurSel()));

	if (pComboData->nType == FindComboData::PIDL_type)
	{
		// init SearchThreadData structure
		SearchThreadData* pData = new SearchThreadData;
		pData->pWnd = this;
		pData->pidlRoot = pComboData->pidl;
		pData->bSearchRemovableDisks = m_bSearchRemovableDisks;
		pData->pController = new CSearchController<CFindSimilarFrameWnd>(
									&CFindSimilarFrameWnd::Searching,
									&CFindSimilarFrameWnd::EnteringFolder,
									&CFindSimilarFrameWnd::Found,
									this);

		// start thread
		AfxBeginThread(SearchThreadFunction, pData);
	}
	else if (pComboData->projects_type)
	{
		SearchProjectData* pData = new SearchProjectData;
		pData->pWnd = this;
		pData->strProjectName = pComboData->strProjectName;
		pData->pController = new CSearchController<CFindSimilarFrameWnd>(
									&CFindSimilarFrameWnd::Searching,
									&CFindSimilarFrameWnd::EnteringFolder,
									&CFindSimilarFrameWnd::Found,
									this);

		// start thread
		AfxBeginThread(SearchProjectFunction, pData);
	}
}

void CFindSimilarFrameWnd::OnCancelButton()
{
	m_bSearching = false;
	m_bCanceled  = true;
}

void CFindSimilarFrameWnd::OnLookInDeleteItem(NMHDR* pNMHDR, LRESULT* pResult)
{
	NMCOMBOBOXEX* pItem = reinterpret_cast<NMCOMBOBOXEX*>(pNMHDR);

	// just delete the item data
	delete reinterpret_cast<CFindFrameBase::FindComboData*>(pItem->ceItem.lParam);

	*pResult = 0;
}

void CFindSimilarFrameWnd::OnLookInSelChange()
{
	CFindFrameBase::LookInSelChange();
}

LRESULT CFindSimilarFrameWnd::OnFontFound(WPARAM wParam, LPARAM /*lParam*/)
{
	CFontItem* pItem = reinterpret_cast<CFontItem*>(wParam);
	GetFontListView()->AddSimilarItem(pItem);

	// remove temporary reference
	theApp.GetFontManager()->RemoveReference(pItem);

	return 0;
}

LRESULT CFindSimilarFrameWnd::OnEnteringFolder(WPARAM wParam, LPARAM /*lParam*/)
{
	LPCTSTR pPath = reinterpret_cast<LPCTSTR>(wParam);
	m_wndStatusBar.Searching(pPath);

	if (pPath)
		delete [] pPath;

	return 0;
}

LRESULT CFindSimilarFrameWnd::OnSearchFinished(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	m_bSearching = false;

	int nCount = GetFontListView()->GetListCtrl().GetItemCount();

	if (m_bCanceled)
	{
		m_wndStatusBar.Canceled(nCount);
		m_bCanceled = false;
	}
	else
		m_wndStatusBar.Finished(nCount);

	return 0;
}
