/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "OpenTypeDataStructures.h"

namespace CruxTechnologies
{

	class COpenTypeData
	{
	public:
		COpenTypeData();
		virtual ~COpenTypeData();

	public:
		enum eStatus
		{
			kStatus_Uninitialized = 0,
			kStatus_OK,
			kStatus_OpenFileError,
			kStatus_ReadFileError,
			kStatus_CorruptFileError,
			kStatus_FileTooBig
		};

		eStatus Initialize(const wchar_t* pszFileName, size_t nFileSize);

		// Gets a name string.
		// Default language ID is American English, default platform is Microsoft.
		// For other Microsoft LCID's see http://www.microsoft.com/OpenType/OTSpec/name.htm.
		// Returns a borrowed pointer to the name string object.  Caller does not own
		// pointer, so caller should not delete it.
		const std::wstring* GetName(
			OpenTypeFontData::CNameRecord::eNameID nID,
			uint16_t nLanguageID = 0x0409,	// Microsoft LCID for American English
			OpenTypeFontData::ePlatformID nPlatformID =
				OpenTypeFontData::kPlatformID_Microsoft) const;

		// Returns a reference to the container of all name records
		const OpenTypeFontData::CNamingTable::NameRecordContainer_Type&
			GetNameRecords() const;

		const OpenTypeFontData::CcmapTable::EncodingRecordList_Type* GetCharMaps() const;

		const OpenTypeFontData::COS2Table& GetOS2Table() const;

		bool IsSymbol() const;

	private:
		OpenTypeFontData::CHeader m_Header;
		OpenTypeFontData::CNamingTable m_NamingTable;
		OpenTypeFontData::CcmapTable m_cmapTable;
		OpenTypeFontData::COS2Table m_OS2Table;
		eStatus	m_nStatus;
	};

}