/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxwin.h"
#include "afxcmn.h"


// CUpdateCheckDlg dialog

class CUpdateCheckDlg : public CDialog
{
	DECLARE_DYNAMIC(CUpdateCheckDlg)

public:
	CUpdateCheckDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CUpdateCheckDlg();

// Dialog Data
	enum { IDD = IDD_UPDATECHECK_DIALOG };

// overrides
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

private:
	static UINT DownloadRelease(LPVOID pParam);
	bool Aborting() const;
	void OnDownloadProgress(int nCurrent, int nTotal);
	void BeginUpdate();

// controls
protected:
	CStatic m_ctrlUpdateCheckIcon;
	CString m_strStatusStatic;
	CButton m_ctrlCheckForUpdateButton;
	CButton m_ctrlCancelButton;
	CProgressCtrl m_ctrlDownloadProgress;
	CComQIPtr<ITaskbarList3> m_pTaskbarList;

// state
protected:
	bool m_bUpdateFound;
	bool m_bDownloading, m_bAborting;
	bool m_bProgressInitialized;
	CWinThread* m_pDownloadThread;
	CString m_strDownloadedFile;
	static UINT s_rwmProgressUpdate;
	static UINT s_rwmFinished;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckforupdateButton();
	afx_msg void OnBnClickedCancel();
	afx_msg LRESULT OnProgressMessage(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnFinishedMessage(WPARAM wParam, LPARAM lParam);
};
