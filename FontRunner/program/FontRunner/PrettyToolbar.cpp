/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// PrettyToolbar.cpp : implementation file
//

#include "stdafx.h"
#include "WinToolbox.h"
#include "PrettyToolbar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPrettyToolbar

IMPLEMENT_DYNAMIC(CPrettyToolbar, CToolBar)
CPrettyToolbar::CPrettyToolbar()
{
}

CPrettyToolbar::~CPrettyToolbar()
{
}

bool CPrettyToolbar::LoadToolBar(int nButtonWidth,
								 UINT nToolBar,
								 UINT nToolBarHot,
								 UINT nToolBarDisabled,
								 UINT nToolBar24,
								 UINT nToolBarHot24,
								 UINT nToolBarDisabled24)
{
	// use alpha blended version if Common Controls DLL is 6 or higher
	bool bUseAlpha = (CWinToolbox::GetDllVersion(_T("comctl32.dll")) >= PACKVERSION(6,00));

	// set normal toolbar images
	m_himlNormal = SetAlphaBlendedToolBar(TB_SETIMAGELIST,
										  bUseAlpha ? nToolBar : nToolBar24,
										  nButtonWidth);
	if (!m_himlNormal)
		return false;
	
	// set hot toolbar images
	m_himlHot = SetAlphaBlendedToolBar(TB_SETHOTIMAGELIST,
									   bUseAlpha ? nToolBarHot : nToolBarHot24,
									   nButtonWidth);
	if (!m_himlHot)
		return false;

	// set disabled toolbar images
	m_himlDisabled = SetAlphaBlendedToolBar(TB_SETDISABLEDIMAGELIST,
											bUseAlpha ? nToolBarDisabled : nToolBarDisabled24,
											nButtonWidth);
	if (!m_himlDisabled)
		return false;

	// if we made it down here, we were successful
	return true;
}

BOOL CPrettyToolbar::LoadToolBar(UINT nIDResource)
{
	return CToolBar::LoadToolBar(nIDResource);
}

boost::signals2::connection CPrettyToolbar::connect_to_rclick(const rclick_signal_type::slot_type& slot)
{
	return m_rclick_signal.connect(slot);
}

HIMAGELIST CPrettyToolbar::SetAlphaBlendedToolBar(UINT nToolBarType, UINT nToolBar, int nButtonWidth)
{
	if (nToolBar == 0)
		return NULL;

	HIMAGELIST himlToolbar = NULL;
	bool bResult = true;

	HBITMAP hBitmap = (HBITMAP)::LoadImage(AfxGetInstanceHandle(),
								  MAKEINTRESOURCE(nToolBar),
								  IMAGE_BITMAP,
								  0, 0,
		                          LR_DEFAULTSIZE | LR_CREATEDIBSECTION);

	if (hBitmap)
	{
		// attach to a CBitmap class
		CBitmap bmpToolbar;
		bmpToolbar.Attach(hBitmap);

		// create a BITMAP information structure
		BITMAP bmToolbar;
		bmpToolbar.GetBitmap(&bmToolbar);

		// figure out how many buttons
		int nNumButtons = bmToolbar.bmWidth / nButtonWidth;

		// create transparent color
		RGBTRIPLE* rgb = reinterpret_cast<RGBTRIPLE*>(bmToolbar.bmBits);
		COLORREF rgbMask = RGB(rgb->rgbtRed, rgb->rgbtGreen, rgb->rgbtBlue);

		// create imagelist object
		CImageList ilToolbar;
		bResult = (ilToolbar.Create(nButtonWidth, bmToolbar.bmHeight, ILC_COLOR32 | ILC_MASK, nNumButtons, 0) == TRUE);
		
		if (bResult)
			bResult = (ilToolbar.Add(&bmpToolbar, rgbMask) != -1);

		if (bResult)
		{
			// send imagelist to toolbar
			SendMessage(nToolBarType, 0, (LPARAM)ilToolbar.m_hImageList);

			// all done, clean up
			himlToolbar = ilToolbar.Detach();
			bmpToolbar.Detach();
			::DeleteObject(hBitmap);
		}
	}
	
	return himlToolbar;
}

bool CPrettyToolbar::SetTrueColorToolBar(UINT nToolBarType, UINT nToolBar, int nButtonWidth)
{
	CImageList	ilToolBar;
	CBitmap		bmpToolBar;
	BITMAP		bmToolBar;
	CSize		sizToolBar;
	int			nNumButtons;
	
	if (!bmpToolBar.Attach(LoadImage(AfxGetInstanceHandle(),
								  MAKEINTRESOURCE(nToolBar),
								  IMAGE_BITMAP, 0, 0,
		                          LR_DEFAULTSIZE|LR_CREATEDIBSECTION)) || !bmpToolBar.GetBitmap(&bmToolBar))
		return FALSE;

	sizToolBar  = CSize(bmToolBar.bmWidth, bmToolBar.bmHeight); 
	nNumButtons = sizToolBar.cx / nButtonWidth;
	RGBTRIPLE* rgb		= (RGBTRIPLE*)(bmToolBar.bmBits);
	COLORREF   rgbMask	= RGB(rgb[0].rgbtRed,
		                      rgb[0].rgbtGreen,
							  rgb[0].rgbtBlue);
	
	if (!ilToolBar.Create(nButtonWidth, sizToolBar.cy,
		                   ILC_COLOR24|ILC_MASK,
						   nNumButtons, 0))
		return FALSE;
	
	if (ilToolBar.Add(&bmpToolBar, rgbMask) == -1)
		return FALSE;

	SendMessage(nToolBarType, 0, (LPARAM)ilToolBar.m_hImageList);
	ilToolBar.Detach(); 
	bmpToolBar.Detach();
	
	return TRUE;
}


BEGIN_MESSAGE_MAP(CPrettyToolbar, CToolBar)
	ON_NOTIFY_REFLECT(NM_RCLICK, &CPrettyToolbar::OnNMRClick)
END_MESSAGE_MAP()



// CPrettyToolbar message handlers


void CPrettyToolbar::PostNcDestroy()
{
	// destroy image lists
	if (m_himlNormal)
		ImageList_Destroy(m_himlNormal);

	if (m_himlHot)
		ImageList_Destroy(m_himlHot);

	if (m_himlDisabled)
		ImageList_Destroy(m_himlDisabled);

	CToolBar::PostNcDestroy();
}

void CPrettyToolbar::OnNMRClick(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCLICK pNMClick = reinterpret_cast<LPNMCLICK>(pNMHDR);
	
	// convert to screen coordinates
	POINT pt = { pNMClick->pt.x, pNMClick->pt.y };
	ClientToScreen(&pt);

	// fire signal
	m_rclick_signal(pt.x, pt.y);

	*pResult = 0;
}
