/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontMatcher.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CFontMatcher::CFontMatcher()
{
}

CFontMatcher::~CFontMatcher()
{
}

template <typename T>
T difference(T a, T b)
{
	return (a < b) ? b - a : a - b;
}

int CFontMatcher::ComputeDistance(const PANOSE* pPanose1, const PANOSE* pPanose2)
{
	if (pPanose1->bFamilyType != pPanose2->bFamilyType ||
		pPanose1->bFamilyType == PAN_ANY || pPanose2->bFamilyType == PAN_ANY ||
		pPanose1->bFamilyType == PAN_NO_FIT || pPanose2->bFamilyType == PAN_NO_FIT)
		return 100;	// completely dissimilar

	int nDistance = 0;

	// Latin text
	if (pPanose1->bFamilyType == PAN_FAMILY_TEXT_DISPLAY)
	{
		// if serif doesn't match
		if ((SansSerif(pPanose1->bSerifStyle) != SansSerif(pPanose2->bSerifStyle)) &&
			(Serif(pPanose1->bSerifStyle) != Serif(pPanose2->bSerifStyle)))
		{
			nDistance += 25;
		}

		// compute weight difference, 10 for every weight difference
		nDistance += 10 * difference(pPanose1->bWeight, pPanose2->bWeight);

		// 5 for every proportional difference
		if (pPanose1->bProportion != pPanose2->bProportion)
		{
			if (pPanose1->bProportion == PAN_PROP_MONOSPACED)
				nDistance += 10 * pPanose2->bProportion - 2;
			else if (pPanose2->bProportion == PAN_PROP_MONOSPACED)
				nDistance += 10 * pPanose1->bProportion - 2;
			else
				nDistance += 10 * difference(pPanose1->bProportion, pPanose2->bProportion);
		}

		// 2 for every contrast difference
		nDistance += 2 * difference(pPanose1->bContrast, pPanose2->bContrast);

		// 1 for every stroke variation difference
		nDistance += difference(pPanose1->bStrokeVariation, pPanose2->bStrokeVariation);

		// 1 for every arm style difference
		nDistance += difference(pPanose1->bArmStyle, pPanose2->bArmStyle);

		// 1 for every letterform difference
		nDistance += difference(pPanose1->bLetterform, pPanose2->bLetterform);

		// 1 for every midline difference
		nDistance += difference(pPanose1->bMidline, pPanose2->bMidline);

		// 1 for every x-height difference
		nDistance += difference(pPanose1->bXHeight, pPanose2->bXHeight);
	}
	else
	{
		const unsigned char* pData1 = (unsigned char*)pPanose1;
		const unsigned char* pData2 = (unsigned char*)pPanose2;

		for (int i = 1; i < 9; ++i)
			nDistance += difference(pData1[i], pData2[i]);
	}

	return nDistance;
}

bool CFontMatcher::SansSerif(unsigned char nValue)
{
	return (nValue >= 0xB && nValue <= 0xD);
}

bool CFontMatcher::Serif(unsigned char nValue)
{
	return (nValue >= 2 && nValue <= 0xA);
}