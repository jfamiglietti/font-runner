/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/
// ConfirmDeleteDialog.cpp : implementation file
//

#include "stdafx.h"
#include "FontRunner.h"
#include "ConfirmDeleteDialog.h"

#include "HRESULTException.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CConfirmDeleteDialog dialog

IMPLEMENT_DYNAMIC(CConfirmDeleteDialog, CDialog)

CConfirmDeleteDialog::CConfirmDeleteDialog(bool bMultiple, CWnd* pParent /*=NULL*/)
	: CDialog(CConfirmDeleteDialog::IDD, pParent),
	  m_bMultiple(bMultiple),
	  m_hRecycleBinIcon(NULL)
{
}

CConfirmDeleteDialog::~CConfirmDeleteDialog()
{
	if (m_hRecycleBinIcon)
		::DestroyIcon(m_hRecycleBinIcon);
}

void CConfirmDeleteDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MESSAGE_STATIC, m_ctrlMessageStatic);
	DDX_Control(pDX, IDC_ICON_STATIC, m_ctrlRecycleBinIcon);
}


BEGIN_MESSAGE_MAP(CConfirmDeleteDialog, CDialog)
END_MESSAGE_MAP()


// CConfirmDeleteDialog message handlers
BOOL CConfirmDeleteDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	if (m_bMultiple)
	{
		CString strMessage;
		strMessage.LoadString(IDS_CONFIRMDELETE_MULTIPLE);

		m_ctrlMessageStatic.SetWindowText(strMessage);
	}

	if (!m_hRecycleBinIcon)
	{
		// get recycle bin icon

		// get PIDL for Recycle Bin
		LPITEMIDLIST pidl = NULL;
		if (SUCCEEDED(::SHGetFolderLocation(NULL, CSIDL_BITBUCKET, NULL, 0, &pidl)))
		{
			// get icon
			SHFILEINFO sfi;
			if (::SHGetFileInfo((LPCTSTR)pidl, 0, &sfi, sizeof(SHFILEINFO), SHGFI_PIDL | SHGFI_ICON | SHGFI_LARGEICON))
				m_hRecycleBinIcon = sfi.hIcon;

			// free PIDL
			::CoTaskMemFree(pidl);
		}
	}

	// set the icon
	m_ctrlRecycleBinIcon.SetIcon(m_hRecycleBinIcon);

	return TRUE;
}
