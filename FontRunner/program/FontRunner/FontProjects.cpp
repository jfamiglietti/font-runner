/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "FontProjects.h"

#include "HRESULTException.h"

#include <sstream>
#include <algorithm>
#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//========================================================================================
// Font Projects collection internals
struct CFontProjects::_impl
{
	projectlist_type projectlist;
};

//========================================================================================
// Function object that adds fonts to a project node
struct AddFont
{
	AddFont(IXMLDOMDocument* pXMLDoc, IXMLDOMNode* pParent)
		: m_pXMLDoc(pXMLDoc), m_pParent(pParent)
	{}

	void operator()(const filelist_type::value_type& name) const
	{
		// create element
		CComPtr<IXMLDOMElement> pElement = NULL;
		CheckResult(m_pXMLDoc->createElement(CComBSTR("font"), &pElement));

		CheckResult(pElement->put_nodeTypedValue(CComVariant(name)));

		IXMLDOMNode* pNewChild = NULL; // ATL bug prevents this from being a smart pointer
		CheckResult(m_pParent->appendChild(pElement, &pNewChild));
		pNewChild->Release();
	}

private:
	IXMLDOMDocument* m_pXMLDoc;
	IXMLDOMNode* m_pParent;
	CheckResultFunctor CheckResult;
};

//========================================================================================
// Function object that adds projects to the XML document
struct AddProject
{
	AddProject(IXMLDOMDocument* pXMLDoc, IXMLDOMNode* pParent)
		: m_pXMLDoc(pXMLDoc), m_pParent(pParent)
	{}

	void operator()(const projectlist_type::value_type& value) const
	{
		// create element
		CComPtr<IXMLDOMElement> pElement = NULL;
		CheckResult(m_pXMLDoc->createElement(CComBSTR("project"), &pElement));

		// create attribute
		CComPtr<IXMLDOMAttribute> pNameAttribute = NULL;
		CheckResult(m_pXMLDoc->createAttribute(CComBSTR("name"), &pNameAttribute));

		CheckResult(pNameAttribute->put_nodeTypedValue(CComVariant(value.first)));

		// add to attributes
		CComPtr<IXMLDOMNamedNodeMap> pNodeMap = NULL;
		CheckResult(pElement->get_attributes(&pNodeMap));

		IXMLDOMNode* pNewChild = NULL; // ATL bug prevents this from being a smart pointer
		CheckResult(pNodeMap->setNamedItem(pNameAttribute, &pNewChild));
		CheckResult(m_pParent->appendChild(pElement, &pNewChild));

		// add fonts to this new child
		std::for_each(value.second->begin(),
					  value.second->end(),
					  AddFont(m_pXMLDoc, pNewChild));
		
		pNewChild->Release();
	}

private:
	IXMLDOMDocument* m_pXMLDoc;
	IXMLDOMNode* m_pParent;
	CheckResultFunctor CheckResult;
};

//========================================================================================
// Font Projects collection
CFontProjects::CFontProjects() : m_pImpl(new _impl)
{
}

CFontProjects::~CFontProjects()
{
}

CComPtr<IXMLDOMNode> FindFontProjectsNode(CComPtr<IXMLDOMDocument>& pXMLDoc)
{
	// get the document element (this is the shoot data)
	CComPtr<IXMLDOMNodeList> pDocNodeList = NULL;
	pXMLDoc->get_childNodes(&pDocNodeList);

	// look for the font projects node
	long nLength = 0;
	pDocNodeList->get_length(&nLength);
	for (long nDocNodeIndex = 0; nDocNodeIndex < nLength; nDocNodeIndex++)
	{
		CComPtr<IXMLDOMNode> pTest;
		pDocNodeList->nextNode(&pTest);

		if (pTest)
		{
			CComBSTR bstrName;
			pTest->get_nodeName(&bstrName);

			if (bstrName == "fontprojects")
				return pTest;
		}
	}

	return NULL;
}



void CFontProjects::Load(LPCTSTR szFilename) throw(...)
{
	// make sure file exists
	if (!::PathFileExists(szFilename))
		return;

	// function to check HRESULTs
	CheckResultFunctor CheckResult;

	try
	{
		// make sure project list is empty
		m_pImpl->projectlist.clear();

		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		VARIANT_BOOL bSuccess = FALSE;
		CComVariant varFileName(szFilename);

		pXMLDoc->load(varFileName, &bSuccess);

		if (bSuccess)
		{
			CComPtr<IXMLDOMNode> pFontProjectsNode = FindFontProjectsNode(pXMLDoc);

			// not valid file if node isn't found
			if (!pFontProjectsNode)
				return;

			// iterate through child nodes
			CComPtr<IXMLDOMNodeList> pProjects = NULL;
			pFontProjectsNode->get_childNodes(&pProjects);

			long nLength = 0;
			pProjects->get_length(&nLength);
			for (int nProjectIndex = 0; nProjectIndex < nLength; ++nProjectIndex)
			{
				CComPtr<IXMLDOMNode> pProject;
				pProjects->nextNode(&pProject);

				DOMNodeType nType;
				pProject->get_nodeType(&nType);

				if (nType == NODE_ELEMENT)
				{
					// make sure this is a project element
					CComBSTR bstrName;
					pProject->get_nodeName(&bstrName);

					if (bstrName != "project")
						continue;

					// get name attribute
					CComPtr<IXMLDOMNamedNodeMap> pNodeMap = NULL;
					pProject->get_attributes(&pNodeMap);

					CComPtr<IXMLDOMNode> pNameNode = NULL;
					pNodeMap->getNamedItem(CComBSTR("name"), &pNameNode);

					// without a name, there's nothing we can do
					if (pNameNode)
						pNameNode->get_text(&bstrName);
					else
						continue;

					// insert into map
					CString strName(bstrName);
					std::pair<projectlist_type::iterator, bool> result = 
						m_pImpl->projectlist.insert(projectlist_type::value_type(strName, filelist_ptr(new filelist_type())));

					// get children (the files)
					CComPtr<IXMLDOMNodeList> pFiles = NULL;
					pProject->get_childNodes(&pFiles);
					long nFiles = 0;
					pFiles->get_length(&nFiles);

					for (int nFileIndex = 0; nFileIndex < nFiles; ++nFileIndex)
					{
						CComPtr<IXMLDOMNode> pFile;
						pFiles->nextNode(&pFile);

						pFile->get_nodeType(&nType);
						if (nType == NODE_ELEMENT)
						{
							// skip it if it is not a file element
							pFile->get_nodeName(&bstrName);
							if (bstrName != "font")
								continue;

							CComBSTR bstrFilename;
							pFile->get_text(&bstrFilename);
							CString strFilename(bstrFilename);

							// finally, insert it
							result.first->second->insert(filelist_type::value_type(strFilename));
						}
					}
				}
			}
		}
	}
	catch (CHRESULTException&)
	{
		return;
	}
}

void CFontProjects::Save(LPCTSTR szFilename) throw(...)
{
	CheckResultFunctor CheckResult;

	try
	{
		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		CComPtr<IXMLDOMProcessingInstruction> pPI = NULL;
		CheckResult(pXMLDoc->createProcessingInstruction(CComBSTR("xml"), CComBSTR("version=\"1.0\""), &pPI));

		// make a null variant
		CComVariant varNull;
		varNull.vt = VT_NULL;

		// make a temp node
		CComPtr<IXMLDOMNode> pTemp = NULL;

		// insert it
		CheckResult(pXMLDoc->insertBefore(pPI, varNull, &pTemp));

		// create a root element for font projects
		CComVariant varElementNodeType(NODE_ELEMENT);
		CComPtr<IXMLDOMNode> pFontProjectsNode = NULL, pTempRoot = NULL;
		CheckResult(pXMLDoc->createNode(varElementNodeType, CComBSTR("fontprojects"), CComBSTR(""), &pTempRoot));
		
		// stick it in the document
		CheckResult(pXMLDoc->appendChild(pTempRoot, &pFontProjectsNode));

		// create projects
		std::for_each(m_pImpl->projectlist.begin(),
					  m_pImpl->projectlist.end(),
					  AddProject(pXMLDoc, pFontProjectsNode));

		// finally, save document to file
		CheckResult(pXMLDoc->save(CComVariant(szFilename)));
	}
	catch (CHRESULTException&)
	{
		return;
	}
}

void CFontProjects::Add(LPCTSTR szProjectName)
{
	std::pair<projectlist_type::iterator, bool> itResult =
		m_pImpl->projectlist.insert(projectlist_type::value_type(szProjectName, filelist_ptr(new filelist_type())));
}

void CFontProjects::Remove(LPCTSTR szProjectName)
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szProjectName);

	if (it != m_pImpl->projectlist.end())
		m_pImpl->projectlist.erase(it);
}

bool CFontProjects::Rename(LPCTSTR szOldName, LPCTSTR szNewName)
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szNewName);
	if (it != m_pImpl->projectlist.end())
		return false;

	it = m_pImpl->projectlist.find(szOldName);

	if (it != m_pImpl->projectlist.end())
	{
		// save pointer
		filelist_ptr pList = it->second;
		m_pImpl->projectlist.erase(it);

		// insert new
		m_pImpl->projectlist.insert(projectlist_type::value_type(szNewName, pList));

		return true;
	}
	else
		return false;
}

bool CFontProjects::Exists(LPCTSTR szProjectName) const
{
	return (m_pImpl->projectlist.find(szProjectName) != m_pImpl->projectlist.end());
}

projectlist_type::const_iterator CFontProjects::Find(LPCTSTR szProjectName) const
{
	return m_pImpl->projectlist.find(szProjectName);
}

void CFontProjects::AddFile(LPCTSTR szProjectName, LPCTSTR szFileName)
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szProjectName);
	if (it != m_pImpl->projectlist.end())
		it->second->insert(filelist_type::value_type(szFileName));
	else
		throw CFontProjectException(CFontProjectException::kError_ProjectNotFound);
}

void CFontProjects::RemoveFile(LPCTSTR szProjectName, LPCTSTR szFontName)
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szProjectName);
	if (it != m_pImpl->projectlist.end())
		it->second->erase(szFontName);
	else
		throw CFontProjectException(CFontProjectException::kError_ProjectNotFound);
}

bool CFontProjects::FileExists(LPCTSTR szProjectName, LPCTSTR szFontName) const
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szProjectName);
	if (it != m_pImpl->projectlist.end())
		return (it->second->find(szFontName) != it->second->end());
	else
		throw CFontProjectException(CFontProjectException::kError_ProjectNotFound);
}

std::wstring CFontProjects::GetNewProjectName(LPCTSTR szAttemptedName) const
{
	// see if this string will work
	if (!Exists(szAttemptedName))
		return std::wstring(szAttemptedName);
	else
	{
		// we'll only try 200 names
		for (int i = 1; i < 200; ++i)
		{
			TCHAR szNumber[4];
			_itow_s(i, szNumber, 4, 10);
			std::wstring strName(szAttemptedName);
			strName.append(_T(" ("));
			strName.append(szNumber);
			strName.append(_T(")"));

			if (!Exists(strName.c_str()))
				return strName;
		}
	}

	throw CFontProjectException(CFontProjectException::kError_NamesExhausted);
}

size_t CFontProjects::GetProjectCount() const
{
	return m_pImpl->projectlist.size();
}

projectlist_type::const_iterator CFontProjects::GetListBeginning() const
{
	return m_pImpl->projectlist.begin();
}

projectlist_type::const_iterator CFontProjects::GetListEnd() const
{
	return m_pImpl->projectlist.end();
}

boost::weak_ptr<filelist_type> CFontProjects::GetFileList(LPCTSTR szProjectName) const
{
	projectlist_type::iterator it = m_pImpl->projectlist.find(szProjectName);
	if (it != m_pImpl->projectlist.end())
		return it->second;
	else
		return boost::weak_ptr<filelist_type>();
}