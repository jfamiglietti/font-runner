/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>

namespace CruxTechnologies
{
	// version information embedded in license info
	struct version_type
	{
		unsigned long major;
		unsigned long minor;
		unsigned long release;
		unsigned long build;
	};

	// returns true if version1 is newer than version2
	bool version_newer(const version_type& version1, const version_type& version2);

	// converts string version to struct version
	// pszVersion must be NULL-terminated
	void string_to_version(LPCTSTR pszVersion, version_type& version);
	void string_to_version(const std::string& strVersion, version_type& version);
}
