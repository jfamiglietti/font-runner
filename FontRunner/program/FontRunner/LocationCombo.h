/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <string>
#include <boost/shared_ptr.hpp>

// CLocationCombo

class CLocationCombo : public CComboBoxEx
{
	DECLARE_DYNAMIC(CLocationCombo)

public:
	CLocationCombo();
	virtual ~CLocationCombo();

private:
	CImageList m_iml;
	int m_nProjectIconIndex;
	int m_nFolderIconIndex;

	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);

public:
	int AddPath(LPCTSTR szPath, bool bAddToTop = true);
	int FindPath(LPCTSTR szPath);
	void ClearLocationHistory();
	void ApplyLimit();
	int AddProject(const std::wstring& strName, bool bAddToTop = true);
	int FindProject(const std::wstring& strName);
	void Cleanup();

private:
	void MoveToTop(int nIndex);
	void LoadLocationHistory();
	void SaveLocationHistory() const;

// signal handlers
protected:
	void OnOptionsChange();
	void OnClearLocationBar();
	void OnSelectedFolderChange(const CString& strFolder, const CString* pstrFile);
	void OnSelectedProjectChange(const std::wstring&);


protected:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnDropFiles(HDROP);
	afx_msg int OnCreate(LPCREATESTRUCT);
	afx_msg void OnDestroy();
	afx_msg void OnSelChange();
	afx_msg void OnDropDown();
};


