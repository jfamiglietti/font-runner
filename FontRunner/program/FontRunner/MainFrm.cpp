/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "FontRunnerSignals.h"
#include "FontRunner.h"

#include "FontRunnerDoc.h"
#include "FontRunnerView.h"

// panes
#include "ShellTreeView.h"
#include "FontProjectFrame.h"
#include "FontListView.h"
#include "FontListFrame.h"
#include "RichTextPreviewView.h"

#include "Splash.h"
#include "WinToolbox.h"
#include "help/Help.h"

#include "MainFrm.h"

#include "ProgramOptions.h"
#include "HistoryList.h"
#include "LowercaseComparison.h"
#include "FavoriteFolders.h"
#include "MenuBar.h"
#include "PrettyToolbar.h"
#include "FavoritesDlg.h"
#include "UpdateCheckDlg.h"

#include <strsafe.h>
#include <shlwapi.h>
#include <htmlhelp.h>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// undefine min and max macros
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_COMMAND_RANGE(ID_MENU_COMMANDS, ID_MENU_COMMANDS_END, OnMenuBarCommands)
	ON_COMMAND(ID_VIEW_REFRESH, OnViewRefresh)
	ON_COMMAND(ID_VIEW_INSTALLED, OnViewInstalled)
	ON_COMMAND(ID_HELP_CONTENTSANDINDEXF1, OnHelpContentsandindexf1)

	ON_COMMAND(ID_VIEW_AUTOINSTALL, OnViewAutoInstall)
	ON_UPDATE_COMMAND_UI(ID_VIEW_AUTOINSTALL, OnUpdateViewAutoInstall)

	// these are needed to display staus bar indicator text properly
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_FONTNAME, OnUpdateStatusBarPane)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_FILENAME, OnUpdateStatusBarPane)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_FILESIZE, OnUpdateStatusBarPane)
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_FONTSDISPLAYED, OnUpdateStatusBarPane)
	ON_COMMAND(ID_PREVIEW_FGCOLOR, OnPreviewFgcolor)
	ON_COMMAND(ID_PREVIEW_BGCOLOR, OnPreviewBgcolor)
	ON_COMMAND(ID_PREVIEW_BOLD, OnPreviewBold)
	ON_COMMAND(ID_PREVIEW_UNDERLINE, OnPreviewUnderline)
	ON_COMMAND(ID_PREVIEW_ITALIC, OnPreviewItalic)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_BOLD, OnUpdatePreviewBold)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_UNDERLINE, OnUpdatePreviewUnderline)
	ON_UPDATE_COMMAND_UI(ID_PREVIEW_ITALIC, OnUpdatePreviewItalic)
	ON_REGISTERED_MESSAGE(CFontRunnerApp::s_uwmAreYouFontRunner, OnAreYouFontRunner)
	ON_COMMAND(ID_VIEW_BACK, OnViewBack)
	ON_COMMAND(ID_VIEW_FORWARD, OnViewForward)
	ON_UPDATE_COMMAND_UI(ID_VIEW_BACK, OnUpdateViewBack)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FORWARD, OnUpdateViewForward)
	ON_COMMAND(ID_VIEW_UPFOLDER, OnViewUpFolder)
	ON_UPDATE_COMMAND_UI(ID_VIEW_UPFOLDER, OnUpdateViewUpFolder)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FONTLISTSIZE, OnUpdateFontListSize)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FAVORITES, OnUpdateFavorites)
	ON_COMMAND(ID_VIEW_MAINTOOLBAR, OnViewMainToolBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_MAINTOOLBAR, OnUpdateViewMainToolBar)
	ON_COMMAND(ID_VIEW_LOCATIONBAR, OnViewLocationBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_LOCATIONBAR, OnUpdateViewLocationBar)
	ON_COMMAND(ID_VIEW_FONTLISTPREVIEWSIZE, OnViewFontListPreviewSize)
	ON_UPDATE_COMMAND_UI(ID_VIEW_FONTLISTPREVIEWSIZE, OnUpdateViewFontListPreviewSize)
	ON_COMMAND(ID_TOOLBAR_FONTSHARINGBUTTON, OnViewFontSharingButton)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_FONTSHARINGBUTTON, OnUpdateViewFontSharingButton)
	ON_COMMAND(ID_TOOLBAR_LOCKTHETOOLBARS, &CMainFrame::OnToolbarLockthetoolbars)
	ON_UPDATE_COMMAND_UI(ID_TOOLBAR_LOCKTHETOOLBARS, &CMainFrame::OnUpdateToolbarLockthetoolbars)
	ON_WM_SIZE()
	ON_WM_SIZING()
	ON_WM_SYSCOMMAND()
	ON_WM_WINDOWPOSCHANGING()
	ON_COMMAND(ID_HELP_CONTENTSANDINDEXF1, OnHelpContentsAndIndex)
	ON_COMMAND(ID_HELP_WHATSNEW, OnHelpWhatsNew)
	ON_NOTIFY(TBN_DROPDOWN, AFX_IDW_TOOLBAR, OnToolbarDropDown)
	ON_COMMAND(ID_FAVORITESMENU_ADD, OnAddFavorite)
	ON_UPDATE_COMMAND_UI(ID_FAVORITESMENU_ADD, OnUpdateAddFavorite)
	ON_COMMAND(ID_FAVORITESMENU_MANAGE, OnManageFavorites)
	ON_COMMAND_RANGE(ID_FAVORITESMENU_BEGIN, ID_FAVORITESMENU_END, OnFavoritesMenu)
	ON_COMMAND(ID_EDIT_SELECTALL, OnEditSelectAll)
	ON_CBN_SELCHANGE(ID_FONTLISTSIZE_COMBO, OnFontListSizeSelChange)
	ON_CBN_EDITCHANGE(ID_FONTLISTSIZE_COMBO, OnFontListSizeEditChange)
	ON_WM_MEASUREITEM()
	ON_WM_DRAWITEM()
	ON_WM_TIMER()
	ON_NOTIFY(TTN_POP, 0, OnNotifyFavoritesBalloonPop)
	ON_WM_ACTIVATE()
	ON_WM_MENUCHAR()
	ON_WM_SYSCOLORCHANGE()
	ON_WM_THEMECHANGED()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,
	ID_INDICATOR_FONTSDISPLAYED
};

// CMainFrame construction/destruction

CMainFrame::CMainFrame()
 : m_pPreviewFrame(NULL),
   m_bRefreshing(false),
   m_pFavoritesMenu(NULL),
   m_hdcFavoritesMenu(NULL),
   m_nFavoritesMenuID(0),
   m_bFoundCurrentFavorite(false),
   m_pHistoryList(new CHistoryList),
   m_bHistoryNav(false),
   m_sizWindow(-1, -1),
   m_bMinimized(false),
   m_bMaximized(false),
   m_bMaximizedOnStartup(false),
   m_bSysCommandRestore(false),
   m_bMainToolBarVisible(true),
   m_nMainToolBarIndex((UINT)-1),
   m_bLocationBarVisible(true),
   m_nLocationBarIndex((UINT)-1),
   m_bFontListPreviewSizeToolbarVisible(true),
   m_nFontListPreviewSizeToolbarIndex((UINT)-1),
   m_bFontSharingButtonVisible(true),
   m_nFontSharingButtonIndex((UINT)-1),
   m_nFavoritesBalloonTimer(0),
   m_nFavoritesBalloonTimerID(WM_USER + 104),
   m_nCurrentActiveMenu(-1),
   m_bTrackingHotMenuItem(false),
   m_cnNumBands(5)
{
}

CMainFrame::~CMainFrame()
{
}

void CMainFrame::GotoURL(UINT nURLID)
{
	CString strURL;
	strURL.LoadString(nURLID);
	::ShellExecute(NULL, _T("open"), strURL, NULL, NULL, SW_SHOW);
}

struct AddToListSizeCombo
{
	AddToListSizeCombo(CComboBox* pCombo) : m_pCombo(pCombo)
	{
		m_strPixels.LoadString(IDS_FONTLIST_PX);
	}

	void operator()(const unsigned short nValue) const
	{
		CString strText;
		strText.Format(m_strPixels, nValue);

		int nIndex = m_pCombo->AddString(strText);
		m_pCombo->SetItemData(nIndex, nValue);
	}

private:
	CComboBox*	m_pCombo;
	CString		m_strPixels;
};

bool CMainFrame::CreateMainToolbar()
{
	if ((!m_pwndToolbar->CreateEx(this, TBSTYLE_FLAT | TBSTYLE_LIST | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)) ||
       (!m_pwndToolbar->LoadToolBar(IDR_MAINFRAME)))
    {
        TRACE0("Failed to create toolbar\n");
        return false; // fail to create
    }

	DWORD dwOldStyles = m_pwndToolbar->GetToolBarCtrl().GetExtendedStyle();
	DWORD dwNewStyles = TBSTYLE_EX_MIXEDBUTTONS | TBSTYLE_EX_DRAWDDARROWS |
						TBSTYLE_EX_DOUBLEBUFFER | TBSTYLE_EX_HIDECLIPPEDBUTTONS;

	m_pwndToolbar->GetToolBarCtrl().SetExtendedStyle(dwOldStyles | dwNewStyles);

	if (theApp.VistaOrLater())
		m_pwndToolbar->LoadToolBar(kToolbarButtonSize,
									IDB_TOOLBAR_VISTA_BITMAP,
									IDB_TOOLBARHOT_VISTA_BITMAP,
									IDB_TOOLBARDISABLED_VISTA_BITMAP,
									IDB_TOOLBAR256_BITMAP,
									IDB_TOOLBARHOT256_BITMAP);
	else
		m_pwndToolbar->LoadToolBar(kToolbarButtonSize,
									IDB_TOOLBAR_BITMAP,
									IDB_TOOLBARHOT_BITMAP,
									IDB_TOOLBARDISABLED_BITMAP,
									IDB_TOOLBAR256_BITMAP,
									IDB_TOOLBARHOT256_BITMAP);

	CString strText;

	// load back text
	strText.LoadString(IDS_GENERAL_BACK);
	int nBackIndex = m_pwndToolbar->CommandToIndex(ID_VIEW_BACK);
	m_pwndToolbar->SetButtonText(nBackIndex, strText);
	m_pwndToolbar->SetButtonStyle(nBackIndex, BTNS_SHOWTEXT);

	// set print text and make it a dropdown
	strText.LoadString(IDS_GENERAL_PRINT);
	int nPrintIndex = m_pwndToolbar->CommandToIndex(ID_FILE_PRINT);
	m_pwndToolbar->SetButtonText(nPrintIndex, strText);
	m_pwndToolbar->SetButtonStyle(nPrintIndex, BTNS_DROPDOWN | BTNS_SHOWTEXT);

	// set search button text
	strText.LoadString(IDS_GENERAL_SEARCH);
	int nSearchIndex = m_pwndToolbar->CommandToIndex(ID_VIEW_SEARCH);
	m_pwndToolbar->SetButtonText(nSearchIndex, strText);
	m_pwndToolbar->SetButtonStyle(nSearchIndex, BTNS_SHOWTEXT);

	// favorites button is a dropdown
	m_pwndToolbar->SetButtonStyle(m_pwndToolbar->CommandToIndex(ID_VIEW_FAVORITES), BTNS_WHOLEDROPDOWN);

	return true;
}

bool CMainFrame::InitFavoritesBalloon()
{
	// create message balloon
	if (!m_ctrlFavoritesBalloon.m_hWnd &&
		m_ctrlFavoritesBalloon.Create(this, TTS_NOPREFIX | TTS_ALWAYSTIP | TTS_BALLOON | TTS_CLOSE))
	{
		ZeroMemory(&m_tiFavoritesBalloon, sizeof(TOOLINFO));
		m_tiFavoritesBalloon.cbSize = sizeof(TOOLINFO);
		m_tiFavoritesBalloon.uFlags = TTF_TRACK | TTF_TRANSPARENT;
		m_tiFavoritesBalloon.hinst = AfxGetApp()->m_hInstance;
		m_tiFavoritesBalloon.uId = ID_FAVORITESMENU_ADD;
		m_pwndToolbar->GetItemRect(m_pwndToolbar->CommandToIndex(ID_VIEW_FAVORITES), &m_tiFavoritesBalloon.rect);
		m_ctrlFavoritesBalloon.SendMessage(TTM_ADDTOOL, 0, (LPARAM)&m_tiFavoritesBalloon);

		return true;
	}

	return false;
}

bool CMainFrame::ShowFavoritesBalloon(const CString* pstrFolder)
{
	PopFavoritesBalloon();

	InitFavoritesBalloon();

	CRect rect;
	m_pwndToolbar->GetItemRect(m_pwndToolbar->CommandToIndex(ID_VIEW_FAVORITES), &rect);
	m_pwndToolbar->ClientToScreen(&rect);
	m_tiFavoritesBalloon.rect = rect;

	// set messages
	CString strTitle, strMessage;
	WPARAM nIcon = TTI_INFO;

	if (pstrFolder)
	{
		strTitle.LoadString(IDS_FAVORITES_ADD_TITLE);
		AfxFormatString1(strMessage, IDS_FAVORITES_ADD_MESSAGE, *pstrFolder);
	}
	else
	{
		strTitle.LoadString(IDS_FAVORITES_EXISTS_TITLE);
		strMessage.LoadString(IDS_FAVORITES_EXISTS_MESSAGE);
		nIcon = TTI_WARNING;
	}

	m_tiFavoritesBalloon.lpszText = (LPWSTR)strMessage.GetBuffer();
	m_ctrlFavoritesBalloon.SendMessage(TTM_UPDATETIPTEXT, 0, (LPARAM)&m_tiFavoritesBalloon);
	m_ctrlFavoritesBalloon.SendMessage(TTM_SETTITLE, nIcon, (LPARAM)(LPCTSTR)strTitle);
	m_ctrlFavoritesBalloon.SendMessage(TTM_TRACKPOSITION, 0, (LPARAM)MAKELONG(rect.left + 24, rect.bottom - 8));
	m_ctrlFavoritesBalloon.SendMessage(TTM_TRACKACTIVATE, (WPARAM)TRUE, (LPARAM)&m_tiFavoritesBalloon);

	strMessage.ReleaseBuffer();

	// make balloon pop in 5 seconds
	m_nFavoritesBalloonTimer = SetTimer(m_nFavoritesBalloonTimerID, 5000, NULL);

	return true;
}

void CMainFrame::PopFavoritesBalloon()
{
	if (m_nFavoritesBalloonTimer)
	{
		m_ctrlFavoritesBalloon.SendMessage(TTM_TRACKACTIVATE, (WPARAM)FALSE, (LPARAM)&m_tiFavoritesBalloon);
		KillTimer(m_nFavoritesBalloonTimer);
		m_nFavoritesBalloonTimer = 0;
	}
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	// show splash screen
	CSplashWnd::ShowSplashScreen(this);

	// create menu bar
	if (!m_pwndMenuBar)
		m_pwndMenuBar.reset(new CMenuBar());

	// create main toolbar
	if (!m_pwndToolbar)
		m_pwndToolbar.reset(new CPrettyToolbar());

	m_pwndToolbar->SetBorders(1, 1, 1, 1);

	// create menu bar
	if (!m_pwndMenuBar->Create(this))
		return -1;

	// create main toolbar
	if (!CreateMainToolbar())
		return -1;

	// create location combo
	UINT nEditHeight = 0;
	if (m_wndLocationCombo.Create(CBS_DROPDOWN | WS_VSCROLL | WS_VISIBLE | WS_CHILD,
								  CRect(0, 0, 300, 400), this, ID_LOCATION_COMBO))
	{
		// measure font
		CDC* pDC = GetDC();
		CFont* pDialogFont = theApp.GetDialogFont();
		CFont* pOldFont = pDC->SelectObject(pDialogFont);

		nEditHeight = CWinToolbox::GetEditHeight(pDC);
		m_wndLocationCombo.SetFont(pDialogFont);

		pDC->SelectObject(pOldFont);
		ReleaseDC(pDC);
	}
	else
	{
		TRACE0("Failed to create font list size combo\n");
        return -1; // fail to create
	}

	CString strText;

	// create font list size combo
	if (m_wndFontListSizeCombo.Create(CBS_DROPDOWN | WS_VSCROLL | WS_VISIBLE | WS_CHILD,
								  CRect(0, 0, 75, 400), this, ID_FONTLISTSIZE_COMBO))
	{
		m_wndFontListSizeCombo.SetFont(theApp.GetDialogFont());

		// populate with common sizes
		const commonsizelist_type& sizelist = theApp.GetCommonSizeList();
		std::for_each(sizelist.begin(), sizelist.end(), AddToListSizeCombo(&m_wndFontListSizeCombo));

		// show current size
		CString strPixels;
		strPixels.LoadString(IDS_FONTLIST_PX);
		strText.Format(strPixels, theApp.GetProgramOptions()->GetFontListCharSize());

		m_wndFontListSizeCombo.SetWindowText(strText);
	}
	else
	{
		TRACE0("Failed to create font list size combo\n");
        return -1; // fail to create
	}

	// create font list toolbar...
	if (!m_pwndFontListToolbar.get())
		m_pwndFontListToolbar.reset(new CPrettyToolbar());
	
	m_pwndFontListToolbar->SetBorders(1, 1, 1, 1);

	if ((!m_pwndFontListToolbar->CreateEx(this, TBSTYLE_FLAT | TBSTYLE_TRANSPARENT,
							  WS_CHILD | WS_VISIBLE | CBRS_ALIGN_TOP |
							  CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC)) ||
       (!m_pwndFontListToolbar->LoadToolBar(IDR_FONTLIST_TOOLBAR)))
    {
        TRACE0("Failed to create fontlist toolbar\n");
        return -1; // fail to create
    }

	m_pwndFontListToolbar->LoadToolBar(kToolbarButtonSize,
							 IDB_FONTLISTTOOLBAR_NORMAL,
							 IDB_FONTLISTTOOLBAR_HOT,
							 IDB_FONTLISTTOOLBAR_DISABLED,
							 IDB_FONTLISTTOOLBAR_NORMAL256,
							 IDB_FONTLISTTOOLBAR_HOT256);

	// ...setup button info...
	m_pwndFontListToolbar->SetButtonInfo(0, ID_VIEW_AUTOINSTALL, TBBS_CHECKBOX, 0);

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// set up rebar
	if (!m_wndRebar.Create(this))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	CProgramOptions* pProgramOptions = theApp.GetProgramOptions();
	size_t nLayoutSize = pProgramOptions->GetMainRebarLayoutSize();

	// get size of main toolbar
	CRect rectMainToolbar;
	int nLastButton = m_pwndToolbar->GetToolBarCtrl().GetButtonCount() - 1;
	m_pwndToolbar->GetToolBarCtrl().GetItemRect(nLastButton, &rectMainToolbar);

	const DWORD dwGripperFlags = RBBS_NOGRIPPER | RBBS_GRIPPERALWAYS;
	bool bNeedRebarSetup = true;
	boost::scoped_ptr<unsigned char> pLayoutData;
	REBARBANDINFO* pBandInfo = NULL;
	if (nLayoutSize)
	{
		pLayoutData.reset(new unsigned char[nLayoutSize]);
		pProgramOptions->GetMainRebarLayout(pLayoutData.get(), nLayoutSize);
		pBandInfo = reinterpret_cast<REBARBANDINFO*>(pLayoutData.get());

		// make sure size matches before using loaded rebar info
		if (pBandInfo[0].cbSize == sizeof(REBARBANDINFO))
		{
			if (nLayoutSize == sizeof(REBARBANDINFO) * m_cnNumBands)
			{
				for (unsigned int n = 0; n < m_cnNumBands; ++n)
				{
					switch (pBandInfo[n].lParam)
					{
						// menu bar
						case 0:
							m_wndRebar.AddBar(m_pwndMenuBar.get(), 0, 0, pBandInfo[n].fStyle);
							break;

						// main toolbar
						case 1:
							m_wndRebar.AddBar(m_pwndToolbar.get(), 0, 0, pBandInfo[n].fStyle);
							m_nMainToolBarIndex = n;
							m_bMainToolBarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);

							// set correct child size in rebar
							pBandInfo[n].cyMinChild = pBandInfo[n].cyChild = pBandInfo[n].cyMaxChild = rectMainToolbar.Height();

							break;

						// location combo
						case 2:
							strText.LoadString(IDS_MAINFRAME_LOCATION);
							m_wndRebar.AddBar(&m_wndLocationCombo, strText, 0, pBandInfo[n].fStyle);
							m_nLocationBarIndex = n;
							m_bLocationBarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;

						// size combo
						case 3:
							strText.LoadString(IDS_FONTLIST_SIZELABEL);
							m_wndRebar.AddBar(&m_wndFontListSizeCombo, strText, 0, pBandInfo[n].fStyle);
							m_nFontListPreviewSizeToolbarIndex = n;
							m_bFontListPreviewSizeToolbarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;

						// font list toolbar
						case 4:
							m_wndRebar.AddBar(m_pwndFontListToolbar.get(), 0, pBandInfo[n].fStyle);
							m_nFontSharingButtonIndex = n;
							m_bFontSharingButtonVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;
					}

					// ensure correct colors are shown
					pBandInfo[n].fMask |= RBBIM_COLORS;
					pBandInfo[n].clrFore = ::GetSysColor(COLOR_MENUTEXT);
					pBandInfo[n].clrBack = ::GetSysColor(COLOR_MENU);

					m_wndRebar.GetReBarCtrl().SetBandInfo(n, &pBandInfo[n]);
				}

				// success = we don't need any further rebar setup after this
				bNeedRebarSetup = false;
			}
			else if (nLayoutSize == sizeof(REBARBANDINFO) * (m_cnNumBands - 1))
			{
				DWORD dwMenuBarFlags = 0;
				if (pBandInfo[0].fStyle & dwGripperFlags)
					dwMenuBarFlags = dwGripperFlags;

				// menu bar first
				m_wndRebar.AddBar(m_pwndMenuBar.get(), 0, 0, dwMenuBarFlags);

				// the first band should be on a new line, this way the menu
				// starts out where it was in the previous version
				pBandInfo[0].fStyle |= RBBS_BREAK;

				// everything else according to old order
				for (unsigned int n = 0; n < m_cnNumBands - 1; ++n)
				{
					switch (pBandInfo[n].lParam)
					{
						// main toolbar
						case 0:
							m_wndRebar.AddBar(m_pwndToolbar.get(), 0, 0, pBandInfo[n].fStyle);
							m_nMainToolBarIndex = n + 1;
							m_bMainToolBarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);

							// set correct child size in rebar
							pBandInfo[n].cyMinChild = pBandInfo[n].cyChild = pBandInfo[n].cyMaxChild = rectMainToolbar.Height();

							break;

						// location combo
						case 1:
							strText.LoadString(IDS_MAINFRAME_LOCATION);
							m_wndRebar.AddBar(&m_wndLocationCombo, strText, 0, pBandInfo[n].fStyle);
							m_nLocationBarIndex = n + 1;
							m_bLocationBarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;

						// size combo
						case 2:
							strText.LoadString(IDS_FONTLIST_SIZELABEL);
							m_wndRebar.AddBar(&m_wndFontListSizeCombo, strText, 0, pBandInfo[n].fStyle);
							m_nFontListPreviewSizeToolbarIndex = n + 1;
							m_bFontListPreviewSizeToolbarVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;

						// font list toolbar
						case 3:
							m_wndRebar.AddBar(m_pwndFontListToolbar.get(), 0, pBandInfo[n].fStyle);
							m_nFontSharingButtonIndex = n + 1;
							m_bFontSharingButtonVisible = !(pBandInfo[n].fStyle & RBBS_HIDDEN);
							break;
					}

					// band indexes have moved up one as result of menu
					++pBandInfo[n].lParam;

					// ensure correct colors are shown
					pBandInfo[n].fMask |= RBBIM_COLORS;
					pBandInfo[n].clrFore = ::GetSysColor(COLOR_MENUTEXT);
					pBandInfo[n].clrBack = ::GetSysColor(COLOR_MENU);

					m_wndRebar.GetReBarCtrl().SetBandInfo(n + 1, &pBandInfo[n]);
				}

				// success = we don't need any further rebar setup after this
				bNeedRebarSetup = false;
			}
		}
	}
	
	if (bNeedRebarSetup)
	{
		// main menu bar
		m_wndRebar.AddBar(m_pwndMenuBar.get(), 0, 0, dwGripperFlags);

		// get size of toolbar
		CRect rectMenuBar;
		nLastButton = m_pwndMenuBar->GetToolBarCtrl().GetButtonCount() - 1;
		m_pwndMenuBar->GetToolBarCtrl().GetItemRect(nLastButton, &rectMenuBar);

		// set correct child size in rebar
		REBARBANDINFO rbbi;
		rbbi.cbSize = sizeof(REBARBANDINFO);
		rbbi.fMask = RBBIM_CHILDSIZE;
		rbbi.cxMinChild = rectMenuBar.right;
		rbbi.cyMinChild = rbbi.cyChild = rbbi.cyMaxChild = rectMenuBar.Height();
		rbbi.cyIntegral = 0;
		m_wndRebar.GetReBarCtrl().SetBandInfo(0, &rbbi);

		// main toolbar
		m_wndRebar.AddBar(m_pwndToolbar.get(), 0, 0, RBBS_BREAK | RBBS_USECHEVRON | dwGripperFlags);

		// set correct child size in rebar
		rbbi.cbSize = sizeof(REBARBANDINFO);
		rbbi.fMask = RBBIM_CHILDSIZE;
		rbbi.cxMinChild = rectMainToolbar.right;
		rbbi.cyMinChild = rbbi.cyChild = rbbi.cyMaxChild = rectMainToolbar.Height();
		rbbi.cyIntegral = 0;
		m_wndRebar.GetReBarCtrl().SetBandInfo(1, &rbbi);
		m_nMainToolBarIndex = 1;

		// location combo
		strText.LoadString(IDS_MAINFRAME_LOCATION);
		m_wndRebar.AddBar(&m_wndLocationCombo, strText, 0, RBBS_BREAK | dwGripperFlags);
		m_nLocationBarIndex = 2;

		// size combo
		strText.LoadString(IDS_FONTLIST_SIZELABEL);
		m_wndRebar.AddBar(&m_wndFontListSizeCombo, strText, 0, dwGripperFlags);
		m_nFontListPreviewSizeToolbarIndex = 3;

		// font list toolbar
		m_wndRebar.AddBar(m_pwndFontListToolbar.get(), 0, 0, dwGripperFlags);
		m_nFontSharingButtonIndex = 4;

		// give each band an index
		for (unsigned int n = 0; n < m_cnNumBands; ++n)
		{
			rbbi.cbSize = sizeof(rbbi);
			rbbi.fMask = RBBIM_LPARAM;
			rbbi.lParam = n;
			m_wndRebar.GetReBarCtrl().SetBandInfo(n, &rbbi);
		}
	}

	// setup status bar
	UINT nID, nStyle;
	int nWidth;

	m_wndStatusBar.GetPaneInfo(0, nID, nStyle, nWidth);
	m_wndStatusBar.SetPaneInfo(0, nID, SBPS_STRETCH | SBPS_NORMAL, nWidth);

	// remove text from info panes (these will be filled in as the user browses fonts)
	m_wndStatusBar.SetPaneText(1, _T(""));

	// maximize if that's how the user left it
	if (theApp.GetProgramOptions()->GetMainWindowMaximized())
	{
		m_bMaximizedOnStartup = true;
		ShowWindow(SW_MAXIMIZE);
	}

	// make sure main window is in screen work area
	CWinToolbox::MoveWindowIntoWorkArea(m_hWnd);

	// Base class OnCreate screws up this size.  Fix it.
	GetMainSplitter()->SetColumnInfo(0, theApp.GetProgramOptions()->GetTreeViewWidth(), 0);
	GetMainSplitter()->RecalcLayout();

	// listen to main splitter
	BindListenerToSplitter(this, m_wndSplitterMain, &CMainFrame::OnTrackRow_MainSplitter, &CMainFrame::OnTrackCol_MainSplitter);
	BindListenerToSplitter(this, m_wndTreeViewSplit, &CMainFrame::OnTrackRow_TreeSplitter, &CMainFrame::OnTrackCol_TreeSplitter);
	BindListenerToSplitter(this, m_wndFontViewSplit, &CMainFrame::OnTrackRow_DetailSplitter, &CMainFrame::OnTrackCol_DetailSplitter);

	// listen to toolbars
	m_pwndMenuBar->connect_to_rclick(boost::bind(&CMainFrame::OnToolbarRClick, this, _1, _2));
	m_wndRebar.connect_to_rclick(boost::bind(&CMainFrame::OnToolbarRClick, this, _1, _2));
	m_pwndToolbar->connect_to_rclick(boost::bind(&CMainFrame::OnToolbarRClick, this, _1, _2));
	m_pwndFontListToolbar->connect_to_rclick(boost::bind(&CMainFrame::OnToolbarRClick, this, _1, _2));

	m_ilPrintMenu.Create(16, 16, ILC_COLOR32, 2, 0);
	HICON hIcon = theApp.LoadIcon(IDI_PRINTER_ICON);
	m_ilPrintMenu.Add(hIcon);
	::DestroyIcon(hIcon);

	hIcon = theApp.LoadIcon(IDI_PRINT_PREVIEW_ICON);
	m_ilPrintMenu.Add(hIcon);
	::DestroyIcon(hIcon);

	// save detail pane height
	int nMin = 0;
	m_wndFontViewSplit.GetRowInfo(1, m_nDetailPaneHeight, nMin);

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	// no standard menu -- it's in a toolbar
	if (cs.hMenu)
	{
		::DestroyMenu(cs.hMenu);
		cs.hMenu = NULL;
	}

	CProgramOptions* pOptions = theApp.GetProgramOptions();

	CPoint pt;
	CSize size;
	pOptions->GetMainWindowPosition(pt);
	pOptions->GetMainWindowSize(size);

	// initialize window in middle of primary monitor
	if (size.cx == -1 && size.cy == -1)
	{
		CRect rectWorkArea;
		::SystemParametersInfo(SPI_GETWORKAREA, 0, &rectWorkArea, FALSE);

		pt.x = (LONG)(rectWorkArea.Width() * 0.125);
		pt.y = (LONG)(rectWorkArea.Height() * 0.125);
		size.cx = rectWorkArea.Width() - (pt.x * 2);
		size.cy = rectWorkArea.Height() - (pt.y * 2);
	}

	cs.x = pt.x;
	cs.y = pt.y;
	cs.cx = size.cx;
	cs.cy = size.cy;

	return TRUE;
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
	if (pMsg->message == WM_SYSKEYDOWN && pMsg->wParam == VK_MENU)
		m_pwndMenuBar->SetPrefix(true);
	else if (pMsg->message == WM_KEYUP && pMsg->wParam == VK_MENU)
		m_pwndMenuBar->SetPrefix(false);
	else if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)
		m_pwndMenuBar->Highlight(-1);

	return CFrameWnd::PreTranslateMessage(pMsg);
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	CFontRunnerSignals* pSignals = theApp.GetSignals();

	// need to be notified of program shutdown
	pSignals->ConnectTo_Shutdown(boost::bind(&CMainFrame::OnFontRunnerShutdown, this));

	// notification when folder changes
	pSignals->ConnectTo_SelectedFolderChange(boost::bind(&CMainFrame::OnSelectedFolderChange, this, _1, _2));

	// notification when project changes
	pSignals->ConnectTo_SelectedProjectChange(boost::bind(&CMainFrame::OnSelectedProjectChange, this, _1));

	// get a pointer to the options structure so we know
	// how to size the panes
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// create a splitter with 1 row, 2 columns as the main splitter
	m_wndSplitterMain.CreateStatic(this, 1, 2);

	// create tree view splitter
	m_wndTreeViewSplit.CreateStatic(&m_wndSplitterMain,
									2, 1,
									WS_CHILD | WS_VISIBLE | WS_BORDER,
									m_wndSplitterMain.IdFromRowCol(0, 0));

	// create splitter for font list view and preview pane
	m_wndFontViewSplit.CreateStatic(&m_wndSplitterMain,
									2, 1,
									WS_CHILD | WS_VISIBLE | WS_BORDER,
									m_wndSplitterMain.IdFromRowCol(0, 1));
	
	CSize sizeView(pOptions->GetTreeViewWidth(), pOptions->GetTreeViewHeight());

	// add the tree view
	if (sizeView.cx == 0xFFFFFFFF || sizeView.cy == 0xFFFFFFFF)
	{
		// default width of tree view pane is 1/6 of total window width
		sizeView.cx = (LONG)(lpcs->cx * (1.0 / 6.0));

		// also 50% of window height
		sizeView.cy = (LONG)(lpcs->cy * 0.5);

		// save sizes now (other functions will need them)
		pOptions->SetTreeViewWidth(sizeView.cx);
		pOptions->SetTreeViewHeight(sizeView.cy);
	}

	VERIFY(m_wndTreeViewSplit.CreateView(0, 0,
										 RUNTIME_CLASS(CShellTreeView),
										 sizeView,
										 pContext));

	VERIFY(m_wndTreeViewSplit.CreateView(1, 0,
										 RUNTIME_CLASS(CFontProjectFrame),
										 sizeView,
										 pContext));

	// create two views inside the nested splitter
	sizeView.cx = 0; // doesn't matter now
	sizeView.cy = pOptions->GetFontListHeight();
	if (sizeView.cy == 0xFFFFFFFF)
	{
		// 60% of window height
		sizeView.cy = (LONG)(lpcs->cy * 0.6);
		pOptions->SetFontListHeight(sizeView.cy);
	}
	VERIFY(m_wndFontViewSplit.CreateView(0, 0, 
										 RUNTIME_CLASS(CFontListFrame),
										 sizeView,
										 pContext));

	VERIFY(m_wndFontViewSplit.CreateView(1, 0,
										 RUNTIME_CLASS(CFontDetailFrame),
										 sizeView,
										 pContext));

	// a view needs to be the active view
	CView* pView = dynamic_cast<CView*>(m_wndFontViewSplit.GetPane(0, 0));
	SetActiveView(pView);

	// save pointers to some frames
	m_pPreviewFrame = dynamic_cast<CFontDetailFrame*>(m_wndFontViewSplit.GetPane(1,0));

	ASSERT(m_pPreviewFrame);

	return TRUE;
}

void CMainFrame::OnFontRunnerShutdown()
{
	// if minimized, restore
	if (IsIconic())
		ShowWindow(SW_RESTORE);

	// get window size and position so that we can save them to registry
	CRect rect;
	GetWindowRect(&rect);

	CProgramOptions* pOptions = theApp.GetProgramOptions();
	pOptions->SetMainWindowPosition(CPoint(rect.left, rect.top));
	pOptions->SetMainWindowSize(CSize(rect.Width(), rect.Height()));
	pOptions->SetMainWindowMaximized(IsZoomed() != 0);

	int nMin = 0;
	int nValue = 0;

	// get width of tree pane
	GetMainSplitter()->GetColumnInfo(0, nValue, nMin);
	pOptions->SetTreeViewWidth(nValue);

	// tree view splitter may not always exist
	if (::IsWindow(GetTreeViewSplit()->GetSafeHwnd()))
	{
		GetTreeViewSplit()->GetRowInfo(0, nValue, nMin);
		pOptions->SetTreeViewHeight(nValue);
	}

	// get height of font list
	GetFontViewSplit()->GetRowInfo(0, nValue, nMin);
	pOptions->SetFontListHeight(nValue);

	// save rebar state
	size_t nRebarLayoutDataSize = sizeof(REBARBANDINFO) * m_cnNumBands;
	boost::scoped_array<unsigned char> pRebarLayout(new unsigned char[nRebarLayoutDataSize]);
	unsigned char* pLayoutData = pRebarLayout.get();

	// set up min/max sizes and ideal sizes for main toolbar
	REBARBANDINFO rbbi;
	for (unsigned int n = 0; n < m_cnNumBands; ++n)
	{
		rbbi.cbSize = sizeof(REBARBANDINFO);
		rbbi.fMask = RBBIM_SIZE | RBBIM_STYLE | RBBIM_LPARAM | RBBIM_CHILDSIZE;
		m_wndRebar.GetReBarCtrl().GetBandInfo(n, &rbbi);
		memcpy(pLayoutData, &rbbi, sizeof(REBARBANDINFO));
		pLayoutData += sizeof(REBARBANDINFO);
	}

	pOptions->SetMainRebarLayout(pRebarLayout.get(), nRebarLayoutDataSize);
}

void CMainFrame::OnFavoritesEnum(const std::wstring& str)
{
	// make sure it exists
	if (!::PathFileExists(str.c_str()))
		return;

	// stop if out of menu IDs
	if (m_nFavoritesMenuID > ID_FAVORITESMENU_END)
		return;

	// copy item to char buffer
	wchar_t szItem[MAX_PATH];
	std::memset(szItem, 0, MAX_PATH);
	str._Copy_s(szItem, MAX_PATH, str.length());

	// compact the path
	if (!::PathCompactPath(m_hdcFavoritesMenu, szItem, 250))
		return;
	
	// create and init a menu item info struct
	MENUITEMINFO mii;
	::memset(&mii, 0, sizeof(MENUITEMINFO));
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_ID | MIIM_STATE | MIIM_STRING | MIIM_DATA;
	mii.fType = MFT_STRING;
	mii.wID = m_nFavoritesMenuID;
	mii.dwTypeData = szItem;
	mii.cch = (UINT)str.length();
	mii.dwItemData = (ULONG_PTR)&str;

	if (!m_bFoundCurrentFavorite && lowercase_compare(str, m_strCurrentFavoriteFolder) == 0)
	{
		mii.fState = MFS_CHECKED;
		m_bFoundCurrentFavorite = true;
	}
	else
		mii.fState = MFS_UNCHECKED;

	m_pFavoritesMenu->InsertMenuItem(m_nFavoritesMenuID++, &mii);
}

void CMainFrame::UpdateBandColors()
{
	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(REBARBANDINFO);
	rbbi.fMask = RBBIM_COLORS;
	rbbi.clrFore = ::GetSysColor(COLOR_MENUTEXT);
	rbbi.clrBack = ::GetSysColor(COLOR_MENU);
	for (UINT n = 0; n < m_cnNumBands; ++n)
		m_wndRebar.GetReBarCtrl().SetBandInfo(n, &rbbi);
}

// bitmaps are 16 x 16
static const int bitmapsize = 16;
static const int bitmapmargin = 2;

void CMainFrame::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (lpMeasureItemStruct->CtlType != ODT_MENU)
	{
		CFrameWnd::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
		return;
	}

	// figure out which string to load
	UINT nTextID =
		(lpMeasureItemStruct->itemID == ID_FILE_PRINT) ?
			IDS_GENERAL_PRINTMORE : IDS_GENERAL_PRINTPREVIEW;

	// load string
	CString strText;
	strText.LoadString(nTextID);
	
	// get device context and save its state
	CDC* pDC = GetDC();
	int nSavedDC = pDC->SaveDC();

	// get default menu-drawing font
	CFont menufont;
	NONCLIENTMETRICS ncm;
	ncm.cbSize = sizeof(NONCLIENTMETRICS);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, &ncm, 0);
	menufont.CreateFontIndirect(&ncm.lfMenuFont);
	pDC->SelectObject(&menufont);
	
	CSize size = pDC->GetTextExtent(strText);
	lpMeasureItemStruct->itemWidth = size.cx + bitmapsize + bitmapmargin * 4;
	lpMeasureItemStruct->itemHeight = std::max<const int>(bitmapsize + bitmapmargin * 2, size.cy);

	// restore and release DC
	pDC->RestoreDC(nSavedDC);
}

void CMainFrame::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if (lpDrawItemStruct->CtlType != ODT_MENU)
	{
		CFrameWnd::OnDrawItem(nIDCtl, lpDrawItemStruct);
		return;
	}

	// figure out which string to load
	UINT nTextID =
		(lpDrawItemStruct->itemID == ID_FILE_PRINT) ?
			IDS_GENERAL_PRINTMORE : IDS_GENERAL_PRINTPREVIEW;

	// load string
	CString strText;
	strText.LoadString(nTextID);
	
	// get device context and save its state
	CDC dc;
	dc.Attach(lpDrawItemStruct->hDC);
	int nSavedDC = dc.SaveDC();

	bool bSelected = (lpDrawItemStruct->itemState & ODS_SELECTED);

	// paint background
	COLORREF clrBackground =
		::GetSysColor(bSelected ? COLOR_HIGHLIGHT : COLOR_MENU);
	dc.FillSolidRect(&lpDrawItemStruct->rcItem, clrBackground);

	// draw icon
	m_ilPrintMenu.Draw(
		&dc,
		(lpDrawItemStruct->itemID == ID_FILE_PRINT) ? 0 : 1,
		CPoint(lpDrawItemStruct->rcItem.left + bitmapmargin, lpDrawItemStruct->rcItem.top + bitmapmargin),
		ILD_NORMAL);

	// get default menu-drawing font
	CFont menufont;
	NONCLIENTMETRICS ncm;
	ncm.cbSize = sizeof(NONCLIENTMETRICS);
	SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, &ncm, 0);
	menufont.CreateFontIndirect(&ncm.lfMenuFont);
	dc.SelectObject(&menufont);

	// determine text color
	COLORREF clrText = ::GetSysColor(bSelected ? COLOR_HIGHLIGHTTEXT : COLOR_MENUTEXT);
	dc.SetTextColor(clrText);
	dc.SetBkColor(clrBackground);

	// determine text position
	CSize sizeText = dc.GetTextExtent(strText);
	int nHeight = lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top;

	dc.TextOut(lpDrawItemStruct->rcItem.left + bitmapmargin * 3 + bitmapsize,
			   lpDrawItemStruct->rcItem.top + ((nHeight / 2) - (sizeText.cy / 2)), strText);

	// restore and release DC
	dc.RestoreDC(nSavedDC);
	dc.Detach();
}

void CMainFrame::OnPrintDropDown()
{
	CMenu menu;
	menu.CreatePopupMenu();
	menu.AppendMenu(MFT_OWNERDRAW, ID_FILE_PRINT);
	menu.AppendMenu(MFT_OWNERDRAW, ID_FILE_PRINT_PREVIEW);

	// get screen coordinates of toolbar button
	CRect rectScreen, rectButton;
	m_pwndToolbar->GetToolBarCtrl().GetWindowRect(&rectScreen);
	UINT nIndex = m_pwndToolbar->GetToolBarCtrl().CommandToIndex(ID_FILE_PRINT);
	m_pwndToolbar->GetToolBarCtrl().GetItemRect(nIndex, &rectButton);
	CPoint pt(rectScreen.left + rectButton.left,
			  rectScreen.top + rectButton.top + rectButton.bottom);

	// show menu
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);
}

void CMainFrame::OnFavoritesDropDown()
{
	MENUITEMINFO mii;
	CMenu menu;
	menu.CreatePopupMenu();

	CFontRunnerDoc* pDoc = dynamic_cast<CFontRunnerDoc*>(GetActiveDocument());
	ASSERT(pDoc);
	if (!pDoc)
		return;

	CFavoriteFolders* pFavorites = theApp.GetFavoriteFolders();

	if (!pFavorites->Empty())
	{
		// need a DC to compact the path
		CDC* pDC = GetDC();
		
		// get default menu-drawing font
		NONCLIENTMETRICS ncm;
		ncm.cbSize = sizeof(NONCLIENTMETRICS);
		::SystemParametersInfo(SPI_GETNONCLIENTMETRICS, ncm.cbSize, &ncm, 0);
		CFont menufont;
		menufont.CreateFontIndirect(&ncm.lfMenuFont);
		CFont* pOldFont = pDC->SelectObject(&menufont);

		// setup for enum
		m_pFavoritesMenu = &menu;
		m_hdcFavoritesMenu = pDC->GetSafeHdc();
		m_nFavoritesMenuID = ID_FAVORITESMENU_BEGIN;
		m_bFoundCurrentFavorite = false;
		m_strCurrentFavoriteFolder = (LPCTSTR)CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ();

		pFavorites->ForEach(boost::bind(&CMainFrame::OnFavoritesEnum, this, _1));

		// cleanup DC -- we're done with it
		pDC->SelectObject(pOldFont);
		ReleaseDC(pDC);

		// add separator
		::memset(&mii, 0, sizeof(MENUITEMINFO));
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fType = MFT_SEPARATOR;
		menu.InsertMenuItem(menu.GetMenuItemCount(), &mii, true);
	}

	CString strMenu;

	strMenu.LoadString(IDS_FAVORITES_ADD);
	::memset(&mii, 0, sizeof(MENUITEMINFO));
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_ID | MIIM_STRING;
	mii.fType = MFT_STRING;
	mii.wID = ID_FAVORITESMENU_ADD;
	mii.cch = strMenu.GetLength();
	mii.dwTypeData = strMenu.GetBuffer();

	// insert add option
	menu.InsertMenuItem(ID_FAVORITESMENU_ADD, &mii);
	strMenu.ReleaseBuffer();
	
	strMenu.LoadString(IDS_FAVORITES_MANAGE);
	::memset(&mii, 0, sizeof(MENUITEMINFO));
	mii.cbSize = sizeof(MENUITEMINFO);
	mii.fMask = MIIM_ID | MIIM_STRING;
	mii.fType = MFT_STRING;
	mii.wID = ID_FAVORITESMENU_MANAGE;
	mii.cch = strMenu.GetLength();
	mii.dwTypeData = strMenu.GetBuffer();

	// finally, insert manage option
	menu.InsertMenuItem(ID_FAVORITESMENU_MANAGE, &mii);
	strMenu.ReleaseBuffer();

	// get screen coordinates of toolbar button
	CRect rectScreen, rectButton;
	m_pwndToolbar->GetToolBarCtrl().GetWindowRect(&rectScreen);
	UINT nIndex = m_pwndToolbar->GetToolBarCtrl().CommandToIndex(ID_VIEW_FAVORITES);
	m_pwndToolbar->GetToolBarCtrl().GetItemRect(nIndex, &rectButton);
	CPoint pt(rectScreen.left + rectButton.left,
			  rectScreen.top + rectButton.top + rectButton.bottom);

	// show menu
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pt.x, pt.y, this);
}

// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG


// CMainFrame message handlers

void CMainFrame::OnMenuBarCommands(UINT /*nCommandID*/)
{
	
}

LRESULT CMainFrame::OnAreYouFontRunner(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return CFontRunnerApp::s_uwmAreYouFontRunner;
}

void CMainFrame::OnViewRefresh()
{
	// trip refreshing flag
	m_bRefreshing = true;

	theApp.GetSignals()->Fire_Refresh();

	// trip refreshing flag
	m_bRefreshing = false;
}

void CMainFrame::OnViewInstalled()
{
	// tell tree to navigate to system fonts folder
	theApp.GetSignals()->Fire_SelectedFolderChange(CFontRunnerDoc::GetDoc()->m_strSystemFontsFolder);
}

void CMainFrame::OnHelpContentsandindexf1()
{
	// launch HTML help
	HtmlHelp(HIDP_START, HH_HELP_CONTEXT);
}

void CMainFrame::OnUpdateStatusBarPane(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(TRUE);
}

void CMainFrame::OnPreviewFgcolor()
{
	theApp.GetSignals()->Fire_OpenColorPalette(true);
}

void CMainFrame::OnPreviewBgcolor()
{
	theApp.GetSignals()->Fire_OpenColorPalette(false);
}

void CMainFrame::OnPreviewBold()
{
	// negate bold flag
	theApp.GetProgramOptions()->SetPreviewFontBold(!theApp.GetProgramOptions()->IsPreviewFontBold());

	// refresh to show changes
	CRichTextPreviewView* pView = (CRichTextPreviewView*)m_pPreviewFrame->GetActiveView();
	pView->RefreshFontView();

	// update toolbar
	m_pPreviewFrame->CheckPreviewToolbarButton(ID_PREVIEW_BOLD, theApp.GetProgramOptions()->IsPreviewFontBold());
}

void CMainFrame::OnPreviewItalic()
{
	// negate italic flag
	theApp.GetProgramOptions()->SetPreviewFontItalic(!theApp.GetProgramOptions()->IsPreviewFontItalic());

	// refresh to show changes
	CRichTextPreviewView* pView = (CRichTextPreviewView*)m_pPreviewFrame->GetActiveView();
	pView->RefreshFontView();

	// update toolbar
	m_pPreviewFrame->CheckPreviewToolbarButton(ID_PREVIEW_ITALIC, theApp.GetProgramOptions()->IsPreviewFontItalic());
}

void CMainFrame::OnPreviewUnderline()
{
	// negate underline flag
	theApp.GetProgramOptions()->SetPreviewFontUnderlined(!theApp.GetProgramOptions()->IsPreviewFontUnderlined());

	// refresh to show changes
	CRichTextPreviewView* pView = (CRichTextPreviewView*)m_pPreviewFrame->GetActiveView();
	pView->RefreshFontView();

	// update toolbar
	m_pPreviewFrame->CheckPreviewToolbarButton(ID_PREVIEW_UNDERLINE, theApp.GetProgramOptions()->IsPreviewFontUnderlined());
}

void CMainFrame::OnUpdatePreviewBold(CCmdUI *pCmdUI)
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();
	if (pOptions)
	{
		pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontBold());
		pCmdUI->Enable(pOptions->GetFontDetailMode() == 0);
	}
}

void CMainFrame::OnUpdatePreviewUnderline(CCmdUI *pCmdUI)
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();
	if (pOptions)
	{
		pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontUnderlined());
		pCmdUI->Enable(pOptions->GetFontDetailMode() == 0);
	}
}

void CMainFrame::OnUpdatePreviewItalic(CCmdUI *pCmdUI)
{
	CProgramOptions* pOptions = theApp.GetProgramOptions();
	if (pOptions)
	{
		pCmdUI->SetCheck(theApp.GetProgramOptions()->IsPreviewFontItalic());
		pCmdUI->Enable(pOptions->GetFontDetailMode() == 0);
	}
}

void CMainFrame::OnViewBack()
{
	if (m_pHistoryList->CanGoBack())
	{
		m_bHistoryNav = true;
		const HistoryItem* pItem = m_pHistoryList->GoBack();
		if (pItem->nType == HistoryItem::kType_Folder)
			theApp.GetSignals()->Fire_SelectedFolderChange((LPCTSTR)pItem->strName);
		else
			theApp.GetSignals()->Fire_SelectedProjectChange((LPCTSTR)pItem->strName);
		m_bHistoryNav = false;
	}
}

void CMainFrame::OnViewForward()
{
	if (m_pHistoryList->CanGoForward())
	{
		m_bHistoryNav = true;
		const HistoryItem* pItem = m_pHistoryList->GoForward();
		if (pItem->nType == HistoryItem::kType_Folder)
			theApp.GetSignals()->Fire_SelectedFolderChange((LPCTSTR)pItem->strName);
		else
			theApp.GetSignals()->Fire_SelectedProjectChange((LPCTSTR)pItem->strName);
		m_bHistoryNav = false;
	}
}

void CMainFrame::OnUpdateViewBack(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_pHistoryList->CanGoBack());
}

void CMainFrame::OnUpdateViewForward(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(m_pHistoryList->CanGoForward());
}

void CMainFrame::OnViewUpFolder()
{
	theApp.GetSignals()->Fire_FolderUp();
}

void CMainFrame::OnUpdateViewUpFolder(CCmdUI *pCmdUI)
{
	pCmdUI->Enable(theApp.GetSignals()->Query_FolderUp());
}

void CMainFrame::OnViewMainToolBar()
{
	m_bMainToolBarVisible = !m_bMainToolBarVisible;

	m_wndRebar.GetReBarCtrl().ShowBand(m_nMainToolBarIndex, m_bMainToolBarVisible);
}

void CMainFrame::OnUpdateViewMainToolBar(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bMainToolBarVisible ? BST_CHECKED : BST_UNCHECKED);
}

void CMainFrame::OnViewLocationBar()
{
	m_bLocationBarVisible = !m_bLocationBarVisible;

	m_wndRebar.GetReBarCtrl().ShowBand(m_nLocationBarIndex, m_bLocationBarVisible);
}

void CMainFrame::OnUpdateViewLocationBar(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bLocationBarVisible ? BST_CHECKED : BST_UNCHECKED);
}

void CMainFrame::OnViewFontListPreviewSize()
{
	m_bFontListPreviewSizeToolbarVisible = !m_bFontListPreviewSizeToolbarVisible;

	m_wndRebar.GetReBarCtrl().ShowBand(m_nFontListPreviewSizeToolbarIndex, m_bFontListPreviewSizeToolbarVisible);
}

void CMainFrame::OnUpdateViewFontListPreviewSize(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bFontListPreviewSizeToolbarVisible ? BST_CHECKED : BST_UNCHECKED);
}

void CMainFrame::OnViewFontSharingButton()
{
	m_bFontSharingButtonVisible = !m_bFontSharingButtonVisible;

	m_wndRebar.GetReBarCtrl().ShowBand(m_nFontSharingButtonIndex, m_bFontSharingButtonVisible);
}

void CMainFrame::OnUpdateViewFontSharingButton(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(m_bFontSharingButtonVisible ? BST_CHECKED : BST_UNCHECKED);
}

void CMainFrame::OnToolbarLockthetoolbars()
{
	for (UINT n = 0; n <= m_wndRebar.GetReBarCtrl().GetBandCount(); ++n)
	{
		REBARBANDINFO rbbi;
		rbbi.cbSize = sizeof(rbbi);
		rbbi.fMask  = RBBIM_STYLE;
		
		m_wndRebar.GetReBarCtrl().GetBandInfo(n, &rbbi);
		rbbi.fStyle ^= RBBS_NOGRIPPER | RBBS_GRIPPERALWAYS;
		m_wndRebar.GetReBarCtrl().SetBandInfo(n, &rbbi);
	}
}

void CMainFrame::OnUpdateToolbarLockthetoolbars(CCmdUI *pCmdUI)
{
	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask  = RBBIM_STYLE;
	m_wndRebar.GetReBarCtrl().GetBandInfo(0, &rbbi);

	pCmdUI->SetCheck(rbbi.fStyle & (RBBS_NOGRIPPER | RBBS_GRIPPERALWAYS) ? BST_CHECKED : BST_UNCHECKED);
}

void CMainFrame::OnSize(UINT nType, int cx, int cy)
{
	CFrameWnd::OnSize(nType, cx, cy);

	// grab program options pointer
	CProgramOptions* pOptions = theApp.GetProgramOptions();

	// see if we're changing minimized/maximized state
	bool bZoomed = (IsZoomed() != 0);
	bool bIconic = (IsIconic() != 0);

	// save restored size if maximizing
	if (!m_bMaximized && bZoomed && !m_bMaximizedOnStartup)
		pOptions->SaveRestoredMetrics();

	// if restoring for the first time after a startup maximize, use the restored metrics
	if (m_bSysCommandRestore)
	{
		// set main window size
		SIZE size;
		pOptions->GetRestoredWindowSize(size);
		POINT pt;
		pOptions->GetRestoredWindowPosition(pt);
		SetWindowPos(NULL, pt.x, pt.y, size.cx, size.cy, SWP_NOZORDER | SWP_NOOWNERZORDER);

		// set layout
		m_wndFontViewSplit.SetRowInfo(0, pOptions->GetRestoredFontListHeight(), 0);
		m_wndFontViewSplit.RecalcLayout();

		m_bMaximizedOnStartup = false;
		m_bSysCommandRestore = false;
	}

	// resize row
	if (::IsWindow(m_wndFontViewSplit.m_hWnd) && !m_bMinimized && !bIconic && pOptions && m_sizWindow.cy != -1)
	{
		m_wndFontViewSplit.SetRowInfo(0, pOptions->GetFontListHeight() + (cy - m_sizWindow.cy), 0);
		m_wndFontViewSplit.RecalcLayout();
	}

	// update these flags
	m_bMaximized = bZoomed;
	m_bMinimized = bIconic;
}

void CMainFrame::OnSizing(UINT fwSide, LPRECT pRect)
{
	CFrameWnd::OnSizing(fwSide, pRect);

	if ((::IsWindow(m_wndFontViewSplit.m_hWnd)) && (theApp.GetProgramOptions()) && !IsIconic())
	{
		// save pre-sizing sizes
		int nMin = 0;
		int nSize = 0;
		m_wndFontViewSplit.GetRowInfo(0, nSize, nMin);
		theApp.GetProgramOptions()->SetFontListHeight(nSize);

		CRect rcWindow;
		GetClientRect(&rcWindow);
		m_sizWindow.SetSize(rcWindow.Width(), rcWindow.Height());
	}
}

void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	// this ID can have an extra 2 if result of a title bar double-click
	if (nID & 0x0002)
		nID -= 2;

	if (nID == SC_KEYMENU && m_pwndMenuBar->OnKeyMenu((UINT)lParam))
		return;

	if (nID == SC_MINIMIZE)
	{
		// save this in case we close while minimized
		int nMin = 0;
		int nSize = 0;
		m_wndFontViewSplit.GetRowInfo(0, nSize, nMin);
		theApp.GetProgramOptions()->SetFontListHeight(nSize);
	}

	if ((nID == SC_RESTORE) && m_bMaximized && m_bMaximizedOnStartup)
		m_bSysCommandRestore = true;

	// save window size
	if ((nID == SC_MINIMIZE || nID == SC_MAXIMIZE) && !IsIconic())
	{
		CRect rcWindow;
		GetClientRect(&rcWindow);
		m_sizWindow.SetSize(rcWindow.Width(), rcWindow.Height());
	}

	CFrameWnd::OnSysCommand(nID, lParam);
}

void CMainFrame::OnWindowPosChanging(WINDOWPOS* lpwndpos)
{
	CFrameWnd::OnWindowPosChanging(lpwndpos);

	if (!(lpwndpos->flags & SWP_NOSIZE) &&
		!(lpwndpos->flags & SWP_FRAMECHANGED))
	{
		// save new size
		POINT pt = { lpwndpos->x, lpwndpos->y };
		theApp.GetProgramOptions()->SetMainWindowPosition(pt);

		SIZE size = { lpwndpos->cx, lpwndpos->cy };
		theApp.GetProgramOptions()->SetMainWindowSize(size);

		int nMin = 0;
		int nSize = 0;
		m_wndFontViewSplit.GetRowInfo(0, nSize, nMin);
		theApp.GetProgramOptions()->SetFontListHeight(nSize);

		m_wndSplitterMain.GetColumnInfo(0, nSize, nMin);
		theApp.GetProgramOptions()->SetTreeViewWidth(nSize);

		if (::IsWindow(m_wndTreeViewSplit.GetSafeHwnd()))
		{
			m_wndTreeViewSplit.GetRowInfo(0, nSize, nMin);
			theApp.GetProgramOptions()->SetTreeViewHeight(nSize);
		}
	}
}

void CMainFrame::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	AfxGetApp()->HtmlHelp(dwData, nCmd);
}

void CMainFrame::OnViewAutoInstall()
{
	theApp.GetProgramOptions()->SetAutoInstall(!theApp.GetProgramOptions()->AutoInstall());
	theApp.GetSignals()->Fire_TempAutoInstallChange(theApp.GetProgramOptions()->AutoInstall());
}

void CMainFrame::OnUpdateViewAutoInstall(CCmdUI* pCmdUI)
{
	pCmdUI->SetCheck(theApp.GetProgramOptions()->AutoInstall());
}

void CMainFrame::OnUpdateFontListSize(CCmdUI *pCmdUI)
{
	// always enable this button
	pCmdUI->Enable(true);
}

void CMainFrame::OnToolbarDropDown(NMHDR* pNMHDR, LRESULT *pResult)
{
	NMTOOLBAR* pNMToolbar = (NMTOOLBAR*)pNMHDR;

	// these are the only toolbar dropdown notifications we're handling
	ASSERT(pNMToolbar->iItem == ID_VIEW_FAVORITES ||
		   pNMToolbar->iItem == ID_FILE_PRINT);

	switch (pNMToolbar->iItem)
	{
		case ID_VIEW_FAVORITES:
			OnFavoritesDropDown();
			break;
		case ID_FILE_PRINT:
			OnPrintDropDown();
			break;
	}

	*pResult = 0;
}

void CMainFrame::OnAddFavorite()
{
	CFavoriteFolders* pFavorites = theApp.GetFavoriteFolders();

	CString strCurrent = CFontRunnerDoc::GetDoc()->GetCurrentFolderNameFQ();
	if (!pFavorites->Exists((LPCTSTR)strCurrent))
	{
		pFavorites->AddFolder((LPCTSTR)strCurrent);
		ShowFavoritesBalloon(&strCurrent);
	}
	else
		ShowFavoritesBalloon(NULL);
}

void CMainFrame::OnUpdateAddFavorite(CCmdUI* pCmdUI)
{
	// can't add font project to favorites
	pCmdUI->Enable(!CFontRunnerDoc::GetDoc()->UsingFontProject());
}

void CMainFrame::OnManageFavorites()
{
	// open favorites dialog
	CFavoritesDlg dlg;
	dlg.DoModal();
}

void CMainFrame::OnFavoritesMenu(UINT nID)
{
	CFavoriteFolders* pFavorites = theApp.GetFavoriteFolders();
	const std::wstring& str = pFavorites->GetAt((std::size_t)nID - ID_FAVORITESMENU_BEGIN);

	// navigate to the user's favorite folder
	theApp.GetSignals()->Fire_SelectedFolderChange(str.c_str());
}

void CMainFrame::OnUpdateFavorites(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(true);
}

void CMainFrame::OnSelectedFolderChange(const CString& strFolder, const CString* /*pstrFile*/)
{
	if (!m_bHistoryNav)
		m_pHistoryList->Add(HistoryItem::kType_Folder, strFolder);
}

void CMainFrame::OnSelectedProjectChange(const std::wstring& strProjectName)
{
	if (!m_bHistoryNav)
		m_pHistoryList->Add(HistoryItem::kType_Project, strProjectName.c_str());
}

void CMainFrame::OnEditSelectAll()
{
	CFontListFrame* pFrame = dynamic_cast<CFontListFrame*>(m_wndFontViewSplit.GetPane(0, 0));
	ASSERT(pFrame);
	if (pFrame)
	{
		CFontListView* pView = dynamic_cast<CFontListView*>(pFrame->GetActiveView());
		ASSERT(pView);
		
		if (pView)
			pView->SelectAll();
	}
}

void CMainFrame::OnFontListSizeSelChange()
{
	unsigned short nSize = (unsigned short)m_wndFontListSizeCombo.GetItemData(m_wndFontListSizeCombo.GetCurSel());
	theApp.GetSignals()->Fire_PreviewSizeChange(nSize);
}

void CMainFrame::OnFontListSizeEditChange()
{
	CString strText;
	m_wndFontListSizeCombo.GetWindowText(strText);

	strText.Remove(_T('p'));
	strText.Remove(_T('x'));
	strText.Trim();

	try
	{
		unsigned short nSize = boost::lexical_cast<unsigned short>((LPCTSTR)strText);

		if (nSize < 6 || nSize > 300)
			throw std::out_of_range("out of range");

		// can only make it down here if no errors
		theApp.GetSignals()->Fire_PreviewSizeChange(nSize);
	}
	catch (boost::bad_lexical_cast&)
	{
	}
	catch (std::out_of_range&)
	{
	}
}

void CMainFrame::RemoveCurrentHistoryItem()
{
	m_bHistoryNav = true;
	const HistoryItem* pItem = m_pHistoryList->RemoveCurrent();

	if (pItem)
	{
		if (pItem->nType == HistoryItem::kType_Folder)
			theApp.GetSignals()->Fire_SelectedFolderChange((LPCTSTR)pItem->strName);
		else
			theApp.GetSignals()->Fire_SelectedProjectChange((LPCTSTR)pItem->strName);
	}
	m_bHistoryNav = false;
}

void CMainFrame::OnHelpContentsAndIndex()
{
	HtmlHelp(HIDP_START, HH_HELP_CONTEXT);
}

void CMainFrame::OnHelpWhatsNew()
{
	HtmlHelp(HIDP_WHATSNEW, HH_HELP_CONTEXT);
}

// splitter listeners
void CMainFrame::OnTrackRow_MainSplitter(int /*y*/, int /*row*/)
{
}

void CMainFrame::OnTrackCol_MainSplitter(int /*x*/, int /*col*/)
{
}

void CMainFrame::OnTrackRow_TreeSplitter(int /*y*/, int /*row*/)
{
}

void CMainFrame::OnTrackCol_TreeSplitter(int /*x*/, int /*col*/)
{
}

void CMainFrame::OnTrackRow_DetailSplitter(int /*y*/, int /*row*/)
{
	int nSize = 0, nMin = 0;
	m_wndFontViewSplit.RecalcLayout();
	m_wndFontViewSplit.GetRowInfo(1, nSize, nMin);
	m_nDetailPaneHeight = nSize;
}

void CMainFrame::OnTrackCol_DetailSplitter(int /*x*/, int /*col*/)
{
}

void CMainFrame::OnToolbarRClick(unsigned int x, unsigned int y)
{
	CMenu mainmenu;
	if (!mainmenu.LoadMenu(IDR_MAINFRAME))
		return;

	// get view submenu
	CMenu* pViewMenu = mainmenu.GetSubMenu(1);
	ASSERT(pViewMenu);
	if (!pViewMenu)
		return;

	// get toolbar submenu
	CMenu* pToolbarMenu = pViewMenu->GetSubMenu(3);
	ASSERT(pToolbarMenu);
	if (!pToolbarMenu)
		return;

	// show toolbar submenu
	pToolbarMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, x, y, this);
}

void CMainFrame::OnTimer(UINT_PTR nIDEvent)
{
	if (nIDEvent == m_nFavoritesBalloonTimer)
		PopFavoritesBalloon();
}

void CMainFrame::OnNotifyFavoritesBalloonPop(NMHDR* /*pNMHDR*/, LRESULT* pResult)
{
	if (m_nFavoritesBalloonTimer)
	{
		KillTimer(m_nFavoritesBalloonTimer);
		m_nFavoritesBalloonTimer = 0;

		// make sure tracking is off
		m_ctrlFavoritesBalloon.SendMessage(TTM_TRACKACTIVATE, (WPARAM)FALSE, (LPARAM)&m_tiFavoritesBalloon);
	}

	*pResult = 0;
}

void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	m_pwndMenuBar->Activate(nState != WA_INACTIVE);

	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
}


LRESULT CMainFrame::OnMenuChar(UINT nChar, UINT nFlags, CMenu* pMenu)
{
	if (m_pwndMenuBar->ShowDropDownMenuByChar(nChar))
		return -1;

	return CFrameWnd::OnMenuChar(nChar, nFlags, pMenu);
}

void CMainFrame::OnSysColorChange()
{
	UpdateBandColors();
}

LRESULT CMainFrame::OnThemeChanged()
{
	UpdateBandColors();

	return 0;
}