/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "HistoryList.h"

#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef std::vector<HistoryItem> historylist_type;

struct CHistoryList::_impl
{
	_impl() : itCurrent(historylist.end())
	{}

	historylist_type historylist;
	historylist_type::iterator itCurrent;
};

CHistoryList::CHistoryList() : m_pImpl(new _impl)
{
}

CHistoryList::~CHistoryList()
{
}

void CHistoryList::Add(HistoryItem::eType nType, const LPCTSTR szName)
{
	// clear the list from here foward
	if (!m_pImpl->historylist.empty() && (m_pImpl->itCurrent != m_pImpl->historylist.end() - 1))
		m_pImpl->historylist.erase(m_pImpl->itCurrent + 1, m_pImpl->historylist.end());

	// add a new history item
	HistoryItem item;
	item.nType = nType;
	item.strName = szName;
	m_pImpl->historylist.push_back(item);

	// save new iterator
	m_pImpl->itCurrent = m_pImpl->historylist.end() - 1;
}

// Clears the entire history list
void CHistoryList::Reset()
{
	m_pImpl->historylist.clear();
	m_pImpl->itCurrent = m_pImpl->historylist.end();
}

// Moves to the beginning of the list and returns the first item
const HistoryItem* CHistoryList::GoToBeginning()
{
	if (m_pImpl->historylist.empty())
		return NULL;

	m_pImpl->itCurrent = m_pImpl->historylist.begin();
	return &(*m_pImpl->itCurrent);
}

// Moves to the end of the list and returns the last item
const HistoryItem* CHistoryList::GoToEnd()
{
	if (m_pImpl->historylist.empty())
		return NULL;

	m_pImpl->itCurrent = m_pImpl->historylist.end();
	return &(*m_pImpl->itCurrent);
}

// Indicates whether the list is empty
bool CHistoryList::IsEmpty() const
{
	return m_pImpl->historylist.empty();
}

// Indicates whether there is an item before the current one
bool CHistoryList::CanGoBack() const
{
	return ((m_pImpl->itCurrent != m_pImpl->historylist.begin()) &&
			(m_pImpl->itCurrent != m_pImpl->historylist.end()));
}

// Indicates whether there is an item after the current one
bool CHistoryList::CanGoForward() const
{
	return (!m_pImpl->historylist.empty() && m_pImpl->itCurrent != m_pImpl->historylist.end() - 1);
}

// Moves back one item
const HistoryItem* CHistoryList::GoBack()
{
	if (CanGoBack())
		return &(*--m_pImpl->itCurrent);
	else
		return NULL;
}

// Moves forward one item
const HistoryItem* CHistoryList::GoForward()
{
	if (CanGoForward())
		return &(*++m_pImpl->itCurrent);
	else
		return NULL;
}

// Deletes current item and everything after it and moves back one item
const HistoryItem* CHistoryList::RemoveCurrent()
{
	const HistoryItem* pItem = NULL;
	if (CanGoBack())
	{
		pItem = &(*--m_pImpl->itCurrent);
		m_pImpl->historylist.erase(m_pImpl->itCurrent + 1, m_pImpl->historylist.end());
	}
	else if (!m_pImpl->historylist.empty())
	{
		m_pImpl->historylist.clear();
		m_pImpl->itCurrent = m_pImpl->historylist.end();
	}

	return pItem;  
}
