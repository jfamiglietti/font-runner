/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

//////////////////////////////////////////////////////////////////////////////
// Key.h - This is the common Crux Windows registry class

#include "stdafx.h"
#include "Key.h"

#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CKey::CKey() : m_hOpenKey(NULL)
{
}

CKey::~CKey()
{
	if (m_hOpenKey)
		CloseKey();
}

bool CKey::OpenKey(HKEY hRootKey, LPCTSTR lpszRegSection, REGSAM samDesired)
{
	// see if we can open key
	LONG nResult = ::RegOpenKeyEx(hRootKey, lpszRegSection, 0, samDesired, &m_hOpenKey);

	// return success status
	if (nResult == ERROR_SUCCESS)
		return true;
	else
	{
		m_hOpenKey = NULL;
		return false;
	}
}

bool CKey::CreateKey(HKEY hRootKey, LPCTSTR lpszRegSection)
{
	DWORD dwDisposition = REG_CREATED_NEW_KEY;
	LONG  nResult;

	nResult = ::RegCreateKeyEx(hRootKey,
							 lpszRegSection,
							 0,
							 NULL,
							 REG_OPTION_NON_VOLATILE,
							 KEY_READ | KEY_WRITE,
							 NULL,
							 &m_hOpenKey,
							 &dwDisposition);

	if (nResult == ERROR_SUCCESS)
		return true;
	else
	{
		m_hOpenKey = NULL;
		return false;
	}
}

bool CKey::IsOpen() const
{
	return (m_hOpenKey != NULL);
}

DWORD CKey::GetString(LPCTSTR lpszName, CString& strReturn) const
{
	ASSERT(m_hOpenKey);

	DWORD nReturn;
	DWORD dwType = REG_SZ;
	CString strBuffer;

	DWORD dwSize = 0;

	if (m_hOpenKey)
	{
		nReturn = ::RegQueryValueEx(m_hOpenKey,
								  lpszName,
								  NULL,
								  &dwType,
								  NULL,
								  &dwSize);

		if (nReturn == ERROR_SUCCESS && dwSize >= sizeof(TCHAR))
		{
			try
			{
				// allocate one char too many
				size_t nCharSize = dwSize / sizeof(TCHAR);
				boost::scoped_array<TCHAR> pBuffer(new TCHAR[nCharSize + 1]);
				nReturn = ::RegQueryValueEx(m_hOpenKey,
										  (LPTSTR)lpszName,
										  NULL,
										  &dwType,
										  (BYTE*)pBuffer.get(),
										  &dwSize);

				if (nReturn == ERROR_SUCCESS)
				{
					// make sure extra char is NULL
					pBuffer[nCharSize] = 0;
					strReturn = pBuffer.get();
				}
				else
					dwSize = 0;
			}
			catch (...)
			{
			}
		}
	}

	return dwSize;
}

DWORD CKey::GetString(UINT nIDName, CString& strReturn) const
{
	CString strName;
	strName.LoadString(nIDName);

	return GetString(strName, strReturn);
}

DWORD CKey::GetBinary(LPCTSTR lpszName, LPBYTE lpReturnData, DWORD dwReturnSize) const
{
	ASSERT(m_hOpenKey);

	// return flag
	DWORD dwReturn = 0;

	// type of data we are getting
	DWORD dwType = REG_BINARY;

	// size of data returnd
	DWORD dwSize = 0;

	// only continue if key is open
	if (m_hOpenKey)
	{
		dwReturn = ::RegQueryValueEx(m_hOpenKey,
								   lpszName,
								   NULL,
								   &dwType,
								   NULL,
								   &dwSize);

		if (dwReturn == ERROR_SUCCESS && dwSize <= dwReturnSize)
		{
			boost::scoped_array<BYTE> pBuffer(new BYTE[dwSize]);
			dwReturn = ::RegQueryValueEx(m_hOpenKey,
									   lpszName,
									   NULL,
									   &dwType,
									   pBuffer.get(),
									   &dwSize);
			if (dwReturn == ERROR_SUCCESS)
				memcpy(lpReturnData, pBuffer.get(), dwSize);
			else
				dwSize = 0;
		}
		else
		{
#if 0
			// in debug mode, we want to display an error message box
			LPTSTR lpMsgBuf;
			::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
							FORMAT_MESSAGE_FROM_SYSTEM | 
							FORMAT_MESSAGE_IGNORE_INSERTS,
							NULL,
							dwReturn,
							MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
							(LPTSTR)&lpMsgBuf,
							0,
							NULL);
			::MessageBox(NULL, lpMsgBuf, _T("Debug Message: Error reading from registry"), MB_OK + MB_ICONERROR);
			::LocalFree(lpMsgBuf);
#endif
		}
	}

	// return success status
	return dwSize;
}

DWORD CKey::GetBinary(UINT nIDName, LPBYTE lpReturnData, DWORD dwReturnSize) const
{
	CString strName;
	strName.LoadString(nIDName);

	return GetBinary(strName, lpReturnData, dwReturnSize);
}

bool CKey::GetDWORD(LPCTSTR lpszName, DWORD* lpdwValue) const
{
	ASSERT(m_hOpenKey);
	DWORD nReturn;
	DWORD dwParam = 0;
	DWORD dwSize = sizeof(dwParam);
	
	if (m_hOpenKey)
		nReturn = ::RegQueryValueEx(m_hOpenKey,
									(LPTSTR)lpszName,
									NULL,
									NULL,
									(BYTE*)&dwParam,
									&dwSize);
	else
		return false;

	if (nReturn == ERROR_SUCCESS)
	{
		*lpdwValue = dwParam;
		return true;
	}
	else
		return false;
}

bool CKey::GetDWORD(UINT nIDName, DWORD* lpdwValue) const
{
	CString strName;
	strName.LoadString(nIDName);

	return GetDWORD(strName, lpdwValue);
}

bool CKey::GetBoolean(LPCTSTR lpszName, bool& bReturn) const
{
	DWORD dwValue = 0;

	if (GetDWORD(lpszName, &dwValue))
	{
		bReturn = (dwValue == TRUE);
		return true;
	}
	else
		return false;
}

bool CKey::GetBoolean(UINT nIDName, bool& bReturn) const
{
	CString strName;
	strName.LoadString(nIDName);

	return GetBoolean(strName, bReturn);
}

bool CKey::CloseKey()
{
	DWORD nReturn = RegCloseKey(m_hOpenKey);

	if (nReturn == ERROR_SUCCESS)
	{
		m_hOpenKey = NULL;
		return true;
	}
	else
		return false;
}

bool CKey::SetString(LPCTSTR lpszRegSection, LPCTSTR lpszValue) const
{
	ASSERT(m_hOpenKey);
	LONG nReturn;
	DWORD dwSize = (DWORD)lstrlen(lpszValue) + 1;

	if (m_hOpenKey)
		nReturn = ::RegSetValueEx(m_hOpenKey,
								lpszRegSection,
								NULL,
								REG_SZ,
								(CONST BYTE*)lpszValue,
								dwSize * sizeof(TCHAR));
	else
		return false;

	if (nReturn == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool CKey::SetString(UINT nRegSection, LPCTSTR lpszValue) const
{
	CString strName;
	strName.LoadString(nRegSection);

	return SetString(strName, lpszValue);
}

bool CKey::SetDWORD(LPCTSTR lpszRegSection, DWORD dwValue) const
{
	ASSERT(m_hOpenKey);
	LONG nReturn;
	DWORD dwSize = sizeof(dwValue);

	if (m_hOpenKey)
		nReturn = ::RegSetValueEx(m_hOpenKey,
								lpszRegSection,
								NULL,
								REG_DWORD,
								(CONST BYTE*)&dwValue,
								dwSize);
	else
		return false;

	if (nReturn == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool CKey::SetDWORD(UINT nRegSection, DWORD dwValue) const
{
	CString strName;
	strName.LoadString(nRegSection);

	return SetDWORD(strName, dwValue);
}

bool CKey::SetBinary(LPCTSTR lpszRegSection, void* pData, DWORD dwSize) const
{
	ASSERT(m_hOpenKey);
	LONG nReturn;

	if (m_hOpenKey)
		nReturn = ::RegSetValueEx(m_hOpenKey,
								lpszRegSection,
								NULL,
								REG_BINARY,
								(CONST BYTE*)pData,
								dwSize);
	else
		return false;

	if (nReturn == ERROR_SUCCESS)
		return true;
	else
		return false;
}

bool CKey::SetBinary(UINT nRegSection, void* pData, DWORD dwSize) const
{
	CString strName;
	strName.LoadString(nRegSection);

	return SetBinary(strName, pData, dwSize);
}

bool CKey::DeleteValue(LPCTSTR lpszName) const
{
	ASSERT(m_hOpenKey);
	LONG nReturn = 0;

	if (m_hOpenKey)
	{
		nReturn = ::RegDeleteValue(m_hOpenKey, lpszName);
	}
	else
		return false;

	return (nReturn == ERROR_SUCCESS);
}

bool CKey::FindValue(LPCTSTR lpszValue, CString& strReturn) const
{
	ASSERT(m_hOpenKey);
	LONG nReturn = ERROR_SUCCESS;

	if (m_hOpenKey)
	{
		// all we're looking for is number of values, the longest name and longest value
		DWORD dwValues = 0, dwLongestName = 0, dwLongestValue = 0;
		nReturn = ::RegQueryInfoKey(m_hOpenKey, NULL, NULL, NULL, NULL, NULL, NULL,
						&dwValues, &dwLongestName, &dwLongestValue, NULL, NULL);

		if (nReturn == ERROR_SUCCESS)
		{
			boost::scoped_array<TCHAR> pName(new TCHAR[dwLongestName + 1]);
			boost::scoped_array<TCHAR> pValue(new TCHAR[dwLongestValue + 1]);

			for (DWORD dwIndex = 0; dwIndex < dwValues; ++dwIndex)
			{
				DWORD dwType = 0, dwNameLength = dwLongestName, dwValueLength = dwLongestValue;
				nReturn = ::RegEnumValue(m_hOpenKey, dwIndex, pName.get(), &dwNameLength,
								NULL, &dwType, (LPBYTE)pValue.get(), &dwValueLength);

				// terminate strings
				pName[dwNameLength] = 0;
				pValue[dwValueLength] = 0;

				// is this the one we want?
				if (_tcscmp(lpszValue, pValue.get()) == 0)
				{
					strReturn = pName.get();
					return true;
				}
			}
		}
	}

	// couldn't find it
	return false;
}
