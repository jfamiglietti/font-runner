/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include <boost/shared_ptr.hpp>

class CFontItem;

class CFontManager
{
public:
	CFontManager();
	virtual ~CFontManager();

public:
	// Adds a font to the manager, if the font was already installed
	// in the manager, then the return value points to the existing object.
	// If not, it points to a newly-constructed object.
	CFontItem* AddReference(LPCTSTR szFileName);

	// Adds a reference to a font already managed
	CFontItem* AddReference(const CFontItem* pItem);

	// removes a font from the manager, if this is not the last reference,
	// it will not be removed from the manager
	unsigned int RemoveReference(CFontItem* pItem);

	// if the font item is valid
	bool IsItemValid(const CFontItem* pItem) const;

	// removes the font from the manager, notifies everyone that they have to release the font
	bool UnManage(const CFontItem* pItem);

	// Call when the font file is deleted (or moved)
	// The item is simply removed from the manager and everyone is notified what happened
	void FontFileDeleted(const CString& strFileName);
	void FontItemDeleted(const CFontItem* pItem);

	// get/set temp auto-install feature
	void SetTempAutoInstall(bool bTempAutoInstall);
	bool GetTempAutoInstall() const;
	bool ToggleTempAutoInstall();

	// checks to see if a directory has fonts in use
	bool PathInUse(LPCTSTR szPath) const;
	void UnManageFontsInPath(LPCTSTR szPath);

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;
};
