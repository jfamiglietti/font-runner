/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontListFrame.cpp : implementation file
//

#include "stdafx.h"
#include "FontListFrame.h"
#include "FontListView.h"
#include "FontRunner.h"

#include "pidlutils.h"
#include "ShellUtils.h"
#include "HRESULTEXception.h"

#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CFontListFrame

IMPLEMENT_DYNCREATE(CFontListFrame, CFrameWnd)

CFontListFrame::CFontListFrame() : m_bSystemFonts(false)
{
}

CFontListFrame::~CFontListFrame()
{
}

BOOL CFontListFrame::OnCreateClient(LPCREATESTRUCT /*lpcs*/, CCreateContext* pContext)
{
	// assign new view to new context
	CCreateContext context;
	pContext = &context;
	pContext->m_pNewViewClass = RUNTIME_CLASS(CFontListView);

	// create the new font list view
	CFontListView* pView = NULL;
	pView = (CFontListView*)CreateView(pContext, AFX_IDW_PANE_FIRST);

	// set this one and only view as the active view
	SetActiveView(pView, FALSE);
	
	// connect to signals
	theApp.GetSignals()->ConnectTo_ViewingSystemFonts(boost::bind(&CFontListFrame::OnViewingSystemFonts, this, _1));
	theApp.GetSignals()->ConnectTo_SelectedFolderChange(boost::bind(&CFontListFrame::OnSelectedFolderChange, this, _1, _2));

	return TRUE;
}

BOOL CFontListFrame::IsFrameWnd() const
{
	// we don't want to be the frame window for a print preview
	if (theApp.InPrintPreview())
		return FALSE;
	else
		return CFrameWnd::IsFrameWnd();
}

void CFontListFrame::OnViewingSystemFonts(bool bSystemFonts)
{
	if (m_bSystemFonts != bSystemFonts)
	{
		m_wndRebar.GetReBarCtrl().ShowBand(0, bSystemFonts);
		m_wndRebar.GetReBarCtrl().ShowBand(1, bSystemFonts);
		m_bSystemFonts = bSystemFonts;
	}
}

void CFontListFrame::OnSelectedFolderChange(const CString& strFolder, const CString*)
{
	m_strCurrentFolder = strFolder;
}

BEGIN_MESSAGE_MAP(CFontListFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_BN_CLICKED(ID_EXPLORE_SYSFONTS, OnBnClickedExplore)
END_MESSAGE_MAP()


// CFontProjectFrame message handlers
int CFontListFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	m_fntCaption.CreateFont(24, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ANSI_CHARSET, 
							OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
							DEFAULT_PITCH | FF_DONTCARE, _T("Trebuchet MS Bold"));

	// create the rebar
	if (!m_wndRebar.Create(this, 0))
	{
		TRACE0("Failed to create rebar\n");
		return -1;      // fail to create
	}

	// create image list for rebar
	m_wndRebarImageList.Create(32, 32, ILC_COLOR32, 1, 0);
	HICON hIcon = theApp.LoadIcon(theApp.VistaOrLater() ? IDI_FONTFOLDER_VISTA_ICON : IDI_FONTFOLDER_ICON);
	m_wndRebarImageList.Add(hIcon);
	::DestroyIcon(hIcon);
	m_wndRebar.GetReBarCtrl().SetImageList(&m_wndRebarImageList);
	
	CString strText;
	strText.LoadString(IDS_FONTLIST_WINDOWSFONTSFOLDER);

	m_wndStaticText.Create(strText, SS_LEFT | SS_CENTERIMAGE | WS_VISIBLE | WS_CHILD, CRect(0, 0, 400, 32), this);
	m_wndStaticText.SetFont(&m_fntCaption, false);
	::SetWindowLongPtr(m_wndStaticText.GetSafeHwnd(), GWL_EXSTYLE, WS_EX_TRANSPARENT);

	m_wndRebar.AddBar(&m_wndStaticText, NULL, NULL, RBBS_NOGRIPPER);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	REBARBANDINFO rbbi;
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_IMAGE | RBBIM_STYLE;
	rbbi.iImage = 0;
	rbbi.cxMinChild = 32;
	rbbi.cyMinChild = 40;
	rbbi.cx = rbbi.cxIdeal = 9999;
	rbbi.fStyle = RBBS_HIDDEN | RBBS_NOGRIPPER;
	m_wndRebar.GetReBarCtrl().SetBandInfo(0, &rbbi);

	strText.LoadString(IDS_FONTLIST_OPENINEXPLORER);
	
	if (!m_btnExplore.Create(strText, BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE, CRect(0, 0, 0, 0),
		this, ID_EXPLORE_SYSFONTS))
	{
		TRACE0("Failed to create button\n");
		return -1;
	}

	m_btnExplore.SetFont(theApp.GetDialogFont());

	// measure button text
	CDC* pDC = GetDC();
	CFont* pfntOld = pDC->SelectObject(theApp.GetDialogFont());
	CSize size = pDC->GetTextExtent(strText);
	pDC->SelectObject(pfntOld);
	ReleaseDC(pDC);

	m_wndRebar.AddBar(&m_btnExplore, NULL, NULL, RBBS_NOGRIPPER);

	// set up min/max sizes and ideal sizes for pieces of the rebar
	rbbi.cbSize = sizeof(rbbi);
	rbbi.fMask = RBBIM_CHILDSIZE | RBBIM_IDEALSIZE | RBBIM_SIZE | RBBIM_STYLE;
	rbbi.cxMinChild = size.cx + 24;
	rbbi.cyMinChild = size.cy + 12;
	rbbi.cx = rbbi.cxIdeal = 150;
	rbbi.fStyle = RBBS_HIDDEN | RBBS_NOGRIPPER;
	m_wndRebar.GetReBarCtrl().SetBandInfo(1, &rbbi);

	return 0;
}

void CFontListFrame::OnBnClickedExplore()
{
	if (!m_strCurrentFolder.IsEmpty())
		ShellExecute(NULL, _T("open"), m_strCurrentFolder, NULL, NULL, SW_SHOW);
}