/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "OpenTypeData.h"
#include <boost/shared_ptr.hpp>

class CFontItem
{
public:
	CFontItem(LPCTSTR szFileName);
	virtual ~CFontItem();

public:
	// less-than and equality via FQ filename
	bool operator < (const CFontItem& other) const;
	bool operator == (const CFontItem& other) const;
	bool FontNameEqual(LPCTSTR szFontName) const;

	// for list control sorting
	int CompareFontNames(const CFontItem* pOther) const;
	int CompareFileNames(const CFontItem* pOther) const;
	int CompareFileSizes(const CFontItem* pOther) const;

// AddReference/RemoveReference are only accessible by CFontManager
friend class CFontManager;
protected:
	// increments/decrements the reference count on this font, returns the new reference count
	long AddReference();
	long RemoveReference();

public:
	// returns current reference count
	long GetRefCount() const;

	// returns pointer to open type data
	boost::shared_ptr<CruxTechnologies::COpenTypeData> GetOpenTypeData() const;

	// get font status
	CruxTechnologies::COpenTypeData::eStatus GetStatus() const;

	// get file name
	const CString& GetFullFileName() const;
	const CString& GetFileName() const;

	// get file size
	DWORD GetFileSize() const;
	const CString& GetFileSizeString() const;

	// get font objects
	CFont& GetDisplayFont();

	// installed status
	bool IsInstalled() const;
	void Installed(bool bInstalled);

	// get/set temp auto-install feature
	void SetTempAutoInstall(bool bTempAutoInstall);
	
	void TemporarilyMarkPrivate();
	void RestorePreviousCharacteristics();

	// whether or not the client will attempt to delete the file after
	// this object has been destroyed
	void MarkForDeletion();
	void UnmarkForDeletion();
	bool IsMarkedForDeletion() const;

	// similarity functions
	void SetDistance(LPCTSTR szDistanceFrom, int nDistance);
	int GetDistance(LPCTSTR szDistanceFrom) const;

	// returns true if this font is in use and is in this path
	bool PathInUse(LPCTSTR szPath) const;

private:
	void CacheLowerCaseNames();

private:
	struct _impl;
	boost::shared_ptr<_impl> m_pImpl;

	struct LowerCaseCacheType;
	boost::shared_ptr<LowerCaseCacheType> m_pLowerCaseCache;
};
