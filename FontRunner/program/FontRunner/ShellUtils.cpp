/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StdAfx.h"
#include "ShellUtils.h"

#include "pidlutils.h"
#include "HRESULTException.h"
#include "memory_streambuffer.h"

#include <shlwapi.h>
#include <strsafe.h>

#include <numeric>
#include <boost/bind.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace ShellUtils;

// returns name of shell object ID'd by a shell folder and PIDL
bool ShellUtils::GetName(IShellFolder* lpsf,
						 LPCITEMIDLIST lpi,
						 DWORD dwFlags,
						 LPTSTR lpFriendlyName,
						 size_t nFriendlyNameSize)
{
	bool bSuccess = TRUE;
	STRRET str;

	if (lpsf == NULL || lpi == NULL)
		return false;

	if (SUCCEEDED(lpsf->GetDisplayNameOf(lpi, dwFlags, &str)))
	{
		TCHAR* pName = NULL;
		if (SUCCEEDED(::StrRetToStr(&str, lpi, &pName)))
		{
			::StringCchCopy(lpFriendlyName, nFriendlyNameSize, pName);
			::CoTaskMemFree(pName);
		}
	}
	else
		bSuccess = FALSE;

	return bSuccess;
}

// returns name of shell object ID'd by a shell folder and PIDL
CString ShellUtils::GetName(IShellFolder* lpsf, LPITEMIDLIST lpi, DWORD dwFlags)
{
	TCHAR szName[MAX_PATH];
	memset(szName, 0, MAX_PATH);
	GetName(lpsf, lpi, dwFlags, szName, MAX_PATH);

	return szName;
}

HRESULT ShellUtils::InvokeContextMenuCommand(HWND hwnd, LPCTSTR szFullFileName, LPCSTR lpCommand)
{
	// split file from directory
	TCHAR szDirectory[MAX_PATH];
	::StringCchCopy(szDirectory, MAX_PATH, szFullFileName);
	::PathRemoveFileSpec(szDirectory);
	LPTSTR pszFileName = ::PathFindFileName(szFullFileName);

	CheckResultFunctor CheckResult;

	try
	{
		// need desktop folder
		CComPtr<IShellFolder> pDesktop;
		::SHGetDesktopFolder(&pDesktop);

		// get FQ pidl to containing folder
		pidl_ptr pidlParent, pidlFile;
		CheckResult(pDesktop->ParseDisplayName(hwnd, NULL, (LPOLESTR)(LPCTSTR)szDirectory, NULL, &pidlParent, NULL));

		// get shell folder object of containing folder
		CComPtr<IShellFolder> pFolder;
		CheckResult(pDesktop->BindToObject(pidlParent.get(), NULL, IID_IShellFolder, (void**)&pFolder));
		CheckResult(pFolder->ParseDisplayName(hwnd, NULL, (LPOLESTR)(LPCTSTR)pszFileName, NULL, &pidlFile, NULL));

		CComPtr<IContextMenu> pContextMenu;
		CheckResult(pFolder->GetUIObjectOf(hwnd,
										   1,
										   (const struct _ITEMIDLIST**)&pidlFile,
										   IID_IContextMenu,NULL,
										   (void**)&pContextMenu));

		// if we successfully got an IContextMenu interface, use it to
		// invoke whichever command was passed-in
		CMINVOKECOMMANDINFO ici;
		memset(&ici, 0, sizeof(CMINVOKECOMMANDINFO));
		ici.cbSize = sizeof(CMINVOKECOMMANDINFO);
		ici.hwnd = hwnd;
		ici.lpVerb = lpCommand;
		ici.nShow = SW_SHOWNORMAL;
		CheckResult(pContextMenu->InvokeCommand(&ici));
	}
	catch (CHRESULTException& e)
	{
		return e.GetHRESULT();
	}

	return S_OK;
}

// returns the effect performed on the data object after it is dragged
DROPEFFECT ShellUtils::CheckPerformedEffect(IDataObject* pDataObject)
{
	DROPEFFECT dwReturn = DROPEFFECT_NONE;

	STGMEDIUM stgm;
	FORMATETC fe = { (CLIPFORMAT)::RegisterClipboardFormat(CFSTR_PERFORMEDDROPEFFECT),
					  NULL, DVASPECT_CONTENT, -1, TYMED_HGLOBAL };

	if (SUCCEEDED(pDataObject->GetData(&fe, &stgm)))
	{
		// if we got a big enough HGLOBAL, look for the pre-optimized drop effect
		if ((stgm.tymed & TYMED_HGLOBAL) && ::GlobalSize(stgm.hGlobal) >= sizeof(DROPEFFECT))
		{
			DROPEFFECT* pdwEffect = (DROPEFFECT*)::GlobalLock(stgm.hGlobal);
			if (pdwEffect)
			{
				dwReturn = *pdwEffect;
				::GlobalUnlock(stgm.hGlobal);
			}
		}

		// cleanup
		::ReleaseStgMedium(&stgm);
	}

	return dwReturn;
}

// function object copies a file name string to a memory buffer
struct CopyFileNameToBuffer
{
	CopyFileNameToBuffer(std::wostream& outputstream)
		: m_outputstream(outputstream)
	{}

	void operator ()(LPCTSTR pszFile)
	{
		//ASSERT(m_pszCurrentFile != NULL);

		m_outputstream.write(pszFile, ::lstrlen(pszFile) + 1);
	}

	std::wostream& m_outputstream;
};

// puts file names into a data object
int ShellUtils::SetDataObjectWithFileNames(IDataObject* pDataObject,
										   const std::vector<LPCTSTR>& filenames,
										   size_t nStringListLength,
										   CLIPFORMAT cf)
{
	// add size of DROPFILES for total amount of memory we'll need
	size_t nDropFilesSize = sizeof(DROPFILES) + nStringListLength;

	// use the Global allocator for this
	HGLOBAL hDropFilesAlloc = ::GlobalAlloc(GHND | GMEM_SHARE, nDropFilesSize);
	if (!hDropFilesAlloc)
		return 0;

	// attempt to lock global memory
	DROPFILES* pDropFiles = reinterpret_cast<DROPFILES*>(::GlobalLock(hDropFilesAlloc));
	if (!pDropFiles)
	{
		::GlobalFree(hDropFilesAlloc);
		return 0;
	}

	// zero memory for good measure
	ZeroMemory(pDropFiles, nDropFilesSize);

	// filenames are located just beyond the end of the DROPFILES struct
	pDropFiles->pFiles = sizeof(DROPFILES);

	// for Unicode builds, set the Unicode flag
#ifdef _UNICODE
	pDropFiles->fWide = true;
#endif

	// set up a stream
	crux::memory_streambuf<wchar_t>
		filename_buffer((wchar_t*)((char*)pDropFiles + sizeof(DROPFILES)), nDropFilesSize);
	std::wostream filename_stream(&filename_buffer);

	// copy the file names to the buffer just beyond the end of the DROPFILES struct
	std::for_each(filenames.begin(), filenames.end(),
				  CopyFileNameToBuffer(filename_stream));

	// unlock global memory
	::GlobalUnlock(hDropFilesAlloc);

	// init FORMATETC
	FORMATETC formatEtc;
	formatEtc.cfFormat = cf;
	formatEtc.ptd = NULL;
	formatEtc.dwAspect = DVASPECT_CONTENT;
	formatEtc.lindex = -1;
	formatEtc.tymed = TYMED_HGLOBAL;

	// init STGMEDIUM
	STGMEDIUM stgMedium;
	stgMedium.tymed = TYMED_HGLOBAL;
	stgMedium.hGlobal = hDropFilesAlloc;
	stgMedium.pUnkForRelease = NULL;

	// set data
	pDataObject->SetData(&formatEtc, &stgMedium, FALSE);

	return (int)filenames.size();
}

// function object that converts a fully-qualified file name to a fully-qualified PIDL
struct ConvertFileNamesToPIDLs
{
	ConvertFileNamesToPIDLs(IShellFolder* pDesktop) : m_pDesktop(pDesktop)
	{}

	pidlobject_ptr operator()(LPCTSTR pszFileName) const
	{
		LPITEMIDLIST pidl = NULL;
		CheckResult(m_pDesktop->ParseDisplayName(NULL, NULL, (LPWSTR)pszFileName, NULL, &pidl, NULL));

		pidlobject_ptr pidlptr(new pidl_object(pidl, &CoTaskMemFree));
		
		return pidlptr;
	}

private:
	IShellFolder* m_pDesktop;
	CheckResultFunctor CheckResult;
};

// function used in accumulate algorithm to add up pidl sizes
size_t SumPIDLSizes(size_t nTotal, const pidlobject_ptr& ptr)
{
	return nTotal + ptr->size();
}

// puts PIDLs into a data object
int ShellUtils::SetDataObjectWithPIDLs(IDataObject* pDataObject, const std::vector<LPCTSTR>& filenames)
{
	try
	{
		CheckResultFunctor CheckResult;

		// We successfully added file names to the data object, now add pidls
		CComPtr<IShellFolder> pDesktop;
		CheckResult(::SHGetDesktopFolder(&pDesktop));

		// first pidl is blank
		LPITEMIDLIST pidlDesktop;
		CheckResult(::SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlDesktop));

		// convert filenames to pidls
		std::vector<pidlobject_ptr> pidls(filenames.size() + 1);
		pidls[0].reset(new pidl_object(pidlDesktop, &ILFree));
		std::transform(filenames.begin(), filenames.end(),
					   pidls.begin() + 1, ConvertFileNamesToPIDLs(pDesktop));

		// sum total size of all pidls
		size_t nTotalPIDLSize =
			std::accumulate(pidls.begin(), pidls.end(), (size_t)0, boost::bind(&SumPIDLSizes, _1, _2));

		// total size is:
		//   size of CIDA cidl member
		// + size of UINT array of offsets
		// + size of NULL parent PIDL
		// + size of all the other PIDLs
		size_t nOffset = (sizeof(UINT) * (pidls.size() + 1));
		size_t nGlobalSize = nOffset + nTotalPIDLSize;

		// allocate enough global memory for all the pidls
		HGLOBAL hShellIDListAlloc = ::GlobalAlloc(GHND | GMEM_SHARE, nGlobalSize);
		if (!hShellIDListAlloc)
			return 0;

		// attempt to lock global memory
		CIDA* pShellIDList = reinterpret_cast<CIDA*>(::GlobalLock(hShellIDListAlloc));
		if (!pShellIDList)
		{
			::GlobalFree(hShellIDListAlloc);
			return 0;
		}

		// zero memory for good measure
		ZeroMemory(pShellIDList, nGlobalSize);

		// set number of PIDLs being transferred
		pShellIDList->cidl = (UINT)pidls.size() - 1;

		size_t nCurrentOffset = nOffset;
		for (size_t n = 0; n < pidls.size(); ++n)
		{
			pShellIDList->aoffset[n] = (UINT)nCurrentOffset;

			// now copy the pidl there
			memcpy(((char*)pShellIDList) + nCurrentOffset, pidls[n]->get(), pidls[n]->size());

			// move current
			nCurrentOffset += pidls[n]->size();
		}

		// unlock the memory
		::GlobalUnlock(hShellIDListAlloc);

		// init FORMATETC
		FORMATETC formatEtc;
		formatEtc.cfFormat = (CLIPFORMAT)::RegisterClipboardFormat(CFSTR_SHELLIDLIST);
		formatEtc.ptd = NULL;
		formatEtc.dwAspect = DVASPECT_CONTENT;
		formatEtc.lindex = -1;
		formatEtc.tymed = TYMED_HGLOBAL;

		// init STGMEDIUM
		STGMEDIUM stgMedium;
		stgMedium.tymed = TYMED_HGLOBAL;
		stgMedium.hGlobal = hShellIDListAlloc;
		stgMedium.pUnkForRelease = NULL;

		// set data
		pDataObject->SetData(&formatEtc, &stgMedium, FALSE);
	}
	catch (CHRESULTException& e)
	{
#ifdef _DEBUG
		CString strDebug;
		strDebug.Format(_T("FAILED HRESULT is 0x%08x.\n"), e.GetHRESULT());
		OutputDebugString(strDebug);
#else
		e;
#endif

		return 0;
	}

	return (int)filenames.size();
}
