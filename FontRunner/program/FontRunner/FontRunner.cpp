/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

// FontRunner.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "FontRunner.h"
#include "MainFrm.h"

#include "AboutFontRunner.h"

#include "ProgramOptions.h"
#include "FontItem.h"
#include "FontProjects.h"
#include "FavoriteFolders.h"
#include "FontRunnerSignals.h"
#include "FontRunnerDoc.h"
#include "FontRunnerView.h"
#include "FontManager.h"
#include "Splash.h"
#include "WinToolbox.h"
#include "Key.h"

#include <shlwapi.h>
#include <strsafe.h>

#include <boost/shared_ptr.hpp>
#include <boost/scoped_array.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// register Font Runner's instance searching registered message
UINT CFontRunnerApp::s_uwmAreYouFontRunner = ::RegisterWindowMessage(FONTRUNNER_REGWM_NAME);

// CFontRunnerApp

BEGIN_MESSAGE_MAP(CFontRunnerApp, CWinApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()


// CFontRunnerApp construction

CFontRunnerApp::CFontRunnerApp()
 : m_bSetup(false),
   m_commonsizelist(14),
   m_bInitialized(false),
   m_dwMajorOSVersion(0),
   m_bInPrintPreview(false)
{
	m_commonsizelist[0] = 12;
	m_commonsizelist[1] = 14;
	m_commonsizelist[2] = 18;
	m_commonsizelist[3] = 24;
	m_commonsizelist[4] = 32;
	m_commonsizelist[5] = 36;
	m_commonsizelist[6] = 48;
	m_commonsizelist[7] = 64;
	m_commonsizelist[8] = 70;
	m_commonsizelist[9] = 100;
	m_commonsizelist[10] = 120;
	m_commonsizelist[11] = 150;
	m_commonsizelist[12] = 200;
	m_commonsizelist[13] = 240;
}


// The one and only CFontRunnerApp object

CFontRunnerApp theApp;

// CFontRunnerApp initialization

BOOL CFontRunnerApp::InitInstance()
{
	// before we do anything, make sure this is the only instance of Font Runner
	if (FirstInstance() != NULL)
		return FALSE;

	// enable HTML help
	EnableHtmlHelp();

	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();

	// create a font to use on dialog controls
	CreateDialogFont();

	// Initialize OLE libraries
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();

	// grab location of Windows font folder
	TCHAR szFontFolder[MAX_PATH];
	::SHGetFolderPath(NULL, CSIDL_FONTS, NULL, SHGFP_TYPE_CURRENT, szFontFolder);
	::PathRemoveBackslash(szFontFolder);
	m_strWindowsFontFolder = szFontFolder;

	try
	{
		// create program options
		m_pProgramOptions.reset(new CProgramOptions());
		m_pProgramOptions->Load();

		CString strUpdater = m_pProgramOptions->GetUpdaterLocation();
		if (!strUpdater.IsEmpty())
		{
			// delete the updater
			::DeleteFile(strUpdater);

			// remove updater from registry
			m_pProgramOptions->ClearUpdaterLocation();
		}

		// create signals class
		m_pSignals.reset(new CFontRunnerSignals());

		// register for signals
		m_pSignals->ConnectTo_PreviewSizeChange(boost::bind(&CFontRunnerApp::OnFontListPreviewSizeChange, this, _1));

		// create font projects
		ImportFontRunnerProProjects();
		m_pFontProjects.reset(new CFontProjects());
		m_pFontProjects->Load(GetAppDataFilename(IDS_FONTPROJECTVIEW_XML, false));

		// create favorites object and load from file
		ImportFontRunnerProFavorites();
		m_pFavoriteFolders.reset(new CFavoriteFolders());
		m_pFavoriteFolders->Load(GetAppDataFilename(IDS_FAVORITES_XMLFILE, false));

		// create font manager
		m_pFontManager.reset(new CFontManager());
	}
	catch (CFontProjectException&)
	{
		// an exception here is bad
		return FALSE;
	}
	catch (CFavoriteFoldersException&)
	{
		// an exception here is bad
		return FALSE;
	}
	catch (std::bad_alloc&)
	{
		// not enough memory to continue
		return FALSE;
	}

	// set path of this application
	::GetModuleFileName(AfxGetInstanceHandle(), m_strAppPath.GetBuffer(MAX_PATH), MAX_PATH);
	m_strAppPath.ReleaseBuffer();
	m_strAppPath = m_strAppPath.Left(m_strAppPath.ReverseFind('\\') + 1);

	// init help
	SetHelpMode(afxHTMLHelp);

	// First free the string allocated by MFC at CWinApp startup.
	// The string is allocated before InitInstance is called.
	if (m_pszHelpFilePath)
		free((void*)m_pszHelpFilePath);

	// Change the name of the .HLP file.
	// The CWinApp destructor will free the memory.
	CString strFileName;
	strFileName.LoadString(IDS_MISC_HELPFILE);
	m_pszHelpFilePath = _tcsdup(m_strAppPath + strFileName);
	
	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CFontRunnerDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CFontRunnerView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// show splash screen
	CSplashWnd::EnableSplashScreen(cmdInfo.m_bShowSplash);

	// Dispatch commands specified on the command line.  Will return FALSE if
	// app was launched with /RegServer, /Register, /Unregserver or /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// The one and only window has been initialized, so show and update it
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();
	// call DragAcceptFiles only if there's a suffix
	//  In an SDI app, this should occur after ProcessShellCommand

	// create app version string
	m_strVersion = CWinToolbox::GetAppVersionString();

	// general init is complete
	m_bInitialized = true;

	// if we have a file or path from the command-line, open it
	if (!cmdInfo.m_strFileName.IsEmpty() && ::PathFileExists(cmdInfo.m_strFileName))
	{
		if (::PathIsDirectory(cmdInfo.m_strFileName))
			m_pSignals->Fire_SelectedFolderChange(cmdInfo.m_strFileName);
		else
		{
			TCHAR szPath[MAX_PATH];
			::StringCchCopy(szPath, MAX_PATH, cmdInfo.m_strFileName);

			if (::PathRemoveFileSpec(szPath) && ::PathIsDirectory(szPath))
				m_pSignals->Fire_SelectedFolderChange(szPath);

			// Ask the font manager to manage this font.  We don't need it once
			// we we've used the returned CFontItem to select it, so the smart pointer
			// will unreference it automatically.
			boost::shared_ptr<CFontItem>
				pInfo(
					m_pFontManager->AddReference(cmdInfo.m_strFileName),
					boost::bind(&CFontManager::RemoveReference, m_pFontManager.get(), _1));

			if (pInfo->GetStatus() == CruxTechnologies::COpenTypeData::kStatus_OK)
				m_pSignals->Fire_SelectedFontChange(pInfo.get());
		}
	}
	// otherwise, if we want to use last directory, then select it
	else if (m_pProgramOptions->GetStartupOption() == CProgramOptions::kStartupOption_DefaultFolder &&
		::PathFileExists(m_pProgramOptions->GetDefaultFolder()))
	{
		m_pSignals->Fire_SelectedFolderChange(m_pProgramOptions->GetDefaultFolder());
	}
	else if (m_pProgramOptions->GetStartupOption() == CProgramOptions::kStartupOption_DefaultProject)
	{
		// navigate to default folder
		if (::PathFileExists(m_pProgramOptions->GetDefaultFolder()))
			m_pSignals->Fire_NavigateToFolder(m_pProgramOptions->GetDefaultFolder());
		if (m_pFontProjects->Exists(m_pProgramOptions->GetDefaultProject()))
			m_pSignals->Fire_SelectedProjectChange(std::wstring(m_pProgramOptions->GetDefaultProject()));
	}
	else if (m_pProgramOptions->GetStartupOption() == CProgramOptions::kStartupOption_LastItem)
	{
		if (m_pProgramOptions->UsingFontProject())
		{
			if (::PathFileExists(m_pProgramOptions->GetDefaultFolder()))
				m_pSignals->Fire_NavigateToFolder(m_pProgramOptions->GetDefaultFolder());
			if (m_pFontProjects->Exists(m_pProgramOptions->GetLastProject()))
				m_pSignals->Fire_SelectedProjectChange(std::wstring(m_pProgramOptions->GetLastProject()));
		}
		else if (::PathFileExists(m_pProgramOptions->GetLastFolder()))
			m_pSignals->Fire_SelectedFolderChange(m_pProgramOptions->GetLastFolder());
	}

	return TRUE;
}

int CFontRunnerApp::ExitInstance()
{
	// save favorites
	if (m_pFavoriteFolders.get())
		m_pFavoriteFolders->Save(GetAppDataFilename(IDS_FAVORITES_XMLFILE));

	// save font projects
	if (m_pFontProjects.get())
		m_pFontProjects->Save(GetAppDataFilename(IDS_FONTPROJECTVIEW_XML));

	if (!m_strUpdater.IsEmpty())
	{
		// run updater
		::ShellExecute(NULL, _T("open"), m_strUpdater, NULL, NULL, SW_SHOW);

		// save updater location
		if (m_pProgramOptions.get())
			m_pProgramOptions->SetUpdaterLocation(m_strUpdater);
	}

	// save program options
	if (m_pProgramOptions.get())
		m_pProgramOptions->Save();

	return CWinApp::ExitInstance();
}

void CFontRunnerApp::RunUpdaterOnShutdown(LPCTSTR szUpdaterLocation)
{
	m_strUpdater = szUpdaterLocation;
}

CString CFontRunnerApp::GetAppDataFilename(UINT nFileID, bool bCreateAppDataFolder, UINT nAppDataFolder) const
{
	// get user's app data path
	TCHAR szUserAppDataPath[MAX_PATH];
	::SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA, NULL, SHGFP_TYPE_CURRENT, szUserAppDataPath);

	// get font runner app data path
	CString strFontRunnerAppDataPath;
	strFontRunnerAppDataPath.LoadString(nAppDataFolder);

	// make total app path
	TCHAR szAppDataPath[MAX_PATH];
	::PathCombine(szAppDataPath, szUserAppDataPath, strFontRunnerAppDataPath);

	if (bCreateAppDataFolder && !::PathFileExists(szAppDataPath))
		::SHCreateDirectoryEx(NULL, szAppDataPath, NULL);

	// get file name
	CString strFileName;
	strFileName.LoadString(nFileID);

	// make FQ filename
	TCHAR szFullFileName[MAX_PATH];
	::PathCombine(szFullFileName, szAppDataPath, strFileName);

	// return it
	return szFullFileName;
}

void CFontRunnerApp::CreateDialogFont()
{
	// get a font for use on tabs
	int nSize = 12;
	BYTE nCharSet = ANSI_CHARSET;
	HFONT hFont = (HFONT)::GetStockObject(DEFAULT_GUI_FONT);
	LOGFONT lf;
	if ((hFont != NULL) && (::GetObject(hFont, sizeof(LOGFONT), &lf) != 0))
	{
		nSize = lf.lfHeight;
		nCharSet = lf.lfCharSet;
	}

	// try and create a dialog font
	LPCTSTR lpszFacename = _T("MS Shell Dlg 2");
	if (!m_fontDialog.CreateFont(nSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, nCharSet,
									OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
									DEFAULT_PITCH | FF_DONTCARE, lpszFacename))
	{
		// creation of MS Shell Dlg 2 was not successful, use old reliable system font
		lpszFacename = _T("MS Shell Dlg");
		m_fontDialog.CreateFont(nSize, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, nCharSet,
								OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
								DEFAULT_PITCH | FF_DONTCARE, lpszFacename);
	}

	// create a bold version too
	m_fontBoldDialog.CreateFont(nSize, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, nCharSet,
								OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
								DEFAULT_PITCH | FF_DONTCARE, lpszFacename);
}

BOOL CALLBACK CFontRunnerApp::InstanceSearcher(HWND hWnd, LPARAM lParam)
{
	DWORD_PTR result;
	LRESULT lResult = ::SendMessageTimeout(hWnd,
											s_uwmAreYouFontRunner,
											0, 0, 
											SMTO_BLOCK |
											SMTO_ABORTIFHUNG,
											200,
											&result);
	// ignore this and continue
	if (lResult == 0)
		return TRUE;

	if (result == s_uwmAreYouFontRunner)
	{
		// found another instance
		HWND* target = (HWND*)lParam;
		*target = hWnd;

		 // stop search
		 return FALSE; 
	}
	
	// not found, continue search
	return TRUE;
}

HWND CFontRunnerApp::FirstInstance()
{
	// (this is the more reliable mutex method)

	BOOL bAlreadyRunning;
	HWND hwndOther = NULL;

	m_hAppMutex = CreateMutex(NULL, FALSE, FONTRUNNER_MUTEX_NAME);

	// we're running if this mutex has already been created
	bAlreadyRunning = ((GetLastError() == ERROR_ALREADY_EXISTS) ||
							 (GetLastError() == ERROR_ACCESS_DENIED));


	// if we know there's another instance, lets look for its window
	if (bAlreadyRunning)
	{
		EnumWindows(InstanceSearcher, (LPARAM)&hwndOther);

		if (hwndOther)
		{
			// pop up old window
			SetForegroundWindow(hwndOther);

			// restore if iconic
			if (IsIconic(hwndOther))
				ShowWindow(hwndOther, SW_RESTORE);
		}
	}
	
	// no other instance was found
	return hwndOther;
}

void CFontRunnerApp::HtmlHelp(DWORD_PTR dwData, UINT nCmd)
{
	CString strHelpFile(m_pszHelpFilePath);

	if (nCmd == HH_DISPLAY_TEXT_POPUP)
		strHelpFile += "::/FontRunnerPopups.txt";

	HWND hwnd = NULL;
	CWnd* pwndMain = GetMainWnd();
	if (pwndMain)
		hwnd = pwndMain->GetSafeHwnd();

	::HtmlHelp(hwnd, strHelpFile, nCmd, dwData);
}

void CFontRunnerApp::OnFontListPreviewSizeChange(unsigned short nSize)
{
	m_pProgramOptions->SetPreviewFontSize(nSize);
}

LPCTSTR CFontRunnerApp::GetWindowsFontFolder() const
{
	return (LPCTSTR)m_strWindowsFontFolder;
}

// CFontRunnerApp message handlers
BOOL CFontRunnerApp::PreTranslateMessage(MSG* pMsg)
{
	if (CSplashWnd::PreTranslateAppMessage(pMsg))
		return TRUE;

	return CWinApp::PreTranslateMessage(pMsg);
}

void CFontRunnerApp::SwitchToDroppedItem(HDROP hDrop)
{
	// if multiple files are dragged, just get the first one
	UINT nFileCount = ::DragQueryFile(hDrop, 0xFFFFFFFF, NULL, 0);

	if (nFileCount > 0)
	{
		UINT nSize = ::DragQueryFile(hDrop, 0, NULL, 0);
		boost::scoped_array<TCHAR> pszFileName(new TCHAR[nSize + 1]);

		// get filename
		::DragQueryFile(hDrop, 0, pszFileName.get(), nSize + 1);

		if (::PathIsDirectory(pszFileName.get()))
			m_pSignals->Fire_SelectedFolderChange(pszFileName.get());
		else
		{
			boost::scoped_array<TCHAR> pszDirectoryName(new TCHAR[nSize + 1]);
			::StringCchCopy(pszDirectoryName.get(), nSize + 1, pszFileName.get());
			::PathRemoveFileSpec(pszDirectoryName.get());

			CString strFileName(::PathFindFileName(pszFileName.get()));

			m_pSignals->Fire_SelectedFolderChange(pszDirectoryName.get(), &strFileName);
		}
	}
}

bool CFontRunnerApp::VistaOrLater()
{
	// get version
	if (m_dwMajorOSVersion == 0)
	{
		OSVERSIONINFO osvi;
		osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
		::GetVersionEx(&osvi);

		m_dwMajorOSVersion = osvi.dwMajorVersion;
	}

	return (m_dwMajorOSVersion >= 6);
}

void CFontRunnerApp::ImportFontRunnerProProjects()
{
	// if a Font Runner Pro projects file exists, we copy it to our new location
	CString strOldFileName = GetAppDataFilename(IDS_FONTPROJECTVIEW_XML, false, IDS_MISC_APPDATAFOLDER_PRO);
	if (::PathFileExists(strOldFileName))
	{
		// copy Font Runner Pro file
		CString strNewFileName = GetAppDataFilename(IDS_FONTPROJECTVIEW_XML);
		::CopyFile(strOldFileName, strNewFileName, TRUE);

		// rename old Font Runner Pro file
		CString strRenamedFileName = strOldFileName + _T(".old");
		::MoveFile(strOldFileName, strRenamedFileName);
	}
}

void CFontRunnerApp::ImportFontRunnerProFavorites()
{
	// if a Font Runner Pro favorites file exists, ask if it should be copied to the new location
	CString strOldFileName = GetAppDataFilename(IDS_FAVORITES_XMLFILE, false, IDS_MISC_APPDATAFOLDER_PRO);
	if (::PathFileExists(strOldFileName))
	{
		CString strNewFileName = GetAppDataFilename(IDS_FAVORITES_XMLFILE);

		// copy unless user says not to and we don't bother asking the user unless
		// a Font Runner favorites file exists
		bool bCopy = true;
		if (::PathFileExists(strNewFileName))
			bCopy = (AfxMessageBox(IDS_FAVORITES_IMPORTPRO, MB_YESNO | MB_ICONQUESTION) == IDYES);

		// copy Font Runner Pro file
		if (bCopy)
			::CopyFile(strOldFileName, strNewFileName, FALSE);

		// rename old Font Runner Pro file
		CString strRenamedFileName = strOldFileName + _T(".old");
		::MoveFile(strOldFileName, strRenamedFileName);
	}
}
