/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// CFavoritesDlg dialog

class CFavoritesDlg : public CDialog
{
	DECLARE_DYNAMIC(CFavoritesDlg)

public:
	CFavoritesDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFavoritesDlg();

private:
	void AddFolderToListCtrl(const std::wstring& str);

// Dialog Data
	enum { IDD = IDD_FAVORITES_DIALOG };
private:
	CImageList m_ilFavorites;
	CListCtrl m_ctrlFavoritesList;
	CButton m_btnRemove;

// overrides
protected:
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

public:
	DECLARE_MESSAGE_MAP()
	afx_msg void OnLvnItemchangedFavoritesList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedAddcurrentButton();
	afx_msg void OnBnClickedAddotherButton();
	afx_msg void OnBnClickedRemoveButton();
};
