/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "StdAfx.h"
#include "FavoriteFolders.h"

#include "LowercaseComparison.h"
#include "HRESULTException.h"

#include <set>
#include <shlwapi.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

typedef std::set<std::wstring, lowercase_less<std::wstring> > folderlist_type;
typedef std::pair<folderlist_type::iterator, bool> folderlist_insert_result;
struct CFavoriteFolders::_impl
{
	folderlist_type folderlist;
};

CFavoriteFolders::CFavoriteFolders() : m_pImpl(new _impl)
{
}

CFavoriteFolders::~CFavoriteFolders()
{
}

CComPtr<IXMLDOMNode> FindFavoritesNode(CComPtr<IXMLDOMDocument>& pXMLDoc)
{
	// get the document element (this is the shoot data)
	CComPtr<IXMLDOMNodeList> pDocNodeList = NULL;
	pXMLDoc->get_childNodes(&pDocNodeList);

	// look for the favorites node
	long nLength = 0;
	pDocNodeList->get_length(&nLength);
	for (long nDocNodeIndex = 0; nDocNodeIndex < nLength; nDocNodeIndex++)
	{
		CComPtr<IXMLDOMNode> pTest;
		pDocNodeList->nextNode(&pTest);

		if (pTest)
		{
			CComBSTR bstrName;
			pTest->get_nodeName(&bstrName);

			if (bstrName == "favorites")
				return pTest;
		}
	}

	return NULL;
}

void CFavoriteFolders::Load(LPCTSTR szFilename)
{
	if (!::PathFileExists(szFilename))
		return;

	// for convenience
	folderlist_type& folderlist = m_pImpl->folderlist;

	// function for throwing HRESULT exceptions
	CheckResultFunctor CheckResult;

	try
	{
		// make sure favorites list is empty
		folderlist.clear();

		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		VARIANT_BOOL bSuccess = FALSE;
		CComVariant varFileName(szFilename);

		pXMLDoc->load(varFileName, &bSuccess);

		if (bSuccess)
		{
			CComPtr<IXMLDOMNode> pFavoritesNode = FindFavoritesNode(pXMLDoc);

			if (!pFavoritesNode)
				throw CFavoriteFoldersException(CFavoriteFoldersException::kError_FileInvalid);

			// iterate through child nodes
			CComPtr<IXMLDOMNodeList> pFolders = NULL;
			pFavoritesNode->get_childNodes(&pFolders);

			long nLength = 0;
			pFolders->get_length(&nLength);
			for (int nFolderIndex = 0; nFolderIndex < nLength; ++nFolderIndex)
			{
				CComPtr<IXMLDOMNode> pFolder;
				pFolders->nextNode(&pFolder);

				DOMNodeType nType;
				pFolder->get_nodeType(&nType);

				if (nType == NODE_ELEMENT)
				{
					// make sure this is a folder element
					CComBSTR bstrName;
					pFolder->get_nodeName(&bstrName);

					if (bstrName != "folder")
						continue;

					// get filename
					CComBSTR bstrFilename;
					pFolder->get_text(&bstrFilename);
					CString strFilename(bstrFilename);

					// put it into our list
					folderlist.insert((LPCTSTR)strFilename);
				}
			}
		}
	}
	catch (CHRESULTException&)
	{
		throw CFavoriteFoldersException(CFavoriteFoldersException::kError_FileInvalid);
	}
}

//========================================================================================
// Function object that adds folders to the XML document
struct AddFolderToXML
{
	AddFolderToXML(IXMLDOMDocument* pXMLDoc, IXMLDOMNode* pParent)
		: m_pXMLDoc(pXMLDoc), m_pParent(pParent)
	{}

	void operator()(const std::wstring& strFolder) const
	{
		// weed out folders that don't exist
		if (!::PathFileExists(strFolder.c_str()))
			return;

		// create element
		CComPtr<IXMLDOMElement> pElement = NULL;
		CheckResult(m_pXMLDoc->createElement(CComBSTR("folder"), &pElement));

		CheckResult(pElement->put_nodeTypedValue(CComVariant(strFolder.c_str())));

		IXMLDOMNode* pNewChild = NULL; // ATL bug prevents this from being a smart pointer
		CheckResult(m_pParent->appendChild(pElement, &pNewChild));

		// cleanup
		pNewChild->Release();
	}

private:
	IXMLDOMDocument* m_pXMLDoc;
	IXMLDOMNode* m_pParent;
	CheckResultFunctor CheckResult;
};

void CFavoriteFolders::Save(LPCTSTR szFilename)
{
	CheckResultFunctor CheckResult;

	try
	{
		// create a new XML document
		CComPtr<IXMLDOMDocument> pXMLDoc = NULL;
		CheckResult(pXMLDoc.CoCreateInstance(__uuidof(DOMDocument)));

		CComPtr<IXMLDOMProcessingInstruction> pPI = NULL;
		CheckResult(pXMLDoc->createProcessingInstruction(CComBSTR("xml"), CComBSTR("version=\"1.0\""), &pPI));

		// make a null variant
		CComVariant varNull;
		varNull.vt = VT_NULL;

		// make a temp node
		CComPtr<IXMLDOMNode> pTemp = NULL;

		// insert it
		CheckResult(pXMLDoc->insertBefore(pPI, varNull, &pTemp));

		// create a root element for favorites
		CComVariant varElementNodeType(NODE_ELEMENT);
		CComPtr<IXMLDOMNode> pFavoritesNode = NULL, pTempRoot = NULL;
		CheckResult(pXMLDoc->createNode(varElementNodeType, CComBSTR("favorites"), CComBSTR(""), &pTempRoot));
		
		// stick it in the document
		CheckResult(pXMLDoc->appendChild(pTempRoot, &pFavoritesNode));

		// create projects
		folderlist_type& folderlist = m_pImpl->folderlist;
		std::for_each(folderlist.begin(), folderlist.end(), AddFolderToXML(pXMLDoc, pFavoritesNode));

		// finally, save document to file
		CheckResult(pXMLDoc->save(CComVariant(szFilename)));
	}
	catch (CHRESULTException&)
	{
		throw CFavoriteFoldersException(CFavoriteFoldersException::kError_XMLSaveError);
	}
}

bool CFavoriteFolders::Empty() const
{
	return m_pImpl->folderlist.empty();
}

std::size_t CFavoriteFolders::Count() const
{
	return m_pImpl->folderlist.size();
}

const std::wstring& CFavoriteFolders::GetAt(std::size_t nIndex) const throw(...)
{
	// for convenience
	folderlist_type& folderlist = m_pImpl->folderlist;
	std::size_t nCurrent = 0;

	if (nIndex >= folderlist.size())
		throw std::out_of_range("Value is out of range");

	for (folderlist_type::const_iterator it = folderlist.begin(); it != folderlist.end(); ++it)
	{
		if (nCurrent == nIndex)
			return *it;

		++nCurrent;
	}

	throw std::out_of_range("Value is out of range");
}

bool CFavoriteFolders::Exists(LPCTSTR szFolderName) const
{
	// for convenience
	folderlist_type& folderlist = m_pImpl->folderlist;

	folderlist_type::const_iterator it =
		std::find_if(folderlist.begin(), folderlist.end(), lowercase_equals<std::wstring>(szFolderName));

	return (it != folderlist.end());
}

void CFavoriteFolders::AddFolder(LPCTSTR szFolderName)
{
	// a little precaution
	ASSERT(::PathIsDirectory(szFolderName));

	if (!Exists(szFolderName))
		folderlist_insert_result result = m_pImpl->folderlist.insert(szFolderName);
}

void CFavoriteFolders::RemoveFolder(LPCTSTR szFolderName)
{
	// for convenience
	folderlist_type& folderlist = m_pImpl->folderlist;

	// look for item
	folderlist_type::iterator it =
		std::find_if(folderlist.begin(), folderlist.end(), lowercase_equals<std::wstring>(szFolderName));

	// erase it if we found it
	if (it != folderlist.end())
		folderlist.erase(it);
}

void CFavoriteFolders::ForEach(EnumCallbackType function) const
{
	// for convenience
	folderlist_type& folderlist = m_pImpl->folderlist;

	// iterate items, passing them to the enumerator
	std::for_each(folderlist.begin(), folderlist.end(), function);
}