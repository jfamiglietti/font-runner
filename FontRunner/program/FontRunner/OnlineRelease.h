/*
Copyright 2000-2013 John Famiglietti

This file is part of Font Runner.

Font Runner is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Font Runner is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Font Runner.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "VersionUtils.h"
#include <boost/function.hpp>

struct version_info_type
{
	CruxTechnologies::version_type version;
	CString strQuickInfoURL;
};

// This class obtains information about the latest release of a Crux product.
// It will also obtain the release itself.
class COnlineRelease
{
public:
	COnlineRelease();
	virtual ~COnlineRelease();

public:
	enum eProduct
	{
		kFontRunner32 = 2,
		kFontRunner64,
		kUnknownProduct
	};

public:
	// callback to check abort status
	// function returns true if aborting
	typedef boost::function<bool ()> abortcallback_type;

	// callback to provide progress update
	typedef boost::function<void (unsigned int /*current*/, unsigned int /*total*/)> progresscallback_type;

public:
	// call first to initialize this class with the latest release info
	bool ObtainCurrentReleaseInfo(eProduct nProduct, version_info_type& version_info) const;

	// downloads release binary to specified location
	// returns true if successful
	// pszLocation is an in/out parameter, on success, it returns the full path and name
	// of the downloaded file
	// abortcallback and progresscallback can be NULL if not needed
	bool DownloadRelease(eProduct nProduct,
						 LPCTSTR szVersion,
						 LPTSTR pszDownloadTo, /* in: directory for downloaded file.  out: FQ filename */
						 abortcallback_type abortcallback,
						 progresscallback_type progresscallback) const;
};
