# NSI script to package MSI file in EXE

Name "Font Runner 3"

# Defines
!define REGKEY "SOFTWARE\$(^Name)"
!define VERSION 3.2.3.157
!define COMPANY "Crux Technologies, Inc."
!define URL http://www.cruxtech.com

# Included files
!include Sections.nsh

# Installer pages
Page instfiles

# Installer attributes
OutFile bin\FontRunner_${VERSION}_x86.exe
InstallDir $TEMP\fr3x86
CRCCheck on
XPStyle on
Icon ..\icons\FontRunnerPro3-Installer-Vista.ico
SilentInstall silent
VIProductVersion ${VERSION}
VIAddVersionKey ProductName "Font Runner 3"
VIAddVersionKey ProductVersion "${VERSION}"
VIAddVersionKey CompanyName "${COMPANY}"
VIAddVersionKey CompanyWebsite "${URL}"
VIAddVersionKey FileVersion "${VERSION}"
VIAddVersionKey FileDescription ""
VIAddVersionKey LegalCopyright ""

# Installer sections
Section -Main SEC0000
    # create temp directory for vcredist install
    CreateDirectory $INSTDIR
    SetOutPath $INSTDIR
    
    File ..\bin\Release\FontRunner3_x86.msi
    
    # extract files
    ExecWait 'msiexec /i "$INSTDIR\FontRunner3_x86.msi"'
    
    # remove temp files
    RmDir /r $INSTDIR
SectionEnd

# Installer functions
Function .onInit
    InitPluginsDir
FunctionEnd

