#pragma once

// parent class prototype
class CPaletteCtrl;

typedef struct _COLORPALETTE
{
	RECT		rectArea;
	COLORREF	clrColor;
} COLORPALETTE, *PCOLORPALETTE;

class CBasicPaletteWinCtrl  : public CWindowImpl<CBasicPaletteWinCtrl>
{
public:
	CBasicPaletteWinCtrl();
	virtual ~CBasicPaletteWinCtrl();

// constants
protected:
	enum
	{
		kPaletteWidth = 384,
		kPaletteHeight = 256,
		kColorColumns = 16,
		kColorRows = 10,
		kTotalColors = 138
	};

// attributes
protected:
	static COLORPALETTE	s_mapColor[];
	static bool			s_bCached;
	
	HWND			m_hwndParent;

	// mouse over color
	COLORREF		m_clrOver;

	// selected colors
	int				m_nSelectedIndex;

	// parent window class
	CPaletteCtrl*	m_pParent;

// operations
protected:
	int PtToIndex(POINT pt);
	int ColorToIndex(COLORREF clr);

public:
	void InitPalette(CPaletteCtrl* pParent);
	void SetColor(COLORREF clrNew);

protected:
BEGIN_MSG_MAP(CPaletteWinCtrl)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
public:
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};
