// PaletteCtrl.h : Declaration of the CPaletteCtrl
#pragma once
#include "resource.h"       // main symbols
#include <atlctl.h>
#include "CruxColorPicker.h"
#include "PaletteWinCtrl.h"
#include "BasicPaletteWinCtrl.h"
#include "_IPaletteCtrlEvents_CP.h"

class CPaletteCtrlLic
{
protected:
   static BOOL VerifyLicenseKey(BSTR bstr)
   {
      USES_CONVERSION;
      return !lstrcmp(OLE2T(bstr), _T("PaletteCtrl license"));
   }

   static BOOL GetLicenseKey(DWORD dwReserved, BSTR* pBstr) 
   {
      USES_CONVERSION;
      *pBstr = SysAllocString( T2OLE(_T("PaletteCtrl license"))); 
      return TRUE;
   }

   static BOOL IsLicenseValid() 
   {  
	   return TRUE; 
   }
};

// CPaletteCtrl
class ATL_NO_VTABLE CPaletteCtrl : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public IDispatchImpl<IPaletteCtrl, &IID_IPaletteCtrl, &LIBID_CruxColorPickerLib, /*wMajor =*/ 1, /*wMinor =*/ 0>,
	public IPersistStreamInitImpl<CPaletteCtrl>,
	public IOleControlImpl<CPaletteCtrl>,
	public IOleObjectImpl<CPaletteCtrl>,
	public IOleInPlaceActiveObjectImpl<CPaletteCtrl>,
	public IViewObjectExImpl<CPaletteCtrl>,
	public IOleInPlaceObjectWindowlessImpl<CPaletteCtrl>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CPaletteCtrl>,
	public CProxy_IPaletteCtrlEvents<CPaletteCtrl>, 
	public IPersistStorageImpl<CPaletteCtrl>,
	public ISpecifyPropertyPagesImpl<CPaletteCtrl>,
	public IQuickActivateImpl<CPaletteCtrl>,
	public IDataObjectImpl<CPaletteCtrl>,
	public IProvideClassInfo2Impl<&CLSID_PaletteCtrl, &__uuidof(_IPaletteCtrlEvents), &LIBID_CruxColorPickerLib>,
	public CComCoClass<CPaletteCtrl, &CLSID_PaletteCtrl>,
	public CComCompositeControl<CPaletteCtrl>
{
	// constructor
public:
	CPaletteCtrl();

	// controls
protected:
	CPaletteWinCtrl			m_wndPaletteCtrl;
	CBasicPaletteWinCtrl	m_wndBasicPaletteCtrl;
	CWindow					m_wndTabCtrl;

	COLORREF				m_clrCurrent;
	HFONT					m_hTabFont;

	enum eTabPage
	{
		kTabPage_Basic = 0,
		kTabPage_Advanced
	};

	// operations
public:
	void OnMouseOverColor(COLORREF clrOver);
	void OnMouseDownColor(COLORREF clrDown);

DECLARE_CLASSFACTORY2(CPaletteCtrlLic)

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE | 
	OLEMISC_CANTLINKINSIDE | 
	OLEMISC_INSIDEOUT | 
	OLEMISC_ACTIVATEWHENVISIBLE | 
	OLEMISC_SETCLIENTSITEFIRST
)

DECLARE_REGISTRY_RESOURCEID(IDR_PALETTECTRL)

BEGIN_COM_MAP(CPaletteCtrl)
	COM_INTERFACE_ENTRY(IPaletteCtrl)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IViewObjectEx)
	COM_INTERFACE_ENTRY(IViewObject2)
	COM_INTERFACE_ENTRY(IViewObject)
	COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceObject)
	COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
	COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
	COM_INTERFACE_ENTRY(IOleControl)
	COM_INTERFACE_ENTRY(IOleObject)
	COM_INTERFACE_ENTRY(IPersistStreamInit)
	COM_INTERFACE_ENTRY2(IPersist, IPersistStreamInit)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
	COM_INTERFACE_ENTRY(IQuickActivate)
	COM_INTERFACE_ENTRY(IPersistStorage)
	COM_INTERFACE_ENTRY(IDataObject)
	COM_INTERFACE_ENTRY(IProvideClassInfo)
	COM_INTERFACE_ENTRY(IProvideClassInfo2)
END_COM_MAP()

BEGIN_PROP_MAP(CPaletteCtrl)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// Example entries
	// PROP_ENTRY("Property Description", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()

BEGIN_CONNECTION_POINT_MAP(CPaletteCtrl)
	CONNECTION_POINT_ENTRY(__uuidof(_IPaletteCtrlEvents))
END_CONNECTION_POINT_MAP()

BEGIN_MSG_MAP(CPaletteCtrl)
	MESSAGE_HANDLER(WM_INITDIALOG, OnInitDialog)
	MESSAGE_HANDLER(WM_DESTROY, OnDestroy)
	NOTIFY_HANDLER(IDC_PALETTE_TAB, TCN_SELCHANGE, OnPaletteTabSelChange)
	CHAIN_MSG_MAP(CComCompositeControl<CPaletteCtrl>)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
LRESULT OnPaletteTabSelChange(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

BEGIN_SINK_MAP(CPaletteCtrl)
	//Make sure the Event Handlers have __stdcall calling convention
END_SINK_MAP()

	STDMETHOD(OnAmbientPropertyChange)(DISPID dispid)
	{
		if (dispid == DISPID_AMBIENT_BACKCOLOR)
		{
			SetBackgroundColorFromAmbient();
			FireViewChange();
		}
		return IOleControlImpl<CPaletteCtrl>::OnAmbientPropertyChange(dispid);
	}
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* arr[] = 
		{
			&IID_IPaletteCtrl,
		};

		for (int i=0; i<sizeof(arr)/sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IPaletteCtrl

	enum { IDD = IDD_PALETTECTRL };

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}
	STDMETHOD(get_SelectedColor)(ULONG* pVal);
	STDMETHOD(put_SelectedColor)(ULONG newVal);
};

OBJECT_ENTRY_AUTO(__uuidof(PaletteCtrl), CPaletteCtrl)
