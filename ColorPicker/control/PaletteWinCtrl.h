#pragma once

// parent class prototype
class CPaletteCtrl;

class CPaletteWinCtrl : public CWindowImpl<CPaletteWinCtrl>
{
// construction / destruction
public:
	CPaletteWinCtrl();
	virtual ~CPaletteWinCtrl();

// operations
public:

	// attributes
public:
	enum	// constants
	{
		kSelectorWidth = 15,
		kColorSections = 6,
		kPaletteWidth = 384,
		kPaletteHeight = 256,
		kMaxColor = 255,
		kHalfColor = 127
	};

protected:
	HBITMAP			m_bmpPaletteCache;
	HBITMAP			m_bmpPaletteSelect;
	bool			m_bCached;
	HWND			m_hwndParent;
	int				m_nSelectBitmapWidth;
	int				m_nSelectBitmapHeight;

	// mouse capturing
	bool			m_bMouseDown;
	RECT			m_rectOriginalClipArea;

	// mouse over color
	COLORREF		m_clrOver;

	// selected colors
	POINT			m_ptCurrent;
	COLORREF		m_clrCurrent;

	// the color map
	RGBTRIPLE		(*m_prgbMap)[kPaletteHeight];

	// parent window class
	CPaletteCtrl*	m_pParent;

// operations
public:
	void InitPalette(CPaletteCtrl* pParent);
	void SetColor(COLORREF clrCurrent);

protected:
	void CreateRGBMap();
	void FillUp(const int& x, const int& nRed, const int& nGreen, const int& nBlue);
	void FillDown(const int& x, const int& nRed, const int& nGreen, const int& nBlue);
	void CreatePaletteCache();
	POINT MapColorToPoint(COLORREF clr);

protected:
BEGIN_MSG_MAP(CPaletteWinCtrl)
	MESSAGE_HANDLER(WM_PAINT, OnPaint)
	MESSAGE_HANDLER(WM_LBUTTONDOWN, OnLButtonDown)
	MESSAGE_HANDLER(WM_LBUTTONUP, OnLButtonUp)
	MESSAGE_HANDLER(WM_MOUSEMOVE, OnMouseMove)
END_MSG_MAP()
// Handler prototypes:
//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);
public:
	LRESULT OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnLButtonUp(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	LRESULT OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
};
