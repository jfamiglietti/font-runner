#include "StdAfx.h"
#include "BasicPaletteWinctrl.h"
#include "PaletteCtrl.h"

// init color palette
COLORPALETTE CBasicPaletteWinCtrl::s_mapColor[] = 
{
	// row 1, basic colors
	{ { 0,0,0,0,}, 0x008000 }, { { 0,0,0,0,}, 0x00FF00 }, { { 0,0,0,0,}, 0x808000 }, { { 0,0,0,0,}, 0xFFFF00 },
	{ { 0,0,0,0,}, 0x800000 }, { { 0,0,0,0,}, 0xFF0000 }, { { 0,0,0,0,}, 0x800080 }, { { 0,0,0,0,}, 0xFF00FF },
	{ { 0,0,0,0,}, 0x000080 }, { { 0,0,0,0,}, 0x0000FF }, { { 0,0,0,0,}, 0x008080 }, { { 0,0,0,0,}, 0x00FFFF },
	{ { 0,0,0,0,}, 0xFFFFFF }, { { 0,0,0,0,}, 0xC0C0C0 }, { { 0,0,0,0,}, 0x808080 }, { { 0,0,0,0,}, 0x000000 },
	
	// row 2
	{ { 0,0,0,0,}, 0x2F6B55 }, { { 0,0,0,0,}, 0x006400 }, { { 0,0,0,0,}, 0x4F4F2F }, { { 0,0,0,0,}, 0x908070 },
	{ { 0,0,0,0,}, 0x8B0000 }, { { 0,0,0,0,}, 0x701919 }, { { 0,0,0,0,}, 0x82004B }, { { 0,0,0,0,}, 0x8B008B },
	{ { 0,0,0,0,}, 0x2A2AA5 }, { { 0,0,0,0,}, 0x00008B }, { { 0,0,0,0,}, 0x2D52A0 }, { { 0,0,0,0,}, 0x13458B },
	{ { 0,0,0,0,}, 0x0B86B8 }, { { 0,0,0,0,}, 0xDCF5F5 }, { { 0,0,0,0,}, 0xF0FFF0 }, { { 0,0,0,0,}, 0x696969 },
	
	// row 3
	{ { 0,0,0,0,}, 0x238E6B }, { { 0,0,0,0,}, 0x228B22 }, { { 0,0,0,0,}, 0x8B8B00 }, { { 0,0,0,0,}, 0x998877 },
	{ { 0,0,0,0,}, 0xCD0000 }, { { 0,0,0,0,}, 0x8B3D48 }, { { 0,0,0,0,}, 0xD30094 }, { { 0,0,0,0,}, 0x8515C7 },
	{ { 0,0,0,0,}, 0x5C5CCD }, { { 0,0,0,0,}, 0x2222B2 }, { { 0,0,0,0,}, 0x1E69D2 }, { { 0,0,0,0,}, 0x3F85CD },
	{ { 0,0,0,0,}, 0x20A5DA }, { { 0,0,0,0,}, 0xD2FAFA }, { { 0,0,0,0,}, 0xFAFFF5 }, { { 0,0,0,0,}, 0xA9A9A9 },

	// row 4
	{ { 0,0,0,0,}, 0x32CD9A }, { { 0,0,0,0,}, 0x578B2E }, { { 0,0,0,0,}, 0xA09E5F }, { { 0,0,0,0,}, 0xB48246 },
	{ { 0,0,0,0,}, 0xE16941 }, { { 0,0,0,0,}, 0xE22B8A }, { { 0,0,0,0,}, 0xCC3299 }, { { 0,0,0,0,}, 0x9314FF },
	{ { 0,0,0,0,}, 0x8F8FBC }, { { 0,0,0,0,}, 0x3C14DC }, { { 0,0,0,0,}, 0x008CFF }, { { 0,0,0,0,}, 0x87B8DE },
	{ { 0,0,0,0,}, 0x6BB7BD }, { { 0,0,0,0,}, 0xE0FFFF }, { { 0,0,0,0,}, 0xFFFFF0 }, { { 0,0,0,0,}, 0xD3D3D3 },

	// row 5
	{ { 0,0,0,0,}, 0x00FC7C }, { { 0,0,0,0,}, 0x71B33C }, { { 0,0,0,0,}, 0xAAB220 }, { { 0,0,0,0,}, 0xFFBF00 },
	{ { 0,0,0,0,}, 0xFF901E }, { { 0,0,0,0,}, 0xCD5A6A }, { { 0,0,0,0,}, 0xD355BA }, { { 0,0,0,0,}, 0x9370DB },
	{ { 0,0,0,0,}, 0x7280FA }, { { 0,0,0,0,}, 0x0045FF }, { { 0,0,0,0,}, 0x60A4F4 }, { { 0,0,0,0,}, 0x8CB4D2 },
	{ { 0,0,0,0,}, 0x00D7FF }, { { 0,0,0,0,}, 0xF0FFFF }, { { 0,0,0,0,}, 0xFFF8F8 }, { { 0,0,0,0,}, 0xDCDCDC },

	// row 6
	{ { 0,0,0,0,}, 0x00FF7F }, { { 0,0,0,0,}, 0x32CD32 }, { { 0,0,0,0,}, 0xAACD66 }, { { 0,0,0,0,}, 0xD1CE00 },
	{ { 0,0,0,0,}, 0xED9564 }, { { 0,0,0,0,}, 0xEE687B }, { { 0,0,0,0,}, 0xD670DA }, { { 0,0,0,0,}, 0xB469FF },
	{ { 0,0,0,0,}, 0x8080F0 }, { { 0,0,0,0,}, 0x4763FF }, { { 0,0,0,0,}, 0x00A5FF }, { { 0,0,0,0,}, 0xC4E4FF },
	{ { 0,0,0,0,}, 0x8CE6F0 }, { { 0,0,0,0,}, 0xDCF8FF }, { { 0,0,0,0,}, 0xE6F0FA }, { { 0,0,0,0,}, 0xF5F5F5 },

	// row 7
	{ { 0,0,0,0,}, 0x2FFFAD }, { { 0,0,0,0,}, 0x8BBC8F }, { { 0,0,0,0,}, 0xD0E040 }, { { 0,0,0,0,}, 0xCCD148 },
	{ { 0,0,0,0,}, 0xEBCE87 }, { { 0,0,0,0,}, 0xDB7093 }, { { 0,0,0,0,}, 0xEE82EE }, { { 0,0,0,0,}, 0xC1B6FF },
	{ { 0,0,0,0,}, 0x7A96E9 }, { { 0,0,0,0,}, 0x507FFF }, { { 0,0,0,0,}, 0xADDEFF }, { { 0,0,0,0,}, 0xCDEBFF },
	{ { 0,0,0,0,}, 0xAAE8EE }, { { 0,0,0,0,}, 0xE6F5FD }, { { 0,0,0,0,}, 0xEEF5FF }, { { 0,0,0,0,}, 0xFFF8F8 },

	// row 8
	{ { 0,0,0,0,}, 0x98FB98 }, { { 0,0,0,0,}, 0x7FFF00 }, { { 0,0,0,0,}, 0xD4FF7F }, { { 0,0,0,0,}, 0xE6E0B0 },
	{ { 0,0,0,0,}, 0xFACE87 }, { { 0,0,0,0,}, 0xDEC4B0 }, { { 0,0,0,0,}, 0xDDA0DD }, { { 0,0,0,0,}, 0xCBC0FF },
	{ { 0,0,0,0,}, 0x7AA0FF }, { { 0,0,0,0,}, 0xB3DEF5 }, { { 0,0,0,0,}, 0xB5E4FF }, { { 0,0,0,0,}, 0xD7EBFA },
	{ { 0,0,0,0,}, 0xCDFAFF }, { { 0,0,0,0,}, 0xF0FAFF }, { { 0,0,0,0,}, 0xFAFAFF }, { { 0,0,0,0,}, 0xFFF8F0 },

	// row 9
	{ { 0,0,0,0,}, 0x90EE90 }, { { 0,0,0,0,}, 0x9AFA00 }, { { 0,0,0,0,}, 0xEEEEAF }, { { 0,0,0,0,}, 0xFFFFE0 },
	{ { 0,0,0,0,}, 0xE6D8AD }, { { 0,0,0,0,}, 0xFAE6E6 }, { { 0,0,0,0,}, 0xD8BFD8 }, { { 0,0,0,0,}, 0xE1E4FF },
	{ { 0,0,0,0,}, 0xB9DAFF }, { { 0,0,0,0,}, 0xD5EFFF }
};
bool CBasicPaletteWinCtrl::s_bCached = false;

CBasicPaletteWinCtrl::CBasicPaletteWinCtrl()
{
	// nothing selected
	m_nSelectedIndex = -1;
}

CBasicPaletteWinCtrl::~CBasicPaletteWinCtrl()
{
}

void CBasicPaletteWinCtrl::InitPalette(CPaletteCtrl* pParent)
{
	LONG_PTR dwStyle = GetWindowLongPtr(GWL_STYLE);
    SetWindowLong(GWL_STYLE, (LONG)dwStyle | SS_NOTIFY);

	// save parent class pointer
	m_pParent = pParent;

	// get cell width and height
	int nCellWidth = (kPaletteWidth / kColorColumns);
	int nCellHeight = (kPaletteHeight / kColorRows);

	// upper left corner of cell
	POINT ptCell = { 0, 0 };

	// create palette
	int nColumnCount = 0;
	for (int nIndex = 0; nIndex < kTotalColors; nIndex++)
	{
		s_mapColor[nIndex].rectArea.left = ptCell.x;
		s_mapColor[nIndex].rectArea.top = ptCell.y;
		s_mapColor[nIndex].rectArea.right = ptCell.x + nCellWidth;
		s_mapColor[nIndex].rectArea.bottom = ptCell.y + nCellHeight;
		
		// increment
		nColumnCount++;
		if (nColumnCount == kColorColumns)
		{
			ptCell.x = 0;
			ptCell.y += nCellHeight;
			nColumnCount = 0;
		}
		else
			ptCell.x += nCellWidth;
	}

	// palette is now cached
	s_bCached = true;

	// resize window
	SetWindowPos(NULL, 0, 0, kPaletteWidth, kPaletteHeight, SWP_NOMOVE | SWP_NOZORDER);
}

void CBasicPaletteWinCtrl::SetColor(COLORREF clrNew)
{
	m_nSelectedIndex = ColorToIndex(clrNew);
	Invalidate();
}

int CBasicPaletteWinCtrl::PtToIndex(POINT pt)
{
	return (pt.x / (kPaletteWidth / kColorColumns)) + (kColorColumns * (pt.y / (kPaletteHeight / kColorRows)));
}

int CBasicPaletteWinCtrl::ColorToIndex(COLORREF clr)
{
	int nIndex = 0;
	while (nIndex < kTotalColors)
	{
		if (s_mapColor[nIndex].clrColor == clr)
			return nIndex;

		nIndex++;
	}

	return -1;
}

// CPaletteWinCtrl message handlers

LRESULT CBasicPaletteWinCtrl::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// start painting
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(&ps);

	// create outline border pen
	HPEN hPen = ::CreatePen(PS_SOLID, 1, RGB(0,0,0));

	for (int nIndex = 0; nIndex < kTotalColors; nIndex++)
	{
		// create brush for this rectangle
		HBRUSH hBrush = ::CreateSolidBrush(s_mapColor[nIndex].clrColor);

		RECT rectDraw = s_mapColor[nIndex].rectArea;
	
		::InflateRect(&rectDraw, -1, -1);
		::FillRect(hdc, &rectDraw, hBrush);

		// if this area is selected, draw outline
		if (nIndex == m_nSelectedIndex)
		{
			// create black pen
			HPEN hPenBlack = ::CreatePen(PS_SOLID, 1, RGB(0,0,0));
			HPEN hPenWhite = ::CreatePen(PS_SOLID, 1, RGB(255,255,255));
			
			HBRUSH hOldBrush = (HBRUSH)::SelectObject(hdc, hBrush);
			HPEN hOldPen = (HPEN)::SelectObject(hdc, hPenBlack);
			::Rectangle(hdc, rectDraw.left, rectDraw.top, rectDraw.right, rectDraw.bottom);

			// draw inner, white rectangle
			::SelectObject(hdc, hPenWhite);
			::InflateRect(&rectDraw, -1, -1);
			::MoveToEx(hdc, rectDraw.left, rectDraw.top, NULL);
			::LineTo(hdc, rectDraw.right - 1, rectDraw.top);
			::LineTo(hdc, rectDraw.right - 1, rectDraw.bottom - 1);
			::LineTo(hdc, rectDraw.left, rectDraw.bottom - 1);
			::LineTo(hdc, rectDraw.left, rectDraw.top);

			::SelectObject(hdc, hPen);
			::SelectObject(hdc, hOldBrush);

			// delete pens
			::DeleteObject(hPenBlack);
			::DeleteObject(hPenWhite);
		}

		// delete brush
		::DeleteObject(hBrush);
	}

	// delete pen
	::DeleteObject(hPen);

	// done painting
	EndPaint(&ps);

	return 0L;
}

LRESULT CBasicPaletteWinCtrl::OnLButtonDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// convert point to color index
	POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
	int nIndex = PtToIndex(pt);

	// let parent know about click
	m_pParent->OnMouseDownColor(s_mapColor[nIndex].clrColor);

	// select new color
	m_nSelectedIndex = nIndex;

	// invalidate
	Invalidate(FALSE);

	return 0;
}

LRESULT CBasicPaletteWinCtrl::OnMouseMove(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	// convert point to color index
	POINT pt = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
	int nIndex = PtToIndex(pt);

	// let parent know about mouse move
	m_pParent->OnMouseOverColor(s_mapColor[nIndex].clrColor);

	return 0;
}