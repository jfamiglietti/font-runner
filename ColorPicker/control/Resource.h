//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CruxColorPicker.rc
//
#define IDS_PROJNAME                    100
#define IDR_CRUXCOLORPICKER             101
#define IDS_PALETTE_TAB_BASIC           101
#define IDB_PALETTECTRL                 102
#define IDS_PALETTE_TAB_ADVANCED        102
#define IDR_PALETTECTRL                 103
#define IDD_PALETTECTRL                 104
#define IDC_PALETTE_TAB                 201
#define IDB_BITMAP1                     201
#define IDB_PALSEL_BITMAP               201
#define IDC_PALETTE_STATIC              202
#define IDC_BASICPALETTE_STATIC         207

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
