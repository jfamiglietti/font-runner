This is the source distribution of Font Runner.

Here's how to build and run it:

1. Download Boost (http://www.boost.org)  Any recent version should do.

2. Extract Boost so it is rooted at Libraries\Boost within this source tree
(For example, the bootstrap file should be at Libraries\Boost\bootstrap.bat)

3. Build Boost
  a) Open a Visual Studio Command Prompt
  b) Run this command in the root of the Boost folder:
     bjam -j8 --with-regex --build-type=minimal stage --stagedir=bin/vc100/x86 toolset="msvc-10.0" threading=multi link=static debug release
  c) Open a Visual Studio x64 Win64 Command Prompt
  d) Run this command in the root of the Boost folder:
  e) bjam -j8 --with-regex --build-type=minimal stage --stagedir=bin/vc100/x64 toolset="msvc-10.0" threading=multi link=static address-model=64 debug release

4. You will probably have to run Visual Studio as administrator in order for
registration of the Color Picker control to succeed.

5. Open FontRunner\program\FontRunner\FontRunner.sln.

To build the installers:

1. Download and install Wix (http://wixtoolset.org/)
2. Ensure the the Relase Win32 and x64 versions of FontRunner are built.
3. Open FontRunner\installer\Installers.sln
4. Build the Release|Any CPU configuration.
5. The FontRunner\installer\bin\Release folder should contain the 32-bit and 64-bit installers.

Feel free to hack on it. Any questions or comments should use the contact form on codeandrun.com.